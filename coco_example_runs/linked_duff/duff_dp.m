function Jp = duff_dp(x,p)

% State Variables
x1 = x(1,:);
x2 = x(2,:);
x3 = x(3,:);

% Parameters
m = p(1,:);
c = p(2,:);
k = p(3,:);
eps = p(4,:);
A = p(5,:);
w = p(6,:);

% Jacobian
% Checked using:
% x = rand(3,1); p = rand(6,1);
% coco_ezDFDP('f(x,p)', @duff, x, p) - duff_dp(x,p)
Jp = zeros(3,6,numel(x1));
Jp(2,1,:) = (-1./m.^2).*(A.*cos(x3) - c.*x2 - k.*x1 - eps.*x1.^3);
Jp(2,2,:) = -x2./m;
Jp(2,3,:) = -x1./m;
Jp(2,4,:) = (-x1.^3)./m;
Jp(2,5,:) = (1./m).*cos(x3);
Jp(3,6,:) = 1;

end