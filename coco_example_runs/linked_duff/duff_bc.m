function y = duff_bc(data, T, x0, x1, p)

y = [x1(1)-x0(1); x1(2)-x0(2); x1(3)-x0(3)-2*pi;x0(2)];

end

