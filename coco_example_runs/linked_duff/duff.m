function y = duff(x,p)

% State Variables
x1 = x(1,:);
x2 = x(2,:);
x3 = x(3,:);

% Parameters
m = p(1,:);
c = p(2,:);
k = p(3,:);
eps = p(4,:);
A = p(5,:);
w = p(6,:);

% Vector Field
y = [x2; ...
     (1./m).*(A.*cos(x3) - c.*x2 - k.*x1 - eps.*x1.^3); ...
     w.*ones(size(x3))];

end