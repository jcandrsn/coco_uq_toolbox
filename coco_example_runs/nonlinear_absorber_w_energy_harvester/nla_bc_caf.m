function [data, y] = nla_bc_caf(prob, data, u)
% Version that's compatible with coco_add_func

pr = data.pr;
bc = pr.bc_data;

x0 = u(bc.x0_idx);
x1 = u(bc.x1_idx);

y = [x1(1:5)-x0(1:5); x1(6)-x0(6)-2*pi];

end

