function Jx = nla_dx(x, p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Absorber Mass Position
x4 = x(4,:);  % Absorber Mass Velocity
x5 = x(5,:);  % Voltage
x6 = x(6,:);  % Phase

% Parameters
X0 = p(1,:);
z1 = p(2,:);
mu = p(3,:);
z2 = p(4,:);
b  = p(5,:);
al = p(6,:);
Xi = p(7,:);
la = p(8,:);
ka = p(9,:);
Om = p(10,:);

% Calculated from Mathematica, checked data entry using
% x0 = rand(6,5); p0 = rand(10,5);
% coco_ezDFDX('f(x,p)', @nla, x0, p0) - nla_dx(x0, p0)
Jx = zeros(6,6,numel(x1));

Jx(1,2,:) =  ones(size(x2));
Jx(2,1,:) = -(1 + (b.^2).*mu + 3.*al.*(b.^2).*mu.*(x3 - x1).^2);
Jx(2,2,:) = -(z1+z2.*mu);
Jx(2,3,:) =  (b.^2).*mu + 3.*mu.*al.*(b.^2).*(x3 - x1).^2;
Jx(2,4,:) =  mu.*z2;
Jx(2,6,:) = -X0.*sin(x6);
Jx(3,4,:) =  ones(size(x4));
Jx(4,1,:) =  b.^2 + 3.*al.*(b.^2).*(x3 - x1).^2;
Jx(4,2,:) =  z2;
Jx(4,3,:) = -(b.^2 + 3.*al.*(b.^2).*(x3 - x1).^2);
Jx(4,4,:) = -z2;
Jx(4,5,:) =  Xi.*ones(size(x5));
Jx(5,4,:) =  -ka.*ones(size(x4));
Jx(5,5,:) =  -la.*ones(size(x5));

end