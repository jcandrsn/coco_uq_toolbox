function Jxx = rms_obj_dxdx(x, p)

V = x(5,:);
X0 = p(1,:);

Jxx = zeros(1,6,6,numel(V));
Jxx(1,5,5,:) = 2./X0;

end