function Jp = nla_dp(x,p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Absorber Mass Position
x4 = x(4,:);  % Absorber Mass Velocity
x5 = x(5,:);  % Voltage
x6 = x(6,:);  % Phase

% Parameters
X0 = p(1,:);
z1 = p(2,:);
mu = p(3,:);
z2 = p(4,:);
b  = p(5,:);
al = p(6,:);
Xi = p(7,:);
la = p(8,:);
ka = p(9,:);
Om = p(10,:);

% Calculated from Mathematica, checked data entry using
% x0 = rand(6,5); p0 = rand(10,5);
% coco_ezDFDP('f(x,p)', @nla, x0, p0) - nla_dp(x0, p0)
Jp = zeros(6,10,numel(x1));

Jp(2,1,:)  =  cos(x6);
Jp(2,2,:)  = -x2;
Jp(2,3,:)  =  (b.^2).*(x3 - x1) + al.*(b.^2).*(x3 - x1).^3 + z2.*(x4 - x2);
Jp(2,4,:)  =  mu.*(x4-x2);
Jp(2,5,:)  =  2.*b.*mu.*(x3 - x1) + 2*al.*b.*mu.*(x3 - x1).^3;
Jp(2,6,:)  =  (b.^2).*mu.*(x3 - x1).^3;
Jp(4,4,:)  = -(x4 - x2);
Jp(4,5,:)  = -2*b.*((x3 - x1) + al.*(x3 - x1).^3);
Jp(4,6,:)  = -(b.^2).*(x3 - x1).^3;
Jp(4,7,:)  =  x5;
Jp(5,8,:)  =  -x5;
Jp(5,9,:)  =  -x4;
Jp(6,10,:) =  ones(size(Om));

end