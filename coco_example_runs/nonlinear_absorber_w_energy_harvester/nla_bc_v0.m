function y = nla_bc_v0(data, T, x0, x1, p)

y = [x1(1:5)-x0(1:5); x1(6)-x0(6)-2*pi; x0(2)];

end