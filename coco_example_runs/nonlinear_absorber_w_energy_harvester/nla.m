function y = nla(x, p)

% Model of a non-linear absorber.  Mass-Spring-Damper with
% a nonlinear spring atop a Mass-Spring-Damper with a
% linear spring.
%                  ____
%                 | m2 |
%                 |____|
%        nl        |  |
%        spring -> Z  U <- Damper 2
%                 _|__|_
%                |  m1  |
%                |______|
%                  |  |
% linear Spring--> Z  U Damper 1
%               ___|__|___
%

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Absorber Mass Position
x4 = x(4,:);  % Absorber Mass Velocity
x5 = x(5,:);  % Voltage
x6 = x(6,:);  % Phase

% Parameters
X0 = p(1,:);
z1 = p(2,:);
mu = p(3,:);
z2 = p(4,:);
b  = p(5,:);
al = p(6,:);
Xi = p(7,:);
la = p(8,:);
ka = p(9,:);
Om = p(10,:);

y = [x2; ...
     X0.*cos(x6) - z1.*x2 - x1 + ...
     mu.*z2.*(x4 - x2) + mu.*(b.^2).*(x3 - x1) + ...
     mu.*(b.^2).*al.*(x3 - x1).^3; ...
     x4; ...
     Xi.*x5 - z2.*(x4 - x2) - (b.^2).*(x3 - x1) - ...
     (b.^2).*al.*(x3 - x1).^3; ...
     -la.*x5 - ka.*x4; ...
     Om.*ones(size(x6))];
end