function Jxx = nla_dxdx(x, p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Absorber Mass Position
x4 = x(4,:);  % Absorber Mass Velocity
x5 = x(5,:);  % Voltage
x6 = x(6,:);  % Phase

% Parameters
X0 = p(1,:);
z1 = p(2,:);
mu = p(3,:);
z2 = p(4,:);
b  = p(5,:);
al = p(6,:);
Xi = p(7,:);
la = p(8,:);
ka = p(9,:);
Om = p(10,:);

% Calculated from Mathematica, checked data entry using
% x0 = rand(6,2); p0 = rand(10,2);
% dxdx_approx = coco_ezDFDX('f(x,p)', @nla_dx, x0, p0);
% dxdx = nla_dxdx(x0, p0);
% dxdx_approx - dxdx
Jxx = zeros(6,6,6,numel(x1));

Jxx(2,1,1,:) =  6*al.*(b.^2).*mu.*(x3 - x1);
Jxx(2,1,3,:) = -6*al.*(b.^2).*mu.*(x3 - x1);
Jxx(2,3,1,:) = -6*al.*(b.^2).*mu.*(x3 - x1);
Jxx(2,3,3,:) =  6*al.*(b.^2).*mu.*(x3 - x1);
Jxx(2,6,6,:) = -X0.*cos(x6);
Jxx(4,1,1,:) = -6*al.*(b.^2).*(x3 - x1);
Jxx(4,1,3,:) =  6*al.*(b.^2).*(x3 - x1);
Jxx(4,3,1,:) =  6*al.*(b.^2).*(x3 - x1);
Jxx(4,3,3,:) = -6*al.*(b.^2).*(x3 - x1);

end