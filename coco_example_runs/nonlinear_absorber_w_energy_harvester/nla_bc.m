function y = nla_bc(data, T, x0, x1, p)

Om = p(10);

y = [x1(1:5)-x0(1:5); x1(6)-x0(6)-2*pi];

end
