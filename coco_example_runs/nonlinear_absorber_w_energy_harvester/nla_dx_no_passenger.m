function Jx = nla_dx(x, p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Absorber Mass Position
x4 = x(4,:);  % Absorber Mass Velocity
x5 = x(5,:);  % Phase

% Parameters
X0 = p(1,:);
z1 = p(2,:);
z2 = p(3,:);
a = p(4,:);
b = p(5,:);
m = p(6,:);
Om = p(7,:);

% Calculated from Mathematica, checked data entry using
% x0 = rand(5,1); p0 = rand(7,1);
% coco_ezDFDX('f(x,p)', @nla, x0, p0) - nla_dx(x0, p0)
Jx = zeros(5,5,numel(x1));
Jx(1,2,:) = 1;
Jx(2,1,:) = -1-b.^2.*m-3.*a.*(b.^2).*m.*(x3-x1).^2;
Jx(2,2,:) = -z1-m.*z2;
Jx(2,3,:) = (b.^2).*m + 3*a.*(b.^2).*m.*(x3-x1).^2;
Jx(2,4,:) = m.*z2;
Jx(2,5,:) = -X0.*sin(x5);
Jx(3,4,:) = 1;
Jx(4,1,:) = b.^2+3*a.*(b.^2).*(x3-x1).^2;
Jx(4,2,:) = z2;
Jx(4,3,:) = -b.^2-3.*a.*(b.^2).*(x3-x1).^2;
Jx(4,4,:) = -z2;

end