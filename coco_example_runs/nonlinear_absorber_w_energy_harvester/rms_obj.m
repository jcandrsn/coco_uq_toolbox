function y = rms_obj(x, p)

% Normalized RMS value of the voltage signal.

V = x(5,:);
X0 = p(1,:);

y = (V.^2)./X0;

end

