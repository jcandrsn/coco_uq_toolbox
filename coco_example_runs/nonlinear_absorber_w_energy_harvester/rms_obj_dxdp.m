function Jx = rms_obj_dxdp(x, p)

V = x(5,:);
X0 = p(1,:);

Jx = zeros(1,6,10,numel(V));
Jx(1,5,1,:) = -(2*V)./(X0.^2);

end