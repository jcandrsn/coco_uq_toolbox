function Jpp = nla_dpdp(x,p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Absorber Mass Position
x4 = x(4,:);  % Absorber Mass Velocity
x5 = x(5,:);  % Voltage
x6 = x(6,:);  % Phase

% Parameters
X0 = p(1,:);
z1 = p(2,:);
mu = p(3,:);
z2 = p(4,:);
b  = p(5,:);
al = p(6,:);
Xi = p(7,:);
la = p(8,:);
ka = p(9,:);
Om = p(10,:);

% Checked using
% x0 = rand(6,2); p0 = rand(10,2);
% dpdp_approx = coco_ezDFDP('f(x,p)', @nla_dp, x0, p0);
% dpdp = nla_dpdp(x0, p0);
% dpdp_approx - dpdp

Jpp = zeros(6,10,10,numel(x1));

Jpp(2,3,4,:) =  x4 - x2;
Jpp(2,3,5,:) =  2*b.*(x3 - x1) + 2*al.*b.*(x3 - x1).^3;
Jpp(2,3,6,:) =  (b.^2).*(x3 - x1).^3;
Jpp(2,4,3,:) =  x4 - x2;
Jpp(2,5,3,:) =  2*b.*(x3 - x1) + 2*al.*b.*(x3 - x1).^3;
Jpp(2,5,5,:) =  2*mu.*(x3 - x1) + 2*al.*mu.*(x3 - x1).^3;
Jpp(2,5,6,:) =  2*b.*mu.*(x3 - x1).^3;
Jpp(2,6,3,:) =  (b.^2).*(x3 - x1).^3;
Jpp(2,6,5,:) =  2*b.*mu.*(x3 - x1).^3;
Jpp(4,5,5,:) =  -2*(x3 - x1) - 2*al.*(x3 - x1).^3;
Jpp(4,5,6,:) =  -2*b.*(x3 - x1).^3;
Jpp(4,6,5,:) =  -2*b.*(x3 - x1).^3;

end