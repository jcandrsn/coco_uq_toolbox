function y = nla(x, p)

% Model of a non-linear absorber.  Mass-Spring-Damper with
% a nonlinear spring atop a Mass-Spring-Damper with a
% linear spring.
%                  ____
%                 | m2 |
%                 |____|
%                  |  |
% nl spring -----> Z  U <- Damper 2
%                 _|__|_
%                |  m1  |
%                |______|
%                  |  |
% linear Spring--> Z  U Damper 1
%               ___|__|___
%
% Non-dimensionalized per a similar system in the paper
% Energy Harvesting From Nonlinear Vibration Absorbers
% (though without the energy harvesting circuit)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Absorber Mass Position
x4 = x(4,:);  % Absorber Mass Velocity
x5 = x(5,:);  % Phase

% Parameters
X0 = p(1,:);
z1 = p(2,:);
z2 = p(3,:);
a = p(4,:);
b = p(5,:);
m = p(6,:);
Om = p(7,:);

y = [x2;...
     X0.*cos(x5) - z1.*x2 - x1 + m.*z2.*(x4 - x2) + m.*(b.^2).*(x3 - x1) + m.*(b.^2).*a.*(x3 - x1).^3;...
     x4;...
     -z2.*(x4 - x2) - (b.^2).*(x3 - x1) - (b.^2).*a.*(x3 - x1).^3;...
     Om.*ones(size(x5))];
end