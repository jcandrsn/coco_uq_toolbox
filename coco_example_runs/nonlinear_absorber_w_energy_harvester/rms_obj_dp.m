function Jp = rms_obj_dp(x, p)

V = x(5,:);
X0 = p(1,:);

Jp = zeros(1,10,numel(X0));

Jp(1,1,:) = -(V.^2)./(X0.^2);

end

