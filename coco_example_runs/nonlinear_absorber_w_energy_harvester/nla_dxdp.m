function Jxp = nla_dxdp(x, p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Absorber Mass Position
x4 = x(4,:);  % Absorber Mass Velocity
x5 = x(5,:);  % Voltage
x6 = x(6,:);  % Phase

% Parameters
X0 = p(1,:);
z1 = p(2,:);
mu = p(3,:);
z2 = p(4,:);
b  = p(5,:);
al = p(6,:);
Xi = p(7,:);
la = p(8,:);
ka = p(9,:);
Om = p(10,:);

% Checked using
% x0 = rand(6,2); p0 = rand(10,2);
% dxdp_approx = coco_ezDFDP('f(x,p)', @nla_dx, x0, p0)
% dxdp = nla_dxdp(x0, p0)
% dxdp_approx - dxdp
Jxp = zeros(6,6,10,numel(x1));

Jxp(2,1,3,:) = -(b.^2 + 3*al.*(b.^2).*(x3-x1).^2);
Jxp(2,1,5,:) = -(2*b.*mu + 6*al.*b.*mu.*(x3 - x1).^2);
Jxp(2,1,6,:) = -3*(b.^2).*mu.*(x3 - x1).^2;
Jxp(2,2,2,:) = -ones(size(z1));
Jxp(2,2,3,:) = -z2;
Jxp(2,2,4,:) = -mu;
Jxp(2,3,3,:) =  b.^2 + 3*al.*(b.^2).*(x3-x1).^2;
Jxp(2,3,5,:) =  2*b.*mu + 6*al.*b.*mu.*(x3 - x1).^2;
Jxp(2,3,6,:) =  3*(b.^2).*mu.*(x3 - x1).^2;
Jxp(2,4,3,:) =  z2;
Jxp(2,4,4,:) =  mu;
Jxp(2,6,1,:) = -sin(x6);
Jxp(4,1,5,:) =  2*b + 6*al.*b.*(x3 - x1).^2;
Jxp(4,1,6,:) =  3*(b.^2).*(x3 - x1).^2;
Jxp(4,2,4,:) =  ones(size(z2));
Jxp(4,3,5,:) = -(2*b + 6*al.*b.*(x3 - x1).^2);
Jxp(4,3,6,:) = -3*(b.^2).*(x3 - x1).^2;
Jxp(4,4,4,:) = -ones(size(z2));
Jxp(4,5,7,:) =  ones(size(Xi));
Jxp(5,4,9,:) = -ones(size(ka));
Jxp(5,5,8,:) = -ones(size(la));

end