function Jp = rms_obj_dpdp(x, p)

V = x(5,:);
X0 = p(1,:);

Jp = zeros(1,10,10,numel(X0));

Jp(1,1,1,:) = 2*(V.^2)./(X0.^3);

end

