function Jx = nla_dx(x, p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Passenger Position
x4 = x(4,:);  % Passenger Velocity
x5 = x(5,:);  % Absorber Mass Position
x6 = x(6,:);  % Absorber Mass Velocity
x7 = x(7,:);  % Phase

% Parameters
F = p(1,:);
m = p(2,:);
c = p(3,:);
k = p(4,:);
mp = p(5,:);
cs = p(6,:);
ks = p(7,:);
ma = p(8,:);
ca = p(9,:);
ka = p(10,:);
aa = p(11,:);
om = p(12,:);

% Calculated from Mathematica, checked data entry using
% x0 = rand(7,5); p0 = rand(12,5);
% coco_ezDFDX('f(x,p)', @nla, x0, p0) - nla_dx(x0, p0)
Jx = zeros(7,7,numel(x1));

Jx(1,2,:) =  ones(size(x2));
Jx(2,1,:) = -(k + ka + ks + 3.*aa.*(x5 - x1).^2)./m;
Jx(2,2,:) = -(c + ca + cs)./m;
Jx(2,3,:) =  ks./m;
Jx(2,4,:) =  cs./m;
Jx(2,5,:) = (ka + 3.*aa.*(x5 - x1).^2)./m;
Jx(2,6,:) =  ca./m;
Jx(2,7,:) = -(F.*sin(x7)./m);
Jx(3,4,:) =  ones(size(x4));
Jx(4,1,:) =  ks./mp;
Jx(4,2,:) =  cs./mp;
Jx(4,3,:) = -ks./mp;
Jx(4,4,:) = -cs./mp;
Jx(5,6,:) =  ones(size(x6));
Jx(6,1,:) =  (ka + 3.*aa.*(x5 - x1).^2)./ma;
Jx(6,2,:) =  ca./ma;
Jx(6,5,:) = -(ka + 3.*aa.*(x5 - x1).^2)./ma;
Jx(6,6,:) = -ca./ma;

end