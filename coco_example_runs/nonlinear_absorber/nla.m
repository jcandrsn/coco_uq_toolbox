function y = nla(x, p)

% Model of a non-linear absorber.  Mass-Spring-Damper with
% a nonlinear spring atop a Mass-Spring-Damper with a
% linear spring.
%                  ____
%                 | m2 |
%                 |____|
%        nl        |  |
%        spring -> Z  U <- Damper 2
%                 _|__|_
%                |  m1  |
%                |______|
%                  |  |
% linear Spring--> Z  U Damper 1
%               ___|__|___
%

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Passenger Position
x4 = x(4,:);  % Passenger Velocity
x5 = x(5,:);  % Absorber Mass Position
x6 = x(6,:);  % Absorber Mass Velocity
x7 = x(7,:);  % Phase

% Parameters
F = p(1,:);
m = p(2,:);
c = p(3,:);
k = p(4,:);
mp = p(5,:);
cs = p(6,:);
ks = p(7,:);
ma = p(8,:);
ca = p(9,:);
ka = p(10,:);
aa = p(11,:);
om = p(12,:);


y = [x2; ...
     (1./m).*(F.*cos(x7) - c.*x2 - k.*x1 + ...
       cs.*(x4 - x2) + ks.*(x3 - x1) + ...
       ca.*(x6 - x2) + ka.*(x5 - x1) + aa.*(x5 - x1).^3); ...
     x4; ...
     (1./mp).*(-cs.*(x4 - x2) - ks.*(x3 - x1)); ...
     x6; ...
     (1./ma).*(-ca.*(x6 - x2) - ka.*(x5 - x1) - aa.*(x5 - x1).^3); ...
     om.*ones(size(x7))];
end