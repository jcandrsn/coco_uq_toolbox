function Jxp = nla_dxdp(x, p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Passenger Position
x4 = x(4,:);  % Passenger Velocity
x5 = x(5,:);  % Absorber Mass Position
x6 = x(6,:);  % Absorber Mass Velocity
x7 = x(7,:);  % Phase

% Parameters
F = p(1,:);
m = p(2,:);
c = p(3,:);
k = p(4,:);
mp = p(5,:);
cs = p(6,:);
ks = p(7,:);
ma = p(8,:);
ca = p(9,:);
ka = p(10,:);
aa = p(11,:);
om = p(12,:);

% Checked using
% x0 = rand(7,2); p0 = rand(12,2);
% dxdp_approx = coco_ezDFDP('f(x,p)', @nla_dx, x0, p0)
% dxdp = nla_dxdp(x0, p0)
% dxdp_approx - dxdp
Jxp = zeros(7,7,12,numel(x1));

Jxp(2,1,2,:)  = (k + ka + ks + 3.*aa.*(x5 - x1).^2)./(m.^2);
Jxp(2,1,4,:)  = -1./m;
Jxp(2,1,7,:)  = -1./m;
Jxp(2,1,10,:) = -1./m;
Jxp(2,1,11,:) = -(3.*(x5 - x1).^2)./m;
Jxp(2,2,2,:)  = (c + ca + cs)./(m.^2);
Jxp(2,2,3,:)  = -1./m;
Jxp(2,2,6,:)  = -1./m;
Jxp(2,2,9,:)  = -1./m;
Jxp(2,3,2,:)  =  -ks./(m.^2);
Jxp(2,3,7,:)  =  1./m;
Jxp(2,4,2,:)  =  -cs./(m.^2);
Jxp(2,4,6,:)  =  1./m;
Jxp(2,5,2,:)  = -(ka + 3.*aa.*(x5 - x1).^2)./(m.^2);
Jxp(2,5,10,:) = 1./m;
Jxp(2,5,11,:) = (3.*(x5 - x1).^2)./m;
Jxp(2,6,2,:)  =  -ca./(m.^2);
Jxp(2,6,9,:)  =  1./m;
Jxp(2,7,1,:)  = -(sin(x7)./m);
Jxp(2,7,2,:)  = (F.*sin(x7)./(m.^2));
Jxp(4,1,5,:)  =  -ks./(mp.^2);
Jxp(4,1,7,:)  =  1./mp;
Jxp(4,2,5,:)  =  -cs./(mp.^2);
Jxp(4,2,6,:)  =  1./mp;
Jxp(4,3,5,:)  =  ks./(mp.^2);
Jxp(4,3,7,:)  = -1./mp;
Jxp(4,4,5,:)  =  cs./(mp.^2);
Jxp(4,4,6,:)  = -1./mp;
Jxp(6,1,8,:)  =  -(ka + 3.*aa.*(x5 - x1).^2)./(ma.^2);
Jxp(6,1,10,:) =  1./ma;
Jxp(6,1,11,:) =  (3.*(x5 - x1).^2)./ma;
Jxp(6,2,8,:)  =  -ca./(ma.^2);
Jxp(6,2,9,:)  =  1./ma;
Jxp(6,5,8,:)  = (ka + 3.*aa.*(x5 - x1).^2)./(ma.^2);
Jxp(6,5,10,:) = -1./ma;
Jxp(6,5,11,:) = -(3.*(x5 - x1).^2)./ma;
Jxp(6,6,8,:) =  ca./(ma.^2);
Jxp(6,6,9,:) = -1./ma;

end