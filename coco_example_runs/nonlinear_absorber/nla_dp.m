function Jp = nla_dp(x,p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Passenger Position
x4 = x(4,:);  % Passenger Velocity
x5 = x(5,:);  % Absorber Mass Position
x6 = x(6,:);  % Absorber Mass Velocity
x7 = x(7,:);  % Phase

% Parameters
F = p(1,:);
m = p(2,:);
c = p(3,:);
k = p(4,:);
mp = p(5,:);
cs = p(6,:);
ks = p(7,:);
ma = p(8,:);
ca = p(9,:);
ka = p(10,:);
aa = p(11,:);
om = p(12,:);

% Calculated from Mathematica, checked data entry using
% x0 = rand(7,5); p0 = rand(12,5);
% coco_ezDFDP('f(x,p)', @nla, x0, p0) - nla_dp(x0, p0)
Jp = zeros(7,12,numel(x1));

Jp(2,1,:)  =  cos(x7)./m;
Jp(2,2,:)  = -(1./(m.^2)).*(F.*cos(x7) - c.*x2 - k.*x1 + ...
               cs.*(x4 - x2) + ks.*(x3 - x1) + ...
               ca.*(x6 - x2) + ka.*(x5 - x1) + aa.*(x5 - x1).^3);
Jp(2,3,:)  = -(x2./m);
Jp(2,4,:)  = -(x1./m);
Jp(2,6,:)  =  (x4-x2)./m;
Jp(2,7,:)  =  (x3-x1)./m;
Jp(2,9,:)  =  (x6-x2)./m;
Jp(2,10,:) =  (x5-x1)./m;
Jp(2,11,:) =  ((x5-x1).^3)./m;
Jp(4,5,:)  = -(-ks.*(x3 - x1) - cs.*(x4 - x2))./(mp.^2);
Jp(4,6,:)  =  (x2 - x4)./mp;
Jp(4,7,:)  =  (x1 - x3)./mp;
Jp(6,8,:)  = -(-ca.*(x6 - x2) - ka.*(x5 - x1) - aa.*(x5 - x1).^3)./(ma.^2);
Jp(6,9,:)  = -(x6 - x2)./ma;
Jp(6,10,:) = -(x5 - x1)./ma;
Jp(6,11,:) = -((x5 - x1).^3)./ma;
Jp(7,12,:) =  ones(size(om));


end