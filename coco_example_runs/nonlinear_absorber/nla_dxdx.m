function Jxx = nla_dxdx(x, p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Passenger Position
x4 = x(4,:);  % Passenger Velocity
x5 = x(5,:);  % Absorber Mass Position
x6 = x(6,:);  % Absorber Mass Velocity
x7 = x(7,:);  % Phase

% Parameters
F = p(1,:);
m = p(2,:);
c = p(3,:);
k = p(4,:);
mp = p(5,:);
cs = p(6,:);
ks = p(7,:);
ma = p(8,:);
ca = p(9,:);
ka = p(10,:);
aa = p(11,:);
om = p(12,:);

% Calculated from Mathematica, checked data entry using
% x0 = rand(7,2); p0 = rand(12,2);
% coco_ezDFDX('f(x,p)', @nla_dx, x0, p0) - nla_dxdx(x0, p0)
Jxx = zeros(7,7,7,numel(x1));

Jxx(2,1,1,:) = -(-6.*aa.*(x5 - x1))./m;
Jxx(2,1,5,:) = -(6.*aa.*(x5 - x1))./m;
Jxx(2,5,1,:) = -(6.*aa.*(x5 - x1))./m;
Jxx(2,5,5,:) =  (6.*aa.*(x5 - x1))./m;
Jxx(2,7,7,:) = -(F.*cos(x7)./m);
Jxx(6,1,1,:) =  (-6.*aa.*(x5 - x1))./ma;
Jxx(6,1,5,:) =  (6.*aa.*(x5 - x1))./ma;
Jxx(6,5,1,:) =  (6.*aa.*(x5 - x1))./ma;
Jxx(6,5,5,:) = -(6.*aa.*(x5 - x1))./ma;

end