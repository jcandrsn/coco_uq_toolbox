function y = nla_bc(data, T, x0, x1, p)

y = [x1(1:6)-x0(1:6); x1(7)-x0(7)-2*pi];

end
