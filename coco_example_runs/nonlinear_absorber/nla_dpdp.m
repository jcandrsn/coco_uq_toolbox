function Jpp = nla_dpdp(x,p)

% State Variables
x1 = x(1,:);  % Base Mass Position
x2 = x(2,:);  % Base Mass Velocity
x3 = x(3,:);  % Passenger Position
x4 = x(4,:);  % Passenger Velocity
x5 = x(5,:);  % Absorber Mass Position
x6 = x(6,:);  % Absorber Mass Velocity
x7 = x(7,:);  % Phase

% Parameters
F = p(1,:);
m = p(2,:);
c = p(3,:);
k = p(4,:);
mp = p(5,:);
cs = p(6,:);
ks = p(7,:);
ma = p(8,:);
ca = p(9,:);
ka = p(10,:);
aa = p(11,:);
om = p(12,:);

% Checked using
% x0 = rand(7,2); p0 = rand(12,2);
% dpdp_approx = coco_ezDFDP('f(x,p)', @nla_dp, x0, p0);
% dpdp = nla_dpdp(x0, p0);
% dpdp_approx - dpdp

Jpp = zeros(7,12,12,numel(x1));

Jpp(2,1,2,:)  =  -cos(x7)./(m.^2);
Jpp(2,2,1,:)  = -(1./(m.^2)).*(cos(x7));
Jpp(2,2,2,:)  = (2./(m.^3)).*(F.*cos(x7) - c.*x2 - k.*x1 + ...
                cs.*(x4 - x2) + ks.*(x3 - x1) + ...
                ca.*(x6 - x2) + ka.*(x5 - x1) + aa.*(x5 - x1).^3);
Jpp(2,2,3,:)  = (1./(m.^2)).*(x2);
Jpp(2,2,4,:)  = (1./(m.^2)).*(x1);
Jpp(2,2,6,:)  = -(1./(m.^2)).*(x4 - x2);
Jpp(2,2,7,:)  = -(1./(m.^2)).*(x3 - x1);
Jpp(2,2,9,:)  = -(1./(m.^2)).*(x6 - x2);
Jpp(2,2,10,:)  = -(1./(m.^2)).*(x5 - x1);
Jpp(2,2,11,:)  = -(1./(m.^2)).*((x5 - x1).^3);
Jpp(2,3,2,:)  = (x2./(m.^2));
Jpp(2,4,2,:)  = (x1./(m.^2));
Jpp(2,6,2,:)  =  -(x4-x2)./(m.^2);
Jpp(2,7,2,:)  =  -(x3-x1)./(m.^2);
Jpp(2,9,2,:)  =  -(x6-x2)./(m.^2);
Jpp(2,10,2,:) =  -(x5-x1)./(m.^2);
Jpp(2,11,2,:) =  -((x5-x1).^3)./(m.^2);
Jpp(4,5,5,:)  = 2.*(-ks.*(x3 - x1) - cs.*(x4 - x2))./(mp.^3);
Jpp(4,5,6,:)  = (x4 - x2)./(mp.^2);
Jpp(4,5,7,:)  = (x3 - x1)./(mp.^2);
Jpp(4,6,5,:)  = (x4 - x2)./(mp.^2);
Jpp(4,7,5,:)  = (x3 - x1)./(mp.^2);
Jpp(6,8,8,:)  = 2.*(-ca.*(x6 - x2) - ka.*(x5 - x1) - aa.*(x5 - x1).^3)./(ma.^3);
Jpp(6,8,9,:)  = (x6 - x2)./(ma.^2);
Jpp(6,8,10,:)  = (x5 - x1)./(ma.^2);
Jpp(6,8,11,:)  = ((x5 - x1).^3)./(ma.^2);
Jpp(6,9,8,:)  = (x6 - x2)./(ma.^2);
Jpp(6,10,8,:) = (x5 - x1)./(ma.^2);
Jpp(6,11,8,:) = ((x5 - x1).^3)./(ma.^2);

end