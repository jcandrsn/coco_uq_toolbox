function [data, y] = nla_bc_caf(prob, data, u)
% Version that's compatible with coco_add_func

pr = data.pr;
bc = pr.bvp_bc;

x0 = u(bc.x0_idx);
x1 = u(bc.x1_idx);

y = [x1(1:6)-x0(1:6); x1(7)-x0(7)-2*pi];

end

