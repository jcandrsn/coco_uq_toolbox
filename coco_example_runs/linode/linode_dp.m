function J = linode_dp(t, x, p)

x1 = x(1,:);
phi = p(2,:);
om = p(3,:);

J = zeros(2,3,numel(x1));
J(2,1,:) = -x1;
J(2,2,:) = -sin(om.*t + phi);
J(2,3,:) = -t.*sin(om.*t + phi);

end
