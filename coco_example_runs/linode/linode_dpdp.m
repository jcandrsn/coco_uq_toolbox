function dJ = linode_dpdp(t, x, p)

x1 = x(1,:);
phi = p(2,:);
om = p(3,:);

dJ = zeros(2,3,3,numel(x1));
% J(2,2,:) = -sin(om.*t + phi);
dJ(2,2,2,:) = -cos(om.*t + phi);
dJ(2,2,3,:) = -t.*cos(om.*t + phi);
% J(2,3,:) = -t.*sin(om.*t + phi);
dJ(2,3,2,:) = -t.*cos(om.*t + phi);
dJ(2,3,3,:) = -(t.^2).*cos(om.*t + phi);

end
