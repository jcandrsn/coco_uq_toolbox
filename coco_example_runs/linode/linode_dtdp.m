function dJ = linode_dtdp(t, x, p)


phi = p(2,:);
om = p(3,:);

dJ = zeros(2,3,numel(t));
% J(2,:) = -om.*sin(om.*t + phi);
dJ(2,2,:) = -om.*cos(om.*t + phi);
dJ(2,3,:) = -t.*om.*cos(om.*t + phi) - sin(om.*t + phi);

end
