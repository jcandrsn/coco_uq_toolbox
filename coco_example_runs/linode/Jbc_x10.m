function J = Jbc_x10(data, T0, T, x0, x1, p)
%FBC Summary of this function goes here
%   Detailed explanation goes here

om = p(3);

%      1   2   3   4   5   6   7   8   9
J = [  0,  0, -1,  0,  1,  0,  0,  0,  0;
       0,  0,  0, -1,  0,  1,  0,  0,  0;
       1,  0,  0,  0,  0,  0,  0,  0,  0;
       0,  1,  0,  0,  0,  0,  0,  0,  2*pi/om.^2;
       0,  0,  0,  1,  0,  0,  0,  0,  0];

end

