function J = linode_dt(t, x, p)

x1 = x(1,:);
phi = p(2,:);
om = p(3,:);

J = zeros(2,numel(x1));
J(2,:) = -om.*sin(om.*t + phi);

end
