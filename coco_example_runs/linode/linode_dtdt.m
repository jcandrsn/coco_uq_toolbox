function dJ = linode_dtdt(t, x, p)

x1 = x(1,:);
phi = p(2,:);
om = p(3,:);

dJ = zeros(2,numel(x1));
% J(2,:) = -om.*sin(om.*t + phi);
dJ(2,:) = -(om.^2).*cos(om.*t + phi);

end
