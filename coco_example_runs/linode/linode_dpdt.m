function dJ = linode_dpdt(t, x, p)

x1 = x(1,:);
phi = p(2,:);
om = p(3,:);

dJ = zeros(2,3,numel(x1));
dJ(2,2,:) = -om.*cos(om.*t + phi);
% J(2,3,:) = -t.*sin(om.*t + phi);
dJ(2,3,:) = -t.*om.*cos(om.*t + phi) + sin(om.*t + phi);

end
