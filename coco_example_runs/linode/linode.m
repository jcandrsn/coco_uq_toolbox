function y = linode(t, x, p)
%LINODE_HET   'coll'-compatible encoding of linode vector field.
%
% Encoding is of a non-autonomous vector field.

x1 = x(1,:);
x2 = x(2,:);
k = p(1,:);
phi = p(2,:);
om = p(3,:);

y(1,:) = x2;
y(2,:) = -x2 - k.*x1 + cos(om.*t + phi);

end
