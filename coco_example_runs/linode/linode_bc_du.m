function [data, J] = linode_bc_du(prob, data, u) %#ok<INUSL>
%LINODE_HET_BC   'bvp'-compatible encoding of linode boundary conditions
%
% Encoding is of a non-autonomous vector field.

x0 = u(1:2);
x1 = u(3:4);
T0 = u(5);
T  = u(6);
p3 = u(7);

% y = [x1(1:2)-x0(1:2); T0; T-2*pi./om];

%      1   2   3   4   5   6   7
J = [ -1,  0,  1,  0,  0,  0,  0;
       0, -1,  0,  1,  0,  0,  0;
       0,  0,  0,  0,  1,  0,  0;
       0,  0,  0,  0,  0,  1,  2*pi/p3.^2];

end
