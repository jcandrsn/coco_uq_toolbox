function y = fbc(data, T0, T, x0, x1, p)
%FBC Summary of this function goes here
%   Detailed explanation goes here

om = p(3);
y = [x1-x0; T0; T-2*pi./om];

end

