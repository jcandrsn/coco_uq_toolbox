function y = adj_F(t, la, p)

la1 = la(1,:);
la2 = la(2,:);

k = p(1,:);

y(1,:) = k.*la2;
y(2,:) = la2 - la1;

end