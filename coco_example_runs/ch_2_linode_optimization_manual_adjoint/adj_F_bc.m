function [data, y] = adj_F_bc(prob, data, u)

la0 = u(1:2);
la1 = u(3:4);
T0 = u(5);
T  = u(6);
la3 = u(7);
la4 = u(8);
eta1 = u(9);

y = [-la0(1) + la3 + eta1; ...
     -la0(2) + la4; ...
      la1(1) - la3; ...
      la1(2) - la4; ...
      T0; T-2*pi];

end

