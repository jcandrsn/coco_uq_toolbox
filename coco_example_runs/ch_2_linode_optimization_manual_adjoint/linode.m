function y = linode(t, x, p)

x1 = x(1,:);
x2 = x(2,:);
k = p(1,:);
phi = p(2,:);

y(1,:) = x2;
y(2,:) = cos(t + phi) - x2 - k.*x1;

end
