function [data, y] = linode_bc(prob, data, u)

x0 = u(1:2);
x1 = u(3:4);
T0 = u(5);
T  = u(6);

y = [x0 - x1; T0; T-2*pi];

end
