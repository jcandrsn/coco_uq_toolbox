function J = linode_dt(t, x, p)

x1 = x(1,:);
phi = p(2,:);

J = zeros(2,numel(x1));
J(2,:) = -sin(t + phi);

end
