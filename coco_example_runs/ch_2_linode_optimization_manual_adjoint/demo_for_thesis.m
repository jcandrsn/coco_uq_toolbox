clear variables
%%
% Initial parameter set for linear stifness, k, and phase
% angle phi
p0 = [4;0];
% Let Transients die out
[~, x0]   = ode45(@(t,x) linode(t, x, p0), ...
  [0 6*pi], [1; 0]);
% Approximate periodic orbit
[t0, x0]   = ode45(@(t,x) linode(t, x, p0), ...
  [0 2*pi], x0(end,:));

% Initialize problem instance and set collocation options
NTST = 15; NCOL = 4;
prob = coco_prob();
prob = coco_set(prob, 'ode', 'autonomous', false);
prob = coco_set(prob, 'coll', 'NTST', NTST, ...
  'NCOL', NCOL);

coll_args = {@linode, @linode_dx, ...
  @linode_dp, @linode_dt, t0, x0, ...
  {'k','phi'}, p0};
prob = ode_isol2coll(prob, 'orig', coll_args{:});

[data, uidx] = coco_get_func_data(prob, 'orig.coll', ...
  'data', 'uidx');
maps = data.coll_seg.maps;

% Add Boundary Conditions
prob = coco_add_func(prob, 'orig.po', @linode_bc, ...
  data, 'zero', 'uidx', uidx([maps.x0_idx; ...
                              maps.x1_idx; ...
                              maps.T0_idx; ...
                              maps.T_idx]));

% Objective Function
prob = coco_add_pars(prob, 'mu1', maps.x0_idx(1), 'mu1');

coll_args = {@adj_F, @adj_F_dx, @adj_F_dp, @adj_F_dt, ...
  t0, zeros(size(x0)), p0};
prob = ode_isol2coll(prob, 'adj', coll_args{:});

[a_data, a_uidx] = coco_get_func_data(prob, ...
  'adj.coll', 'data', 'uidx');
a_maps = a_data.coll_seg.maps;

% Glue parameters together
x1idx = uidx([maps.p_idx]);
x2idx = a_uidx([a_maps.p_idx]);
prob = coco_add_glue(prob, 'glue', x1idx, x2idx);

% Adjoint boundary condition
prob = coco_add_func(prob, 'adj.bc', @adj_F_bc, ...
  @adj_F_bc_du, a_data, 'zero', 'uidx', ...
  a_uidx([a_maps.x0_idx; a_maps.x1_idx; ...
          a_maps.T0_idx; a_maps.T_idx]), ...
 'u0', [0;0;0]); % < 3 extra are la3, la4, and eta1

adjbc_uidx = coco_get_func_data(prob, 'adj.bc', ...
  'uidx');
prob = coco_add_pars(prob, 'la3', adjbc_uidx(7), ...
  'la3', 'active');
prob = coco_add_pars(prob, 'la4', adjbc_uidx(8), ...
  'la4', 'active');
prob = coco_add_pars(prob, 'eta1', adjbc_uidx(9), ...
  'eta1', 'inactive');

% Get u1 and lambda2, T, and p indices for use in
% stationarity equation
ulap = [uidx(maps.xbp_idx(1:2:end)); ...
        a_uidx(a_maps.xbp_idx(2:2:end)); ...
        uidx(maps.T_idx); 
        uidx(maps.p_idx)];

% Two additional u's are eta_k, eta_phi
prob = coco_add_func(prob, 'stationarity', @stationarity, ...
  @stationarity_du, data, 'zero',...
  'uidx', ulap, 'u0', [0;0]); 
adjbc_uidx = coco_get_func_data(prob, 'stationarity', 'uidx');
prob = coco_add_pars(prob, 'dmus', adjbc_uidx(end-1:end), ...
                     {'eta_k', 'eta_phi'}, 'inactive');

bd1 = coco(prob, 'init', [], 1, {'mu1', 'k', 'eta1', 'eta_phi'}, {[0.1 2]});

%% Drive eta1 to 1
run_name = 'opt2';
fprintf(['\nFrom the previously found Branch Point, continue along an ', ...
         'intersecting solution manifold where\nthe mus and etas are ', ...
         'able to take on non-trivial values.\n'])
BPlab = coco_bd_labs(bd1, 'BP');

prob = coco_prob();
prob = coco_set(prob, 'ode', 'autonomous', false);
prob = coco_set(prob, 'coll', 'NTST', NTST, 'NCOL', NCOL);

prob = ode_BP2coll(prob, 'orig', 'init', BPlab(end));

[data, uidx] = coco_get_func_data(prob, 'orig.coll', 'data', 'uidx');
maps = data.coll_seg.maps;

% Add Boundary Conditions
prob = coco_add_func(prob, 'orig.po', @linode_bc, data, 'zero', ...
   'uidx', uidx([maps.x0_idx; maps.x1_idx; maps.T0_idx; maps.T_idx]));

% Objective Function
prob = coco_add_pars(prob, 'mu1', maps.x0_idx(1), 'mu1');

% Adjoint System
prob = ode_BP2coll(prob, 'adj', 'init', BPlab(end));

[a_data, a_uidx] = coco_get_func_data(prob, 'adj.coll', 'data', 'uidx');
a_maps = a_data.coll_seg.maps;

% Glue parameters together
x1idx = uidx([maps.p_idx]);
x2idx = a_uidx([a_maps.p_idx]);
prob = coco_add_glue(prob, 'glue', x1idx, x2idx);

% Adjoint boundary condition
prob = coco_add_func(prob, 'adj.bc', @adj_F_bc, ...
                     @adj_F_bc_du, a_data, 'zero', 'uidx', ...
                     a_uidx([a_maps.x0_idx; a_maps.x1_idx; ...
                             a_maps.T0_idx; a_maps.T_idx]), ...
                     'u0', [0;0;0]); % < 3 extra are la3, la4, and eta1

adjbc_uidx = coco_get_func_data(prob, 'adj.bc', 'uidx');
prob = coco_add_pars(prob, 'la3', adjbc_uidx(7), 'la3', 'active');
prob = coco_add_pars(prob, 'la4', adjbc_uidx(8), 'la4', 'active');
prob = coco_add_pars(prob, 'eta1', adjbc_uidx(9), 'eta1', 'inactive');

% Get u1 and lambda2, T, and p indices for use in stationarity equation
ulap = [uidx(maps.xbp_idx(1:2:end)); a_uidx(a_maps.xbp_idx(2:2:end)); ...
        uidx(maps.T_idx); uidx(maps.p_idx)];

% Note that the data being passed to coco_add_func is the original problems
% data structure.  This works because the mesh adaptivity is removed, but
% would cause problems if adjoint and original problems have different
% mesh points

% Two additional u's are eta_k, eta_phi
prob = coco_add_func(prob, 'stationarity', @stationarity, ...
                     @stationarity_du, data, 'zero', ...
                     'uidx', ulap, 'u0', [0;0]); 
adjbc_uidx = coco_get_func_data(prob, 'stationarity', 'uidx');
prob = coco_add_pars(prob, 'dmus', adjbc_uidx(end-1:end), ...
                     {'eta_k', 'eta_phi'}, 'inactive');

bd2 = coco(prob, 'eta1', [], 1, {'eta1', 'mu1', 'k', 'eta_phi', 'la3', 'la4'}, {[0 1]});

%% Drive eta_phi to 0

EPlab = coco_bd_labs(bd2, 'EP');

prob = coco_prob();
prob = coco_set(prob, 'ode', 'autonomous', false);
prob = coco_set(prob, 'coll', 'NTST', NTST, 'NCOL', NCOL);

prob = ode_coll2coll(prob, 'orig', 'eta1', EPlab(end));
 
[data, uidx] = coco_get_func_data(prob, 'orig.coll', 'data', 'uidx');
maps = data.coll_seg.maps;

% Add Boundary Conditions
prob = coco_add_func(prob, 'orig.po', @linode_bc, data, 'zero', ...
   'uidx', uidx([maps.x0_idx; maps.x1_idx; maps.T0_idx; maps.T_idx]));

% Objective Function
prob = coco_add_pars(prob, 'mu1', maps.x0_idx(1), 'mu1');

% Adjoint System
prob = ode_coll2coll(prob, 'adj', 'eta1', EPlab(end));

[a_data, a_uidx] = coco_get_func_data(prob, 'adj.coll', 'data', 'uidx');
a_maps = a_data.coll_seg.maps;

% Glue parameters together
x1idx = uidx([maps.p_idx]);
x2idx = a_uidx([a_maps.p_idx]);
prob = coco_add_glue(prob, 'glue', x1idx, x2idx);

% Adjoint boundary condition
prob = coco_add_func(prob, 'adj.bc', @adj_F_bc, ...
                     @adj_F_bc_du, a_data, 'zero', 'uidx', ...
                     a_uidx([a_maps.x0_idx; a_maps.x1_idx; ...
                             a_maps.T0_idx; a_maps.T_idx]), ...
                     'u0', [-9.9392e-01;-2.8173e-02;1]); % < 3 extra are la3, la4, and eta1

adjbc_uidx = coco_get_func_data(prob, 'adj.bc', 'uidx');
prob = coco_add_pars(prob, 'la3', adjbc_uidx(7), 'la3', 'active');
prob = coco_add_pars(prob, 'la4', adjbc_uidx(8), 'la4', 'active');
prob = coco_add_pars(prob, 'eta1', adjbc_uidx(9), 'eta1', 'inactive');

% Get u1 and lambda2, T, and p indices for use in stationarity equation
ulap = [uidx(maps.xbp_idx(1:2:end)); a_uidx(a_maps.xbp_idx(2:2:end)); ...
        uidx(maps.T_idx); uidx(maps.p_idx)];

% Two additional u's are eta_k, eta_phi
prob = coco_add_func(prob, 'stationarity', @stationarity, ...
                     @stationarity_du, data, 'zero', ...
                     'uidx', ulap, 'u0', [0;-0.5]);
adjbc_uidx = coco_get_func_data(prob, 'stationarity', 'uidx');
prob = coco_add_pars(prob, 'dmus', adjbc_uidx(end-1:end), ...
                     {'eta_k', 'eta_phi'}, 'inactive');

% Mark the optimal point
prob = coco_add_event(prob, 'OPT', 'eta_phi', 0);

prob = coco_set(prob, 'cont', 'NPR', 100);
bd3 = coco(prob, 'opt', [], 1, {'eta_phi', 'mu1', 'k', 'phi', 'la3', 'la4'}, {[], [0.3 1.5]});