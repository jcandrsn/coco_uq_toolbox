function [data, J] = stationarity_du(prob, data, u)

dim = data.coll_seg.maps.xbp_shp(2);
x1bp = u(1:dim);
la2bp = u(dim+1:2*dim);
T = u(end-4);
phi = u(end-2);

dim = data.coll_seg.int.dim;
w = data.coll_seg.mesh.gwt';
W = data.coll_seg.maps.W(1:dim:end, 1:dim:end);
ka = repmat(data.coll_seg.mesh.ka,[data.coll_seg.int.NCOL,1]);
ka = ka(:);
tcn = data.coll_seg.mesh.tcn;
N = data.coll_seg.maps.NTST;

% Jacobians of portions of the Numerical Integration
J_u = [(0.5*T/N)*((W.^2)*diag(la2bp))'*(ka.*w), ...
       zeros(size(x1bp))]';

J_la = [(0.5*T/N)*((W.^2)*diag(x1bp))'*(ka.*w), ...
        (0.5*T/N)*(diag(sin(T*tcn+phi).*ka)*W)'*w]';

fds = (0.5*T/N)*diag(cos(T*tcn+phi))*diag(tcn);
sdf = diag(sin(T*tcn+phi));

J_T = [(0.5/N)*((W*x1bp).*(W*la2bp).*ka)'*w; ...
       (0.5/N)*((fds + sdf)*((W*la2bp).*ka))'*w];

J_p = [0, 0;
       0, (0.5*T/N)*((W*la2bp).*cos(T*tcn + phi).*ka)'*w];

J_eta = eye(2);

% Combining into Full Jacobian
J = [J_u, J_la, J_T, J_p, J_eta];

end

