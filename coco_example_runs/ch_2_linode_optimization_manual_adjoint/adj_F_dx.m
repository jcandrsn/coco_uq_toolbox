function J = adj_F_dU(t, la, p)

k = p(1,:);

J = zeros(2,2,numel(t));
J(1,2,:) = k;
J(2,1,:) = -1;
J(2,2,:) = 1;

end
