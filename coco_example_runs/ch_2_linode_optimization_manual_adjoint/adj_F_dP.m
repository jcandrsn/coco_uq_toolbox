function J = adj_F_dp(t, la, p)

la2 = la(2,:);

J = zeros(2,2,numel(la2));

J(1,1,:) = la2;

end
