function [data, y] = stationarity(prob, data, u)

dim = data.coll_seg.maps.xbp_shp(2);

x1bp = u(1:dim);
la2bp = u(dim+1:2*dim);
T = u(end-4);
k = u(end-3);
phi = u(end-2);
eta_k = u(end-1);
eta_phi = u(end);

% Numerical Integration
dim = data.coll_seg.int.dim;
W = data.coll_seg.maps.W(1:dim:end, 1:dim:end);
ka = repmat(data.coll_seg.mesh.ka,[data.coll_seg.int.NCOL,1]);
ka = ka(:);
tcn = data.coll_seg.mesh.tcn;
w = data.coll_seg.mesh.gwt';
N = data.coll_seg.maps.NTST;

y1_int = (0.5*T/N)*((W*la2bp).*(W*x1bp).*ka)'*w;
y2_int = (0.5*T/N)*(sin(T*tcn+phi).*(W*la2bp).*ka)'*w;

y = [y1_int + eta_k;
     y2_int + eta_phi];

end

