if not(coco_exist('phase', 'run'))
    % Initial Solution Guess
    % Initial parameter set for linear stifness, k, and phase angle phi
    p0 = [4;0];

    % Let Transients die out
    [~, x0]   = ode45(@(t,x) linode(t, x, p0), [0 6*pi], [1; 0]);

    % Approximate periodic orbit
    [t0, x0]   = ode45(@(t,x) linode(t, x, p0), [0 2*pi], x0(end,:));

    NTST = 25;
    NCOL = 4;
    prob = coco_prob();
    prob = coco_set(prob, 'ode', 'autonomous', false);

    prob = coco_set(prob, 'coll', 'NTST', NTST, 'NCOL', NCOL);
    coll_args = {@linode, @linode_dx, @linode_dp, @linode_dt, ...
      t0, x0, {'k','phi'}, p0};
    prob = ode_isol2coll(prob, 'orig', coll_args{:});

    [data, uidx] = coco_get_func_data(prob, 'orig.coll', 'data', 'uidx');
    maps = data.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, 'orig.po', @linode_bc, data, 'zero', ...
       'uidx', uidx([maps.x0_idx; maps.x1_idx; maps.T0_idx; maps.T_idx]));

    % ICs
    prob = coco_add_pars(prob, 'IC', maps.x0_idx, {'x0', 'v0'});

    prob = coco_add_event(prob, 'VEL', 'v0', 0);

    bd = coco(prob, 'phase', [], 1, {'phi', 'v0', 'x0', 'k'}, {[-0.1 2]});
else
    bd = coco_bd_read('phase');
end

EPlab = coco_bd_labs(bd, 'VEL');

prob = coco_prob();
prob = coco_set(prob, 'ode', 'autonomous', false);
prob = coco_set(prob, 'cont', 'PtMX', 1000, 'h_max', 20);
NTST = 15;
NCOL = 4;
prob = coco_set(prob, 'coll', 'NTST', NTST, 'NCOL', NCOL);

prob = ode_coll2coll(prob, 'orig', 'phase', EPlab(end));

[data, uidx] = coco_get_func_data(prob, 'orig.coll', 'data', 'uidx');
maps = data.coll_seg.maps;

% Add Boundary Conditions
prob = coco_add_func(prob, 'orig.po', @linode_bc, data, 'zero', ...
   'uidx', uidx([maps.x0_idx; maps.x1_idx; maps.T0_idx; maps.T_idx]));

% ICs and lock v0 at 0
prob = coco_add_pars(prob, 'IC', maps.x0_idx, {'x0', 'v0'}, 'inactive');
ks = [10.^(-2:2:0), 3, 10.^(1:2)];
prob = coco_add_event(prob, 'K', 'k', ks);
if not(coco_exist('khigh', 'run'))
    bd2 = coco(prob, 'khigh', [], 1, {'k', 'phi', 'x0', 'v0'}, {[4 10001]});
else
    bd2 = coco_bd_read('khigh');
end
if not(coco_exist('klow', 'run'))
    prob = coco_set(prob, 'cont', 'PtMX', 100, 'h', 0.1, 'almax', 30, 'h_max', 0.2);
    bd = coco(prob, 'klow', [], 1, {'k', 'phi', 'x0', 'v0'}, {[0.001 4]});
else
    bd = coco_bd_read('klow');
end
%% Step 2:
%  pull data and plot the k-sweep with a handful of frequencies marked with
%  red circles

semilogx([cell2mat(bd(2:end,8));cell2mat(bd2(2:end,8))], ...
         [cell2mat(bd(2:end,12));cell2mat(bd2(2:end,12))], 'k-', ...
         'LineWidth', 1)
hold on
idx1 = strcmp('K', bd(:,7));
idx2 = strcmp('K', bd2(:,7));
ks = [cell2mat(bd(idx1,8)); cell2mat(bd2(idx2,8))];
As = [cell2mat(bd(idx1,12)); cell2mat(bd2(idx2,12))];
m = {'ko', 'kp', 'kd', 'ks', 'k^'};
for i = 1:numel(ks)
semilogx(ks(i), As(i), m{i}, 'MarkerSize', 7, 'LineWidth', 1, ...
  'MarkerFaceColor', [0.5,0.5,0.5])
end

% Formatting
grid on
box on
% title('Initial Position vs Stiffness', 'Interpreter', 'latex')
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = 12;
xlabel('Stiffness, $k$', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('Initial Position, $x_{1}\!\left(0\right)$', 'Interpreter', 'latex', 'FontSize', 15)
ylim([0,1.05])


%% plot the trajectories for the handful of identified frequencies.
run_names = {'klow','khigh'};
curve_names = 'orig';
lab_names = 'K';

figure(2); clf; hold on; grid on; box on;
l = cell(size(ks));
k = 1;
m = {'ko', 'kp', 'kd', 'ks', 'k^'};
for j = 1:size(run_names, 2)
bd = coco_bd_read(run_names{j});
lab_names = cellstr(lab_names);
labs = [];
for lab_name=lab_names
    labs = [labs, coco_bd_labs(bd, lab_name)];
end

curve_names = cellstr(curve_names);

for i = 1:size(curve_names, 2)
    for lab = labs
        sol = coll_read_solution(curve_names{i}, run_names{j}, lab);
        t = sol.tbp/sol.T;
        x = sol.xbp(:,1);
        N = 5;
        ii = 5:10:size(t,1);
        plot(sol.T*t, x, 'k-', 'HandleVisibility', 'off')
        plot(sol.T*t(ii), x(ii), m{k}, 'DisplayName', ...
             strcat('$k = ', num2str(sol.p(1)), '$'), 'MarkerSize', 7, ...
             'LineWidth', 1, 'MarkerFaceColor', [0.5,0.5,0.5])
        k = k + 1;        
    end
end
end
title('Position vs Time for Selected Stiffness Values', 'Interpreter', 'latex')

ylim([-1,1])
xlim([0,sol.T])
ax = gca;
ax.XTick = [0,pi/2, pi, 3*pi/2, 2*pi];
ax.TickLabelInterpreter = 'latex';
ax.XTickLabel = {'$0$', '$\frac{\pi}{2}$', '$\pi$', '$\frac{3\pi}{2}$', '$2\pi$'};
ax.FontSize = 12;
xlabel('Time, $t$', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('Position, $x_{1}\!\left(t\right)$', 'Interpreter', 'latex', 'FontSize', 15)
lgd = legend('Orientation', 'vertical');
lgd.Interpreter = 'latex';
lgd.Location = 'best';

bd1 = coco_bd_read('init');
bd2 = coco_bd_read('eta1');
bd3 = coco_bd_read('opt');
col_phi = 9;
col_k = 8;
col_x0 = 12;

k1 = coco_bd_col(bd1, 'k');
k2 = coco_bd_col(bd3, 'k');
phi1 = coco_bd_col(bd1, 'phi');
phi2 = coco_bd_col(bd3, 'phi');
x01 = coco_bd_col(bd1, 'mu1');
x02 = coco_bd_col(bd3, 'mu1');
bp = coco_bd_labs(bd1, 'BP');
sol = coll_read_solution('orig', 'init', bp);
bp2 = coco_bd_labs(bd3, 'OPT');
sol2 = coll_read_solution('orig', 'opt', bp2);

figure(3);hold on;grid on;box on;
plot3(k1, phi1, x01, 'k-', 'DisplayName', 'Original Solution Branch')
plot3(sol.p(1),sol.p(2), sol.xbp(1,1), 'ko', 'HandleVisibility', 'off')
plot3(sol2.p(1),sol2.p(2), sol2.xbp(1,1), 'ko', 'HandleVisibility', 'off')
plot3(k2, phi2, x02, 'k-.', 'DisplayName', 'Second Solution Branch')
xlim([0,4])
ylim([-0.5, pi])
x = [0.6, 0.53];
y = [0.3, 0.23];
annotation('textarrow',x, y,'String','Branch Point', ...
  'Interpreter', 'latex', 'FontSize', 15)
x = [0.5, 0.34];
y = [0.5, 0.57];
annotation('textarrow',x, y,'String','Optimal Point', ...
  'Interpreter', 'latex', 'FontSize', 15)
lgd = legend('Orientation', 'vertical');
lgd.Interpreter = 'latex';
lgd.Location = 'best';
ax = gca;
ax.XTick = 0:0.5:4;
ax.YTick = 0:pi/4:pi;
ax.YTickLabel = {'$0$', '$\frac{\pi}{4}$', '$\frac{\pi}{2}$', ...
  '$\frac{3\pi}{4}$', '$\pi$'};
ax.TickLabelInterpreter = 'latex';
ax.FontSize = 12;
xlabel('Stiffness, k', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('Phase Angle, $\phi$', 'Interpreter', 'latex', 'FontSize', 15)
zlabel('Initial Position, $x_{1}\!\left(0\right)$', 'Interpreter', 'latex', 'FontSize', 15)

col_la3 = 15;
col_la4 = 16;
col_dx0 = 17;
la31 = [cell2mat(bd2(2:end,col_la3))];
la32 = [cell2mat(bd3(2:end,col_la3))];
obj1  = coco_bd_col(bd2, 'mu1');
obj2  = coco_bd_col(bd3, 'mu1');

la41 = [cell2mat(bd2(2:end,col_la4))];
la42 = [cell2mat(bd3(2:end,col_la4))];
     
dx01 = [cell2mat(bd2(2:end,col_dx0))];
dx02 = [cell2mat(bd3(2:end,col_dx0))];
la3opt = coco_read_solution('la3', 'opt', bp2, 'chart');
la4opt = coco_read_solution('la4', 'opt', bp2, 'chart');
x0opt = coco_read_solution('x0', 'opt', bp2, 'chart');
dx0opt = coco_read_solution('d.x0', 'opt', bp2, 'chart');

labs = coco_bd_labs(bd2, 'EP');
bp3 = max(labs);
la3e1 = coco_read_solution('la3', 'eta1', bp3, 'chart');
la4e1 = coco_read_solution('la4', 'eta1', bp3, 'chart');
x0e1 = coco_read_solution('x0', 'eta1', bp3, 'chart');
dx0e1 = coco_read_solution('e', 'eta1', bp3, 'chart');

figure(4)
xlim([-1.2,0])
ylim([-0.4,0.1])
hold on;grid on; box on;
plot3(la31, la41, obj1, 'k-', 'DisplayName', '$\eta_{1} = 0 \rightarrow 1$');
plot3(la32, la42, obj2, 'k-.', 'DisplayName', 'Second Solution Branch');
plot3(la3opt.x, la4opt.x(1), x0opt.x(1), 'ko', 'HandleVisibility', 'off')
plot3(la3e1.x, la4e1.x(1), x0e1.x(1), 'ko', 'HandleVisibility', 'off')
lgd = legend('Orientation', 'vertical');
lgd.Interpreter = 'latex';
lgd.Location = 'best';
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.XTick = -1.2:0.2:0;
ax.YTick = -0.4:0.1:0.2;
ax.FontSize = 12;
xlabel('$\lambda_{bc,1}$', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('$\lambda_{bc,2}$', 'Interpreter', 'latex', 'FontSize', 15)
zlabel('Initial Position, $x_{1}\!\left(0\right)$', 'Interpreter', 'latex', 'FontSize', 15)
x = [0.4, 0.26];
y = [0.8, 0.825];
annotation('textarrow',x, y,'String','Optimal Point', ...
  'Interpreter', 'latex', 'FontSize', 15)
x = [0.4, 0.267];
y = [0.6, 0.71];
annotation('textarrow',x, y,'String','$\eta_{1} = 1$', ...
  'Interpreter', 'latex', 'FontSize', 15)