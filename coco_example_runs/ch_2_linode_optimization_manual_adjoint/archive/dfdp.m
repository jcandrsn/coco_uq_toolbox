function J = dfdp(t, x, p)

x1 = x(1,:);
la2 = x(4,:);
phi = p(2,:);

J = zeros(4,2,numel(x1));
J(2,1,:) = -x1;
J(2,2,:) = -sin(t + phi);
J(3,1,:) = la2;

end