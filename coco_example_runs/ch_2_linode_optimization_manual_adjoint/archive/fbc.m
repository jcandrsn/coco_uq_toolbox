function [data, y] = fbc(prob, data, u)

x_0 = u(1:2);
la_0 = u(3:4);
x_1 = u(5:6);
la_1 = u(7:8);
T0 = u(9);
T  = u(10);
la3 = u(11);
la4 = u(12);
eta1 = u(13);

y = [x_0 - x_1; ...
     -la_0(1) + la3 + eta1; ...
     -la_0(2) + la4; ...
     la_1(1) - la3; ...
     la_1(2) - la4; ...
     T0; T-2*pi];

end