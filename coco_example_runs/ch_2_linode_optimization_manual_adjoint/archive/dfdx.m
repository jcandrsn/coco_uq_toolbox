function J = dfdx(t, x, p)

x1 = x(1,:);
k = p(1,:);

J = zeros(4,4,numel(x1));
J(1,2,:) = 1;
J(2,1,:) = -k;
J(2,2,:) = -1;
J(3,4,:) = k;
J(4,3,:) = -1;
J(4,4,:) = 1;

end