clear variables
%%
% Initial parameter set for linear stifness, k, and phase
% angle phi
p0 = [4;0];
% Let Transients die out
[~, x0]   = ode45(@(t,x) f(t, x, p0), ...
  [0 6*pi], [1; 0; 0; 0]);
% Approximate periodic orbit
[t0, x0]   = ode45(@(t,x) f(t, x, p0), ...
  [0 2*pi], x0(end,:));

% Initialize problem instance and set collocation options
NTST = 15; NCOL = 4;
prob = coco_prob();
prob = coco_set(prob, 'ode', 'autonomous', false);
prob = coco_set(prob, 'coll', 'NTST', NTST, ...
  'NCOL', NCOL, 'NAdapt', 1);

coll_args = {@f, @dfdx, ...
  @dfdp, @dfdt, t0, x0, ...
  {'k','phi'}, p0};
prob = ode_isol2coll(prob, '', coll_args{:});

[data, uidx] = coco_get_func_data(prob, 'coll', ...
  'data', 'uidx');
maps = data.coll_seg.maps;

% Add Boundary Conditions
prob = coco_add_func(prob, 'bc', @fbc, @fbc_du, data, 'zero', ...
  'uidx', uidx([maps.x0_idx; maps.x1_idx; maps.T0_idx; maps.T_idx]), ...
  'u0', [0;0;0]);

bc_uidx = coco_get_func_data(prob, 'bc', ...
  'uidx');
prob = coco_add_pars(prob, 'la3', bc_uidx(11), ...
  'la3', 'active');
prob = coco_add_pars(prob, 'la4', bc_uidx(12), ...
  'la4', 'active');
prob = coco_add_pars(prob, 'd.x0', bc_uidx(13), ...
  'd.x0', 'inactive');

xbp = uidx(reshape(maps.xbp_idx, maps.xbp_shp)');

% Get x1 and la2, T, and p indices for use in stationarity
% equation
ulap = [xbp(:,1); ...
        xbp(:,4); ...
        uidx(maps.T_idx); 
        uidx(maps.p_idx)];

% Two additional u's are d.k, d.phi
prob = coco_add_func(prob, 'stationarity', @stationarity, ...
  @stationarity_dU, data, 'zero',...
  'uidx', ulap, 'u0', [0;0]); 
adjbc_uidx = coco_get_func_data(prob, 'stationarity', 'uidx');
prob = coco_add_pars(prob, 'dmus', adjbc_uidx(end-1:end), ...
                     {'d.k', 'd.phi'}, 'inactive');

% Objective Function
prob = coco_add_pars(prob, 'x0', maps.x0_idx(1), 'x0');
                   
bd1 = coco(prob, 'init', [], 1, {'x0', 'k', 'd.x0', ...
  'd.phi', 'phi'}, {[0.1 2]});