function y = f(t, x, p)

x1 = x(1,:);
x2 = x(2,:);
la1 = x(3,:);
la2 = x(4,:);

k = p(1,:);
phi = p(2,:);

y(1,:) = x2;
y(2,:) = cos(t + phi) - x2 - k.*x1;
y(3,:) = k.*la2;
y(4,:) = la2 - la1;

end