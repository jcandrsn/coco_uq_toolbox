rho = @(k, muk) exp((-(k - muk).^2)/(2*0.2^2))/(sqrt(2*pi*0.2^2));
std = @(k, muk) (k-muk)./(0.2^2);
x10 = @(k) 1./(sqrt((k-1).^2 + 1));

n = 10000;
muks1 = linspace(0.2,1,n/2);
muks2 = linspace(1,8,n/2);
muks = [muks1,muks2(2:end)];
mu = zeros(1,n-1);
var = zeros(1,n-1);
obj = zeros(1,n-1);
dv = zeros(1,n-1);
for i=1:(n-1)
f = @(k) x10(k).*rho(k, muks(i));
f2 = @(k) ((x10(k)).^2).*rho(k, muks(i));
mu(i) = integral(f,-Inf,Inf);
var(i) = integral(f2,-Inf,Inf) - (mu(i))^2;
obj(i) = mu(i) - 3*sqrt(var(i));
end

figure(1)
semilogx(muks(1:end-1),  diff(obj)./diff(muks), 'k-', 'LineWidth', 2);
grid on; box on
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = 12;
xlim([0.2,8])
xlabel('Mean Stiffness, $\mu_{k}$', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('Rate of change of objective function $\mu_r-3\sigma_r$', 'Interpreter', 'latex', 'FontSize', 15)

figure(2)
semilogx(muks,  obj, 'k-', 'LineWidth', 2);
grid on; box on
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = 12;
xlim([0.2,8])
xlabel('Mean Stiffness, $\mu_{k}$', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('Objective function $\mu_{r} - 3\sigma_{r}$', 'Interpreter', 'latex', 'FontSize', 15)


% figure(2)
% bd = coco_bd_read('rdo');
% mu = coco_bd_col(bd, 'orig.resp.mean');
% var = coco_bd_col(bd, 'orig.resp.var');
% muk = coco_bd_col(bd, 'mu.k');
% obj = mu - 3*sqrt(var);
% dobj = diff(mu - 3*sqrt(var))./diff(muk);
% semilogx(muk, obj, 'k-', 'LineWidth', 2);
% grid on; box on
% ax = gca;
% ax.TickLabelInterpreter = 'latex';
% ax.FontSize = 12;
% xlim([0.01,100])
% xlabel('Mean Stiffness, $\mu_{k}$', 'Interpreter', 'latex', 'FontSize', 15)
% ylabel('Objective Function Value, $\mu_{r} - 3\sigma_{r}$', 'Interpreter', 'latex', 'FontSize', 15)

