% Path to vector field, Jacobians, and Response Function
addpath('../linode')
addpath('../utils')

% Let Transients die out
p0 = [3;0;1];
[~, x0] = ode45(@(t,x) linode(t, x, p0), ...
  [0 6*pi], [1; 0]);
% Initial Solution Guess
[t0, x0] = ode45(@(t,x) linode(t, x, p0), ...
  [0 2*pi], [x0(end,:)']);

% Initialize problem instance and set options
prob_init = coco_prob();
prob_init = coco_set(prob_init, 'ode', 'autonomous', false);
prob_init = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
prob_init = coco_set(prob_init, 'cont', 'h', 0.5, 'h_max', 10);

% 'coll'-toolbox arguments
coll_args = {@linode, ...
  @linode_dx, @linode_dp, @linode_dt, ...
  @linode_dxdx, @linode_dxdp, ...
  @linode_dpdp, @linode_dxdt, ...
  @linode_dtdp, @linode_dtdt, ...
  t0, x0, p0};

% Parameter names
pnames = {'k','phi', 'om'};

% 'bvp'-toolbox arguments
% Includes a velocity condition to ensure the initial
% position is a maximum
bvp_args = {@fbc_x10, @Jbc_x10, @dJbc_x10};

% 'uq'-toolbox arguments
uq_args = {{'k'}, {'Normal'}, [[3, 0.2]], {'phi'}, '-add-adjt'};

% Call to sample constructor
prob = uq_isol2bvp_sample(prob_init, 'orig', coll_args{:}, pnames, bvp_args{:}, uq_args{:});
% Add response function, 'resp', to the sample 'orig'
prob = uq_coll_add_response(prob, 'orig', 'resp', 'bv', @x10, @x10_du, @x10_dudu);
prob = uq_coll_add_response_adjoint(prob, 'orig', 'resp');

% Robust Design Optimization Objective Function
response_id = coco_get_id('orig', 'uq', 'responses');

mean_idx = coco_get_func_data(prob, coco_get_id(response_id, 'resp', 'mean'), 'uidx');
var_idx = coco_get_func_data(prob, coco_get_id(response_id, 'resp', 'variance'), 'uidx');

prob = coco_add_func(prob, 'rdo', @rdo, @rdo_dU, @rdo_dUdU, [], 'inactive', 'rdo', 'uidx', [mean_idx;var_idx]);

mean_aidx = coco_get_adjt_data(prob, coco_get_id(response_id, 'resp', 'mean'), 'axidx');
var_aidx = coco_get_adjt_data(prob, coco_get_id(response_id, 'resp', 'variance'), 'axidx');

prob = coco_add_adjt(prob, 'rdo', 'd.rdo', 'aidx', [mean_aidx;var_aidx]);

% Continue in frequency, display mean and variance
uq_bd = coco(prob, 'rdo', [], 1, {'rdo', 'mu.k', 'd.rdo', 'orig.resp.mean', 'orig.resp.var', 'd.om', 'd.sig.k'}, {[],[1e-3,1e3]});

%% Switch Branches
BPLab = coco_bd_labs(uq_bd, 'BP');
prob = prob_init;

% Call to constructor, including flag to construct the
% adjoint equations.
prob = uq_BP2bvp(prob, 'orig', 'rdo', BPLab(1), '-add-adjt', '-add-resp');

mean_idx = coco_get_func_data(prob, coco_get_id(response_id, 'resp', 'mean'), 'uidx');
var_idx = coco_get_func_data(prob, coco_get_id(response_id, 'resp', 'variance'), 'uidx');

prob = coco_add_func(prob, 'rdo', @rdo, @rdo_dU, @rdo_dUdU, ...
  [], 'inactive', 'rdo', 'uidx', [mean_idx;var_idx]);

mean_aidx = coco_get_adjt_data(prob, coco_get_id(response_id, 'resp', 'mean'), 'axidx');
var_aidx = coco_get_adjt_data(prob, coco_get_id(response_id, 'resp', 'variance'), 'axidx');

chart = coco_read_solution('rdo', BPLab(1), 'chart');
cdata = coco_get_chart_data(chart, 'lsol');
[chart, lidx] = coco_read_adjoint('rdo', 'rdo', BPLab(1), 'chart', 'lidx');
l0 = chart.x;
tl0 = cdata.v(lidx);

prob = coco_add_adjt(prob, 'rdo', 'd.rdo', 'aidx', [mean_aidx;var_aidx], 'l0', l0, 'tl0', tl0);
uq_bd = coco(prob, 'rdo.switch', [], 1, {'d.rdo', 'mu.k', 'rdo', 'orig.resp.mean', ...
  'orig.resp.var', 'd.om', 'd.sig.k'}, [0,1]);