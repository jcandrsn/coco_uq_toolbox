rho = @(k, muk) exp((-(k - muk).^2)/(2*0.2^2))/(sqrt(2*pi*0.2^2));
std = @(k, muk) (k-muk)./(0.2^2);
x10 = @(k) 1./(sqrt((k-1).^2 + 1));

n = 5000;
muks1 = linspace(0.2,1,n/2);
muks2 = linspace(1,8,n/2);
muks = [muks1,muks2];
mu = zeros(1,n);
var = zeros(1,n);
obj = zeros(1,n);
dv = zeros(1,n);
for i=1:n
f = @(k) x10(k).*rho(k, muks(i));
f2 = @(k) ((x10(k)).^2).*rho(k, muks(i));
mu(i) = integral(f,-Inf,Inf);
var(i) = integral(f2,-Inf,Inf) - (mu(i))^2;
obj(i) = mu(i) - 3*sqrt(var(i));
% dmuk = @(k) x10(k).*std(k,muks(i)).*rho(k, muks(i));
% dvar1 = @(k) (x10(k).*(rho(k,muks(i))).^2).*std(k, muks(i));
% dvar2 = @(k) 2*(x10(k).*std(k,muks(i)).*rho(k, muks(i)));
% dvar3 = @(k) (x10(k).*std(k,muks(i)));
% dv(1,i) = integral(dmuk,-Inf,Inf) - 3.*(integral(dvar1,-Inf,Inf) - integral(dvar2,-Inf,Inf).*integral(dvar3,-Inf,Inf));
end

figure(1)
semilogx(muks, obj, 'k')
xlim([0.2,8])
grid on
figure(2)
semilogx(muks(2:end), diff(obj)./diff(muks), 'k')
xlim([0.2,8])
grid on