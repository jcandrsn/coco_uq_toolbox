function [data, J] = rdo_dU(prob, data, u)

var = u(2);
J = [1, -3*(1/2)*(var).^(-1/2)];

end