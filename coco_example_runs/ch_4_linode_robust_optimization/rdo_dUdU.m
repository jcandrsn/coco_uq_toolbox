function [data, dJ] = rdo_dUdU(prob, data, u)

var = u(2);

dJ = zeros(1,2,2);
dJ(1,2,2) = 3*(1/4)*(var.^(-3/2));

end