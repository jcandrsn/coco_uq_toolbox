function [data, y] = rdo(prob, data, u)

mean = u(1);
var = u(2);

y = mean - 3*sqrt(var);

end