function J = F_dU(u,p)

% State Vector Assignment
u1 = u(1,:);        % Position
u3 = u(3,:);        % Position

% Parameter Assignment
A = p(1,:);         % Forcing function amplitude
k = p(2,:);         % Linear Stiffness
d = p(3,:);         % Linear Damping
w = p(4,:);         % Forcing function frequency
eps = p(5,:);       % Nonlinear Stiffness

% Assign Jacobian Terms
J = zeros(3,3,numel(u1));

J(1,2,:) = 1;
J(2,1,:) = -k - 3*eps.*u1.^2;
J(2,2,:) = -d;
J(2,3,:) = -A.*w.*sin(w.*u3);

end