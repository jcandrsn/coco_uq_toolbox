function J = F_dP(u,p)

% State Vector Assignment
u1 = u(1,:);        % Position
u2 = u(2,:);        % Velocity
u3 = u(3,:);        % time

A = p(1,:);
w = p(4,:);

J = zeros(3,5,numel(u1));
J(2,1,:) = cos(w.*u3);
J(2,2,:) = -u1;
J(2,3,:) = -u2;
J(2,4,:) = -A.*u3.*sin(w.*u3);
J(2,5,:) = -u1.^3;

end