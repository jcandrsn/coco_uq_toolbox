function data = per_bc_update( data, T, u0, u1, p )

n = numel(u0);
q = numel(p);

data.u0 = u0;
data.f0 = data.fhan(u0,p)';
data.J  = [sparse(n,1), speye(n,n), -speye(n,n), sparse(n,q);
           sparse(1,1), data.f0,    sparse(1,n), sparse(1,q)];

end

