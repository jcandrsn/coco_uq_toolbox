function [data, y] = bc(prob, data, u)

x0 = u(1:3);
x1 = u(4:6);

y = [x1(1:2)-x0(1:2); x1(3)-x0(3)-2*pi; x0(2)]; % periodicity in R2 x S1 on Poincare section

end

