% Dynamics and Jacobians
funcs = {@F, @F_dU, @F_dP};

% Boundary Conditions
bcs = {};

% Start with a simple case of an undamped linear oscillators
pnames = {'A','k','d','w','eps'};
p0 = [1; 1; 0; 1; 0];

% Initial Solution
t0 = linspace(0,2*pi,100)';
u0 = [cos(t0), -sin(t0), t0];

%% Continue linear stiffness to k = 1.5
run_name = 'klin';
k_des = 1.5;

prob = coco_prob();

prob = ode_isol2coll(prob, '', funcs{:}, t0, u0, pnames, p0);

[data, uidx] = coco_get_func_data(prob, 'coll', 'data', 'uidx');
maps = data.coll_seg.maps;
prob = coco_add_func(prob, 'po', @bc, @bc_dU, data, 'zero', ...
  'uidx', uidx([maps.x0_idx; maps.x1_idx]));

prob = coco_add_pars(prob, 'end_points', ...
   [maps.x0_idx; maps.x1_idx; maps.T_idx],...
   {'y1s', 'y2s', 'y3s', 'y1e', 'y2e', 'y3e', 'T'}, 'active');

% prob = coco_set(prob, 'cont', 'NAdapt', 10, 'PtMX', 200);
prob = coco_add_event(prob, upper(run_name), 'k', k_des);

bd = coco(prob, run_name, [], 1, {'k'}, [1.5, 3]);
%% Continue damping coefficient to d = 1
last_run_name = run_name;
run_name = 'dmp';
d_des = 1;

lab = coco_bd_labs(bd, upper(last_run_name));
prob = coco_prob();

prob = ode_coll2coll(prob, '', last_run_name, lab);
[data, uidx] = coco_get_func_data(prob, 'coll', 'data', 'uidx');
maps = data.coll_seg.maps;
prob = coco_add_func(prob, 'po', @bc, @bc_dU, data, 'zero', ...
  'uidx', uidx([maps.x0_idx; maps.x1_idx]));

prob = coco_add_pars(prob, 'end_points', ...
   [maps.x0_idx; maps.x1_idx; maps.T_idx],...
   {'y1s', 'y2s', 'y3s', 'y1e', 'y2e', 'y3e', 'T'}, 'active');

% prob = coco_set(prob, 'cont', 'NAdapt', 10, 'PtMX', 200);
prob = coco_add_event(prob, upper(run_name), 'd', d_des);

bd = coco(prob, run_name, [], 1, {'d'}, [0, 2]);

%% Perform frequency sweep
last_run_name = run_name;
run_name = 'swp';

lab = coco_bd_labs(bd, upper(last_run_name));
prob = coco_prob();

prob = ode_coll2coll(prob, '', last_run_name, lab);
[data, uidx] = coco_get_func_data(prob, 'coll', 'data', 'uidx');
maps = data.coll_seg.maps;
prob = coco_add_func(prob, 'po', @bc, @bc_dU, data, 'zero', ...
  'uidx', uidx([maps.x0_idx; maps.x1_idx]));

prob = coco_add_pars(prob, 'end_points', ...
   [maps.x0_idx; maps.x1_idx; maps.T_idx],...
   {'y1s', 'y2s', 'y3s', 'y1e', 'y2e', 'y3e', 'T'}, 'active');

% Mark the Resonance Peak
coco_add_event(prob, 'RES', 'w', k_des);

% prob = coco_set(prob, 'cont', 'NAdapt', 1, 'PtMX', 0);
bd = coco(prob, run_name, [], 1, {'w'}, [0, 3]);

%% Plotting Functions
run = 'dmp';
bd1 = coco_bd_read(run);
labs = coco_bd_labs(bd1);

figure(1); clf; hold on; grid on; box on;
for lab=labs(1,end)
    sol = coll_read_solution('',run,lab);
    figure(1);
    xlabel('x')
    ylabel('x-dot')
    plot(sol.xbp(:,1), sol.xbp(:,2), 'LineStyle', '-', ...
        'Marker', '.', 'MarkerSize', 6)
end

% Check against steady state solution
figure(2); clf; hold on; grid on; box on;
lab = coco_bd_labs(bd1, upper(run));
sol = coll_read_solution('', run, lab);
t = sol.xbp(:,3);
plot(t, sol.xbp(:,1), 'LineStyle', '-', ...
        'Marker', '.', 'MarkerSize', 6)
ss_sol = steady_state_solution(t, sol.p);
plot(t, ss_sol, 'Marker', 'o', 'MarkerSize', 6)

% Plot Frequency Sweep
figure(3); clf; hold on; grid on; box on;

run = 'swp';
bd1 = coco_bd_read(run);
labs = coco_bd_labs(bd1);
w = [];
x_max = [];
for lab=labs
    sol = coll_read_solution('',run,lab);
    w = [w, sol.p(4)];
    x_max = [x_max, sol.xbp(1,1)];
end

figure(3);
xlabel('w')
ylabel('x-max')
plot(w, x_max, 'Marker', 'x', 'MarkerSize', 6)