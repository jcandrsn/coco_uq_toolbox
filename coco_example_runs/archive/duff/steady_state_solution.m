function y = steady_state_solution(t,p)
% Steady State Solution to Harmonically Forced Damped Linear Oscillator 

A = p(1);
k = p(2);
d = p(3);
w = p(4);

y = A/(sqrt((w^2 - k)^2 + (d*w)^2))*cos(w*t - atan(-d*w/(w^2 - k)));

end

