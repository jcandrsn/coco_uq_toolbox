function y = F(u,p)

% State Vector Assignment
u1 = u(1,:);        % Position
u2 = u(2,:);        % Velocity
u3 = u(3,:);        % time

% Parameter Assignment
A = p(1,:);         % Forcing function amplitude
k = p(2,:);         % Linear Stiffness
d = p(3,:);         % Linear Damping
w = p(4,:);         % Forcing function frequency
eps = p(5,:);       % Nonlinear Stiffness

y(1,:) = u2;
y(2,:) = A.*cos(w.*u3) - d.*u2-k.*u1 - eps.*u1.^3;
y(3,:) = 1;

end