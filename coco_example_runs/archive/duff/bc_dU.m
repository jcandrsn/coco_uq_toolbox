function [data, J] = bc_dU(prob, data, u)


n = max(size(u))/2;

u0 = u(1:n);
u1 = u(n+1:2*n);

J = [-speye(n,n),                speye(n,n);
     sparse([0, 1, zeros(n-2)]), sparse(1,n)];

end

