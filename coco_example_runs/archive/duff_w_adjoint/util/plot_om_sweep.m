function plot_om_sweep(run_name)

bd = coco_bd_read(run_name);
oms = coco_bd_col(bd, 'om');
xs = coco_bd_col(bd, 'xmax');

figure(3); hold on; grid on; box on;
plot(oms, abs(xs))

end