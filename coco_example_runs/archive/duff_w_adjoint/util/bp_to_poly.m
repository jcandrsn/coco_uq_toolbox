function y = bp_to_poly(tbp, xbp, N)
% Takes in a set of basis points, function values at those basis points, 
% and the number of intervals and constructs a piecewise polynomial
% interpolant of the function
ws = warning('off', 'all');
cond1 = size(tbp, 1) == 1 || size(tbp, 2) == 1;
cond2 = size(xbp, 1) == 1 || size(xbp, 2) == 1;
assert(cond1 && cond2, ...
       'Inputs tbp and xbp must be either a column vector or row vector')

% Force input vectors into column vector format:
if size(tbp, 1) == 1
    tbp = transpose(tbp);
end

if size(xbp, 1) == 1
    xbp = transpose(xbp);
end

% Two cases:
% Case 0: tbp and xbp contain repeated values at the element boundaries
% otherwise: tbp and xbp don't contain repated values at element boundaries
switch mod(numel(tbp),N)
    case 0
        % Polynomial Order
        m = (numel(tbp)/N)-1;
        
        % Generate break point locations for piecewise polynomial
        breaks = [1, (m+1):(m+1):numel(tbp)];
        breaks = tbp(breaks);
        
        % Reshape matrices for generation of coefficient matrices below
        tbp = transpose(reshape(tbp, [], N));
        xbp = transpose(reshape(xbp, [], N));
    otherwise
        % Polynomial order
        m = (numel(tbp) - 1)/N;

        % Indices of repeated elements (also the internal break points for the 
        % piecewise polynomial)
        ri = (m+1):m:(numel(tbp)-1);
        r = ones(numel(tbp), 1);
        r(ri) = 2;

        % Create breaks vector from the repeated indices vector, ri, and the
        % endpoint indices
        breaks = [1, ri, numel(tbp)];
        breaks = tbp(breaks);

        % Repeat specified elements and reshape matrices
        tbp = repelem(tbp, r);
        tbp = transpose(reshape(tbp, [], N));

        xbp = repelem(xbp, r);
        xbp = transpose(reshape(xbp, [], N));
end

% Initialize coeffienct matrix
coefs = zeros(N, m+1);

for i=1:N
   % Coefficients for Matlab's piecewise polynomials functions are local, 
   % so each interval must start at zero, which is why tbp(i,1) is 
   % subtracted off from tbp(i,:) for each interval.
   coefs(i,:) = polyfit(tbp(i,:)-tbp(i,1), xbp(i,:), m);
end

y = mkpp(breaks, coefs);
warning(ws)
end