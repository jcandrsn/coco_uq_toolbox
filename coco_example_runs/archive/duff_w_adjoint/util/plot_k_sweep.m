function plot_k_sweep(run_name)

bd = coco_bd_read(run_name);
ks = coco_bd_col(bd, 'k');
xs = coco_bd_col(bd, 'xmax');

figure(3); hold on; grid on; box on;
plot(ks, xs)


end