function [data, J] = duff_bc_dUdU(prob, data, u)

x0 = u(1:3);
x1 = u(4:6);
om = u(7);

ddw = -4*pi / om^3;
J = zeros(4,7,7);
J(3,7,7) = ddw;

end
