last_run_name = 'init';
lab_name = 'EP';
coll_name = 'duff';
lab_number = -1;

% Collect the problem instance from the last run
labs = coco_bd_labs(bd, lab_name);
if lab_number < 0
    num_labs = size(labs,2);
    lab_number = num_labs + lab_number + 1;
    lab = labs(lab_number);
else
    lab = labs(lab_number);
end
sol = coll_read_solution(coll_name, last_run_name, lab);
t0 = sol.tbp;
x0 = sol.xbp;
p0 = sol.p;

prob = coco_prob();
prob = duff_settings(prob, 30);

% zero problems
coll_args = {@duff, @duff_DX, @duff_DP, @duff_DXDX,...
    @duff_DXDP, @duff_DPDP, t0, x0, {'k', 'A', 'om', 'b', 'eps'}, p0};
prob = ode_isol2coll(prob, 'duff', coll_args{:});
[data, uidx] = coco_get_func_data(prob, 'duff.coll', 'data', 'uidx');
maps = data.coll_seg.maps;
prob = coco_add_func(prob, 'bc', @duff_bc, @duff_bc_dU, ...
  @duff_bc_dUdU, data, 'zero', 'uidx', ...
  uidx([maps.x0_idx; maps.x1_idx; maps.p_idx(3)]));
prob = coco_add_pars(prob, 'force', uidx(maps.p_idx(2)), 'f');

% adjoints
prob = adjt_isol2coll(prob, 'duff');
[data, axidx] = coco_get_adjt_data(prob, 'duff.coll', 'data', 'axidx');
opt = data.coll_opt;
prob = coco_add_adjt(prob, 'bc', 'aidx', ...
  axidx([opt.x0_idx; opt.x1_idx; opt.p_idx(3)]));
prob = coco_add_adjt(prob, 'force', 'd.f', 'aidx', axidx(opt.p_idx(2)));

cont_pars = {'f', 'd.f', 'd.duff.coll.T0', 'd.b', 'd.eps', 'd.om', ...
             'd.k', 'A', 'd.A', 'b', 'eps', 'om'};
coco(prob, 'duffing1', [], 1, cont_pars, [-2 0.1]);