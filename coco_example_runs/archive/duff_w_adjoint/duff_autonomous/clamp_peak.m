function bd = clamp_peak(sweep_name, run_name)

prob = coco_prob();
prob = duff_settings(prob);

% Collect orbits from frequency sweep run
bd = coco_bd_read(sweep_name);
labs = coco_bd_labs(bd, 'W1');
i = 4;
for lab=labs
    pname = strcat('P', int2str(i));
    sol = coll_read_solution('duff', sweep_name, lab);
    t0{i} = sol.tbp;
    x0{i} = sol.xbp;
    p0{i} = sol.p;
    coll_args = {@duff, @duff_DX, @duff_DP, t0{i}, x0{i}, p0{i}};

    prob = ode_isol2coll(prob, pname, coll_args{:});

    [data{i}, uidx{i}] = coco_get_func_data(prob, strcat(pname,'.coll'), ...
                                            'data', 'uidx');
    maps{i} = data{i}.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, strcat(pname,'.po'), @duff_bc, ...
                         @duff_bc_dU, data, 'zero', 'uidx', ...
                         uidx{i}([maps{i}.x0_idx; maps{i}.x1_idx; ...
                                  maps{i}.p_idx(3)]));
    if i > 4
        p1idx = uidx{i-1}([maps{i-1}.p_idx([1,2,4,5])]);
        p2idx = uidx{i}([maps{i}.p_idx([1,2,4,5])]);

        Gi = strcat('glue', int2str(i));
        prob = coco_add_glue(prob, Gi, p1idx, p2idx);
    end
    i = i + 1;
end

p_idx = [uidx{4}(maps{4}.p_idx); uidx{5}(maps{5}.p_idx(3))];
prob = coco_add_pars(prob, 'oms', p_idx, ...
                    {'k', 'A', 'om', 'b', 'eps', 'om2'}, 'inactive');
prob = coco_add_func(prob, 'peak', @udiff, @udiff_dU, data, 'inactive', ...
                     'peak', 'xidx', ...
                     [uidx{4}(maps{4}.x0_idx(1)), ...
                      uidx{5}(maps{5}.x0_idx(1))]);

prob = coco_add_func(prob, 'peakf', @udiff, @udiff_dU, data, 'active', ...
                     'peakf', 'xidx', ...
                     [uidx{5}(maps{5}.p_idx(3)), ...
                      uidx{4}(maps{4}.p_idx(3))]);
[~, high] = udiff([],[],[p0{5}(3), p0{4}(3)]);

low = 1e-3;
prob = coco_add_event(prob, 'UZ', 'peakf', low*1.01);

bd = coco(prob, run_name, [], 1, {'peakf', 'om', 'om2'}, [low, high*1.01]);
end