function plot_results(run_name, curve_names, lab_names)


bd = coco_bd_read(run_name);
lab_names = cellstr(lab_names);
labs = [];
for lab_name=lab_names
    labs = [labs, coco_bd_labs(bd, lab_name)];
end

figure(1); clf; hold on; grid on; box on;
figure(2); clf; hold on; grid on; box on;
curve_names = cellstr(curve_names);
l =[];
for i = 1:size(curve_names, 2)
    for lab = labs
        sol = coll_read_solution(curve_names{i}, run_name, lab);
        figure(1)
        xlabel('$t$', 'Interpreter', 'latex')
        ylabel('$x$', 'Interpreter', 'latex')
        l = [l; strcat(curve_names{i}, '-', num2str(lab))];
        plot(sol.tbp/sol.T, sol.xbp(:,1), 'LineStyle', '-', 'Marker', '.', ...
            'MarkerSize', 6)
        figure(2)
        xlabel('$\tau$', 'Interpreter', 'latex')
        ylabel('$\dot{x}$', 'Interpreter', 'latex')
        plot(sol.tbp/sol.T, sol.xbp(:,2), 'LineStyle', '-', 'Marker', '.', ...
            'MarkerSize', 6)
    end
end
figure(1)
l = cellstr(l);
legend(l{:})
end

