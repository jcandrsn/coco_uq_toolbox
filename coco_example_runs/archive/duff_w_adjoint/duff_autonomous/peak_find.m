function bd = peak_find(last_run_name, run_name)

prob = coco_prob();
prob = duff_settings(prob, 20);

bd = coco_bd_read(last_run_name);

labs = coco_bd_labs(bd, 'W2');
i = 1;
for lab = labs;
    pname = strcat('P', int2str(i));
    sol = coll_read_solution('duff', last_run_name, lab);
    t0 = sol.tbp;
    x0 = sol.xbp;
    p0{i} = sol.p;
    coll_args = {@duff, @duff_DX, @duff_DP, ...
                 t0, x0, p0{i}};

    prob = ode_isol2coll(prob, pname, coll_args{:});

    [data{i}, uidx{i}] = coco_get_func_data(prob, strcat(pname,'.coll'), ...
                        'data', 'uidx');
    maps{i} = data{i}.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, strcat(pname,'.po'), @duff_bc, ...
                         @duff_bc_dU, data, 'zero', 'uidx', ...
                         uidx{i}([maps{i}.x0_idx; maps{i}.x1_idx; ...
                                  maps{i}.p_idx(3)]));
    if i > 1
        % The frequences are not the same for the separate fold points,
        % so don't glue them together (om = parameter 3)
        x1idx = uidx{i-1}([maps{i-1}.p_idx([1:2,4:end])]);
        x2idx = uidx{i}([maps{i}.p_idx([1:2,4:end])]);
        prob = coco_add_glue(prob, 'glue', x1idx, x2idx);
    end    
    i = i + 1;
end

p_idx = [uidx{1}(maps{1}.p_idx); uidx{2}(maps{2}.p_idx(3))];
prob = coco_add_pars(prob, 'pars', p_idx, ...
                     {'k', 'A', 'om1', 'b', 'eps', 'om2'}, 'inactive');
prob = coco_add_func(prob, 'peak', @udiff, @udiff_dU, data, 'inactive', ...
                     'peak', 'xidx', [uidx{2}(maps{2}.p_idx(3)), ...
                      uidx{1}(maps{1}.p_idx(3))]);
low = 1e-3;
[~, high] = udiff([],[],[p0{1}(3), p0{2}(3)]);
bd = coco(prob, run_name, [], 1, {'peak', 'om1', 'om2'}, [low, high+0.1]);

end