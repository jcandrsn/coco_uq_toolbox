function [data, J] = duff_bc_dU(prob, data, u)

x0 = u(1:3);
x1 = u(4:6);
om = u(7);

dw = 2*pi / om^2;
%         x0     |    x1    | om |
J = [-1,  0,  0,  1,  0,  0,   0;
      0, -1,  0,  0,  1,  0,   0;
      0,  0, -1,  0,  0,  1,  dw;
      0,  1,  0,  0,  0,  0,   0];


end
