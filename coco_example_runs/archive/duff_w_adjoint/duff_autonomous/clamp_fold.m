function bd = clamp_fold(bd, last_run_name, run_name)
%CLAMP_FOLD Summary of this function goes here
%   Detailed explanation goes here

bd = coco_bd_read(last_run_name);
w1 = coco_bd_labs(bd, 'W1');

prob = coco_prob();
prob = duff_settings(prob);

for i = 1:size(w1,2)
    sol = coll_read_solution('duff', last_run_name, w1(i));
    t0{i} = sol.tbp;
    x0{i} = sol.xbp;
    p0{i} = sol.p;
    coll_args = {@duff, @duff_DX, @duff_DP, t0{i}, x0{i}, p0{i}};
    pname = strcat('P',int2str(i));
    prob = ode_isol2coll(prob, pname, coll_args{:});

    [data{i}, uidx{i}] = coco_get_func_data(prob, strcat(pname,'.coll'), ...
                                            'data', 'uidx');
    maps{i} = data{i}.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, strcat(pname,'.po'), @duff_bc, ...
                         @duff_bc_dU, data, 'zero', 'uidx', ...
                         uidx{i}([maps{i}.x0_idx; maps{i}.x1_idx; ...
                                  maps{i}.p_idx(3)]));
    if i > 1
        p1idx = uidx{i-1}([maps{i-1}.p_idx]);
        p2idx = uidx{i}([maps{i}.p_idx]);

        Gi = strcat('glue', int2str(i));
        prob = coco_add_glue(prob, Gi, p1idx, p2idx);
    end
end
p_idx = uidx{1}(maps{1}.p_idx);
prob = coco_add_pars(prob, 'pars', p_idx, ...
                    {'k', 'A', 'om', 'b', 'eps'}, 'inactive');

low = 1e-3;
switch run_name
    case 'right'
        prob = coco_add_func(prob, 'right', @udiff, @udiff_dU, data, ...
                             'active', 'right', 'xidx', ...
                             [uidx{1}(maps{1}.x0_idx(1)), ...
                              uidx{2}(maps{2}.x0_idx(1))]);
        prob = coco_add_event(prob, 'UZ', 'right', low);
        [~, high] = udiff([],[],[x0{1}(1), x0{2}(1)]);
    case 'left'
        prob = coco_add_func(prob, 'left', @udiff, @udiff_dU, data, ...
                             'active', 'left', 'xidx', ...
                             [uidx{2}(maps{2}.x0_idx(1)), ...
                              uidx{3}(maps{3}.x0_idx(1))]);        
        prob = coco_add_event(prob, 'UZ', 'left', low);
        [~, high] = udiff([],[],[x0{2}(1), x0{3}(1)]);
end
bd = coco(prob, run_name, [], 1, {run_name, 'A', 'om'}, [low, high+0.1]);
end

