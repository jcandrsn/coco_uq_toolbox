function J = duff_DXDP(x, p)

x1 = x(1,:);
x3 = x(3,:);
k = p(1,:);
A = p(2,:);
b = p(4,:);
eps = p(5,:);

J = zeros(3,3,5,numel(x1));
J(2,1,1,:) = -1;
J(2,1,5,:) = -3*x1.^2;
J(2,2,4,:) = -1;
J(2,3,2,:) = -sin(x3);

end
