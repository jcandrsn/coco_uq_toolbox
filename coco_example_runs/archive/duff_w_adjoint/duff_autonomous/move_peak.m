function bd = move_peak(clamp_fold_run_name, clamp_peak_run_name, run_name)
bd = coco_bd_read(clamp_fold_run_name);
lab = coco_bd_labs(bd, 'UZ');

prob = coco_prob();
prob = duff_settings(prob, 30, 4);

for i = 1:3
    pname = strcat('P',int2str(i));
    sol = coll_read_solution(pname, clamp_fold_run_name, lab);
    t0{i} = sol.tbp;
    x0{i} = sol.xbp;
    p0{i} = sol.p;
    coll_args = {@duff, @duff_DX, @duff_DP, t0{i}, x0{i}, p0{i}};

    prob = ode_isol2coll(prob, pname, coll_args{:});

    [data{i}, uidx{i}] = coco_get_func_data(prob, strcat(pname,'.coll'), ...
                                            'data', 'uidx');
    maps{i} = data{i}.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, strcat(pname,'.po'), @duff_bc, ...
                         @duff_bc_dU, data, 'zero', 'uidx', ...
                         uidx{i}([maps{i}.x0_idx; maps{i}.x1_idx; ...
                                  maps{i}.p_idx(3)]));
    if i > 1
        p1idx = uidx{i-1}([maps{i-1}.p_idx]);
        p2idx = uidx{i}([maps{i}.p_idx]);

        Gi = strcat('glue', int2str(i));
        prob = coco_add_glue(prob, Gi, p1idx, p2idx);
    end
end
p_idx = uidx{1}(maps{1}.p_idx);
prob = coco_add_pars(prob, 'pars', p_idx, ...
                    {'k', 'A', 'om', 'b', 'eps'}, 'inactive');
                
prob = coco_add_func(prob, 'right', @udiff, @udiff_dU, data, 'inactive', ...
                     'right', 'xidx', ...
                     [uidx{1}(maps{1}.x0_idx(1)), ...
                      uidx{2}(maps{2}.x0_idx(1))]);

prob = coco_add_func(prob, 'left', @udiff, @udiff_dU, data, 'inactive', ...
                     'left', 'xidx', ...
                     [uidx{2}(maps{2}.x0_idx(1)), ...
                      uidx{3}(maps{3}.x0_idx(1))]);

% Collect orbits from frequency sweep run
bd = coco_bd_read(clamp_peak_run_name);
lab = coco_bd_labs(bd, 'UZ');
for i=4:5
    pname = strcat('P', int2str(i));
    sol = coll_read_solution(pname, clamp_peak_run_name, lab);
    t0{i} = sol.tbp;
    x0{i} = sol.xbp;
    p0{i} = sol.p;
    coll_args = {@duff, @duff_DX, @duff_DP, t0{i}, x0{i}, p0{i}};

    prob = ode_isol2coll(prob, pname, coll_args{:});

    [data{i}, uidx{i}] = coco_get_func_data(prob, strcat(pname,'.coll'), ...
                                            'data', 'uidx');
    maps{i} = data{i}.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, strcat(pname,'.po'), @duff_bc, ...
                         @duff_bc_dU, data, 'zero', 'uidx', ...
                         uidx{i}([maps{i}.x0_idx; maps{i}.x1_idx; ...
                                  maps{i}.p_idx(3)]));
    if i > 1
        p1idx = uidx{i-1}([maps{i-1}.p_idx([1,2,4,5])]);
        p2idx = uidx{i}([maps{i}.p_idx([1,2,4,5])]);

        Gi = strcat('glue', int2str(i));
        prob = coco_add_glue(prob, Gi, p1idx, p2idx);
    end
end

p_idx = [uidx{4}(maps{4}.p_idx(3)); uidx{5}(maps{5}.p_idx(3))];
prob = coco_add_pars(prob, 'oms', p_idx, ...
                    {'om4', 'om5'}, 'inactive');
prob = coco_add_func(prob, 'dpeak', @udiff, @udiff_dU, data, 'inactive', ...
                     'd.peak', 'xidx', ...
                     [uidx{4}(maps{4}.x0_idx(1)), ...
                      uidx{5}(maps{5}.x0_idx(1))]);

prob = coco_add_pars(prob, 'x0', uidx{4}(maps{4}.x0_idx(1:2)), {'xmax', 'x20'}, 'active');

prob = coco_add_func(prob, 'dpeakf', @udiff, @udiff_dU, data, 'inactive', ...
                     'd.peakf', 'xidx', ...
                     [uidx{5}(maps{5}.p_idx(3)), ...
                      uidx{4}(maps{4}.p_idx(3))]);
% [~, high] = udiff([],[],[p0{5}(3), p0{4}(3)]);
% 
% low = 1e-3;
% prob = coco_add_event(prob, 'UZ', 'peakf', low*1.01);
% prob = coco_add_event(prob, 'UZ', run_name, low);

% [~, high] = udiff([],[],[x0{2}(1), x0{3}(1)]);

bd = coco(prob, run_name, [], 1, {'xmax', 'k', 'om', 'A', 'om4', 'om5', 'left', 'right'});
    
end