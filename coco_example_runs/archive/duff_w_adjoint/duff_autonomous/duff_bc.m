function [data, y] = duff_bc(prob, data, u)

x0 = u(1:3);
x1 = u(4:6);
om = u(7);

y = [x1(1) - x0(1);             % Periodic in position
     x1(2) - x0(2);             % Periodic in velocity
     x1(3) - x0(3) - 2*pi/om;   % Periodic in time
     x0(2)];                    % Max position at t = 0

end
