function y = duff(x, p)

x1 = x(1,:);
x2 = x(2,:);
x3 = x(3,:);
k = p(1,:);
A = p(2,:);
om = p(3,:);
b = p(4,:);
eps = p(5,:);

y(1,:) = x2;
y(2,:) = A.*cos(x3)-b.*x2-k.*x1-eps.*x1.^3;
y(3,:) = om;
end
