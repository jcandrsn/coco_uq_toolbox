function bd = amp_up(bd, last_run_name, run_name, coll_name, ...
                              lab_name, lab_number, range, first_mark, ...
                              second_mark, par_to_mark)
    % Collect the problem instance from the last run
    labs = coco_bd_labs(bd, lab_name);
    if lab_number < 0
        num_labs = size(labs,2);
        lab_number = num_labs + lab_number + 1;
        lab = labs(lab_number);
    else
        lab = labs(lab_number);
    end

    sol = coll_read_solution(coll_name, last_run_name, lab);
    t0 = sol.tbp - sol.tbp(1);
    x0 = sol.xbp;
    p0 = sol.p;

    prob = coco_prob();
    prob = duff_settings(prob);

    coll_args = {@duff, @duff_DX, @duff_DP, ...
                 t0, x0, {'k', 'A', 'om', 'b', 'eps'}, p0};
    prob = ode_isol2coll(prob, 'duff', coll_args{:});

    [data, uidx] = coco_get_func_data(prob, 'duff.coll', 'data', 'uidx');
    maps = data.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, 'duff.po', @duff_bc, ...
                         @duff_bc_dU, data, 'zero', 'uidx', ...
                         uidx([maps.x0_idx; maps.x1_idx; maps.p_idx(3)]));

    % Add the initial velocity as an active continuation parameter
    prob = coco_add_pars(prob, 'x20', maps.x0_idx(1:2), {'xmax', 'x20'}, 'active');
    % Mark points near both fold points
    prob = coco_add_event(prob, 'W1', par_to_mark, first_mark);
    prob = coco_add_event(prob, 'W2', par_to_mark, second_mark);

    bd = coco(prob, run_name, [], 1, {'xmax', 'om', 'A', 'k', 'x20'}, [0.2, 10]);
end