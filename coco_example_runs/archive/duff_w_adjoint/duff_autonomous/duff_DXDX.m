function J = duff_DXDX(x, p) 

x1 = x(1,:);
x3 = x(3,:);
A = p(2,:);
eps = p(5,:);

J = zeros(3,3,3,numel(x1));
J(2,1,1,:) = -6*eps.*x1;
J(2,3,3,:) = -A.*cos(x3);

end
