clear all;
%%
run_name = 'init';
if not(coco_exist(run_name, 'run'))
    fprintf('Increase k to 1.0\n')
    bd = initial_run(run_name);
else
    bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'swp';

if not(coco_exist(run_name, 'run'))
    fprintf('Perform a frequency sweep and mark orbits on either side of the fold points\n')
    bd = frequency_sweep(bd, last_run_name, run_name, 'duff', 'EP', -1, [0.9, 3.5], 1.15, -1, 'om');
else
    bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'right';

if not(coco_exist(run_name, 'run'))
    fprintf('test\n')
    bd_right = three_pt_locate_fold(bd, last_run_name, run_name);
else
    bd_right = coco_bd_read(run_name);
end

% run_name = 'left';
% 
% if not(coco_exist(run_name, 'run'))
%     fprintf('Dial in location of Left Fold point')
%     bd_left = locate_fold(bd, last_run_name, run_name);
% else
%     bd_left = coco_bd_read(run_name);
% end
% 
% % Result of 'left' and 'right' runs is a set of orbits on either side of
% % the two fold points in the frequency response.  Next step is to adjust 
% % the horizontal distance between the two fold points
% 
% % To that end, collect P1, P2 from right, P3, P4 from left, combine them
% % into a single problem structure and apply udiff to 'om1' and 'om2'
% run_name = 'hdiff';
% 
% if not(coco_exist(run_name, 'run'))
%     fprintf('Drive Left and Right fold points together by modifying \nForcing Amplitude A')
%     bd = hdiff(bd_right, bd_left, run_name);
% else
%     bd = coco_bd_read(run_name);
% end
% last_run_name = 'opt.fold';% run_name;
% run_name = 'swp2';
% 
% if not(coco_exist(run_name, 'run'))
%     fprintf('Perform a second frequency sweep with the new stiffness')
%     bd = coco_bd_read('opt.fold');
%     bd = frequency_sweep(bd, last_run_name, run_name, 'P1', 'MX', -1, [0, 3.5], -1, 3.4, 'xmax');
% else
%     bd = coco_bd_read(run_name);
% end

%% Optimization run
% Use the adjoint library to construct a problem that will increase the
% dynamic range of the duffing oscillator

last_run_name = 'hdiff';
run_name = 'opt.fold';
if false
    % This sequence of commands actually produces a continuation run, so 
    % I'm saving it, but the underlying problem isn't formulated well, and
    % it does not result in finding a fold point.
    % Collect problem instance after horizontal difference has been driven 
    % to near zero.

    bd = coco_bd_read(last_run_name);

    labs = coco_bd_labs(bd, 'EP');
    lab = labs(1);
    % All the points are nearly equal at this point, so grabbing P1
    sol = coll_read_solution('P1', last_run_name, lab);
    t0 = sol.tbp;
    x0 = sol.xbp;
    p0 = sol.p;

    prob = coco_prob();
    prob = duff_settings(prob, 20);

    coll_args = {@duff, @duff_DX, @duff_DP ...
                 @duff_DXDX, @duff_DXDP, @duff_DPDP, ...
                 t0, x0, {'k', 'A', 'om', 'b', 'eps'}, p0};

    prob = ode_isol2coll(prob, 'duff', coll_args{:});
    [data, uidx] = coco_get_func_data(prob, 'duff.coll', 'data', 'uidx');
    maps = data.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, 'duff.po', @duff_bc, ...
                         @duff_bc_dU, @duff_bc_dUdU, ...
                         data, 'zero', 'uidx', ...
                         uidx([maps.x0_idx; maps.x1_idx; maps.p_idx(3)]));
    % Add the initial velocity as an active continuation parameter
    prob = coco_add_pars(prob, 'x0', maps.x0_idx(1), {'x10'}, 'active');

    % Add Adjoints
    prob = adjt_isol2coll(prob, 'duff');
    [data, axidx] = coco_get_adjt_data(prob, 'duff.coll', 'data', 'axidx');
    opt = data.coll_opt;
    prob = coco_add_adjt(prob, 'duff.po', 'aidx', ...
      axidx([opt.x0_idx; opt.x1_idx; opt.p_idx(3)]));
    prob = coco_add_adjt(prob, 'x0', {'d.x10'}, 'aidx', axidx(opt.x0_idx(1)));

    cont_pars = {'x10', 'd.x10', 'd.duff.coll.T0', 'k', 'd.k', 'd.A', 'd.om', ...
                 'd.b', 'd.eps', 'A', 'om', 'b', 'eps'};
    cont_pars = {'x10', 'd.x10', 'd.duff.coll.T0', 'A', 'd.A', 'd.k', 'd.b', 'd.om', 'd.eps'};
    % % cont_pars = {'k', 'phi'};
    coco(prob, run_name, [], 1, cont_pars, [-100 100]);
end
%%
last_run_name = 'swp2';
run_name = 'peak';
if not(coco_exist(run_name, 'run'))
    fprintf('Find the resonant peak frequency')
    bd = peak_find(last_run_name, run_name);
else
    bd = coco_bd_read(run_name);
end
%% Optimization
% Find a fold in the peak
% Use P1 from the resonant peak finder
% last_run_name = 'peak';
% run_name = 'opt.fold';
% 
% prob = coco_prob();
% prob = duff_settings(prob, 30, 4, 5);
% 
% for i=1:6
%     switch i
%         case {1,2,3,4}
%             run = 'hdiff';
%             bd = coco_bd_read('hdiff');
%             labs = coco_bd_labs(bd, 'EP');
%             lab = labs(1);
%             pname = strcat('P', int2str(i));
%         case{5,6}
%             run = 'peak';
%             bd = coco_bd_read('peak');
%             labs = coco_bd_labs(bd, 'EP');
%             lab = labs(1);
%             pname = strcat('P', int2str(i-4));
%     end
%     
%     sol = coll_read_solution(pname, run, lab);
%     t0{i} = sol.tbp;
%     x0{i} = sol.xbp;
%     p0{i} = sol.p;
%     p = {'k', 'A', 'om', 'b', 'eps'};
%     parnames{i} = strcat(cellstr(p)', cellstr(num2str(i')))';
%     coll_args = {@duff, @duff_DX, @duff_DP, ...
%                  t0{i}, x0{i}, p0{i}};
%     pname = strcat('P', int2str(i));
%     prob = ode_isol2coll(prob, pname, coll_args{:});
% 
%     [data{i}, uidx{i}] = coco_get_func_data(prob, ...
%                                             strcat(pname,'.coll'), ...
%                                             'data', 'uidx');
%     maps{i} = data{i}.coll_seg.maps;
% 
%     % Add Boundary Conditions
%     prob = coco_add_func(prob, strcat(pname,'.po'), @duff_bc, ...
%                          @duff_bc_dU, data, 'zero', 'uidx', ...
%                          uidx{i}([maps{i}.x0_idx; maps{i}.x1_idx; ...
%                                   maps{i}.p_idx(3)]));
%     switch i
%         case {2,4}
%             % All parameters are the same for points on either side of a
%             % fold (except the phase angle which is unique for each orbit)
%             x1idx = uidx{i-1}([maps{i-1}.p_idx]);
%             x2idx = uidx{i}([maps{i}.p_idx]);            
%         case {3,5,6}
%             % The frequences are not the same for the separate fold points,
%             % so don't glue them together (om = parameter 3)
%             x1idx = uidx{i-1}([maps{i-1}.p_idx([1:2,4:end])]);
%             x2idx = uidx{i}([maps{i}.p_idx([1:2,4:end])]);
%     end
%     if i > 1
%         Gi = strcat('glue', int2str(i));
%         prob = coco_add_glue(prob, Gi, x1idx, x2idx);
%     end
% end
% 
% p_idx = [uidx{1}(maps{1}.p_idx); uidx{3}(maps{3}.p_idx(3)); ...
%          uidx{5}(maps{3}.p_idx(3)); uidx{6}(maps{3}.p_idx(3))];
% prob = coco_add_pars(prob, 'pars', p_idx, ...
%                      {'k', 'A', 'om1', 'b', 'eps', 'om2', 'om3', ...
%                      'om4'}, 'inactive');
% 
% prob = coco_add_func(prob, 'right', @udiff, @udiff_dU, data, 'inactive', ...
%                      'right', 'xidx', [uidx{1}(maps{1}.x0_idx(1)), ...
%                       uidx{2}(maps{2}.x0_idx(1))]);
% prob = coco_add_func(prob, 'left', @udiff, @udiff_dU, data, 'inactive', ...
%                      'left', 'xidx', [uidx{3}(maps{3}.x0_idx(1)), ...
%                       uidx{4}(maps{4}.x0_idx(1))]);
% prob = coco_add_func(prob, 'hdiff', @udiff, @udiff_dU, data, 'inactive', ...
%                      'hdiff', 'xidx', [uidx{1}(maps{1}.p_idx(3)), ...
%                       uidx{3}(maps{3}.p_idx(3))]);
% prob = coco_add_func(prob, 'peak_diff', @udiff, @udiff_dU, data, 'inactive', ...
%                      'peak_diff', 'xidx', [uidx{5}(maps{5}.p_idx(3)), ...
%                       uidx{6}(maps{6}.p_idx(3))]);
% prob = coco_add_pars(prob, 'peak', uidx{5}(maps{5}.x0_idx(1)), 'peak', 'active');
% active_pars = {'peak', 'k', 'b', 'om3', 'om1', 'om2'};
% 
% bd = coco(prob, run_name, [], 1, active_pars);


% 
% coll_args = {@duff, @duff_DX, @duff_DP ...
%              @duff_DXDX, @duff_DXDP, @duff_DPDP, ...
%              t0, x0, {'k', 'A', 'om', 'b', 'eps'}, p0};
% 
% prob = ode_isol2coll(prob, 'duff', coll_args{:});
% [data, uidx] = coco_get_func_data(prob, 'duff.coll', 'data', 'uidx');
% maps = data.coll_seg.maps;
% 
% % Add Boundary Conditions
% prob = coco_add_func(prob, 'duff.po', @duff_bc, ...
%                      @duff_bc_dU, @duff_bc_dUdU, ...
%                      data, 'zero', 'uidx', ...
%                      uidx([maps.x0_idx; maps.x1_idx; maps.p_idx(3)]));
% Add the initial velocity as an active continuation parameter
% prob = coco_add_pars(prob, 'x0', maps.x0_idx(1), {'x10'}, 'active');
% 
% % Add Adjoints
% prob = adjt_isol2coll(prob, 'duff');
% [data, axidx] = coco_get_adjt_data(prob, 'duff.coll', 'data', 'axidx');
% opt = data.coll_opt;
% prob = coco_add_adjt(prob, 'duff.po', 'aidx', ...
%   axidx([opt.x0_idx; opt.x1_idx; opt.p_idx(3)]));
% prob = coco_add_adjt(prob, 'x0', {'d.x10'}, 'aidx', axidx(opt.x0_idx(1)));
% 
% cont_pars = {'x10', 'd.x10', 'd.duff.coll.T0', 'k', 'd.k', 'd.A', 'd.om', ...
%              'd.b', 'd.eps', 'A', 'om', 'b', 'eps'};
% cont_pars = {'x10', 'd.x10', 'd.duff.coll.T0', 'A', 'd.A', 'd.k', 'd.b', 'd.om', 'd.eps'};
% % % cont_pars = {'k', 'phi'};
% coco(prob, run_name, [], 1, cont_pars, [-100 100]);

plt = true;
if plt
    figure(1)
    hold on
    bd2 = coco_bd_read('swp');
    plot(cell2mat(bd2(2:end,11)), cell2mat(bd2(2:end,16)));
    w1_labs = coco_bd_labs(bd2, 'FP');
    w2_labs = coco_bd_labs(bd2, 'W1');
    bd_right = coco_bd_read('right');
    bd_left = coco_bd_read('left');
    ep_right = coco_bd_labs(bd_right, 'EP');
    ep_left = coco_bd_labs(bd_left, 'EP');
    eps = [ep_right(end), ep_left(1)];
    for lab = [w1_labs]
        sol = coll_read_solution('duff', 'swp', lab);
        plot(sol.p(3), sol.xbp(1,1), 'o')
    end
    bd = coco_bd_read('swp2');
    plot(cell2mat(bd(2:end,11)), cell2mat(bd(2:end,16))) 
    fp_lab = coco_bd_labs(bd, 'FP');
    sol = coll_read_solution('duff', 'swp2', fp_lab);
%     plot(sol.p(3), sol.xbp(1,1), 'o')    
%     legend({'Sweep 1', 'Sweep 2'})
end

