function J = duff_DP(x, p)

x1 = x(1,:);
x2 = x(2,:);
x3 = x(3,:);
k = p(1,:);
A = p(2,:);
om = p(3,:);
b = p(4,:);
eps = p(5,:);

J = zeros(3,5,numel(x1));
J(2,1,:) = -x1;
J(2,2,:) = cos(x3);
J(2,4,:) = -x2;
J(2,5,:) = -x1.^3;
J(3,3,:) = 1;

end
