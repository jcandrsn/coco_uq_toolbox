function J = duff_DX(x, p) 

x1 = x(1,:);
x3 = x(3,:);
k = p(1,:);
A = p(2,:);
b = p(4,:);
eps = p(5,:);

J = zeros(3,3,numel(x1));
J(1,2,:) = 1;
J(2,1,:) = -k - 3*eps.*x1.^2;
J(2,2,:) = -b;
J(2,3,:) = -A.*sin(x3);

end
