function J = duff_DPDP(x, p)

x1 = x(1,:);
x2 = x(2,:);
k = p(1,:);
A = p(2,:);
om = p(3,:);
b = p(4,:);
eps = p(5,:);

J = zeros(3,5,5,numel(x1));

end
