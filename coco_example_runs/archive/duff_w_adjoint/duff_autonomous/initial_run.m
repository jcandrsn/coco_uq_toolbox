function bd = initial_run(run_name)
    % k;   A;  om;   b; eps
    % Recipes for Continuation parameter set (k is continued to 1)
    p0 = [0.8;   1;  1;   0.1; 0.04];
%     p0 = [0.8; 1; 0.1; 0.1; 0.04];
    x0 = [0,0,0];
    [t0 x0] = ode45(@(t,x) duff(x, p0), [0 100*pi/p0(3)], x0);
    [t0 x0] = ode45(@(t,x) duff(x, p0), [0 2*pi/p0(3)], x0(end,:)');
    
    prob = coco_prob();
    prob = duff_settings(prob);

    coll_args = {@duff, @duff_DX, @duff_DP, ...
                 t0, x0, {'k', 'A', 'om', 'b', 'eps'}, p0};
    prob = ode_isol2coll(prob, 'duff', coll_args{:});

    [data, uidx] = coco_get_func_data(prob, 'duff.coll', 'data', 'uidx');
    maps = data.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, 'duff.po', @duff_bc, @duff_bc_dU, ...
                         data, 'zero', 'uidx', ...
                         uidx([maps.x0_idx; maps.x1_idx; maps.p_idx(3)]));

    % Add the initial velocity as an active continuation parameter
    prob = coco_add_pars(prob, 'x20', maps.x0_idx(1:2), {'xmax', 'x20'}, 'active');

    bd = coco(prob, run_name, [], 1, {'k', 'xmax', 'x20'}, [0.8, 1]);
end