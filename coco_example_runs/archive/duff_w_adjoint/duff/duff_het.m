function y = duff_het(t, x, p)

x1 = x(1,:);
x2 = x(2,:);
k = p(1,:);
phi = p(2,:);
A = p(3,:);
om = p(4,:);
b = p(5,:);
eps = p(6,:);

y(1,:) = x2;
y(2,:) = A.*cos(om.*t + phi)-b.*x2-k.*x1-eps.*x1.^3;

end
