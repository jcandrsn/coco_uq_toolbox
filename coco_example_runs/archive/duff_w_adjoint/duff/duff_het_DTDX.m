function J = duff_het_DTDX(t, x, p)

x1 = x(1,:);
phi = p(2,:);
A = p(3,:);
om = p(4,:);

J = zeros(2,2,numel(x1));

end
