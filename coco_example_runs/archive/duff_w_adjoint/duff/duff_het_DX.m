function J = duff_het_DX(t, x, p) 

x1 = x(1,:);
k = p(1,:);
b = p(5,:);
eps = p(6,:);

J = zeros(2,2,numel(x1));
J(1,2,:) = 1;
J(2,1,:) = -k - 3*eps.*x1.^2;
J(2,2,:) = -b;

end
