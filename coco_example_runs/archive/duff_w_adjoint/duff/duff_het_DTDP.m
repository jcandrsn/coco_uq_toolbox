function J = duff_het_DTDP(t, x, p)
%LINODE_HET_DFDT   'coll'-compatible encoding of Jacobian with respect to time
%
% Encoding is of a non-autonomous vector field.

x1 = x(1,:);
phi = p(2,:);
A = p(3,:);
om = p(4,:);

J = zeros(2,6,numel(x1));
J(2,2,:) = -A.*om.*cos(om.*t + phi);
J(2,3,:) = -om.*sin(om.*t + phi);
J(2,4,:) = -A.*om.*t.*cos(om.*t + phi)-A.*sin(om.*t + phi);


end
