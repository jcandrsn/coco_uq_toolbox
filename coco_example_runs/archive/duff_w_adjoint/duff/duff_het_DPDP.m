function J = duff_het_DPDP(t, x, p)

x1 = x(1,:);
x2 = x(2,:);
k = p(1,:);
phi = p(2,:);
A = p(3,:);
om = p(4,:);
b = p(5,:);
eps = p(6,:);

J = zeros(2,6,6,numel(x1));
J(2,2,2,:) = -A.*cos(om.*t + phi);
J(2,2,3,:) = -sin(om.*t + phi);
J(2,2,4,:) = -A.*t.*cos(om.*t + phi);
J(2,3,2,:) = -sin(om.*t + phi);
J(2,3,4,:) = -t.*sin(om.*t + phi);
J(2,4,2,:) = -A.*t.*cos(om.*t + phi);
J(2,4,3,:) = -t.*sin(om.*t + phi);
J(2,4,4,:) = -A.*(t.^2).*cos(om.*t + phi);

end
