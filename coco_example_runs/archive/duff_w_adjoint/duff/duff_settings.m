function prob = duff_settings(prob, NTST, NCOL)
    switch nargin
        case 2
            NCOL = 4;
        case 1
            NTST = 60;
            NCOL = 4;
    end
    
    prob = coco_set(prob, 'ode', 'autonomous', false);
    prob = coco_set(prob, 'coll', 'NCOL', NCOL, 'NTST', NTST);

    prob = coco_set(prob, 'cont', 'PtMX', 1000, 'h', 0.5, 'almax', 30);
    
    prob = coco_set(prob, 'cont', 'h_max', 10);
    prob = coco_set(prob, 'cont', 'NAdapt', 1, 'NPR', 10);
end