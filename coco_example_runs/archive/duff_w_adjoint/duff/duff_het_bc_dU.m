function [data, J] = duff_het_bc_dU(prob, data, u) %#ok<INUSL>
%LINODE_HET_BC_DU   'bvp'-compatible encoding of Jacobian of 
%                    duff boundary conditions
%
% Encoding is of a non-autonomous vector field.

x0 = u(1:2);
x1 = u(3:4);
om = u(5);
T0 = u(6);
T  = u(7);

dw = 2*pi / om^2;
%       x0  |  x1  | om | T0 | T 
J = [-1,  0,  1,  0,  0,  0,  0;
      0, -1,  0,  1,  0,  0,  0;
      0,  0,  0,  0,  0,  1,  0;
      0,  0,  0,  0, dw,  0,  1];


y = [x1(1:2)-x0(1:2); T0; T-2*pi/om];

end
