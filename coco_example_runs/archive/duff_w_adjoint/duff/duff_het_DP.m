function J = duff_het_DP(t, x, p)

x1 = x(1,:);
x2 = x(2,:);
k = p(1,:);
phi = p(2,:);
A = p(3,:);
om = p(4,:);
b = p(5,:);
eps = p(6,:);

J = zeros(2,6,numel(x1));
J(2,1,:) = -x1;
J(2,2,:) = -A.*sin(om.*t + phi);
J(2,3,:) = cos(om.*t + phi);
J(2,4,:) = -A.*t.*sin(om.*t + phi);
J(2,5,:) = -x2;
J(2,6,:) = -x1.^3;

end
