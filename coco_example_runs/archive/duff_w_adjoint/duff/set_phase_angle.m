function bd = set_phase_angle(run_name)
    %     k, phi, A, om, b, eps
    p0 = [1; 0.1; 2.5; 1; 0.2; 1];
    x0 = [0,0];
    [t0 x0] = ode45(@(t,x) duff_het(t, x, p0), [0 100*pi], x0);
    [t0 x0] = ode45(@(t,x) duff_het(t, x, p0), [0 2*pi], x0(end,:)');

    prob = coco_prob();
    prob = duff_settings(prob);

    coll_args = {@duff_het, @duff_het_DX, @duff_het_DP, @duff_het_DT, ...
                 t0, x0, {'k', 'phi', 'A', 'om', 'b', 'eps'}, p0};
    prob = ode_isol2coll(prob, 'duff', coll_args{:});

    [data, uidx] = coco_get_func_data(prob, 'duff.coll', 'data', 'uidx');
    maps = data.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, 'duff.po', @duff_het_bc, @duff_het_bc_dU, ...
                         data, 'zero', 'uidx', ...
                         uidx([maps.x0_idx; maps.x1_idx; maps.p_idx(4);
                               maps.T0_idx; maps.T_idx]));

    % Add the initial velocity as an active continuation parameter
    prob = coco_add_pars(prob, 'x20', maps.x0_idx(2), 'x20', 'active');
    prob = coco_add_event(prob, upper(run_name), 'x20', 0);

    bd = coco(prob, run_name, [], 1, {'x20', 'phi'}, [-0.1 1]);
end