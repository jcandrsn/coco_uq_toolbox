function [data, y] = duff_het_bc(prob, data, u) %#ok<INUSL>
%LINODE_HET_BC   'bvp'-compatible encoding of duff boundary conditions
%
% Encoding is of a non-autonomous vector field.

x0 = u(1:2);
x1 = u(3:4);
om = u(5);
T0 = u(6);
T  = u(7);

y = [x1(1:2)-x0(1:2); T0; T-2*pi/om];

end
