function plot_results(run_name, curve_name)

bd = coco_bd_read(run_name);
labs = coco_bd_labs(bd);

figure(1); clf; hold on; grid on; box on;
figure(2); clf; hold on; grid on; box on;
for lab = labs
    sol = coll_read_solution(curve_name,run_name, lab);
    figure(1)
    xlabel('t')
    ylabel('x')
    plot(sol.tbp/sol.T, sol.xbp(:,1), 'LineStyle', '-', 'Marker', '.', ...
        'MarkerSize', 6)
    figure(2)
    xlabel('t')
    ylabel('x-dot')
    plot(sol.tbp/sol.T, sol.xbp(:,2), 'LineStyle', '-', 'Marker', '.', ...
        'MarkerSize', 6)
end

end

