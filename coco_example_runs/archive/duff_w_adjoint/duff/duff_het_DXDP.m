function J = duff_het_DXDP(t, x, p)

x1 = x(1,:);
k = p(1,:);
b = p(5,:);
eps = p(6,:);

J = zeros(2,2,6,numel(x1));
J(2,1,1,:) = -1;
J(2,1,6,:) = -3*x1.^2;
J(2,2,5,:) = -1;

end
