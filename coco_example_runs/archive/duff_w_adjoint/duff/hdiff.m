function bd = hdiff(bd_right, bd_left, run_name)
    prob = coco_prob();
    prob = duff_settings(prob);

    for i=1:4
        switch i
            case {1,2}
                run = 'right';
                labs = coco_bd_labs(bd_right, 'EP');
            case{3,4}
                run = 'left';
                labs = coco_bd_labs(bd_left, 'EP');
        end
        lab = labs(end);
        pname = strcat('P', int2str(i));
        sol = coll_read_solution(pname, run, lab);
        t0{i} = sol.tbp;
        x0{i} = sol.xbp;
        p0{i} = sol.p;
        coll_args = {@duff_het, @duff_het_DX, @duff_het_DP, @duff_het_DT, ...
                     t0{i}, x0{i}, p0{i}};

        prob = ode_isol2coll(prob, pname, coll_args{:});

        [data{i}, uidx{i}] = coco_get_func_data(prob, strcat(pname,'.coll'), ...
                                                'data', 'uidx');
        maps{i} = data{i}.coll_seg.maps;

        % Add Boundary Conditions
        prob = coco_add_func(prob, strcat(pname,'.po'), @duff_het_bc_v0, ...
                             @duff_het_bc_v0_dU, data, 'zero', 'uidx', ...
                             uidx{i}([maps{i}.x0_idx; maps{i}.x1_idx; ...
                                      maps{i}.p_idx(4); maps{i}.T0_idx; ...
                                      maps{i}.T_idx]));
        if i > 1
            if i ~= 3
                % All parameters are the same for points on either side of a
                % fold (except the phase angle which is unique for each orbit)
                x1idx = uidx{i-1}([maps{i-1}.p_idx([1,3:end])]);
                x2idx = uidx{i}([maps{i}.p_idx([1,3:end])]);
            else
                % The frequences are not the same for the separate fold points,
                % so don't glue them together (om = parameter 4)
                x1idx = uidx{i-1}([maps{i-1}.p_idx([1,3,5:end])]);
                x2idx = uidx{i}([maps{i}.p_idx([1,3,5:end])]);
            end
            Gi = strcat('glue', int2str(i));
            prob = coco_add_glue(prob, Gi, x1idx, x2idx);
        end    
    end

    p_idx = [uidx{1}(maps{1}.p_idx); uidx{2}(maps{2}.p_idx(2)); ...
               uidx{3}(maps{3}.p_idx([4, 2])); uidx{4}(maps{4}.p_idx(2))];
    prob = coco_add_pars(prob, 'pars', p_idx, ...
                         {'k', 'phi1', 'A', 'om1', 'b', 'eps', 'phi2', ...
                          'om2', 'phi3', 'phi4'}, 'inactive');
    prob = coco_add_func(prob, 'right', @udiff, @udiff_dU, data, 'inactive', ...
                         'right', 'xidx', [uidx{1}(maps{1}.x0_idx(1)), ...
                          uidx{2}(maps{2}.x0_idx(1))]);
    prob = coco_add_func(prob, 'left', @udiff, @udiff_dU, data, 'inactive', ...
                         'left', 'xidx', [uidx{3}(maps{3}.x0_idx(1)), ...
                          uidx{4}(maps{4}.x0_idx(1))]);
    prob = coco_add_func(prob, 'hdiff', @udiff, @udiff_dU, data, 'inactive', ...
                         'hdiff', 'xidx', [uidx{1}(maps{1}.p_idx(4)), ...
                          uidx{3}(maps{3}.p_idx(4))]);
    low = 1e-3;
    [~, high] = udiff([],[],[p0{1}(4), p0{3}(4)]);

    bd = coco(prob, run_name, [], 1, {'hdiff', 'k', 'om1', 'om2', 'phi1', 'phi2', 'phi3', 'phi4'}, [low, high]);
end