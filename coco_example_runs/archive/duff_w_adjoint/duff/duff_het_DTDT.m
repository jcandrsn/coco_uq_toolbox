function J = duff_het_DTDT(t, x, p)

x1 = x(1,:);
phi = p(2,:);
A = p(3,:);
om = p(4,:);

J = zeros(2,numel(x1));
J(2,:) = -A.*(om.^2).*cos(om.*t + phi);

end
