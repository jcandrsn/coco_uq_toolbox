function J = duff_het_DXDX(t, x, p) 

x1 = x(1,:);
eps = p(6,:);

J = zeros(2,2,2,numel(x1));
J(2,1,1,:) = -6*eps.*x1;

end
