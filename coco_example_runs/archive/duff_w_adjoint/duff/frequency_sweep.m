function bd = frequency_sweep(bd, last_run_name, run_name, coll_name, lab_name, range)
    % Collect the problem instance from the last run that satisfies the
    % condition on v0 and reinitialize an instance of coll.
    labs = coco_bd_labs(bd, lab_name);
    lab = labs(1);
    sol = coll_read_solution(coll_name, last_run_name, lab);
    t0 = sol.tbp;
    x0 = sol.xbp;
    p0 = sol.p;

    prob = coco_prob();
    prob = duff_settings(prob);

    coll_args = {@duff_het, @duff_het_DX, @duff_het_DP, @duff_het_DT, ...
                 t0, x0, {'k', 'phi', 'A', 'om', 'b', 'eps'}, p0};
    prob = ode_isol2coll(prob, 'duff', coll_args{:});

    [data, uidx] = coco_get_func_data(prob, 'duff.coll', 'data', 'uidx');
    maps = data.coll_seg.maps;

    % Add Boundary Conditions
    prob = coco_add_func(prob, 'duff.po', @duff_het_bc_v0, ...
                         @duff_het_bc_v0_dU, data, 'zero', 'uidx', ...
                         uidx([maps.x0_idx; maps.x1_idx; maps.p_idx(4);
                               maps.T0_idx; maps.T_idx]));

    % Add the initial velocity as an active continuation parameter
    prob = coco_add_pars(prob, 'x20', maps.x0_idx, {'xmax', 'x20'}, 'active');
    % Mark points near both fold points
    prob = coco_add_event(prob, 'W1', 'om', 3.2);
    prob = coco_add_event(prob, 'W2', 'om', 2.1);

    bd = coco(prob, run_name, [], 1, {'om', 'phi', 'xmax', 'x20'}, range);
end