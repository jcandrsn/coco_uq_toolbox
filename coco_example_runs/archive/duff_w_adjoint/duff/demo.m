clear all; clc;
%%
run_name = 'phs';
if not(coco_exist(run_name, 'run'))
    fprintf('Find the phase angle that correspond to zero velocity at t=0')
    bd = set_phase_angle(run_name);
else
    bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'swp';

if not(coco_exist(run_name, 'run'))
    fprintf('Perform a frequency sweep and mark orbits on either side of the fold points')
    bd = frequency_sweep(bd, last_run_name, run_name, 'duff', upper(last_run_name), [1, 10]);
else
    bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'right';

if not(coco_exist(run_name, 'run'))
    fprintf('Dial in location of Right Fold point')
    bd_right = locate_fold(bd, last_run_name, run_name);
else
    bd_right = coco_bd_read(run_name);
end

run_name = 'left';

if not(coco_exist(run_name, 'run'))
    fprintf('Dial in location of Left Fold point')
    bd_left = locate_fold(bd, last_run_name, run_name);
else
    bd_left = coco_bd_read(run_name);
end

% Result of 'left' and 'right' runs is a set of orbits on either side of
% the two fold points in the frequency response.  Next step is to adjust 
% the horizontal distance between the two fold points

% To that end, collect P1, P2 from right, P3, P4 from left, combine them
% into a single problem structure and apply udiff to 'om1' and 'om2'
run_name = 'hdiff';

if not(coco_exist(run_name, 'run'))
    fprintf('Drive Left and Right fold points together by modifying \n Linear stiffness k')
    bd = hdiff(bd_right, bd_left, run_name);
else
    bd = coco_bd_read(run_name);
end
last_run_name = run_name;
run_name = 'swp2';

if not(coco_exist(run_name, 'run'))
    fprintf('Perform a second frequency sweep with the new stiffness')
    bd = frequency_sweep(bd, last_run_name, run_name, 'P1', 'EP', [1,10]);
else
    bd = coco_bd_read(run_name);
end

%% Optimization run
% Use the adjoint library to construct a problem that will increase the
% dynamic range of the duffing oscillator

last_run_name = 'hdiff';
run_name = 'opt.fold';
% Collect problem instance after horizontal difference has been driven to
% near zero.

bd = coco_bd_read(last_run_name);

labs = coco_bd_labs(bd, 'EP');
lab = labs(1);
% All the points are nearly equal at this point, so grabbing P1
sol = coll_read_solution('P1', last_run_name, lab);
t0 = sol.tbp;
x0 = sol.xbp;
p0 = sol.p;

prob = coco_prob();
prob = duff_settings(prob, 20);

coll_args = {@duff_het, @duff_het_DX, @duff_het_DP, @duff_het_DT, ...
             @duff_het_DXDX, @duff_het_DXDP, @duff_het_DPDP, ...
             @duff_het_DTDX, @duff_het_DTDP, @duff_het_DTDT, ...
             t0, x0, {'k', 'phi', 'A', 'om', 'b', 'eps'}, p0};

prob = ode_isol2coll(prob, 'duff', coll_args{:});
[data, uidx] = coco_get_func_data(prob, 'duff.coll', 'data', 'uidx');
maps = data.coll_seg.maps;

% Add Boundary Conditions
prob = coco_add_func(prob, 'duff.po', @duff_het_bc_v0, ...
                     @duff_het_bc_v0_dU, @duff_het_bc_v0_dUdU, ...
                     data, 'zero', 'uidx', ...
                     uidx([maps.x0_idx; maps.x1_idx; maps.p_idx(4);
                           maps.T0_idx; maps.T_idx]));
% Add the initial velocity as an active continuation parameter
prob = coco_add_pars(prob, 'x0', maps.x0_idx(1), {'x10'}, 'active');

% Add Adjoints
prob = adjt_isol2coll(prob, 'duff');
[data, axidx] = coco_get_adjt_data(prob, 'duff.coll', 'data', 'axidx');
opt = data.coll_opt;
prob = coco_add_adjt(prob, 'duff.po', 'aidx', ...
  axidx([opt.x0_idx; opt.x1_idx; opt.p_idx(4); opt.T0_idx; opt.T_idx]));
prob = coco_add_adjt(prob, 'x0', {'d.x10'}, 'aidx', axidx(opt.x0_idx(1)));

cont_pars = {'x10', 'phi', 'd.k', 'd.phi', 'd.A', 'd.om', ...
             'd.b', 'd.eps', 'k', 'd.x10',  'A', 'om', 'b', 'eps'};
% cont_pars = {'k', 'phi'};
coco(prob, 'duff', [], 1, cont_pars, [0.1 10]);

plt = false;
if plt
    figure(1)
    hold on
    bd = coco_bd_read('swp2');
    plot(cell2mat(bd(2:end,11)), cell2mat(bd(2:end,16)))
    bd2 = coco_bd_read('swp');
    plot(cell2mat(bd2(2:end,11)), cell2mat(bd2(2:end,16)))
end
