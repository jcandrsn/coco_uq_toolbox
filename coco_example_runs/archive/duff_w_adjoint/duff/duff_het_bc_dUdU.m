function [data, J] = duff_het_bc_dUdU(prob, data, u) 

x0 = u(1:2);
x1 = u(3:4);
om = u(5);
T0 = u(6);
T  = u(7);

ddw = -4*pi / om^3;
J = zeros(4,7,7);
J(4,5,5) = ddw;

end
