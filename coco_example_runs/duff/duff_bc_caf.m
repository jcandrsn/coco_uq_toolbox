function [data, y] = duff_bc_caf(prob, data, u)
% Version that's compatible with coco_add_func

pr = data.pr;
bc = pr.bvp_bc;

x0 = u(bc.x0_idx);
x1 = u(bc.x1_idx);

y = [x1(1)-x0(1); x1(2)-x0(2); x1(3)-x0(3)-2*pi];

end

