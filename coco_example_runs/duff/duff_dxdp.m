function dJ = duff_dxdp(x,p)

% State Variables
x1 = x(1,:);
x2 = x(2,:);
x3 = x(3,:);

% Parameters
m = p(1,:);
c = p(2,:);
k = p(3,:);
al = p(4,:);
A = p(5,:);
w = p(6,:);

% Checked using:
% x = rand(3,1); p = rand(7,1);
% coco_ezDFDP('f(x,p)', @duff_dx, x, p) - duff_dxdp(x,p)
dJ = zeros(3,3,6,numel(x1));
dJ(2,1,1,:) = (-1./m.^2).*(-k - 3*al.*x1.^2);
dJ(2,1,3,:) = -1./m;
dJ(2,1,4,:) = (-3*x1.^2)./m;
dJ(2,2,1,:) = c./m.^2;
dJ(2,2,2,:) = -1./m;
dJ(2,3,1,:) = (1./m.^2).*(A.*sin(x3));
dJ(2,3,5,:) = (-1./m).*sin(x3);

end