function J = duff_bc_du(data, T, x0, x1, p)

%     T  x01 x02 x03 x11 x12 x13  m   c   k  eps  A   w
J = [ 0  -1   0   0   1   0   0   0   0   0   0   0   0;
      0   0  -1   0   0   1   0   0   0   0   0   0   0;
      0   0   0  -1   0   0   1   0   0   0   0   0   0;
      0   0   1   0   0   0   0   0   0   0   0   0   0;
     ];
end