function dJ = duff_dxdx(x,p)

% State Variables
x1 = x(1,:);
x2 = x(2,:);
x3 = x(3,:);

% Parameters
m = p(1,:);
c = p(2,:);
k = p(3,:);
al = p(4,:);
A = p(5,:);
w = p(6,:);

% Checked using:
% x = rand(3,1); p = rand(7,1);
% coco_ezDFDX('f(x,p)', @duff_dx, x, p) - duff_dxdx(x,p)
dJ = zeros(3,3,3,numel(x1));
dJ(2,1,1,:) = -6*al.*x1./m;
dJ(2,3,3,:) = (-1./m).*(A.*cos(x3));

end