function [data, J] = duff_bc_caf_du(prob, data, u)


%     T0  T  x01 x02 x03 x11 x12 x13  m   c   k  eps  A   w
J = [ 0   0  -1   0   0   1   0   0   0   0   0   0   0   0;
      0   0   0  -1   0   0   1   0   0   0   0   0   0   0;
      0   0   0   0  -1   0   0   1   0   0   0   0   0   0;
     ];
end