function Jx = duff_dx(x,p)

% State Vectors
x1 = x(1,:);
x3 = x(3,:);

% Parameters
m = p(1,:);
c = p(2,:);
k = p(3,:);
al = p(4,:);
A = p(5,:);
w = p(6,:);

% Jacobian
% Checked using:
% x = rand(3,1); p = rand(7,1);
% coco_ezDFDX('f(x,p)', @duff, x, p) - duff_dx(x,p)
Jx = zeros(3,3,numel(x1));
Jx(1,2,:) = 1;
Jx(2,1,:) = (-k - 3*al.*x1.^2)./m;
Jx(2,2,:) = -c./m;
Jx(2,3,:) = (-1./m).*(A.*sin(x3));

end