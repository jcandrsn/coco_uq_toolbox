function dJ = duff_dpdp(x,p)

% State Variables
x1 = x(1,:);
x2 = x(2,:);
x3 = x(3,:);

% Parameters
m = p(1,:);
c = p(2,:);
k = p(3,:);
al = p(4,:);
A = p(5,:);
w = p(6,:);

% Checked using:
% x = rand(3,1); p = rand(7,1);
% coco_ezDFDP('f(x,p)', @duff_dp, x, p) - duff_dpdp(x,p)
dJ = zeros(3,6,6,numel(x1));
dJ(2,1,1,:) = (2./m.^3).*(A.*cos(x3) - c.*x2 - k.*x1 - al.*x1.^3);
dJ(2,1,2,:) = x2./(m.^2);
dJ(2,1,3,:) = x1./(m.^2);
dJ(2,1,4,:) = (x1.^3)./(m.^2);
dJ(2,1,5,:) = (-1./(m.^2)).*cos(x3);
dJ(2,2,1,:) = x2./(m.^2);
dJ(2,3,1,:) = x1./(m.^2);
dJ(2,4,1,:) = (x1.^3)./(m.^2);
dJ(2,5,1,:) = (-1./(m.^2)).*cos(x3);

end