img_path = ['/Users/jcoleanderson/Google Drive/',...
 'Grad School/00_Thesis Research/00_Document/Images/'];

nds = reshape(uq_gauss_nodes(4,2,{'Hermite', 'Legendre'}),[],2)';
[x,y] = ndgrid(nds(1,:),nds(2,:));

figure1 = figure(1);
hold on;grid on;box on
xlim([-2.5, 2.5])
ylim([-1,1])
plot(x(:), y(:), 'ko', 'MarkerFaceColor', [0.5,0.5,0.5])
plot(0,0, 'kp', 'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)
text(0.05,0.05, '$\left(p_1, p_2\right)$', 'Interpreter', 'latex', 'FontSize', 12)
ha = annotation('arrow');  % store the arrow information in ha
ha.Parent = gca;           % associate the arrow the the current axes
ha.X = [0 0];          % the location in data units
ha.Y = [0.05 0.3];   
ha.LineWidth  = 2;          % make the arrow bolder for the picture
ha = annotation('arrow');  % store the arrow information in ha
ha.Parent = gca;           % associate the arrow the the current axes
ha.X = [0 0];          % the location in data units
ha.Y = [-0.05 -0.3];   
ha.LineWidth  = 2;          % make the arrow bolder for the picture

ha = annotation('arrow');  % store the arrow information in ha
ha.Parent = gca;           % associate the arrow the the current axes
ha.X = [0 0];          % the location in data units
ha.Y = [-0.4 -0.8];   
ha.LineWidth  = 2;          % make the arrow bolder for the picture

ha = annotation('arrow');  % store the arrow information in ha
ha.Parent = gca;           % associate the arrow the the current axes
ha.X = [0 0];          % the location in data units
ha.Y = [0.4 0.8];   
ha.LineWidth  = 2;          % make the arrow bolder for the picture

plot(zeros(1,4), nds(2,:), 'kd', 'MarkerFaceColor', [0.5,0.5,0.5])

for i=1:4
  ha = annotation('arrow');  % store the arrow information in ha
  ha.Parent = gca;           % associate the arrow the the current axes
  ha.X = [0.07, 0.68];
  ha.Y = [nds(2,i), nds(2,i)];
  ha.LineWidth  = 2;          % make the arrow bolder for the picture
  ha = annotation('arrow');  % store the arrow information in ha
  ha.Parent = gca;           % associate the arrow the the current axes
  ha.X = [-0.07, -0.68];
  ha.Y = [nds(2,i), nds(2,i)];
  ha.LineWidth  = 2;          % make the arrow bolder for the picture
  ha = annotation('arrow');  % store the arrow information in ha
  ha.Parent = gca;           % associate the arrow the the current axes
  ha.X = [0.8, 2.25];
  ha.Y = [nds(2,i), nds(2,i)];
  ha.LineWidth  = 2;          % make the arrow bolder for the picture
  ha = annotation('arrow');  % store the arrow information in ha
  ha.Parent = gca;           % associate the arrow the the current axes
  ha.X = [-0.8, -2.25];
  ha.Y = [nds(2,i), nds(2,i)];  
  ha.LineWidth  = 2;          % make the arrow bolder for the picture
end

set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
xlabel('Stochastic Parameter 1, $P_{1}$', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('Stochastic Parameter 2, $P_{2}$', 'Interpreter', 'latex', 'FontSize', 15)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = 12;

figname = 'uq_bvp_gen_samples.eps';
fp = strcat(img_path, figname);
saveas(figure1, fp, 'epsc');

bd = coco_bd_read('freq_sweep');
om_idx = strcmp('om', bd(1,:));
mean_idx = strcmp('orig.resp.mean', bd(1,:));
var_idx = strcmp('orig.resp.var', bd(1,:));

figure2 = figure(2);
mudata = csvread('mu_data_from_mathematica.csv');
semilogx(mudata(:,1),mudata(:,2), 'k-', 'DisplayName', 'Analytical Results', ...
  'LineWidth', 2);
hold on;grid on;box on

xlim([0.001,1000])
xlabel('Excitation Frequency, $\omega$', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('Mean Maximal Displacement $\mu_{X_{1,0}}$', 'Interpreter', 'latex', 'FontSize', 15)
lgd = legend('Orientation', 'vertical');
lgd.Interpreter = 'latex';
lgd.Location = 'best';

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = 12;

figname = 'analytical_mean.eps';
fp = strcat(img_path, figname);
saveas(figure2, fp, 'epsc');

plt_samples = [300,1000:100:1400, 1400:20:1500, 1510:10:1600, 1700];
semilogx(cell2mat(bd(plt_samples,om_idx)), cell2mat(bd(plt_samples,mean_idx)), 'ko', ...
  'DisplayName', 'Numerical Results (PCE)')
ax.FontSize = 12;
figname = 'uq_analytical_comparison_mean.eps';
fp = strcat(img_path, figname);
saveas(figure2, fp, 'epsc');

figure3 = figure(3);
vardata = csvread('var_data_from_mathematica.csv');
semilogx(vardata(:,1), vardata(:,2), 'k-', 'DisplayName', 'Analytical Results', ...
  'LineWidth', 2);
hold on;grid on;box on

xlim([0.001,1000])
xlabel('Excitation Frequency, $\omega$', 'Interpreter', 'latex', 'FontSize', 15)
ylabel('Maximal Displacement Variance, $\sigma^{2}_{X_{1,0}}$', 'Interpreter', 'latex', 'FontSize', 15)
lgd = legend('Orientation', 'vertical');
lgd.Interpreter = 'latex';
lgd.Location = 'best';

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = 12;

figname = 'analytical_variance.eps';
fp = strcat(img_path, figname);
saveas(figure3, fp, 'epsc');

semilogx(cell2mat(bd(plt_samples,om_idx)), cell2mat(bd(plt_samples,var_idx)), 'ko', ...
  'DisplayName', 'Numerical Results (PCE)')
ax.FontSize = 12;
figname = 'uq_analytical_comparison_var.eps';
fp = strcat(img_path, figname);
saveas(figure3, fp, 'epsc');

figure4 = figure(4);
hold on; grid on;
M = 4;
P = 4;
run_name = strcat('UQ_M=',int2str(M),'_P=',int2str(P));
run_name = 'freq_sweep';
% fprintf(run_name);
% fprintf('\n')
bd = coco_bd_read(run_name);
om_idx = strcmp('om', bd(1,:));
mean_idx = strcmp('mean', bd(1,:));
var_idx = strcmp('variance', bd(1,:));

% Plot Mean
semilogx(coco_bd_col(bd, 'om'), coco_bd_col(bd, 'orig.resp.mean'), 'k', ...
  'DisplayName', 'Mean, $\mu_{X_{1,0}}$')

% Plot Mean - 3*Std Dev
semilogx(coco_bd_col(bd, 'om'), coco_bd_col(bd, 'orig.resp.mean') - ...
  3*sqrt(coco_bd_col(bd, 'orig.resp.var')), 'k--', 'DisplayName', ...
  'Mean $\pm$ St Dev, $\mu_{X_{1,0}}\pm\sigma_{X_{1,0}}$')

% Plot Mean + 3*Std Dev
semilogx(coco_bd_col(bd, 'om'), coco_bd_col(bd, 'orig.resp.mean') + ...
  3*sqrt(coco_bd_col(bd, 'orig.resp.var')), 'k--', 'HandleVisibility', 'off')

xlabel('Excitation Frequency, $\omega$', 'Interpreter', 'latex')
ylabel('Maximal Displacement', 'Interpreter', 'latex')
xlim([0.001,1000])
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = 12;
ax.XScale = 'log';
lgd = legend('Location', 'best');
lgd.Interpreter = 'latex';

figname = 'analytical_mean_pm_variance.eps';
fp = strcat(img_path, figname);
saveas(figure4, fp, 'epsc');

labs = coco_bd_labs(bd);
data_mat = zeros(size(labs,2), M+1);
for lab=labs
    for i=1:M
        curve_name = coco_get_id('orig', 'uq', ['sample', int2str(i)]);
        sol = coll_read_solution(curve_name, run_name, lab);
        data_mat(lab, i+1) = sol.xbp(1,1);
    end
    data_mat(lab,1) = sol.p(3);
end

data_mat = sortrows(data_mat,1);
data_samples = [5, 50, 75, 100:10:140, 140:3:152, 153, 155:165];

semilogx(data_mat(data_samples,1), data_mat(data_samples,2), 'o', ...
    'MarkerEdgeColor', 'k', 'MarkerFaceColor', ...
    [100/255 100/255 100/255], 'MarkerSize', 5, 'DisplayName', 'Samples');

semilogx(data_mat(data_samples,1), data_mat(data_samples,3:end), 'o', ...
    'MarkerEdgeColor', 'k', 'MarkerFaceColor', ...
    [100/255 100/255 100/255], 'MarkerSize', 5, 'HandleVisibility', 'off');
  
figname = 'uq_linode_pce_samples.eps';
fp = strcat(img_path, figname);
saveas(figure4, fp, 'epsc');
