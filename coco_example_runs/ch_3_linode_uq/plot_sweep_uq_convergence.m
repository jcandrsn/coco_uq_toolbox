M=5;
figure()
hold on
for P=0:4
run_name = strcat('UQ_M=',int2str(M),'_P=',int2str(P));
bd = coco_bd_read(run_name);
om_idx = strcmp('om', bd(1,:));
mean_idx = strcmp('mean', bd(1,:));
var_idx = strcmp('variance', bd(1,:));
semilogx(cell2mat(bd(2:end,om_idx)), cell2mat(bd(2:end,var_idx)), 'DisplayName', ...
     strcat(['[M, P] = ', mat2str([M P])]))
end
legend('show')
set(gca, 'xscale', 'log')
grid on
hold off

M=5;
figure()
hold on
for P=0:4
run_name = strcat('UQ_M=',int2str(M),'_P=',int2str(P));
bd = coco_bd_read(run_name);
om_idx = strcmp('om', bd(1,:));
mean_idx = strcmp('mean', bd(1,:));
var_idx = strcmp('variance', bd(1,:));
semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,mean_idx)), 'DisplayName', ...
     strcat(['[M, P] = ', mat2str([M P])]))
end
legend('show')
set(gca, 'xscale', 'log')
grid on
hold off

P=4;
figure()
hold on
for M=1:5
run_name = strcat('UQ_M=',int2str(M),'_P=',int2str(P));
bd = coco_bd_read(run_name);
om_idx = strcmp('om', bd(1,:));
mean_idx = strcmp('mean', bd(1,:));
var_idx = strcmp('variance', bd(1,:));
semilogx(cell2mat(bd(2:end,om_idx)), cell2mat(bd(2:end,var_idx)), ...
    'DisplayName', strcat(['[M, P] = ', mat2str([M P])]))
end
set(gca, 'xscale', 'log')
legend('show')
grid on
hold off

P=4;
figure()
hold on
for M=1:5
run_name = strcat('UQ_M=',int2str(M),'_P=',int2str(P));
bd = coco_bd_read(run_name);
om_idx = strcmp('om', bd(1,:));
mean_idx = strcmp('mean', bd(1,:));
var_idx = strcmp('variance', bd(1,:));
semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,mean_idx)), ...
    'DisplayName', strcat(['[M, P] = ', mat2str([M P])]))
end
set(gca, 'xscale', 'log')
legend('show')
grid on
hold off

P=4;
figure()
hold on
for M=3:5
run_name = strcat('UQ_M=',int2str(M),'_P=',int2str(P));
bd = coco_bd_read(run_name);
om_idx = strcmp('om', bd(1,:));
mean_idx = strcmp('mean', bd(1,:));
var_idx = strcmp('variance', bd(1,:));
semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,mean_idx)), ...
    'DisplayName', strcat(['[M, P] = ', mat2str([M P])]))
end
set(gca, 'xscale', 'log')
legend('show')
grid on
hold off


% Doing Plot Insets
% % some data
%      x=0:100;
%      r=rand(size(x));
% % ... displayed
%      line(x,r);
% % the inset
% % ... axis
%      ah=axes('position',[.5 .5 .3 .3]);
%      set(ah,'color',.9*[1 1 1]);
%      title('inset','fontsize',20);
% % ... data
%      xrng=40:60;
%      line(x(xrng),r(xrng),'marker','o');