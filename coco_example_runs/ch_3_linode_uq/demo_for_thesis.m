% Path to vector field, Jacobians, and Response Function
addpath('../linode')
addpath('../utils')

% Let Transients die out
p0 = [3;0;1];
[~, x0]   = ode45(@(t,x) linode(t, x, p0), [0 20*pi], [1; 0]);
% Initial Solution Guess
[t0, x0]   = ode45(@(t,x) linode(t, x, p0), [0 2*pi], [x0(end,:)']);

% Initialize problem instance and set options
prob = coco_prob();
prob = coco_set(prob, 'ode', 'autonomous', false);
prob = coco_set(prob, 'coll', 'NTST', 15, 'NCOL', 4);
prob = coco_set(prob, 'cont', 'PtMX', 1500, 'h', 0.5, 'h_max', 10);

% 'coll'-toolbox arguments
coll_args = {@linode, @linode_dx, @linode_dp, @linode_dt, t0, x0, p0};

% Parameter names
pnames = {'k', 'phi', 'om'};

% 'bvp'-toolbox arguments
% Includes a velocity condition to ensure the initial
% position is a maximum
bvp_args = {@fbc_x10, @Jbc_x10};

% 'uq'-toolbox arguments
% {{Stochastic Parameter}, {Distribution Type}, [[mean, std dev]], {Unique Parameter}}
uq_args = {{'k'}, {'Normal'}, [[3, 0.2]], {'phi'}};

% Call to sample constructor
prob = uq_isol2bvp_sample(prob, 'orig', coll_args{:}, pnames, bvp_args{:}, uq_args{:});

% % Add response function, 'resp', to the sample 'orig'
% prob = uq_coll_add_response(prob, 'orig', 'resp', 'bv', @x10, @x10_du, @x10_dudu);
% 
% % Continue in frequency, display mean and variance
% uq_bd = coco(prob, 'freq_sweep', [], 1, ...
%   {'om', 'orig.resp.mean', 'orig.resp.var'}, ...
%   [0.001, 1000]);