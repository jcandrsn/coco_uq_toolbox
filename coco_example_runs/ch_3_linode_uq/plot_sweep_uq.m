M = 5;
P = 4;
run_name = strcat('UQ_M=',int2str(M),'_P=',int2str(P));

% fprintf(run_name);
% fprintf('\n')
bd = coco_bd_read(run_name);
om_idx = strcmp('om', bd(1,:));
mean_idx = strcmp('mean', bd(1,:));
var_idx = strcmp('variance', bd(1,:));

labs = coco_bd_labs(bd);
data_mat = zeros(size(labs,2), M+1);
for lab=labs
    for i=1:M
        curve_name = strcat('UQ', int2str(i));
        sol = coll_read_solution(curve_name, run_name, lab);
        data_mat(lab, i+1) = sol.xbp(1,1);
    end
    data_mat(lab,1) = sol.p(3);
end
data_mat = sortrows(data_mat,1);
plot_freq = 1;
data_samples = [5:5:30,31:50,62];

freq_or_samples = 'samples';
switch freq_or_samples
    case 'freq'
    semilogx(data_mat(1:plot_freq:end,1), -data_mat(1:plot_freq:end,2), 'o', ...
        'MarkerEdgeColor', 'k', 'MarkerFaceColor', ...
        [100/255 100/255 100/255], 'MarkerSize', 5, 'DisplayName', 'Samples');
    hold on
    semilogx(data_mat(1:plot_freq:end,1), -data_mat(1:plot_freq:end,3:end), 'o', ...
        'MarkerEdgeColor', 'k', 'MarkerFaceColor', ...
        [100/255 100/255 100/255], 'MarkerSize', 5, 'HandleVisibility', 'off');
    % Plot Mean
    semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,mean_idx)), 'k', ...
         'DisplayName', 'Mean, $\mu$')
    % Plot Mean - 3*Std Dev
    semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,mean_idx))- ...
          3*sqrt(cell2mat(bd(2:end,var_idx))), 'k--', 'DisplayName', ...
          'Mean $\pm$ St Dev, $\mu\pm\sigma$')
      
    case 'samples'
    semilogx(data_mat(data_samples,1), -data_mat(data_samples,2), 'o', ...
        'MarkerEdgeColor', 'k', 'MarkerFaceColor', ...
        [100/255 100/255 100/255], 'MarkerSize', 5, 'DisplayName', 'Samples');
    hold on
    semilogx(data_mat(data_samples,1), -data_mat(data_samples,3:end), 'o', ...
        'MarkerEdgeColor', 'k', 'MarkerFaceColor', ...
        [100/255 100/255 100/255], 'MarkerSize', 5, 'HandleVisibility', 'off');
    % Plot Mean
    semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,mean_idx)), 'k', ...
         'DisplayName', 'Mean, $\mu_{x_0}$')
    % Plot Mean - 3*Std Dev
    semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,mean_idx))- ...
          3*sqrt(cell2mat(bd(2:end,var_idx))), 'k--', 'DisplayName', ...
          'Mean $\pm$ St Dev, $\mu_{x_0}\pm\sigma_{x_0}$')        

end
                             
% Plot Mean + 3*Std Dev
semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,mean_idx))+ ...
     3*sqrt(cell2mat(bd(2:end,var_idx))), 'k--', 'HandleVisibility', 'off')

xlabel('Frequency, $\omega$', 'Interpreter', 'latex')
ylabel('Amplitude, $x_{1}\left(0\right)$', 'Interpreter', 'latex')
lgd = legend('Location', 'best');
lgd.Interpreter = 'latex';
grid on

figure(2)
hold on
for i=1:M
    xmax_idx = strcmp(strcat('xmax',num2str(i)),bd(1,:));
    k_idx = strcmp(strcat('k',num2str(i)), bd(1,:));
    k = cell2mat(bd(2,k_idx));
    semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,xmax_idx)), ...
             '-', 'DisplayName', strcat('$k = ', num2str(k), '$'))
end
xlabel('Frequency, $\omega$', 'Interpreter', 'latex')
% ylabel('Amplitude, $x_{1}\left(0\right)$', 'Interpreter', 'latex')
set(gca, 'xscale', 'log')
lgd = legend('Location', 'best');
lgd.Interpreter = 'latex';
grid on

figure(3)
hold on


semilogx(cell2mat(bd(2:end,om_idx)), cell2mat(bd(2:end,var_idx)), 'DisplayName', 'Variance')
xlabel('Frequency, $\omega$', 'Interpreter', 'latex')
% ylabel('Amplitude, $x_{1}\left(0\right)$', 'Interpreter', 'latex')
set(gca, 'xscale', 'log')
lgd = legend('Location', 'best');
lgd.Interpreter = 'latex';
grid on

figure(4)
hold on


semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,xmax_idx)), 'DisplayName', 'Mean')
semilogx(cell2mat(bd(2:end,om_idx)), 100*cell2mat(bd(2:end,var_idx)), 'DisplayName', '100*Variance')
semilogx(cell2mat(bd(2:end,om_idx)), -cell2mat(bd(2:end,xmax_idx))-100*cell2mat(bd(2:end,var_idx)), 'DisplayName', 'Mean-100*Variance')
xlabel('Frequency, $\omega$', 'Interpreter', 'latex')
ylabel('Amplitude, $x_{1}\left(0\right)$', 'Interpreter', 'latex')
set(gca, 'xscale', 'log')
lgd = legend('Location', 'best');
lgd.Interpreter = 'latex';
grid on

% Would be good to inset histograms for selected sample locations
% Collect the alphas at the selected location, generate 10000 samples
% hist(sample, nbins)