function ann = xy_annotation(ax, type, varargin)

linetype = strcmpi(type,'line') || strcmpi(type,'arrow') || ...
    strcmpi(type,'doublearrow') || strcmpi(type,'textarrow');

shapetype = strcmpi(type,'rectangle') || strcmpi(type,'ellipse') || ...
    strcmpi(type,'textbox');

assert(linetype || shapetype, ...
  ['Error:  Annotation type ', '"', type, '"', ' not recognized']);
  
if linetype
  x = varargin{1};
  y = varargin{2};
  opts = varargin(3:end);
elseif shapetype
  dim = varargin{1};
  x = [dim(1), dim(1) + dim(3)];
  y = [dim(2), dim(2) + dim(4)];
  opts = varargin(2:end);
end

pos = get(ax, 'Position');
x0 = pos(1); y0 = pos(2); 
width = pos(3); height = pos(4);
xl = get(ax, 'XLim');
yl = get(ax, 'YLim');
if strcmpi(get(ax, 'XScale'),'linear')
  mx = width / diff(xl);
  bx = x0 - mx*xl(1);
  xt = mx*x + bx;
else
  mx = width / log10(xl(2)/xl(1));
  bx = x0 - mx*(log10(xl(1)));
  xt = mx*log10(x) + bx;
end

if strcmpi(get(ax, 'YScale'),'linear')
  my = height / diff(yl);
  by = y0 - my*yl(1);
  yt = my*y + by;
else
  my = width / log10(yl(2)/yl(1));
  by = y0 - my*(log10(yl(1)));
  yt = my*log10(y) + by;
end

if linetype
  ann = annotation(type, xt, yt, opts{:});
elseif shapetype
  dim = [xt(1), yt(1), diff(xt), diff(yt)];
  ann = annotation(type, dim, opts{:});
end

end