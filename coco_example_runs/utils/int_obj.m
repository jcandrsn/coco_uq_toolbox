function [data, y] = int_obj(prob, data, u)

% Implementation of Formulat 7.16 of Recipes for
% Continuation with the addition of parameter arguments in
% the integrand.
%
% Code provided by Mingwu Li with minor modifications.

maps = data.coll_seg.maps;
int  = data.coll_seg.int;
T   = u(maps.T_idx);    % Extract interval length
x   = u(maps.xbp_idx);  % Extract basepoint values
p   = u(maps.p_idx);    % Extract problem parameters
xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
pp  = repmat(p, maps.p_rep);

objode = data.objhan(xx, pp);
wts1 = repmat(int.wt',[maps.NTST,1]);

y = T/(2*maps.NTST)*objode*wts1;
y = y - u(end);

end