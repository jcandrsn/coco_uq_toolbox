function [data, J] = adj_int_obj_du(prob, data, u)

% Jacobian of adjoint for an integral function
%
% Code provided by Mingwu with modifications required for
% g(x,p) instead of g(x).
maps = data.coll_seg.maps;
int  = data.coll_seg.int;
wts1 = repmat(int.wt',[maps.NTST,1]);
T   = u(maps.T_idx);    % Extract interval length
x   = u(maps.xbp_idx);  % Extract basepoint values
p   = u(maps.p_idx);    % Extract problem parameters
xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
pp  = repmat(p, maps.p_rep);

dcn  = size(maps.W,1);
dbp  = size(maps.W,2);

objhan_dxdx = data.objhan_dxdx(xx, pp);  
objhan_dxdp = data.objhan_dxdp(xx, pp);
objhan_dx = data.objhan_dx(xx, pp);

J_xcn_xbp = sparse(maps.fdxrows, maps.fdxcols, objhan_dxdx(:))*maps.W;
J_xcn_xbp = T/(2*maps.NTST)*J_xcn_xbp;
J_xcn_T0  = zeros(dcn,1);
J_xcn_T   = 1/(2*maps.NTST)*objhan_dx(:);
J_xcn_p = (T/(2*maps.NTST))*sparse(maps.fdprows, maps.fdpcols, objhan_dxdp(:));
J_xcn_u0   = zeros(dcn, 1);
J_xcn = [J_xcn_xbp, J_xcn_T0, J_xcn_T, J_xcn_p, J_xcn_u0];
J_xcn = reshape(full(J_xcn), [1, size(J_xcn,1),size(J_xcn,2)]);

% Final +1 is for J_T0_u0
J_T0 = zeros(1,1,dbp+2+maps.pdim+1);

dim    = maps.x_shp(1);
xcnnum = maps.x_shp(2);
xcndim = maps.x_shp(1)*maps.x_shp(2);
objhan_dx_rows = repmat(reshape(1:xcnnum, [1 xcnnum]), [dim 1]);
objhan_dx_cols = repmat(1:xcndim, [1 1]);
% objhan_dx = data.objhan_dx(xx, pp);
objhan_dx_2 = sparse(objhan_dx_rows, objhan_dx_cols, objhan_dx(:))*maps.W;
dobj_dp = data.objhan_dp(xx, pp);

J_T_xbp = 1/(2*maps.NTST)*objhan_dx_2'*wts1;
J_T_xbp = J_T_xbp';
J_T_T0  = 0;
J_T_T   = 0;
J_T_p   = 1/(2*maps.NTST)*dobj_dp(:)'*kron(wts1,eye(maps.pdim));
% Final 0 is for J_T_u0
J_T = [J_T_xbp, J_T_T0, J_T_T, J_T_p, 0];
J_T = reshape(J_T, [1, size(J_T,1),size(J_T,2)]);

J_p_xcn = permute(data.objhan_dxdp(xx, pp), [1, 3 ,2, 4]);
J_p_xcn = reshape(J_p_xcn, [maps.pdim, prod(maps.x_shp)]);
J_p_xcn = (T/(2*maps.NTST))*J_p_xcn.*repelem(wts1', maps.pdim, maps.x_shp(1));
J_p_xbp = J_p_xcn*maps.W;
J_p_T0 = zeros(maps.pdim,1);
J_p_p = data.objhan_dpdp(xx, pp);

w = kron(wts1, eye(maps.pdim^2));
J_p_p = reshape((T/(2*maps.NTST))*J_p_p(:)'*w, maps.pdim, maps.pdim);
J_p_u0 = zeros(maps.pdim, 1);
J_p = [J_p_xbp J_p_T0 J_T_p' J_p_p J_p_u0];
J_p = reshape(full(J_p), [1, size(J_p,1),size(J_p,2)]);

J_u0 = zeros(1, 1, dbp+2+maps.pdim+1);

J = [J_xcn, J_T0, J_T, J_p, J_u0];

% [data, Jd] = coco_ezDFDX('f(o,d,x)', prob, data, @adj_int_obj, u);
% diff_J = abs(J-Jd);
% max(max(diff_J))
end