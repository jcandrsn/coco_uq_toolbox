function [data, J] = int_obj_du(prob, data, u)

% Derivative of Formulat 7.16 of Recipes for
% Continuation with the addition of parameter arguments in
% the integrand.
%
% Code provided by Mingwu Li with minor modifications.
%
% u is provided from uidx of a coll instance and is of the
% form:
% u  = [  xbp,   T0,   T,   p];
% Therefore, J is of the form:
% du = [J_xpb, J_T0, J_T, J_p];

maps = data.coll_seg.maps;
int  = data.coll_seg.int;
T   = u(maps.T_idx);    % Extract interval length
x   = u(maps.xbp_idx);  % Extract basepoint values
p   = u(maps.p_idx);    % Extract problem parameters
xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
pp  = repmat(p, maps.p_rep);

objode  = data.objhan(xx, pp); 
wts1    = repmat(int.wt',[maps.NTST,1]);
% wts2    = kron(diag(int.wt),eye(int.dim));
% wts2    = kron(eye(maps.NTST),wts2);
% Equivalent Alternative
wts2    = diag(kron(ones(1,maps.NTST), kron(int.wt, ones(1,int.dim))));
dobj_dx = data.objhan_dx(xx, pp);  
dobj_dp = data.objhan_dp(xx, pp);

J_xbp = T/(2*maps.NTST)*dobj_dx(:)'*wts2*maps.W;
J_T0  = 0;
J_T   = 1/(2*maps.NTST)*objode*wts1;
% Need to talk with Mingwu to make sure this is right.
% Works for the example I passed to it, but need to make
% sure it's correct in general.
J_p   = T/(2*maps.NTST)*dobj_dp(:)'*kron(wts1,eye(maps.pdim));

J = [J_xbp J_T0 J_T J_p -1];

end