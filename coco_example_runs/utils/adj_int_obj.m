function [data, y] = adj_int_obj(prob, data, u)

% Adjoint for an integral function
%
% Code provided by Mingwu with modifications.  This is
% untested.  Could modify Mingwu's example script and see
% if the results are identical.

maps = data.coll_seg.maps;
int  = data.coll_seg.int;
T   = u(maps.T_idx);    % Extract interval length
x   = u(maps.xbp_idx);  % Extract basepoint values
p   = u(maps.p_idx);    % Extract problem parameters
xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
pp  = repmat(p, maps.p_rep);

dobj_dx = data.objhan_dx(xx, pp);  
dobj_dp = data.objhan_dp(xx, pp);

objode  = data.objhan(xx, pp); 
wts1 = repmat(int.wt',[maps.NTST,1]);

 
J_xcn = T/(2*maps.NTST)*dobj_dx(:)';
J_T0  = 0;
J_T   = 1/(2*maps.NTST)*objode*wts1;
J_p   = T/(2*maps.NTST)*dobj_dp(:)'*kron(wts1,eye(maps.pdim));

y = [J_xcn J_T0 J_T J_p -1];

end