function [data, dJ] = int_obj_dudu(prob, data, u)

[data, dJ] = coco_ezDFDX('f(o,d,x)', prob, data, @int_obj_du, u);

end