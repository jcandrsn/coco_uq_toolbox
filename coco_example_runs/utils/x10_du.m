function dr = x10_du(data, T0, T, x0, x1, p)

  dr = zeros(2+2*data.xdim + data.pdim, data.nsamples);
  dr(3,:) = 1;

end