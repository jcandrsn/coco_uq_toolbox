function w0 = res_freq(m)
  eps = 0.01;
  c = 2;
  k = 1;
  alpha = 5;
  F = 5;
  
%  Rm = F/c Sqrt[m/k]
% \[Omega] = Sqrt[k/m] + \[Epsilon] (3 \[Alpha])/(8 k) Sqrt[k/m] Rm^2;
  w0 = sqrt(k./m) + eps*(3*alpha/(8*k))*sqrt(k./m).*((F/c)*sqrt(m./k)).^2;
end