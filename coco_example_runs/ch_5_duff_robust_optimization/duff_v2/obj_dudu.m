function [data, dJ] = obj_dudu(prob, data, u)

dJ = zeros(1,2,2);
var = u(2);

m=1;
n=1;
dJ(1,2,2) = n*(1/4)*(var)^(-3/2);
% Checked with
% [data, dJd] = coco_ezDFDX('f(o,d,x)', prob, data, @obj_du, u);
end