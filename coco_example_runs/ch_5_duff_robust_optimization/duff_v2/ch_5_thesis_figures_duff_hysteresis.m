%% Duffing Hysteresis
%  Plot the frequency sweep.  The unstable segment plotted
%  with dashed lines, the stable segments with solid lines,
%  the hysteresis transitions shown with arrows.  The
%  forward sweep plotted with a red line, the reverse sweep
%  plotted with a blue segment.  Remove axis numbers.

font_size = 15;
swp_bd = coco_bd_read('sweep');

om_vals = coco_bd_col(swp_bd, 'om');
R_vals  = coco_bd_col(swp_bd, 'R');
fold_idx = strcmpi(coco_bd_col(swp_bd, 'TYPE'), 'FP');
idx = 1:numel(fold_idx);
fold_idx = idx(fold_idx);
om_idx_left = om_vals <= om_vals(fold_idx(2));
om_idx_right = om_vals >= om_vals(fold_idx(1));

[PK_max, PK_idx] = max(R_vals);
up_idx = (R_vals < PK_max);

figure1 = figure(1);
hold on;grid on;box on;

% Primary frequency response curve.
% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)
% Fold points
% semilogx(om_vals(fold_idx), R_vals(fold_idx), 'ko', ...
%   'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size)
xlim([1.25,1.8]);
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;
ax.XScale = 'log';

xx = [om_vals(fold_idx(1)),om_vals(fold_idx(1))];
yy = [R_vals(fold_idx(1))-0.05, 0.68];
xy_annotation(gca, 'arrow', xx, yy, ...
  'Color', [1,0,0], 'LineStyle', ':', 'LineWidth', 1, ...
  'HeadStyle', 'cback1', 'HeadWidth', 6);

xx = [1.71, 1.78];
yy = [0.6, 0.45];
xy_annotation(gca, 'arrow', xx, yy, ...
  'Color', [1,0,0], 'LineStyle', ':', 'LineWidth', 1, ...
  'HeadStyle', 'cback1', 'HeadWidth', 6);

xx = [1.77, 1.7];
yy = [0.42, 0.57];
xy_annotation(gca, 'arrow', xx, yy, ...
  'Color', [0,0,1], 'LineStyle', '-.', 'LineWidth', 1, ...
  'HeadStyle', 'cback1', 'HeadWidth', 6);

xx = [1.39,1.48];
yy = [1.005,1.19];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'Color', [1,0,0], 'LineStyle', ':', ...
  'String', 'Sweep Up', ...
  'TextColor', [0,0,0], ...
  'VerticalAlignment', 'baseline', ...
  'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.48,1.39];
yy = [1.14,0.96];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'Color', [0,0,1], 'LineStyle', '-.', ...
  'String', 'Sweep Down', ...
  'TextColor', [0,0,0], ...
  'VerticalAlignment', 'top', ...
  'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [om_vals(fold_idx(2)),om_vals(fold_idx(2))];
yy = [R_vals(fold_idx(2))+0.1, 1.47];
xy_annotation(gca, 'arrow', xx, yy, ...
  'Color', [0,0,1], 'LineStyle', '-.', 'LineWidth', 1, ...
  'HeadStyle', 'cback1', 'HeadWidth', 6);

xx = [om_vals(fold_idx(2))-0.05,om_vals(fold_idx(2))];
yy = [1.3,1.3];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'String', '$\Delta \omega$ ', 'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [om_vals(fold_idx(1))+0.05,om_vals(fold_idx(1))];
yy = [1.3,1.3];
xy_annotation(gca, 'arrow', xx, yy, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

figname = 'duffing_hysteresis.eps';
fp = strcat(img_path, figname);

saveas(figure1, fp, 'epsc');