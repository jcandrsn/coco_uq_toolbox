function y = dwdk(k)
y = 1./(2*sqrt(k)) + (3*25*5*0.001)./(15*4*k.^2.*sqrt(k))-(3*5^2*sqrt(k)*5*0.001)./((4*2^2)*k.^3);
end