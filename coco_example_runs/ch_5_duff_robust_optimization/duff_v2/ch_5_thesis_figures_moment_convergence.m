%% Convergence plots:
figname = 'uq_pce_var_convergence.eps';
fp = strcat(img_path, figname);
figure5 = figure(5);

font_size = 15;
mean = zeros(6,5);
var  = zeros(6,5);

for M=1:6
  for P=2:6
    run_name = ['pce_convergence_M_', int2str(M), '_P_', int2str(P)];
    bd = coco_bd_read(run_name);
    mu = coco_bd_col(bd, 'resp.mean');
    sigsq = coco_bd_col(bd, 'resp.var');
    mean(M, P-1) = mu;
    var(M, P-1) = sigsq;
  end
end

M = repmat(1:6, 5, 1)';
P = repmat(2:6, 6, 1);

mesh(M, P, var)
colormap(gray)
hold on
plot3(M(4,2), P(4,2), var(4,2), 'ko', 'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)

shading interp
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
ax.View = [30.5, 29.2];
ax.XLabel.Position = [5,1];
ax.XTick = 1:6;
ax.XLabel.Position = [3.5, 1,0];
ax.XLabel.Rotation = -11;
ax.YLabel.Position = [6.75, 3.5, 0];
ax.YLabel.Rotation = 32;
ax.YTick = 2:6;
xlabel('Integration Order, $M$', 'Interpreter', 'latex');
ylabel('Polynomial Degree, $P$', 'Interpreter', 'latex');
zlabel('Variance, $\sigma^{2}$', 'Interpreter', 'latex');

saveas(figure5, fp, 'epsc');

figname = 'uq_pce_mean_convergence.eps';
fp = strcat(img_path, figname);
figure6 = figure(6);
M = repmat(1:6, 5, 1)';
P = repmat(2:6, 6, 1);

mesh(M, P, mean)
colormap(gray)
hold on
plot3(M(4,2), P(4,2), mean(4,2), 'ko', 'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)

shading interp
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
ax.View = [30.5, 29.2];
ax.XTick = 1:6;
ax.XLabel.Position = [3.5,1,1.3335];
ax.XLabel.Rotation = -11;
ax.YLabel.Position = [6.8,3.5,1.3335];
ax.YLabel.Rotation = 33;
ax.YTick = 2:6;
xlabel('Integration Order, $M$', 'Interpreter', 'latex');
ylabel('Polynomial Degree, $P$', 'Interpreter', 'latex');
zlabel('Mean, $\mu$', 'Interpreter', 'latex');

saveas(figure6, fp, 'epsc');