figure2 = figure(2);
clf
figname = 'curve_build_top_fold.eps';
fp = strcat(img_path, figname);

% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
hold on; grid on; box on;
xlim([1.25,1.8]);
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)

% OM points
semilogx([1.67,1.67],[min(R_vals(om_idx(1:3))),max(R_vals(om_idx(1:3)))], 'k--')
semilogx(om_vals(om_idx(1:3)), R_vals(om_idx(1:3)), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)


xx = [1.68, 1.695];
yy = [1.2, 1.35];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.67, 1.7];
yy = [1.52, 1.51];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

dim = [1.26, 1.11, 0.155, 0.23];
bx = xy_annotation(gca, 'rectangle', dim, ...
  'LineWidth', 1, 'LineStyle', 'none');

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;

om12_bd = coco_bd_read('fold_find_top');
om12_idx = strcmpi(coco_bd_col(om12_bd, 'TYPE'), 'EP');
idx = 1:numel(om12_idx);
om12_idx = idx(om12_idx);
fake_om12_idx = idx(om12_idx)+4;

om1 = coco_bd_col(om12_bd, 'OM1.om');
rom1 = coco_bd_col(om12_bd, 'OM1.R');
rom2 = coco_bd_col(om12_bd, 'OM2.R');
om = [om1(fake_om12_idx(1)),om1(fake_om12_idx(1))];
R = [rom1(fake_om12_idx(1)), rom2(fake_om12_idx(1))];
fold_x = 1.692;
fold_y = 1.455;
semilogx(fold_x, fold_y, 'ko', 'MarkerSize', marker_size_2)

% Top Fold Box
dim = [fold_x-rect_width/2, ...
  fold_y - rect_height/2, ...
  rect_width, rect_height];
xy_annotation(gca, 'rectangle', dim, ...
  'LineStyle', '--');

axes('Position', bx.Position);
hold on; box on; grid on;
set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)
xlim([1.685, 1.715]);
ylim([1.42, 1.48]);

semilogx(om, R, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_1)
semilogx(om1(om12_idx), rom1(om12_idx), 'ko', 'MarkerSize', marker_size_1)

xy_annotation(gca, 'line', ...
  [om1(fake_om12_idx(1))+0.001,om1(fake_om12_idx(1))+0.01], ...
  [rom1(fake_om12_idx(1)), rom1(fake_om12_idx(1))], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'line', ...
  [om1(fake_om12_idx(1))+0.001,om1(fake_om12_idx(1))+0.01], ...
  [rom2(fake_om12_idx(1)), rom2(fake_om12_idx(1))], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'doublearrow', ...
  [om1(fake_om12_idx(1))+0.007, om1(fake_om12_idx(1))+0.007], ...
  [rom1(fake_om12_idx(1)), rom2(fake_om12_idx(1))], ...
  'LineWidth', 1, 'HeadStyle', 'cback1', ...
  'Head1Width', 6, 'Head2Width', 6);

xy_annotation(gca, 'line', ...
  [om1(fake_om12_idx(1))+0.007, om1(fake_om12_idx(1))+0.007], ...
  [rom2(fake_om12_idx(1))-0.005, rom2(fake_om12_idx(1))], ...
  'LineWidth', 1);

xy_annotation(gca, 'textarrow', ...
  [om1(fake_om12_idx(1))+0.012, om1(fake_om12_idx(1))+0.007], ...
  [rom2(fake_om12_idx(1))-0.015, rom2(fake_om12_idx(1))-0.015], ...
  'String', '$\epsilon_R$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'none', 'FontSize', font_size);

semilogx(fold_x, fold_y, 'ko', 'MarkerSize', marker_size_1)

saveas(figure2, fp, 'epsc');