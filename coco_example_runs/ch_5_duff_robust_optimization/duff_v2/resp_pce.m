function [data, y] = resp_pce(prob, data, u)
%UQ_PCE_COEFFICIENTS Summary of this function goes here
%   Evaluate Response function and calculate coefficients of polynomial
%   chaos expansion.

r = u(1:end-data.Nt);

alphas = u(end-data.Nt+1:end);
R = data.wtd_psi_mat*r;

y = alphas - R;

end