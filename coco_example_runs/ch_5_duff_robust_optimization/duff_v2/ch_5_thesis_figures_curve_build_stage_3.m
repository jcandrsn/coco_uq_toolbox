figure2 = figure(2);
clf
figname = 'curve_build_freq_sweep.eps';
fp = strcat(img_path, figname);

% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
hold on; grid on; box on;
xlim([1.25,1.8]);
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

pk_idx = strcmpi(coco_bd_col(swp_bd, 'TYPE'), 'PK');
om_idx = strcmpi(coco_bd_col(swp_bd, 'TYPE'), 'OM');
idx = 1:numel(pk_idx);
pk_idx = idx(pk_idx);
om_idx = idx(om_idx);

% PK points
semilogx([om_vals(pk_idx(1)),om_vals(pk_idx(2))],[1.4,1.4], 'k--')
semilogx(om_vals(pk_idx), R_vals(pk_idx), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)

% OM points
semilogx([1.67,1.67],[min(R_vals(om_idx)),max(R_vals(om_idx))], 'k--')
semilogx(om_vals(om_idx), R_vals(om_idx), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)
xx = [om_vals(fold_idx(1))+0.05,om_vals(fold_idx(1))];
yy = [1.3,1.39];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'String', 'PK', 'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.55, 1.59];
yy = [1.4, 1.4];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'String', 'PK', 'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.62, 1.66];
yy = [R_vals(om_idx(1)), R_vals(om_idx(1))];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'String', 'OM', 'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.74, 1.675];
yy = [1, R_vals(om_idx(2))-0.01];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'String', 'OM', 'Interpreter', 'latex', ...
  'VerticalAlignment', 'middle', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.74, 1.675];
yy = [1, R_vals(om_idx(3))+0.01];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;

saveas(figure2, fp, 'epsc');