last_run_name = 'fold_find_btm';
run_name = 'uq_nds';
bd = coco_bd_read(last_run_name);
labs = coco_bd_labs(bd, 'UZ');
lab = labs(1);
prob = coco_prob();
prob = coco_set(prob, 'coll', 'NTST', 30);

orbit_name = 'PK1';
orbit_pnames = coco_get_id(orbit_name, pnames);
pk1_bvp_sname = coco_get_id(orbit_name, 'bvp');
pk1_coll_sname = coco_get_id(pk1_bvp_sname, 'seg1.coll');
coll_name = 'PK1.bvp.seg1';
sol = coll_read_solution(coll_name, last_run_name, lab);

coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};
bvp_args = {@duff_bc_v2, @duff_bc_v2_du, @duff_bc_v2_dudu};
% 'uq'-toolbox arguments
uq_args = {{orbit_pnames{4}}, {'Uniform'}, [[0.3, 0.7]], ...
  {}};

prob = uq_isol2bvp_sample(prob, 'PK1', ...
  coll_args{:}, orbit_pnames, bvp_args{:}, uq_args{:});

prob = uq_coll_add_response(prob, 'PK1', 'resp', 'bv', ...
  @x10, @x10_du, @x10_dudu);

seg2ndsid = coco_get_id(orbit_name, 'uq.s_par_glue');
PK1_uq_data = coco_get_func_data(prob, seg2ndsid, 'data');

PK1_loidx = coco_get_func_data(prob, coco_get_id(orbit_name, 'lo'), 'uidx');
% PK1_loaidx = coco_get_adjt_data(prob, coco_get_id(orbit_name, 'lo'), 'axidx');

PK1_upidx = coco_get_func_data(prob, coco_get_id(orbit_name, 'up'), 'uidx');
% PK1_upaidx = coco_get_adjt_data(prob, coco_get_id(orbit_name, 'up'), 'axidx');

orbit_name = 'PK2';
orbit_pnames = coco_get_id(orbit_name, pnames);

sol = coll_read_solution(orbit_name, last_run_name, lab);

coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};
uq_args = {{orbit_pnames{4}}, {'Uniform'}, [[0.3, 0.7]], ...
  {}};

prob = uq_isol2bvp_sample(prob, orbit_name, ...
  coll_args{:}, orbit_pnames, bvp_args{:}, uq_args{:});

seg2ndsid = coco_get_id(orbit_name, 'uq.s_par_glue');
PK2_uq_data = coco_get_func_data(prob, seg2ndsid, 'data');

PK2_loidx = coco_get_func_data(prob, coco_get_id(orbit_name, 'lo'), 'uidx');
% PK2_loaidx = coco_get_adjt_data(prob, coco_get_id(orbit_name, 'lo'), 'axidx');

PK2_upidx = coco_get_func_data(prob, coco_get_id(orbit_name, 'up'), 'uidx');
% PK2_upaidx = coco_get_adjt_data(prob, coco_get_id(orbit_name, 'up'), 'axidx');


% PK1 and PK2 share all parameters except forcing
% frequency.  The nonlinear stiffness is (parameter 4) is
% also omitted as it's now controlled by the uniform
% distribution parameters.
PK1_glue_par_idx = PK1_uq_data.s_idx{1};
PK1_glue_par_idx = PK1_glue_par_idx([1:3,5]);
PK1_glue_par_idx = [PK1_glue_par_idx; PK1_loidx; PK1_upidx];

PK2_glue_par_idx = PK2_uq_data.s_idx{1};
PK2_glue_par_idx = PK2_glue_par_idx([1:3,5]);
PK2_glue_par_idx = [PK2_glue_par_idx; PK2_loidx; PK2_upidx];

prob = coco_add_glue(prob, 'PK1_to_PK2_par_glue', ...
  PK1_glue_par_idx, PK2_glue_par_idx);

% pars_to_release = {orbit_pnames{[1:3,5]}};
% pars_to_release = {pars_to_release{:}, 'lo.PK2.alpha', 'up.PK2.alpha'};

% PK1_adj_glue_par_idx = cell2mat(PK1_uq_data.adj_s_idx);
% PK1_adj_glue_par_idx = PK1_adj_glue_par_idx([1:3,5],:);
% PK1_adj_glue_par_idx = PK1_adj_glue_par_idx(:);
% PK1_adj_glue_par_idx = [PK1_adj_glue_par_idx; PK1_loaidx; PK1_upaidx];
% 
% PK2_adj_glue_par_idx = cell2mat(PK2_uq_data.adj_s_idx);
% PK2_adj_glue_par_idx = PK2_adj_glue_par_idx([1:3,5],:);
% PK2_adj_glue_par_idx = PK2_adj_glue_par_idx(:);
% PK2_adj_glue_par_idx = [PK2_adj_glue_par_idx; PK2_loaidx; PK2_upaidx];
% 
% prob = coco_add_adjt(prob, 'PK1_to_PK2_par_glue', 'aidx', ...
%   [PK1_adj_glue_par_idx; PK2_adj_glue_par_idx]);

% orbit_name = 'OM1';
% orbit_pnames = coco_get_id(orbit_name, pnames);
% om1_coll_sname = coco_get_id(orbit_name, 'bvp.seg1');
% sol = coll_read_solution(om1_coll_sname, last_run_name, lab);
% 
% coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};
% uq_args = {{orbit_pnames{4}}, {'Uniform'}, [[0.3, 0.7]], ...
%   {},'-add-adjt'};
% 
% prob = uq_isol2bvp_sample(prob, orbit_name, ...
%   coll_args{:}, orbit_pnames, bvp_args{:}, uq_args{:});
% 
% orbit_name = 'OM2';
% orbit_pnames = coco_get_id(orbit_name, pnames);
% om1_coll_sname = coco_get_id(orbit_name, 'bvp.seg1');
% sol = coll_read_solution(om1_coll_sname, last_run_name, lab);
% 
% coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};
% uq_args = {{orbit_pnames{4}}, {'Uniform'}, [[0.3, 0.7]], ...
%   {},'-add-adjt'};
% 
% prob = uq_isol2bvp_sample(prob, orbit_name, ...
%   coll_args{:}, orbit_pnames, bvp_args{:}, uq_args{:});
% 
% orbit_name = 'OM3';
% orbit_pnames = coco_get_id(orbit_name, pnames);
% om1_coll_sname = coco_get_id(orbit_name, 'bvp.seg1');
% sol = coll_read_solution(om1_coll_sname, last_run_name, lab);
% 
% coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};
% uq_args = {{orbit_pnames{4}}, {'Uniform'}, [[0.3, 0.7]], ...
%   {},'-add-adjt'};
% 
% prob = uq_isol2bvp_sample(prob, orbit_name, ...
%   coll_args{:}, orbit_pnames, bvp_args{:}, uq_args{:});
% 
coco(prob, 'test', [], 0, {'PK1.resp.mean', 'PK1.resp.var', pars_to_release{:}})
