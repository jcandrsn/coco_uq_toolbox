%% Starting Point Build
% Generate the starting point for uncertainty
% quantification.  Result is six orbits, two on either side
% of the maximum frequency response, two on either side of
% the top fold point and two on either side of the bottom
% fold point.
demo_curve_build

%% PCE Integration Nodes
% Find integration nodes used in the PCE
demo_uq_nds

%% KM Sweep
% Sweep in robust objective function value with k and m
% released in search of a Fold Point and corresponding
% Branch Point
demo_for_thesis_fpbp

%% Eta 1 to zero
% From the branch point, continue along the secondary
% direction where the lagrange multipliers can take on
% non-trivial value and drive d.obj to 1
demo_for_thesis_dobj_to_1