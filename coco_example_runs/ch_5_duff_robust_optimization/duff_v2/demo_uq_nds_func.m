function uq_data = demo_uq_nds_func(M,Pt)
last_run_name = 'fold_find_btm';
run_name = 'uq_nds_M_P_vary';
bd = coco_bd_read(last_run_name);
labs = coco_bd_labs(bd, 'UZ');
lab = labs(1);
prob = coco_prob();
prob = coco_set(prob, 'coll', 'NTST', 30);
prob = coco_set(prob, 'cont', 'h', 5, 'h_min', 1e-5, 'h_max', 1e5);
prob = ode_bvp2bvp(prob, 'PK1', last_run_name, lab);
bvp_data = coco_get_func_data(prob, 'PK1.bvp', 'data');

[pk1_data, pk1_uidx] = coco_get_func_data(prob, ...
  'PK1.bvp.seg1.coll', 'data', 'uidx');
pk1_maps = pk1_data.coll_seg.maps;
pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

% Maintain velocity constraint (could also be done with a
% boundary condition)
prob = coco_add_pars(prob, 'PK1.v0', pk1_x0(2), ...
  'PK1.v0', 'inactive');

prob = ode_coll2coll(prob, 'PK2', last_run_name, lab);

% Boundary conditions for PK2
[pk2_data, pk2_uidx] = coco_get_func_data(prob, 'PK2.coll', 'data', 'uidx');
pk2_maps = pk2_data.coll_seg.maps;

args = pk2_uidx([pk2_maps.T0_idx; pk2_maps.T_idx; pk2_maps.x0_idx; ...
  pk2_maps.x1_idx; pk2_maps.p_idx]);
prob = coco_add_func(prob, 'PK2.bvp', @duff_bc_caf, ...
  @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
  'zero', 'uidx', args);

pk2_x0 = pk2_uidx(pk2_maps.x0_idx);

% Maintain velocity constraint (could also be done with a
% boundary condition)
prob = coco_add_pars(prob, 'PK2.v0', pk2_x0(2), ...
  'PK2.v0', 'inactive');

% Glue Parameters together (except forcing frequency)
prob = coco_add_glue(prob, 'par_glue', pk2_uidx(pk2_maps.p_idx(1:5)), ...
  pk1_uidx(pk1_maps.p_idx(1:5)));
prob = coco_add_glue(prob, 'PK.R.glue', pk2_x0(1), pk1_x0(1));

% Parameterize frequency difference
prob = coco_add_func(prob, 'om_diff', @udiff, @udiff_dU, @udiff_dUdU, ...
  [], 'inactive', 'eps_om', 'uidx', [pk2_uidx(pk2_maps.p_idx(6)), ...
  pk1_uidx(pk1_maps.p_idx(6))]);
prob = coco_set_parival(prob, 'eps_om', 1e-2);
for i=1:3
  OMi_name = strcat('OM', int2str(i));
  prob = ode_bvp2bvp(prob, OMi_name, last_run_name, lab);
  % Boundary conditions for OMi
  [om_data{i}, om_uidx{i}] = coco_get_func_data(prob, ...
    coco_get_id(strcat('OM', int2str(i)),'bvp', 'seg1', 'coll'), 'data', 'uidx');
  om_maps{i} = om_data{i}.coll_seg.maps;

  % Maintain velocity constraint
  v0name = coco_get_id(strcat('OM', int2str(i)),'v0');
  om_x0{i} = om_uidx{i}(om_maps{i}.x0_idx);
  prob = coco_add_pars(prob, v0name, om_x0{i}(2), v0name, 'inactive');

  % Glue Parameters together
  if i > 1
    % The fold approximants share all parameter values.
    % They differ only in response amplitude 
    % {'OM1.R', 'OM2.R', 'OM3.R'}

    gluename = ['OM', int2str(i), '.to.OM1'];
    prob = coco_add_glue(prob, gluename, ...
      om_uidx{1}(om_maps{1}.p_idx), ...
      om_uidx{i}(om_maps{i}.p_idx));

    % Parameterize Response Amplitude Difference
    rdiffname = ['eps_R', int2str(i-1)];
    prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
      [], 'inactive', rdiffname, 'uidx', [om_x0{i-1}(1), om_x0{i}(1)]);
    prob = coco_set_parival(prob, rdiffname, 1e-2);
  else
    gluename = ['OM', int2str(i), '.to.PK1'];
    prob = coco_add_glue(prob, gluename, ...
      pk2_uidx(pk2_maps.p_idx(1:5)), ...
      om_uidx{1}(om_maps{1}.p_idx(1:5)));
  end
end

pars_to_release = {'om'};
%% UQ Toolbox Arguments
args = struct(); opts=struct();
args.spnames = {'c'};
args.spdists = {'Uniform'};
args.spdp = [0.1, 0.5];
args.dpdtpars = {'A'};
opts.addadjt = 1;
%% Initialize UQ Data Structure
bvp_id = coco_get_id('PK1', 'bvp');
bc_data = coco_get_func_data(prob, bvp_id, 'data');  
uq_data = bvp_uq_init_data(bc_data, 'PK1');
prob = coco_set(prob, 'uq', 'M', M, 'Pt', Pt);
uq_data = uq_init_data(prob, uq_data, args, opts);

% Mark the integration node locations with the label 'UQ'
prob = coco_add_event(prob, 'UQ', 'c', uq_data.nds);
bd = coco(prob, run_name, [], 1, ...
  {'c', 'A', pars_to_release{:}},[0.1,0.5]);
end