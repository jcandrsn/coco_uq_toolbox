addpath('../../utils');

img_path = ['/Users/jcoleanderson/Google Drive/',...
 'Grad School/00_Thesis Research/00_Document/Images/'];

%% Duffing Hysteresis
%  Plot the frequency sweep.  The unstable segment plotted
%  with dashed lines, the stable segments with solid lines,
%  the hysteresis transitions shown with arrows.  The
%  forward sweep plotted with a red line, the reverse sweep
%  plotted with a blue segment.  Remove axis numbers.
ch_5_thesis_figures_duff_hysteresis

%% Duffing Multiple Orbits
%  Plot the frequency sweep showing the actual fold points
%  and the approximate fold points.  Show the
%  approximations as insets of the larger figure.
font_size = 20;
marker_size_1 = 10;
marker_size_2 = 7;

ch_5_thesis_figures_curve_build_stage_1
ch_5_thesis_figures_curve_build_stage_2
ch_5_thesis_figures_curve_build_stage_3
ch_5_thesis_figures_curve_build_stage_4
ch_5_thesis_figures_curve_build_stage_5
ch_5_thesis_figures_curve_build_stage_6
ch_5_thesis_figures_curve_build_stage_7

%% Duffing Optimal Continuation Run
ch_5_thesis_figures_optimal_run

%% Sigma Level Study
ch_5_thesis_figures_sigma_level_study

%% Convergence plots:
ch_5_thesis_figures_moment_convergence
