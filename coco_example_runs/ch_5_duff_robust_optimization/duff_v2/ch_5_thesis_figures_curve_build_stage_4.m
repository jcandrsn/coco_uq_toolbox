figure2 = figure(2);
clf

figname = 'curve_build_peak.eps';
fp = strcat(img_path, figname);

% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
hold on; grid on; box on;
xlim([1.25,1.8]);
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)

% PK points
pk_idx = strcmpi(coco_bd_col(swp_bd, 'TYPE'), 'PK');
idx = 1:numel(pk_idx);
pk_idx = idx(pk_idx);

semilogx([om_vals(pk_idx(1)),om_vals(pk_idx(2))],[1.4,1.4], 'k--')
semilogx(om_vals(pk_idx), R_vals(pk_idx), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)

xx = [1.59, 1.63];
yy = [1.4, 1.47];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.695, 1.7];
yy = [1.4, 1.47];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;

dim = [1.26, 1.35, 0.155, 0.23];
bx = xy_annotation(gca, 'rectangle', dim, ...
  'LineWidth', 1, 'LineStyle', 'none');

pk_bd = coco_bd_read('pk');
pk_idx = strcmpi(coco_bd_col(pk_bd, 'TYPE'), 'PK');
idx = 1:numel(pk_idx);
pk_idx = idx(pk_idx);
fake_pk_idx = idx(pk_idx)+2;
ompk1 = coco_bd_col(pk_bd, 'om');
rpk1 = coco_bd_col(pk_bd, 'R.PK1');
ompk2 = coco_bd_col(pk_bd, 'pk2.om');
rpk2 = coco_bd_col(pk_bd, 'R.PK2');
om = [ompk1(fake_pk_idx),ompk2(fake_pk_idx)];
R = [rpk1(fake_pk_idx), rpk2(fake_pk_idx)];
semilogx(ompk1(pk_idx-3), rpk1(pk_idx-3), 'ko', 'MarkerSize', marker_size_2)

rect_width = 0.03;
rect_height = 0.06;

% Peak Box
dim = [ompk1(pk_idx-3)-rect_width/2, ...
  rpk1(pk_idx-3) - rect_height/2, ...
  rect_width, rect_height];
xy_annotation(gca, 'rectangle', dim, ...
  'LineStyle', '--');

axes('Position', bx.Position);
hold on; box on; grid on;
set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
xlim([1.64, 1.7]);
ylim([1.475, 1.535]);

semilogx(om, R, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_1)
semilogx(ompk1(pk_idx-3), rpk1(pk_idx-3), 'ko', 'MarkerSize', marker_size_1)

xy_annotation(gca, 'line', ...
  [ompk1(fake_pk_idx),ompk1(fake_pk_idx)], ...
  [rpk1(fake_pk_idx)+0.005, rpk1(fake_pk_idx)+0.025], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'line', ...
  [ompk2(fake_pk_idx),ompk2(fake_pk_idx)], ...
  [rpk2(fake_pk_idx)+0.005, rpk2(fake_pk_idx)+0.025], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'doublearrow', ...
  [ompk1(fake_pk_idx),ompk2(fake_pk_idx)], ...
  [rpk1(fake_pk_idx)+0.02, rpk2(fake_pk_idx)+0.02], ...
  'LineWidth', 1, 'HeadStyle', 'cback1', ...
  'Head1Width', 6, 'Head2Width', 6);

xy_annotation(gca, 'textarrow', ...
  [ompk1(fake_pk_idx)-0.01,ompk1(fake_pk_idx)], ...
  [rpk1(fake_pk_idx)+0.02, rpk2(fake_pk_idx)+0.02], ...
  'String', '$\epsilon_\omega$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'none', 'FontSize', font_size);

saveas(figure2, fp, 'epsc');