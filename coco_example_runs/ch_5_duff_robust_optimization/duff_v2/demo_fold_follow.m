% Path to vector field, Jacobians, and Response Function
addpath('../duff')
addpath('../linode')

% Initialize problem instance and set options
prob_init = coco_prob();
prob_init = coco_set(prob_init, 'ode', 'autonomous', false);
prob_init = coco_set(prob_init, 'coll', 'NTST', 15, ...
  'NCOL', 4);
prob_init = coco_set(prob_init, 'cont', 'PtMX', 100, ...
  'h', 0.5, 'almax', 30, 'h_max', 1000);

% Let Transients die out
p0 = [3;0.46365;1];
[~, x0]   = ode45(@(t,x) linode_het(t, x, p0), ...
  [0 6*pi], [1; 0]);

% Initial Solution Guess
[t0, x0]   = ode45(@(t,x) linode_het(t, x, p0), ...
  [0 2*pi], [x0(end,:)']);

% 'coll'-toolbox arguments
coll_args = {@linode_het, ...
  @linode_het_DFDX, @linode_het_DFDP, @linode_het_DFDT, ...
  @linode_het_DFDXDX, @linode_het_DFDXDP, ...
  @linode_het_DFDPDP, @linode_het_DFDXDT, ...
  @linode_het_DFDTDP, @linode_het_DFDTDT, ...
  t0, x0, p0};

% parameter names
pnames = {'k','phi', 'om'};

% 'bvp'-toolbox arguments
bvp_args = {@fbc_x10, @Jbc_x10, @dJbc_x10};

prob = ode_isol2bvp(prob_init, '', coll_args{:}, pnames, bvp_args{:});
[data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
maps = data.coll_seg.maps;
x0 = uidx(maps.x0_idx);
prob = coco_add_pars(prob, 'A', x0(1), 'A', 'inactive');

bd = coco(prob, 'test', [], 1, {'A', 'phi', 'om', 'k', 'atlas.test.FP'});

%%
FPLab = coco_bd_labs(bd, 'FP');

prob = prob_init;
prob = ode_bvp2bvp(prob, '', 'test', FPLab(1));
[data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
maps = data.coll_seg.maps;
x0 = uidx(maps.x0_idx);
prob = coco_add_pars(prob, 'A', x0(1), 'A', 'inactive');
prob = coco_xchg_pars(prob, 'k', 'atlas.test.FP');
bd = coco(prob, 'test', [], 1, {'A', 'phi', 'om', 'k', 'atlas.test.FP'});