bd4 = coco_bd_read('duff_freq_sweep_M=4_P=4');
% bd8 = coco_bd_read('duff_freq_sweep_M=8_P=4');
% bd10 = coco_bd_read('duff_freq_sweep_M=10_P=4');
% bd12 = coco_bd_read('duff_freq_sweep_M=12_P=4');
% 
figure(1)
hold on

mu.R = cell2mat(bd4(2:end, 31));
om = cell2mat(bd4(2:end, 24));
[~, min_idx] = min(abs(om - 0.95));
[~, max_idx] = min(abs(om - 1.05));
semilogx(om([min_idx:max_idx]), mu.R([min_idx:max_idx]));
% 
% mu.R = cell2mat(bd8(2:end, 47));
% om = cell2mat(bd8(2:end, 36));
% [~, min_idx] = min(abs(om - 0.95));
% [~, max_idx] = min(abs(om - 1.05));
% semilogx(om([min_idx:max_idx]), mu.R([min_idx:max_idx]));
% 
% mu.R = cell2mat(bd10(2:end, 55));
% om = cell2mat(bd10(2:end, 42));
% [~, min_idx] = min(abs(om - 0.95));
% [~, max_idx] = min(abs(om - 1.05));
% semilogx(om([min_idx:max_idx]), mu.R([min_idx:max_idx]));
% 
% mu.R = cell2mat(bd12(2:end, 63));
% om = cell2mat(bd12(2:end, 48));
% [~, min_idx] = min(abs(om - 0.95));
% [~, max_idx] = min(abs(om - 1.05));
% semilogx(om([min_idx:max_idx]), mu.R([min_idx:max_idx]));

% axis([0.95 1.05 0 4])
% legend('M=4', 'M=8', 'M=10')


M=4;
run_name = 'duff_freq_sweep_M=4_P=4';
% bd = coco_bd_read(run_name);
labs = coco_bd_labs(bd4);
data_mat = zeros(size(labs,2), M+1);
j = 1;
for lab=labs
  for i=1:M
    curve_name = strcat('duff.uq.sample', int2str(i));    
    sol = coll_read_solution(curve_name, run_name, lab);
    data_mat(j, i+1) = sol.xbp(1,1);
  end
  data_mat(j,1) = sol.p(6);
  j = j + 1;
end
legend('show')
axis([0.98 1.02 0 3])
set(gca, 'xscale', 'log')
grid on