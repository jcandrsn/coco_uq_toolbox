%% Duffing Optimal Continuation Run

font_size = 20;
figname = 'uq_opt_k-m-obj.eps';
fp = strcat(img_path, figname);
uq_swp_bd = coco_bd_read('uq_sweep');

k = coco_bd_col(uq_swp_bd, 'k');
m = coco_bd_col(uq_swp_bd, 'm');
obj = coco_bd_col(uq_swp_bd, 'obj');

[obj_max, max_idx] = max(obj);

figure3 = figure(3);
hold on; grid on
plot3(m, k, obj, 'k', 'LineWidth', 1)
plot3(m(max_idx), k(max_idx), obj_max, 'ko', 'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)

xlim([0.9,3.1])
ylim([0.9,3.1])
zlim([0.7,1.15])
xlabel('Mass, $m$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Stiffness, $k$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
zlabel('Objective', 'Interpreter', ...
  'latex', 'FontSize', font_size)
view([-114,14])
ax = gca;
ax.XTick = 1:0.5:3;
ax.YTick = 1.5:0.5:3;
ax.ZTick = 0.7:0.05:1.15;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
saveas(figure3, fp, 'epsc');
clf

figname = 'uq_opt_k-m.eps';
fp = strcat(img_path, figname);

plot(k, m, 'k-', 'LineWidth', 1);
hold on; grid on; box on;
plot(k(max_idx), m(max_idx), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)
xlabel('Stiffness, $k$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Mass, $m$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

xlim([0.9,3.1])
ylim([0.9,3.1])

ax = gca;
ax.XTick = 1:0.5:3;
ax.YTick = 1:0.5:3;
ax.ZTick = 0.8:0.05:1.3;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
saveas(figure3, fp, 'epsc');
clf

figname = 'uq_opt_k-obj.eps';
fp = strcat(img_path, figname);

plot(k, obj, 'k-', 'LineWidth', 1);
hold on; grid on; box on;
xlabel('Stiffness, $k$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Objective', 'Interpreter', ...
  'latex', 'FontSize', font_size)
plot(k(max_idx), obj_max, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)

xlim([0.9,3.1])
ylim([0.7,1.15])

ax = gca;
ax.XTick = 1:0.5:3;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
saveas(figure3, fp, 'epsc');
clf

figname = 'uq_opt_obj-m.eps';
fp = strcat(img_path, figname);

plot(obj, m, 'k-', 'LineWidth', 1);
hold on; grid on; box on;
xlabel('Objective', 'Interpreter', ...
  'latex', 'FontSize', font_size);
ylabel('Mass, $m$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
plot(obj_max, m(max_idx), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)

xlim([0.7,1.15])
ylim([0.9,3.1])

ax = gca;
ax.YTick = 1:0.5:3;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
saveas(figure3, fp, 'epsc');

