figure2 = figure(2);
clf
figname = 'curve_build_final.eps';
fp = strcat(img_path, figname);

if ~coco_exist('uq_sweep_2', 'run')
  addpath('../../duff')
  addpath('../../utils')  
  last_run_name = 'fold_find_btm';

  bd = coco_bd_read(last_run_name);
  labs = coco_bd_labs(bd, 'UZ');
  lab = labs(1);
  prob = coco_prob();
  prob = coco_set(prob, 'coll', 'NTST', 30);
  
  prob = ode_bvp2bvp(prob, 'PK1', last_run_name, lab);
  bvp_data = coco_get_func_data(prob, 'PK1.bvp', 'data');

  [pk1_data, pk1_uidx] = coco_get_func_data(prob, ...
    'PK1.bvp.seg1.coll', 'data', 'uidx');
  pk1_maps = pk1_data.coll_seg.maps;
  pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

  % Maintain velocity constraint (could also be done with a
  % boundary condition)
  prob = coco_add_pars(prob, 'PK1.v0', pk1_x0(2), ...
    'PK1.v0', 'inactive');
  prob = coco_add_pars(prob, 'R', pk1_x0(1), ...
    'R', 'inactive');
  bd = coco(prob, 'uq_sweep_2', [], {'om', 'R'});
else
  bd = coco_bd_read('uq_sweep_2');
end

semilogx(coco_bd_col(bd, 'om'), coco_bd_col(bd, 'R'), 'k-', 'LineWidth', 1)
hold on; grid on; box on;
main_axis = gca;
xlim([1.25, 1.8]);

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;

dim = [1.26, 0.7, 0.155, 0.23];
bx = xy_annotation(main_axis, 'rectangle', dim, ...
  'LineWidth', 1, 'LineStyle', 'none');

btm_fold_bd = coco_bd_read('fold_find_btm');
idx = strcmpi(coco_bd_col(btm_fold_bd, 'TYPE'), 'UZ');
idx2 = 1:numel(idx);
idx = idx2(idx);

ompk1 = coco_bd_col(btm_fold_bd, 'om');
rpk1 = coco_bd_col(btm_fold_bd, 'R');
omom = coco_bd_col(btm_fold_bd, 'OM.om');
om1r = coco_bd_col(btm_fold_bd, 'OM1.R');

semilogx([ompk1(idx), ompk1(idx)+0.01, omom(idx), omom(idx), omom(idx)], ...
  [rpk1(idx), rpk1(idx), om1r(idx), om1r(idx)-0.01, om1r(idx)-0.02], ...
  'ko', 'MarkerSize', marker_size_2, 'MarkerFaceColor', [0.5,0.5,0.5]);

saveas(figure2, fp, 'epsc');