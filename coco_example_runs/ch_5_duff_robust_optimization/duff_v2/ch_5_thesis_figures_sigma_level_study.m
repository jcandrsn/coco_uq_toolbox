figure4 = figure(4);
font_size = 15;
figname = 'opt_change_w_n_stdev.eps';
fp = strcat(img_path, figname);
uq_swp_bd = coco_bd_read('uq_sweep_no_adjt_for_plot_m=1_n=1');
mu = coco_bd_col(uq_swp_bd, 'resp.mean');
var = coco_bd_col(uq_swp_bd, 'resp.var');
% Plot the k-m circle
k = linspace(1,3,100);
m_star = sqrt(1 - (k-2).^2);
plot([k, fliplr(k)], [m_star+2, -m_star+2], 'k-', 'LineWidth', 1);
hold on; grid on; box on;
for i=[0,1]
    for j=0:6
      run_name = ['uq_sweep_no_adjt_for_plot_m=',int2str(i),'_n=',int2str(j)];
      if coco_exist(run_name, 'run')
        bd = coco_bd_read(run_name);
        fp_idx = strcmpi(coco_bd_col(bd, 'TYPE'), 'FP');
        k = coco_bd_col(bd, 'k');
        m = coco_bd_col(bd, 'm');
        obj = coco_bd_col(bd, 'obj');
        obj_fp = obj(fp_idx);
        k_fp = k(fp_idx);
        m_fp = m(fp_idx);
        [obj_max, max_idx] = max(obj_fp);

        k_max = k_fp(max_idx);
        m_max = m_fp(max_idx);
        angle = atan2(m_max-2, k_max-2);

        x = 0.6*cos(angle)+2;
        y = 0.6*sin(angle)+2;

        if i == 0
          arrow_label = '-$\sigma$';
          plot(k_max, m_max, 'ko', ...
            'MarkerSize', 7);        
        else
          plot(k_max, m_max, 'ko', ...
            'MarkerFaceColor', [0.5,0.5,0.5], ...
            'MarkerSize', 7);
          if j == 0
            arrow_label = '$\mu$';
          else
            arrow_label = ['$\mu - ', int2str(j), '\sigma$'];
          end
        end
        switch j
          case 0
            x = x-0.5;
          case 1
            if i==1
              x = x-0.25;
            end
        end
        xy_annotation(gca, 'textarrow', ...
          [x, k_max], ...
          [y, m_max], ...
          'String', arrow_label, ...
          'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
          'cback1', 'FontSize', font_size, 'HeadWidth', 6);
      end
    end    
end

xlabel('Stiffness, $k$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Mass, $m$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.XTick = 1:0.5:3;
ax.YTick = 1:0.5:3;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
saveas(figure4, fp, 'epsc');