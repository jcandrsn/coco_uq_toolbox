figure2 = figure(2);
clf

figname = 'curve_build_initial_phase.eps';
fp = strcat(img_path, figname);

xlim([0, 2*pi])
hold on; grid on; box on;
[sol, ~] = coll_read_solution('bvp.seg1', 'set_v0', 1);
pos = plot(sol.tbp, sol.xbp(:,1), 'k', 'DisplayName','Position', 'LineWidth', 1);
vel = plot(sol.tbp, sol.xbp(:,2), 'k--', 'DisplayName','Velocity', 'LineWidth', 1);

lgd = legend([pos, vel]);
lgd.Interpreter = 'latex';
lgd.Location = 'southeast';

xlim([0, 2*pi])
xlabel('Time, $t$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.XTick = [0,pi/2, pi, 3*pi/2, 2*pi];
ax.TickLabelInterpreter = 'latex';
ax.XTickLabel = {'$0$', '$\frac{\pi}{2}$', '$\pi$', '$\frac{3\pi}{2}$', '$2\pi$'};
ax.FontSize = font_size;

saveas(figure2, fp, 'epsc');