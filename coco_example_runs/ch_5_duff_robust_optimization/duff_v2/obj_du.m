function [data, J] = obj_du(prob, data, u)

mu = u(1);
var = u(2);

m = 1;
n = 1;
J = [m, n*(-1/2)*(var)^(-1/2)];

end