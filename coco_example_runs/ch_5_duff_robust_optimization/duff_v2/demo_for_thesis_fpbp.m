last_run_name = 'uq_nds';
run_name = 'uq_sweep';
add_adjt = 1;

% Initialize problem data structure
prob = coco_prob();
prob = coco_set(prob, 'coll', 'NTST', 30);
prob = coco_set(prob, 'cont', 'PtMX', 50);
prob = coco_set(prob, 'cont', 'h', 5, 'h_min', 1e-5, 'h_max', 1e5);
% prob = coco_set(prob, 'corr', 'TOL', 1e-5);
% prob = coco_set(prob, 'corr', 'ItNW', 30);
% For testing only
% prob = coco_set(prob, 'coll', 'TOL', 100);

% Previous run found points to be used for PCE numerical
% integration.  Read that data and identify those points.
bd = coco_bd_read(last_run_name);
uq_labs = coco_bd_labs(bd, 'UQ');

% Sample counter
sample_idx = 0;
om_counter = 0;
% Stochastic parameter indices
pk1_par_idx = cell(1, uq_data.nsamples);
% Response function initial guesses
r_igs = cell(1, uq_data.nsamples);
% Response function input indices
r_idx = cell(1, uq_data.nsamples);
% Stochastic parameter adjoint indices
pk1_par_aidx = cell(1, uq_data.nsamples);
% Response function input adjoint indices
r_aidx = cell(1, uq_data.nsamples);

% drdiffnames = cell(2*uq_data.nsamples, 1);

% Build samples in this for loop
for lab=uq_labs
  sample_idx = sample_idx + 1;
  % Sample name prefix
  sample_id = strcat('s', int2str(sample_idx));
  
  %% PK1
  %  Read data from previous run
  coll_name = 'PK1.bvp.seg1';
  sol = coll_read_solution(coll_name, last_run_name, lab);
  coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};  
  
  %  PK1 sample names for current run
  pk1_sname = coco_get_id(sample_id, 'PK1');
  pk1_bvp_sname = coco_get_id(pk1_sname, 'bvp');
  pk1_coll_sname = coco_get_id(pk1_bvp_sname, 'seg1.coll');

  % bvp_args defined in demo_curve_build
  bvp_pnames = {};%uq_get_sample_par_names(pnames, sample_idx);
  
  prob = ode_isol2bvp(prob, pk1_sname, ...
    coll_args{:}, bvp_pnames, bvp_args{:});
  % Adjoints
  if add_adjt
    prob = adjt_isol2bvp(prob, pk1_sname);
  end

  if sample_idx == 1
    % The deterministic parameters are added when building
    % the first sample of PK1
    s_pnames = {pnames{1}, 'c.1', pnames{[3,4]}, 'A.1', pnames{6}};

    % Keep the data, uidx, and u0 for s1.PK1 for later
    % gluing conditions
    [pk1_s1_data, pk1_s1_uidx] = ...
      coco_get_func_data(prob, pk1_coll_sname, ...
      'data', 'uidx');
    pk1_s1_maps = pk1_s1_data.coll_seg.maps;

    s1_par_fname = coco_get_id(sample_id, 'pars');
    s1_par_pname = s_pnames([1,3,4]);
    s1_par_idx = pk1_s1_uidx(pk1_s1_maps.p_idx([1,3,4]));
    prob = coco_add_pars(prob, s1_par_fname, ...
      s1_par_idx, s1_par_pname);    

    % Adjoints
    if add_adjt
      [s1_pk1_fdata, s1_pk1_aidx] = ...
        coco_get_adjt_data(prob, ...
        pk1_coll_sname, 'data', 'axidx');
      s1_pk1_opt = s1_pk1_fdata.coll_opt;

      prob = coco_add_adjt(prob, s1_par_fname, ...
      coco_get_id('d', s1_par_pname), ...
      'aidx', s1_pk1_aidx(s1_pk1_opt.p_idx([1,3,4])), ...
      'l0', zeros(3,1));
    end
  else
    % Glue deterministic parameters together
    [pk1_si_data, pk1_si_uidx] = ...
      coco_get_func_data(prob, pk1_coll_sname, ...
                         'data', 'uidx');
    pk1_si_maps = pk1_si_data.coll_seg.maps;
    s1_p_idx = pk1_s1_uidx(pk1_s1_maps.p_idx([1,3,4]));
    si_p_idx = pk1_si_uidx(pk1_si_maps.p_idx([1,3,4]));

    glue_name = coco_get_id(sample_id, 'glue_to_s1');
    prob = coco_add_glue(prob, glue_name, s1_p_idx, ...
      si_p_idx);

    % Adjoints
    if add_adjt

      [si_pk1_fdata, si_pk1_aidx] = coco_get_adjt_data(prob, ...
        pk1_coll_sname, 'data', 'axidx');
      si_pk1_opt = si_pk1_fdata.coll_opt;

      % For glue
      s1_p_aidx = s1_pk1_aidx(s1_pk1_opt.p_idx([1,3,4]));
      si_p_aidx = si_pk1_aidx(si_pk1_opt.p_idx([1,3,4]));

      prob = coco_add_adjt(prob, glue_name, ...
        'aidx', [s1_p_aidx; si_p_aidx]);
    end
  end
  
  [pk1_data, pk1_uidx, pk1_u0] = ...
    coco_get_func_data(prob, pk1_coll_sname, ...
    'data', 'uidx', 'u0');
  pk1_maps = pk1_data.coll_seg.maps;
  pk1_x0 = pk1_uidx(pk1_maps.x0_idx);
  
  pk1_par_idx{sample_idx} = pk1_uidx(pk1_maps.p_idx);
  r_igs{sample_idx} = pk1_u0(pk1_maps.x0_idx(1));
  r_idx{sample_idx} = pk1_uidx(pk1_maps.x0_idx(1));

%   % Damping names
%   si_c_fname = coco_get_id(sample_id, 'c');
%   si_c_pname = coco_get_id('c', int2str(sample_idx));
%   si_c_idx = pk1_uidx(pk1_maps.p_idx(2));
%   prob = coco_add_pars(prob, si_c_fname, ...
%     si_c_idx, si_c_pname, 'inactive');
  
  % Forcing amplitude names
  si_A_fname = coco_get_id(sample_id, 'A');
  si_A_pname = coco_get_id('A', int2str(sample_idx));
  si_A_idx = pk1_uidx(pk1_maps.p_idx(5));
  prob = coco_add_pars(prob, si_A_fname, ...
    si_A_idx, si_A_pname, 'inactive');
  
  % Forcing frequency names
  si_om_fname = coco_get_id(sample_id, 'om');
  si_om_pname = coco_get_id('om', int2str(sample_idx));
  si_om_idx = pk1_uidx(pk1_maps.p_idx(6));
  prob = coco_add_pars(prob, si_om_fname, ...
    si_om_idx, si_om_pname, 'inactive');

  % Velocity constraint for PK1
  pk1_v0_sname = coco_get_id(pk1_sname, 'v0');
  prob = coco_add_pars(prob, pk1_v0_sname, pk1_x0(2), ...
    pk1_v0_sname, 'inactive');
  
  if add_adjt
    [pk1_adata, pk1_aidx] = coco_get_adjt_data(prob, ...
      pk1_coll_sname, 'data', 'axidx');
    pk1_opt = pk1_adata.coll_opt;
    
    pk1_par_aidx{sample_idx} = pk1_aidx(pk1_opt.p_idx);
    r_aidx{sample_idx} = pk1_aidx(pk1_opt.x0_idx(1)); 

%     % Damping
%     prob = coco_add_adjt(prob, si_c_fname, ...
%       coco_get_id('d', si_c_pname), ...
%       'aidx', pk1_aidx(pk1_opt.p_idx(2)), ...
%       'l0', 0);
    
    % Forcing Amplitude
    prob = coco_add_adjt(prob, si_A_fname, ...
      coco_get_id('d', si_A_pname), ...
      'aidx', pk1_aidx(pk1_opt.p_idx(5)), ...
      'l0', 0);  
    
    % Forcing frequency
    prob = coco_add_adjt(prob, si_om_fname, ...
      coco_get_id('d', si_om_pname), ...
      'aidx', pk1_aidx(pk1_opt.p_idx(6)), ...
      'l0', 0);

    % Velocity constraint
    dv10_sname = coco_get_id('d', pk1_v0_sname);
    prob = coco_add_adjt(prob, pk1_v0_sname, ...
      dv10_sname, ...
      'aidx', pk1_aidx(pk1_opt.x0_idx(2)), ...
      'l0', 0);
  end
  
  %% PK2
  % Read data from previous run.  PK2 was added with
  % coll2coll in the previous run, so it doesn't have the
  % bvp.seg1 identifiers.
  coll_name = 'PK2';
  sol = coll_read_solution(coll_name, last_run_name, lab);
  
  %  PK2 sample names for current run
  pk2_sname = coco_get_id(sample_id, 'PK2');
  coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};
  prob = ode_isol2bvp(prob, pk2_sname, ...
    coll_args{:}, {}, bvp_args{:});

  % PK2 was added with isol2bvp in this run, so reading the
  % data requires the bvp identifiers.
  pk2_coll_sname = coco_get_id(pk2_sname, 'bvp', 'seg1', 'coll');

  [pk2_data, pk2_uidx] = coco_get_func_data(prob, ...
    pk2_coll_sname, 'data', 'uidx');
  pk2_maps = pk2_data.coll_seg.maps;
  pk2_x0 = pk2_uidx(pk2_maps.x0_idx);

  % Velocity constraint for PK2
  pk2_v0_sname = coco_get_id(pk2_sname, 'v0');
  prob = coco_add_pars(prob, pk2_v0_sname, pk2_x0(2), ...
    pk2_v0_sname, 'inactive');

  % Glue Parameters together (except forcing frequency)
  parglue_sname = coco_get_id(sample_id, ...
    'par_glue_PK2_to_PK1');
  prob = coco_add_glue(prob, parglue_sname, ...
    pk2_uidx(pk2_maps.p_idx(1:5)), ...
    pk1_uidx(pk1_maps.p_idx(1:5)));
  
  pk_r_glue_sname = coco_get_id(sample_id, ...
    'R_glue_to_PK1');
  prob = coco_add_glue(prob, pk_r_glue_sname, ...
    pk2_x0(1), pk1_x0(1));

  % Lock in the frequency difference
  pk_om_diff_sname = coco_get_id(sample_id, ...
    'PK_om_diff');

  prob = coco_add_func(prob, pk_om_diff_sname, ...
    @udiff, @udiff_dU, @udiff_dUdU, [], ...
    'inactive', pk_om_diff_sname, 'uidx', ...
    [pk2_uidx(pk2_maps.p_idx(6)), ...
     pk1_uidx(pk1_maps.p_idx(6))]);

  % Adjoints
  if add_adjt
    % ode
    prob = adjt_isol2bvp(prob, pk2_sname);
    [pk2_fdata, pk2_aidx] = coco_get_adjt_data(prob, ...
      pk2_coll_sname, 'data', 'axidx');
    pk2_opt = pk2_fdata.coll_opt;

    % velocity constraint
    dv20_sname = coco_get_id('d', pk2_v0_sname);
    prob = coco_add_adjt(prob, pk2_v0_sname, ...
      dv20_sname, ...
      'aidx', pk2_aidx(pk2_opt.x0_idx(2)));

    % glue
    prob = coco_add_adjt(prob, parglue_sname, ...
      'aidx', [pk2_aidx(pk2_opt.p_idx(1:5)); ...
      pk1_aidx(pk1_opt.p_idx(1:5))]);

    prob = coco_add_adjt(prob, pk_r_glue_sname, ...
      'aidx', [pk2_aidx(pk2_opt.x0_idx(1)); ...
      pk1_aidx(pk1_opt.x0_idx(1))]);
    
    pk_om_diff_dpname = coco_get_id('d', pk_om_diff_sname);
    pk_om_diff_dpnames{sample_idx} = pk_om_diff_dpname;  
    
    % om constraint
    prob = coco_add_adjt(prob, pk_om_diff_sname, ...
      pk_om_diff_dpname, ...
      'aidx', [pk2_aidx(pk2_opt.p_idx(6)), ...
      pk1_aidx(pk1_opt.p_idx(6))]);

  end
  %% OMs
  % Build OM orbits for the sample in a for loop
  om_fdata = cell(3,1);
  om_aidx = cell(3,1);
  om_opt = cell(3,1);
  om_data = cell(3,1);
  om_uidx = cell(3,1);
  om_maps = cell(3,1);
  om_x0 = cell(3,1);
  om_x0a = cell(3,1);
  
  for om_idx=1:3
    om_id = strcat('OM', int2str(om_idx));
    om_sname = coco_get_id(sample_id, om_id);
    om_seg_name = coco_get_id(om_id, 'bvp', 'seg1');
    sol = coll_read_solution(om_seg_name, last_run_name, lab);

    coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};
    prob = ode_isol2bvp(prob, om_sname, ...
      coll_args{:}, {}, bvp_args{:});

    if add_adjt
      prob = adjt_isol2bvp(prob, om_sname);
    end
    
    om_coll_sname = coco_get_id(om_sname, 'bvp', 'seg1', 'coll');
    [om_data{om_idx}, om_uidx{om_idx}] = coco_get_func_data(prob, ...
      om_coll_sname, 'data', 'uidx');
    om_maps{om_idx} = om_data{om_idx}.coll_seg.maps;

    % Maintain velocity constraint
    v0sname = coco_get_id(sample_id, ['OM', int2str(om_idx)],'v0');
    om_x0{om_idx} = om_uidx{om_idx}(om_maps{om_idx}.x0_idx);
    prob = coco_add_pars(prob, v0sname, om_x0{om_idx}(2), v0sname, ...
      'inactive');
    
    if add_adjt
      [om_fdata{om_idx}, om_aidx{om_idx}] = coco_get_adjt_data(prob, ...
        om_coll_sname, 'data', 'axidx');
      om_opt{om_idx} = om_fdata{om_idx}.coll_opt;
      om_x0a{om_idx} = om_aidx{om_idx}(om_opt{om_idx}.x0_idx);

      dv0sname = coco_get_id('d', v0sname);
      prob = coco_add_adjt(prob, v0sname, dv0sname, ...
        'aidx', om_x0a{om_idx}(2));
    end

    % Glue Parameters together
    if om_idx > 1
      % The fold approximants share all parameter values.
      % They differ only in response amplitude 
      gluename = coco_get_id(sample_id, ['par_glue_OM', ...
        int2str(om_idx), '_to_OM1']);

      prob = coco_add_glue(prob, gluename, ...
        om_uidx{1}(om_maps{1}.p_idx), ...
        om_uidx{om_idx}(om_maps{om_idx}.p_idx));

      % Parameterize Response Amplitude Difference
      rdiffname = ['OM', int2str(om_idx-1), 'R_minus_OM', int2str(om_idx), 'R'];
      rdiffname = coco_get_id(sample_id, rdiffname);
      prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
        [], 'inactive', rdiffname, 'uidx', [om_x0{om_idx-1}(1), om_x0{om_idx}(1)]);
      
      if add_adjt
        prob = coco_add_adjt(prob, gluename, ...
          'aidx', [om_aidx{1}(om_opt{1}.p_idx); ...
          om_aidx{om_idx}(om_opt{om_idx}.p_idx)]);

        om_counter = om_counter + 1;
        drdiffname = coco_get_id('d', rdiffname);
        drdiffnames{om_counter} = drdiffname;
        prob = coco_add_adjt(prob, rdiffname, drdiffname, ...
          'aidx', [om_x0a{om_idx-1}(1), om_x0a{om_idx}(1)]);
      end

    else
      gluename = coco_get_id(sample_id, ['par_glue_OM', ...
        int2str(om_idx), '_to_PK1']);

      prob = coco_add_glue(prob, gluename, ...
        pk1_uidx(pk1_maps.p_idx(1:5)), ...
        om_uidx{1}(om_maps{1}.p_idx(1:5)));
      
      omomname = coco_get_id(sample_id, coco_get_id(om_id, 'om'));
      prob = coco_add_pars(prob, omomname, ...
        om_uidx{om_idx}(om_maps{om_idx}.p_idx(6)), ...
        omomname, 'inactive');
      
      if add_adjt
        prob = coco_add_adjt(prob, gluename, ...
          'aidx', [pk1_aidx(pk1_opt.p_idx(1:5)); ...
          om_aidx{1}(om_opt{1}.p_idx(1:5))]);

        domomname = coco_get_id('d', omomname);
        prob = coco_add_adjt(prob, omomname, domomname, ...
                'aidx', om_aidx{om_idx}(om_opt{om_idx}.p_idx(6)));
      end
    end
  end
end

psi_mat = uq_make_psi_mat(uq_data.nds_grid, uq_data.uq.Pt, uq_data.spdists);
uq_data.wtd_psi_mat = psi_mat*diag(uq_data.wts);
uq_data.s_idx = pk1_par_idx;
if add_adjt
  uq_data.adj_s_idx = pk1_par_aidx;
end
uq_data.addadjt=add_adjt;
prob = uq_add_sample_nodes(prob, uq_data);
% Final data structure for a UQ sample is stored in the
% parameter glue function
uq_data = coco_get_func_data(prob, 'PK1.uq.s_par_glue', 'data');

igs = zeros(uq_data.nsamples, 1);
idx = zeros(1, uq_data.nsamples);
for i=1:uq_data.nsamples
  igs(i) = r_igs{i};
  idx(i) = [r_idx{i}];
end

alpha_ig = uq_data.wtd_psi_mat*igs;
response_id = 'resp';
prob = coco_add_func(prob, response_id, ...
  @resp_pce, @resp_pce_dU, @resp_pce_dUdU, ...
  uq_data, 'zero', 'uidx', idx(:), 'u0', alpha_ig);

if add_adjt
  aidx = zeros(1, uq_data.nsamples);
  for i=1:uq_data.nsamples
    aidx(i) = [r_aidx{i}];
  end

  prob = coco_add_adjt(prob, response_id, ...
    'aidx', aidx(:), 'l0', zeros(size(alpha_ig)));

end

alpha_idx = coco_get_func_data(prob, response_id, 'uidx');
alpha_idx = alpha_idx(end-uq_data.Nt+1:end);

if add_adjt
  alpha_aidx = coco_get_adjt_data(prob, response_id, 'axidx');
  alpha_aidx = alpha_aidx(end-uq_data.Nt+1:end);
end

% Mean Zero Function
mean_id = coco_get_id(response_id, 'pce_mean');
prob = coco_add_func(prob, mean_id, ...
  @uq_pce_mean, @uq_pce_mean_dU, @uq_pce_mean_dUdU, ...
  uq_data, 'zero', 'uidx', alpha_idx, 'u0', alpha_ig(1));

if add_adjt  
  prob = coco_add_adjt(prob, mean_id, 'aidx', ...
    alpha_aidx, 'l0', 0);
end

% Variance Zero Function
var_id = coco_get_id(response_id, 'pce_variance');
prob = coco_add_func(prob, var_id, ...
  @uq_pce_variance, @uq_pce_variance_dU, @uq_pce_variance_dUdU,...
  uq_data, 'zero', 'uidx', alpha_idx, 'u0', sum(alpha_ig(2:end).^2));

if add_adjt
  prob = coco_add_adjt(prob, var_id, 'aidx', ...
    alpha_aidx, 'l0', 0);
end

obj_id = 'obj';
mean_idx = coco_get_func_data(prob, mean_id, 'uidx');
var_idx = coco_get_func_data(prob, var_id, 'uidx');

prob = coco_add_func(prob, obj_id, ...
  @obj, @obj_du, @obj_dudu, [], ...
  'inactive', obj_id, 'uidx', ...
  [mean_idx(end); var_idx(end)]);

if add_adjt
  dobj_id = coco_get_id('d', obj_id);
  mean_aidx = coco_get_adjt_data(prob, mean_id, 'axidx');  
  var_aidx = coco_get_adjt_data(prob, var_id, 'axidx');
  prob = coco_add_adjt(prob, obj_id, dobj_id, ...
    'aidx', [mean_aidx(end); var_aidx(end)]);
end

[uidx, data] = coco_get_func_data(prob, 's1.PK1.bvp.seg1.coll', 'uidx', 'data');
mk_idx = uidx(data.coll_seg.maps.p_idx([1,3]));

prob = coco_add_func(prob, 'km_constraint', ...
  @km_const, @km_const_du, @km_const_dudu, [], ...
  'zero', 'uidx', mk_idx);

if add_adjt
  [aidx, fdata] = coco_get_adjt_data(prob, ...
    's1.PK1.bvp.seg1.coll', 'axidx', 'data');
  mk_aidx = aidx(fdata.coll_opt.p_idx([1,3]));

  prob = coco_add_adjt(prob, 'km_constraint', 'aidx', ...
    mk_aidx);
end

num_samples = uq_data.nsamples;

alnames = uq_get_sample_par_names('alpha', 1:num_samples);
omnames = uq_get_sample_par_names('om', 1:num_samples);
Anames = uq_get_sample_par_names('A', 1:num_samples);

om_diff_ids = cell(1, num_samples);
pk1_ids = cell(1, num_samples);
pk2_ids = cell(1, num_samples);
om1_ids = cell(1, num_samples);
om2_ids = cell(1, num_samples);
om3_ids = cell(1, num_samples);

for i=1:num_samples
  om_diff_ids{i} = coco_get_id(['s', int2str(i)], 'PK_om_diff');
  pk1_ids{i} = coco_get_id(['s', int2str(i)], 'PK1', 'v0');
  pk2_ids{i} = coco_get_id(['s', int2str(i)], 'PK2', 'v0');
  om1_ids{i} = coco_get_id(['s', int2str(i)], 'OM1', 'v0');
  om2_ids{i} = coco_get_id(['s', int2str(i)], 'OM2', 'v0');
  om3_ids{i} = coco_get_id(['s', int2str(i)], 'OM3', 'v0');
end
dom_diff_ids = coco_get_id('d', om_diff_ids);
dv10_snames = coco_get_id('d', pk1_ids);
dv20_snames = coco_get_id('d', pk2_ids);
dv30_snames = coco_get_id('d', om1_ids);
dv40_snames = coco_get_id('d', om2_ids);
dv50_snames = coco_get_id('d', om3_ids);


if add_adjt
  dnames = coco_get_id('d', uq_get_sample_par_names(pnames, 1:4));
  dcnames = coco_get_id('d', {'c.1', 'c.2', 'c.3', 'c.4'});
  domnames = coco_get_id('d', omnames);
  dAnames = coco_get_id('d', Anames);
  dmoments = {'d.lo.c', 'd.up.c'};
  adj_pars2free = {'d.alpha', ...
    dom_diff_ids{:}, dv10_snames{:}, ...
    dv20_snames{:}, dv30_snames{:},  ...
    dv40_snames{:}, dv50_snames{:}, drdiffnames{:}, ...
    dmoments{:}};
  omnames = {omnames{:}, 's1.OM1.om', 's2.OM1.om', 's3.OM1.om', 's4.OM1.om'};
  if not(coco_exist(run_name, 'run'))
    bd = coco(prob, run_name, [], 1, ...
      {'obj', 'k', 'm', 'd.obj', omnames{:}, Anames{:}, 
      adj_pars2free{:}}, [0.1, 3.1]);
  else
    bd = coco_bd_read(run_name);    
  end
else
  % Mean Continuation Parameter
  mean_par_id = coco_get_id(response_id, 'mean');
  mean_idx = coco_get_func_data(prob, mean_id, 'uidx');
  mean_name = coco_get_id(response_id, 'mean');
  prob = coco_add_pars(prob, mean_par_id, mean_idx(end), mean_name);  
  
  % Variance Parameter
  var_par_id = coco_get_id(response_id, 'variance');
  var_idx = coco_get_func_data(prob, var_id, 'uidx');
  var_name = coco_get_id(response_id, 'var');
  prob = coco_add_pars(prob, var_par_id, var_idx(end), var_name);  

  bd = coco(prob, 'uq_sweep_no_adjt_for_plot_m=1_n=0', [], ...
    {'obj', 'k', 'm', mean_name, var_name, omnames{:}, Anames{:}, 's1.OM1.om', 's2.OM1.om', 's3.OM1.om', 's4.OM1.om'}, ...
    [-0.355, 4]);
end