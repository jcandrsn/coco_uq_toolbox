last_run_name = 'uq_sweep';
run_name = 'uq_dobj_to_1';
add_adjt=1;

% Initialize problem data structure
prob = coco_prob();
prob = coco_set(prob, 'coll', 'NTST', 30);
prob = coco_set(prob, 'cont', 'PtMX', 50);
prob = coco_set(prob, 'cont', 'h', 5, 'h_min', 1e-5, 'h_max', 1e5);
prob = coco_set(prob, 'corr', 'TOL', 1e-5);
prob = coco_set(prob, 'corr', 'ItNW', 30);
% % For testing only
% prob = coco_set(prob, 'coll', 'TOL', 100);

% Previous run located a fold point coincident with a
% branch point.
bd = coco_bd_read(last_run_name);
BPlabs = coco_bd_labs(bd, 'BP');
BPlab = BPlabs(2);

om_counter = 0;
% Stochastic parameter indices
s_idx = cell(1, uq_data.nsamples);
% Response function initial guesses
r_igs = cell(1, uq_data.nsamples);
% Response function input indices
r_idx = cell(1, uq_data.nsamples);
% Stochastic parameter adjoint indices
adj_s_idx = cell(1, uq_data.nsamples);
% Response function input adjoint indices
r_aidx = cell(1, uq_data.nsamples);
% Cell array for OM orbit names
om_sname = cell(1,3);

for sample_idx=1:uq_data.nsamples

  sample_id = strcat('s', int2str(sample_idx));
  pk1_sname = coco_get_id(sample_id, 'PK1');
  pk2_sname = coco_get_id(sample_id, 'PK2');
  om_sname{1} = coco_get_id(sample_id, 'OM1');
  om_sname{2} = coco_get_id(sample_id, 'OM2');
  om_sname{3} = coco_get_id(sample_id, 'OM3');

  prob = ode_BP2bvp(prob, pk1_sname, last_run_name, BPlab);
  prob = ode_BP2bvp(prob, pk2_sname, last_run_name, BPlab);
  prob = ode_BP2bvp(prob, om_sname{1}, last_run_name, BPlab);
  prob = ode_BP2bvp(prob, om_sname{2}, last_run_name, BPlab);
  prob = ode_BP2bvp(prob, om_sname{3}, last_run_name, BPlab);
  
  adjt_args = struct();
  if add_adjt
    adjt_args.run = last_run_name;
    adjt_args.lab = BPlab;
    adjt_chart = coco_read_solution(last_run_name, BPlab, 'chart');
    cdata = coco_get_chart_data(adjt_chart, 'lsol');

    prob = adjt_BP2bvp(prob, pk1_sname, last_run_name, BPlab);
    prob = adjt_BP2bvp(prob, pk2_sname, last_run_name, BPlab);
    prob = adjt_BP2bvp(prob, om_sname{1}, last_run_name, BPlab);
    prob = adjt_BP2bvp(prob, om_sname{2}, last_run_name, BPlab);
    prob = adjt_BP2bvp(prob, om_sname{3}, last_run_name, BPlab);
  end
  %% PK1
  pk1_coll_sname = coco_get_id(pk1_sname, 'bvp', ...
    'seg1', 'coll');
  % Peak 1 Information
  [pk1_data, pk1_uidx, pk1_u0] = ...
    coco_get_func_data(prob, pk1_coll_sname, ...
    'data', 'uidx', 'u0');
  pk1_maps = pk1_data.coll_seg.maps;
  pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

  if add_adjt
      [pk1_fdata, pk1_aidx] = coco_get_adjt_data(prob, ...
        pk1_coll_sname, 'data', 'axidx');
      pk1_opt = pk1_fdata.coll_opt;
  end

  if sample_idx == 1
    % Name the parameters
    par_fname = coco_get_id(sample_id, 'pars');
    par_pname = pnames([1,3,4]);
    par_idx = pk1_uidx(pk1_maps.p_idx([1,3,4]));
    prob = coco_add_pars(prob, par_fname, ...
      par_idx, par_pname);    

    % Store first sample's info for glue
    s1_pk1_data = pk1_data;
    s1_pk1_uidx = pk1_uidx;
    s1_pk1_maps = pk1_maps;

    % Adjoints
    if add_adjt
      [chart, lidx] = coco_read_adjoint(par_fname, ...
        last_run_name, BPlab, 'chart', 'lidx');

      prob = coco_add_adjt(prob, par_fname, ...
      coco_get_id('d', par_pname), ...
      'aidx', pk1_aidx(pk1_opt.p_idx([1,3,4])), ...
      'l0', chart.x, 'tl0', cdata.v(lidx));

      s1_pk1_fdata = pk1_fdata;
      s1_pk1_aidx  = pk1_aidx;
      s1_pk1_opt   = pk1_opt;

      adj_s_idx{sample_idx} = pk1_aidx(pk1_opt.p_idx);
      r_aidx{sample_idx} = pk1_aidx(pk1_opt.x0_idx(1));
    end
  else
    % Glue deterministic parameters together
    s1_p_idx = s1_pk1_uidx(s1_pk1_maps.p_idx([1,3,4]));
    p_idx = pk1_uidx(pk1_maps.p_idx([1,3,4]));

    glue_name = coco_get_id(sample_id, 'glue_to_s1');
    prob = coco_add_glue(prob, glue_name, s1_p_idx, p_idx);

    % Adjoints
    if add_adjt
      % For glue
      [chart, lidx] = coco_read_adjoint(glue_name, ...
        last_run_name, BPlab, 'chart', 'lidx');
      s1_p_aidx = s1_pk1_aidx(s1_pk1_opt.p_idx([1,3,4]));
      p_aidx = pk1_aidx(pk1_opt.p_idx([1,3,4]));
      
      prob = coco_add_adjt(prob, glue_name, ...
        'aidx', [s1_p_aidx; p_aidx], 'l0', chart.x, ...
        'tl0', cdata.v(lidx));

      adj_s_idx{sample_idx} = pk1_aidx(sj_pk1_opt.p_idx);
      r_aidx{sample_idx} = pk1_aidx(sj_pk1_opt.x0_idx(1));
    end
  end

  s_idx{sample_idx} = pk1_uidx(pk1_maps.p_idx);
  r_igs{sample_idx} = pk1_u0(pk1_maps.x0_idx(1));
  r_idx{sample_idx} = pk1_uidx(pk1_maps.x0_idx(1));

  % Forcing amplitude names
  si_A_fname = coco_get_id(sample_id, 'A');
  si_A_pname = coco_get_id('A', int2str(sample_idx));
  si_A_idx = pk1_uidx(pk1_maps.p_idx(5));
  prob = coco_add_pars(prob, si_A_fname, ...
    si_A_idx, si_A_pname, 'inactive');
  
  % Forcing frequency names
  si_om_fname = coco_get_id(sample_id, 'om');
  si_om_pname = coco_get_id('om', int2str(sample_idx));
  si_om_idx = pk1_uidx(pk1_maps.p_idx(6));
  prob = coco_add_pars(prob, si_om_fname, ...
    si_om_idx, si_om_pname, 'inactive');

  % Maintain velocity constraint (could also be done with a
  % boundary condition)
  pk1_v0_sname = coco_get_id(pk1_sname, 'v0');
  prob = coco_add_pars(prob, pk1_v0_sname, pk1_x0(2), ...
    pk1_v0_sname, 'inactive');

  % Adjoint
  if add_adjt
    
    [pk1_fdata, pk1_aidx] = coco_get_adjt_data(prob, ...
      pk1_coll_sname, 'data', 'axidx');
    sj_pk1_opt = pk1_fdata.coll_opt;
    
    [chart, lidx] = coco_read_adjoint(si_A_fname, ...
      last_run_name, BPlab, 'chart', 'lidx');
    dA_sname = coco_get_id('d', si_A_pname);
    prob = coco_add_adjt(prob, si_A_fname, ...
      dA_sname, ...
      'aidx', pk1_aidx(pk1_opt.p_idx(5)), ...
      'l0', chart.x, 'tl0', cdata.v(lidx));
    
    [chart, lidx] = coco_read_adjoint(si_om_fname, ...
      last_run_name, BPlab, 'chart', 'lidx');
    dom_sname = coco_get_id('d', si_om_pname);
    prob = coco_add_adjt(prob, si_om_fname, ...
      dom_sname, ...
      'aidx', pk1_aidx(pk1_opt.p_idx(6)), ...
      'l0', chart.x, 'tl0', cdata.v(lidx));
    
    [chart, lidx] = coco_read_adjoint(pk1_v0_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');
    dv10_sname = coco_get_id('d', pk1_v0_sname);
    prob = coco_add_adjt(prob, pk1_v0_sname, ...
      dv10_sname, ...
      'aidx', pk1_aidx(pk1_opt.x0_idx(2)), ...
      'l0', chart.x, 'tl0', cdata.v(lidx));
  end
  %% PK2
  % Maintain velocity constraint (could also be done with a
  % boundary condition)
  pk2_coll_sname = coco_get_id(pk2_sname, 'bvp', ...
    'seg1', 'coll');
  % Peak 2 Information
  [pk2_data, pk2_uidx] = coco_get_func_data(prob, ...
    pk2_coll_sname, 'data', 'uidx');
  pk2_maps = pk2_data.coll_seg.maps;
  pk2_x0 = pk2_uidx(pk2_maps.x0_idx);
  
  pk2_v0_sname = coco_get_id(pk2_sname, 'v0');
  prob = coco_add_pars(prob, pk2_v0_sname, pk2_x0(2), ...
    pk2_v0_sname, 'inactive');
  
  % Glue Parameters together (except forcing frequency)
  parglue_sname = coco_get_id(sample_id, ...
    'par_glue_PK2_to_PK1');
  prob = coco_add_glue(prob, parglue_sname, ...
    pk2_uidx(pk2_maps.p_idx(1:5)), ...
    pk1_uidx(pk1_maps.p_idx(1:5)));

  pk_r_glue_sname = coco_get_id(sample_id, ...
    'R_glue_to_PK1');
  prob = coco_add_glue(prob, pk_r_glue_sname, ...
    pk2_x0(1), pk1_x0(1));

  % Lock in the frequency difference
  pk_om_diff_sname = coco_get_id(sample_id, ...
    'PK_om_diff');
  prob = coco_add_func(prob, pk_om_diff_sname, ...
    @udiff, @udiff_dU, @udiff_dUdU, [], ...
    'inactive', pk_om_diff_sname, 'uidx', ...
    [pk2_uidx(pk2_maps.p_idx(6)), ...
     pk1_uidx(pk1_maps.p_idx(6))]);

  % Adjoints
  if add_adjt
    [pk2_fdata, pk2_aidx] = coco_get_adjt_data(prob, ...
      pk2_coll_sname, 'data', 'axidx');
    pk2_opt = pk2_fdata.coll_opt;
    % velocity constraint
    [chart, lidx] = coco_read_adjoint(pk2_v0_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');

    dv20_sname = coco_get_id('d', pk2_v0_sname);
    prob = coco_add_adjt(prob, pk2_v0_sname, ...
      dv20_sname, ...
      'aidx', pk2_aidx(pk2_opt.x0_idx(2)),...
      'l0', chart.x, 'tl0', cdata.v(lidx));

    % glue
    [chart, lidx] = coco_read_adjoint(parglue_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');

    prob = coco_add_adjt(prob, parglue_sname, ...
      'aidx', [pk2_aidx(pk2_opt.p_idx(1:5)); ...
      pk1_aidx(pk1_opt.p_idx(1:5))], ...
      'l0', chart.x, 'tl0', cdata.v(lidx));

    [chart, lidx] = coco_read_adjoint(pk_r_glue_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');

    prob = coco_add_adjt(prob, pk_r_glue_sname, ...
      'aidx', [pk2_aidx(pk2_opt.x0_idx(1)); ...
      pk1_aidx(pk1_opt.x0_idx(1))], ...
      'l0', chart.x, 'tl0', cdata.v(lidx));

    % om constraint
    [chart, lidx] = coco_read_adjoint(pk_om_diff_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');

    prob = coco_add_adjt(prob, pk_om_diff_sname, ...
      coco_get_id('d', pk_om_diff_sname), ...
      'aidx', [pk2_aidx(pk2_opt.p_idx(6)), ...
      pk1_aidx(pk1_opt.p_idx(6))], ...
      'l0', chart.x, 'tl0', cdata.v(lidx));

  end
  %% OM 
  om_data = cell(1,3);
  om_uidx = cell(1,3);
  om_maps = cell(1,3);
  om_x0 = cell(1,3);
  om_fdata = cell(1,3);
  om_aidx = cell(1,3);
  om_opt = cell(1,3);
  om_x0a = cell(1,3);

  for om_idx=1:3
    om_coll_sname = coco_get_id(om_sname{om_idx}, 'bvp', ...
      'seg1', 'coll');
    [om_data{om_idx}, om_uidx{om_idx}] = coco_get_func_data(prob, ...
      om_coll_sname, 'data', 'uidx');
    om_maps{om_idx} = om_data{om_idx}.coll_seg.maps;

    % Maintain velocity constraint
    v0sname = coco_get_id(om_sname{om_idx},'v0');
    om_x0{om_idx} = om_uidx{om_idx}(om_maps{om_idx}.x0_idx);
    prob = coco_add_pars(prob, v0sname, om_x0{om_idx}(2), ...
      v0sname, 'inactive');    

    if add_adjt
      [om_fdata{om_idx}, om_aidx{om_idx}] = coco_get_adjt_data(prob, ...
        om_coll_sname, 'data', 'axidx');
      om_opt{om_idx} = om_fdata{om_idx}.coll_opt;
      om_x0a{om_idx} = om_aidx{om_idx}(om_opt{om_idx}.x0_idx);

      [chart, lidx] = coco_read_adjoint(pk_om_diff_sname, ...
        last_run_name, BPlab, 'chart', 'lidx');

      dv0sname = coco_get_id('d', v0sname);
      prob = coco_add_adjt(prob, v0sname, dv0sname, ...
        'aidx', om_x0a{om_idx}(2),...
        'l0', chart.x, 'tl0', cdata.v(lidx));
    end
    
    % Glue Parameters together
    if om_idx > 1
      % The fold approximants share all parameter values.
      % They differ only in response amplitude 
      gluename = coco_get_id(sample_id, ['par_glue_OM', ...
        int2str(om_idx), '_to_OM1']);

      prob = coco_add_glue(prob, gluename, ...
        om_uidx{1}(om_maps{1}.p_idx), ...
        om_uidx{om_idx}(om_maps{om_idx}.p_idx));

      % Parameterize Response Amplitude Difference
      rdiffname = ['OM', int2str(om_idx-1), 'R_minus_OM', int2str(om_idx), 'R'];
      rdiffname = coco_get_id(sample_id, rdiffname);
      prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
        [], 'inactive', rdiffname, 'uidx', [om_x0{om_idx-1}(1), om_x0{om_idx}(1)]);

      if add_adjt
        [chart, lidx] = ...
          coco_read_adjoint(gluename, ...
          last_run_name, BPlab, 'chart', 'lidx');

        prob = coco_add_adjt(prob, gluename, ...
          'aidx', [om_aidx{1}(om_opt{1}.p_idx), ...
          om_aidx{om_idx}(om_opt{om_idx}.p_idx)], ...
          'l0', chart.x, 'tl0', cdata.v(lidx));

        [chart, lidx] = ...
          coco_read_adjoint(rdiffname, ...
          last_run_name, BPlab, 'chart', 'lidx');
        
        om_counter = om_counter + 1;
        drdiffname = coco_get_id('d', rdiffname);
        drdiffnames{om_counter} = drdiffname;
        prob = coco_add_adjt(prob, rdiffname, drdiffname, ...
          'aidx', [om_x0a{om_idx-1}(1), om_x0a{om_idx}(1)], ...
          'l0', chart.x, 'tl0', cdata.v(lidx));
      end

    else
      gluename = coco_get_id(sample_id, ['par_glue_OM', ...
        int2str(om_idx), '_to_PK1']);

      prob = coco_add_glue(prob, gluename, ...
        pk1_uidx(pk1_maps.p_idx(1:5)), ...
        om_uidx{1}(om_maps{1}.p_idx(1:5)));
      
      omomname = coco_get_id(om_sname{om_idx}, 'om');
      prob = coco_add_pars(prob, omomname, ...
        om_uidx{om_idx}(om_maps{om_idx}.p_idx(6)), ...
        omomname, 'inactive');
      
      if add_adjt
        [chart, lidx] = ...
          coco_read_adjoint(gluename, ...
          last_run_name, BPlab, 'chart', 'lidx');

        prob = coco_add_adjt(prob, gluename, ...
          'aidx', [pk1_aidx(pk1_opt.p_idx(1:5)), ...
          om_aidx{om_idx}(om_opt{om_idx}.p_idx(1:5))], ...
          'l0', chart.x, 'tl0', cdata.v(lidx));

        [chart, lidx] = ...
          coco_read_adjoint(omomname, ...
          last_run_name, BPlab, 'chart', 'lidx');

        domomname = coco_get_id('d', omomname);
        prob = coco_add_adjt(prob, omomname, domomname, ...
                'aidx', om_aidx{om_idx}(om_opt{om_idx}.p_idx(6)),  ...
                'l0', chart.x, 'tl0', cdata.v(lidx));
      end
    end
  end
end

psi_mat = uq_make_psi_mat(uq_data.nds_grid, uq_data.uq.Pt, uq_data.spdists);
uq_data.wtd_psi_mat = psi_mat*diag(uq_data.wts);
uq_data.s_idx = s_idx;
if add_adjt
  uq_data.adj_s_idx = adj_s_idx;
end
uq_data.addadjt=add_adjt;
prob = uq_add_sample_nodes(prob, uq_data);
% Final data structure for a UQ sample is stored in the
% parameter glue function
uq_data = coco_get_func_data(prob, 'PK1.uq.s_par_glue', 'data');

igs = zeros(uq_data.nsamples, 1);
idx = zeros(1, uq_data.nsamples);
for i=1:uq_data.nsamples
  igs(i) = r_igs{i};
  idx(i) = [r_idx{i}];
end

alpha_ig = uq_data.wtd_psi_mat*igs;
response_id = 'resp';
prob = coco_add_func(prob, response_id, ...
  @resp_pce, @resp_pce_dU, @resp_pce_dUdU, ...
  uq_data, 'zero', 'uidx', idx(:), 'u0', alpha_ig);

if add_adjt
  aidx = zeros(1, uq_data.nsamples);
  for i=1:uq_data.nsamples
    aidx(i) = [r_aidx{i}];
  end
  
  [chart, lidx] = ...
    coco_read_adjoint(response_id, ...
    last_run_name, BPlab, 'chart', 'lidx');
  
  prob = coco_add_adjt(prob, response_id, ...
    'aidx', aidx(:), ...
    'l0', chart.x, 'tl0', cdata.v(lidx));
end

alpha_idx = coco_get_func_data(prob, response_id, 'uidx');
alpha_idx = alpha_idx(end-uq_data.Nt+1:end);

if add_adjt
  alpha_aidx = coco_get_adjt_data(prob, response_id, 'axidx');
  alpha_aidx = alpha_aidx(end-uq_data.Nt+1:end);
end

% Mean Zero Function
mean_id = coco_get_id(response_id, 'pce_mean');
prob = coco_add_func(prob, mean_id, ...
  @uq_pce_mean, @uq_pce_mean_dU, @uq_pce_mean_dUdU, ...
  uq_data, 'zero', 'uidx', alpha_idx, 'u0', alpha_ig(1));

if add_adjt  
  [chart, lidx] = ...
    coco_read_adjoint(mean_id, ...
    last_run_name, BPlab, 'chart', 'lidx');
  
  prob = coco_add_adjt(prob, mean_id, 'aidx', ...
    alpha_aidx, 'l0', chart.x, 'tl0', cdata.v(lidx));
end

% Variance Zero Function
var_id = coco_get_id(response_id, 'pce_variance');
prob = coco_add_func(prob, var_id, ...
  @uq_pce_variance, @uq_pce_variance_dU, @uq_pce_variance_dUdU,...
  uq_data, 'zero', 'uidx', alpha_idx, 'u0', sum(alpha_ig(2:end).^2));

if add_adjt
  [chart, lidx] = ...
    coco_read_adjoint(var_id, ...
    last_run_name, BPlab, 'chart', 'lidx');
  
  prob = coco_add_adjt(prob, var_id, 'aidx', ...
    alpha_aidx, 'l0', chart.x, 'tl0', cdata.v(lidx));
end

obj_id = 'obj';
mean_idx = coco_get_func_data(prob, mean_id, 'uidx');
var_idx = coco_get_func_data(prob, var_id, 'uidx');

prob = coco_add_func(prob, obj_id, ...
  @obj, @obj_du, @obj_dudu, [], ...
  'inactive', obj_id, 'uidx', ...
  [mean_idx(end); var_idx(end)]);

if add_adjt
  [chart, lidx] = coco_read_adjoint(obj_id, ...
    last_run_name, BPlab, 'chart', 'lidx');
  
  mean_aidx = coco_get_adjt_data(prob, mean_id, 'axidx');  
  var_aidx = coco_get_adjt_data(prob, var_id, 'axidx');
  
  dobj_id = coco_get_id('d', obj_id);
  prob = coco_add_adjt(prob, obj_id, dobj_id, ...
    'aidx', [mean_aidx(end); var_aidx(end)], ...
    'l0', chart.x, 'tl0', cdata.v(lidx));
end

[uidx, data] = coco_get_func_data(prob, 's1.PK1.bvp.seg1.coll', 'uidx', 'data');
mk_idx = uidx(data.coll_seg.maps.p_idx([1,3]));

prob = coco_add_func(prob, 'km_constraint', ...
  @km_const, @km_const_du, @km_const_dudu, [], ...
  'zero', 'uidx', mk_idx);

if add_adjt
  [aidx, fdata] = coco_get_adjt_data(prob, ...
    's1.PK1.bvp.seg1.coll', 'axidx', 'data');
  [chart, lidx] = coco_read_adjoint('km_constraint', ...
    last_run_name, BPlab, 'chart', 'lidx');
  
  mk_aidx = aidx(fdata.coll_opt.p_idx([1,3]));

  prob = coco_add_adjt(prob, 'km_constraint', 'aidx', ...
    mk_aidx, 'l0', chart.x, 'tl0', cdata.v(lidx));
end

num_samples = uq_data.nsamples;

alnames = uq_get_sample_par_names('alpha', 1:num_samples);
omnames = uq_get_sample_par_names('om', 1:num_samples);
Anames = uq_get_sample_par_names('A', 1:num_samples);

om_diff_ids = cell(1, num_samples);
pk1_ids = cell(1, num_samples);
pk2_ids = cell(1, num_samples);
om1_ids = cell(1, num_samples);
om2_ids = cell(1, num_samples);
om3_ids = cell(1, num_samples);

for i=1:num_samples
  om_diff_ids{i} = coco_get_id(['s', int2str(i)], 'PK_om_diff');
  pk1_ids{i} = coco_get_id(['s', int2str(i)], 'PK1', 'v0');
  pk2_ids{i} = coco_get_id(['s', int2str(i)], 'PK2', 'v0');
  om1_ids{i} = coco_get_id(['s', int2str(i)], 'OM1', 'v0');
  om2_ids{i} = coco_get_id(['s', int2str(i)], 'OM2', 'v0');
  om3_ids{i} = coco_get_id(['s', int2str(i)], 'OM3', 'v0');
end
dom_diff_ids = coco_get_id('d', om_diff_ids);
dv10_snames = coco_get_id('d', pk1_ids);
dv20_snames = coco_get_id('d', pk2_ids);
dv30_snames = coco_get_id('d', om1_ids);
dv40_snames = coco_get_id('d', om2_ids);
dv50_snames = coco_get_id('d', om3_ids);

dalnames = coco_get_id('d', alnames);
domnames = coco_get_id('d', omnames);
dAnames = coco_get_id('d', Anames);
adj_pars2free = {'d.alpha', ...
    dom_diff_ids{:}, dv10_snames{:}, ...
    dv20_snames{:}, dv30_snames{:},  ...
    dv40_snames{:}, dv50_snames{:}, drdiffnames{:}, ...
    'd.lo.c', 'd.up.c'};

bd = coco(prob, run_name, [], 1, ...
  {'d.obj', 'k', 'm', 'obj', omnames{:}, Anames{:}, ...
  's1.OM1.om', 's2.OM1.om', 's3.OM1.om', 's4.OM1.om', ...
  adj_pars2free{:}}, [0, 1]);