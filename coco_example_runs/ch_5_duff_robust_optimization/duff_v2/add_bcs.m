function [data, y, J] = add_bcs(prob, data, u)

% coco compatible function to add in boundary conditions
% that are in the form required by the bvp toolbox (doesn't
% support updating boundary conditions)

bc = data.bvp_bc;

T0 = u(bc.T0_idx);
T  = u(bc.T_idx);
x0 = u(bc.x0_idx);
x1 = u(bc.x1_idx);
p  = u(bc.p_idx);

args = {T0, T, x0, x1, p};

y  = pr.fhan(pr.bc_data, args{bc.nargs+1:end});

if nargout>=3
  J  = [zeros(numel(y), bc.nargs*numel(T0)) ...
    pr.dfdxhan(pr.bc_data, args{bc.nargs+1:end})];
end

end