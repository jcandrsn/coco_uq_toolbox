% Path to vector field, Jacobians, and Response Function
addpath('../duff')
addpath('../linode')

% Initialize problem instance and set options
prob_init = coco_prob();

prob_init = coco_set(prob_init, 'coll', 'NTST', 50, 'NCOL', 6);
prob_init = coco_set(prob_init, 'cont', 'PtMX', 100);%, ...
%   'h', 0.5, 'almax', 30, 'h_max', 1000);
% prob_init = coco_set(prob_init, 'cont', 'NAdapt', 5, 'NPR', 10);

% % Temporarily allow really bad results for some checking of
% % Jacobians.
% prob_init = coco_set(prob_init, 'coll', 'TOL', 5);


% Let Transients die out
eps = 0.001;
%     m, eps*c, k, eps*alpha, eps*F, omega
p0 = [1; eps*2;1;eps*5;eps*5;1];
[~, x0]   = ode45(@(t,x) duff(x, p0), ...
  [0 24*pi], [1; 0; 0]);

% Initial Solution Guess
[t0, x0]   = ode45(@(t,x) duff(x, p0), ...
  [0 2*pi], [x0(end,1:3)']);

% 'coll'-toolbox arguments
coll_args = {@duff, @duff_dx, @duff_dp, ...
  @duff_dxdx, @duff_dxdp, @duff_dpdp, ...
  t0, x0, p0};

% parameter names
pnames = {'m', 'c', 'k', 'eps', 'A', 'om'};

% 'bvp'-toolbox arguments
bvp_args = {@duff_bc, @duff_bc_du, @duff_bc_dudu};

uq_args = {
  {'c'}, ...        % Stochastic Parameter
  {'Normal'}, ...   % Distribution Type
  [[eps*2,eps*0.1]], ...    % Distribution Parameters
  {}};%, ...
%   '-add-adjt'};   % Option to add the adjoint
                  % equations to the uq probs

prob = uq_isol2bvp_sample(prob_init, 'duff', coll_args{:}, pnames, bvp_args{:}, uq_args{:});
prob = uq_coll_add_response(prob, 'duff', 'resp', 'bv', @x10, @x10_du, @x10_dudu);
% prob = uq_coll_add_response_adjoint(prob, 'duff', 'resp');
prob = coco_set(prob, 'cont', 'PtMX', 2000);%, ...
bd = coco(prob, 'duff', [], 1, {'om', 'duff.resp.mean', 'duff.resp.var'}, [0.02, 100]);
% bd = coco(prob, 'duff', [], 1, {'duff.resp.mean', 'om', 'duff.resp.var', ...
%   'd.duff.resp.mean', 'd.duff.resp.var', 'd.om', 'd.k', 'd.m', 'd.eps', 'd.A'});
% 
% prob = ode_isol2bvp(prob_init, '', coll_args{:}, pnames, bvp_args{:});
% [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
% maps = data.coll_seg.maps;
% x0 = uidx(maps.x0_idx);
% prob = coco_add_pars(prob, 'A', x0(1), 'x01', 'inactive');
% 
% bd = coco(prob, 'duff', [], 1, {'om', 'x01'});
