%% Problem Set-up
addpath('../../duff')
addpath('../../utils')

% Initialize problem instance and set options
prob_init = coco_prob();

prob_init = coco_set(prob_init, 'coll', 'NTST', 15);
% prob_init = coco_set(prob_init, 'cont', 'PtMX', 200);%, 'h_max', 10);
% prob_init = coco_set(prob_init, 'cont', 'PtMX', 100, ...
%   'h', 0.5, 'almax', 30, 'h_max', 1000);
% prob_init = coco_set(prob_init, 'cont', 'NAdapt', 5, 'NPR', 10);

% % Temporarily allow really bad results for some checking of
% % Jacobians.
% prob_init = coco_set(prob_init, 'coll', 'TOL', 5);
eps = 0.1;
m = 1;
c = 2;
k = 2;
alpha = 5;
A = 5;
om = 1;

p0 = [m; eps*c; k; eps*alpha; eps*A; om];

% parameter names
pnames = {'m', 'c', 'k', 'alpha', 'A', 'om'};

%% Step 1:
%  Continue in the the initial position until it is a
%  maximum (corresponds to initial velocity = 0)

run_name = 'set_v0';

if not(coco_exist(run_name, 'run'))
  % Let Transients die out
  [~, x0]   = ode45(@(t,x) duff(x, p0), ...
    [0 24*pi], [1; 0; 0]);
 
  % Initial Solution Guess
  [t0, x0]   = ode45(@(t,x) duff(x, p0), ...
    [0 2*pi], [x0(end,1:3)']);
  
  % 'coll'-toolbox arguments
  coll_args = {@duff, @duff_dx, @duff_dp, ...
    @duff_dxdx, @duff_dxdp, @duff_dpdp, ...
    t0, x0, p0};

  % 'bvp'-toolbox arguments
  bvp_args = {@duff_bc, @duff_bc_du, @duff_bc_dudu};

  prob = ode_isol2bvp(prob_init, '', coll_args{:}, pnames, bvp_args{:});
  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  maps = data.coll_seg.maps;
  x0_coco = uidx(maps.x0_idx);
  prob = coco_add_pars(prob, 'R', x0_coco(1), 'R', 'inactive');
  prob = coco_add_pars(prob, 'v0', x0_coco(2), 'v0', 'inactive');
  
  prob = coco_add_event(prob, 'MAX', 'v0', 0);
  % Find point where velocity is zero so that R is the peak
  % of the trajectory.
  bd = coco(prob, run_name, [], 1, {'R', 'v0'});  
else
  bd = coco_bd_read(run_name);
end
%% Step 2
%  Frequency sweep to find points that will be used to
%  locate approximate peak and approximate folds

last_run_name = run_name;
run_name = 'sweep';
if not(coco_exist(run_name, 'run'))
  labs = coco_bd_labs(bd, 'MAX');
  prob = coco_set(prob_init, 'cont', 'PtMX', 200, 'NAdapt', 1);
  prob = ode_bvp2bvp(prob, '', last_run_name, labs(2));
  
  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  maps = data.coll_seg.maps;
  x0 = uidx(maps.x0_idx);
  prob = coco_add_pars(prob, 'R', x0(1), 'R', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'v0', x0(2), 'v0', 'inactive');

  prob = coco_add_event(prob, 'PK', 'R', 1.42);
  prob = coco_add_event(prob, 'OM', 'om', 1.67);
  
  bd = coco(prob, run_name, [], 1, {'om', 'R'}, {[0.1, 5]});
else
  bd = coco_bd_read(run_name);
end

%% Step 3
%  Drive the frequency difference in PK values to be 1e-3
last_run_name = run_name;
run_name = 'pk';

if not(coco_exist(run_name, 'run'))
  pk_labs = coco_bd_labs(bd, 'PK');

  % Add peaks back to continuation problem structure
  prob = ode_bvp2bvp(prob_init, 'PK1', last_run_name, '', pk_labs(1));
  bvp_data = coco_get_func_data(prob, 'PK1.bvp', 'data');

  [pk1_data, pk1_uidx] = coco_get_func_data(prob, 'PK1.bvp.seg1.coll', 'data', 'uidx');
  pk1_maps = pk1_data.coll_seg.maps;
  pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

  prob = coco_add_pars(prob, 'R.PK1', pk1_x0(1), 'R.PK1', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'v0.PK1', pk1_x0(2), 'v0.PK1', 'inactive');

  prob = ode_coll2coll(prob, 'PK2', last_run_name, 'bvp.seg1', pk_labs(2));

  % Boundary conditions for PK2
  [pk2_data, pk2_uidx] = coco_get_func_data(prob, 'PK2.coll', 'data', 'uidx');
  pk2_maps = pk2_data.coll_seg.maps;

  args = pk2_uidx([pk2_maps.T0_idx; pk2_maps.T_idx; pk2_maps.x0_idx; ...
    pk2_maps.x1_idx; pk2_maps.p_idx]);
  prob = coco_add_func(prob, 'PK2.bvp', @duff_bc_caf, ...
    @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
    'zero', 'uidx', args);

  % Give PK2 Frequency a name.
  prob = coco_add_pars(prob, 'pk2.om', pk2_uidx(pk2_maps.p_idx(end)), 'pk2.om', 'inactive');


  pk2_x0 = pk2_uidx(pk2_maps.x0_idx);
  prob = coco_add_pars(prob, 'R.PK2', pk2_x0(1), 'R.PK2', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'v0.PK2', pk2_x0(2), 'v0.PK2', 'inactive');

  % Glue Parameters together (except forcing frequency)
  prob = coco_add_glue(prob, 'par_glue', pk2_uidx(pk2_maps.p_idx(1:5)), ...
    pk1_uidx(pk1_maps.p_idx(1:5)));

  prob = coco_add_glue(prob, 'R.glue', pk2_x0(1), pk1_x0(1));

  % Parameterize frequency difference
  prob = coco_add_func(prob, 'om_diff', @udiff, @udiff_dU, @udiff_dUdU, ...
    [], 'inactive', 'om_diff', 'uidx', [pk2_uidx(pk2_maps.p_idx(6)), ...
    pk1_uidx(pk1_maps.p_idx(6))]);

  prob = coco_add_event(prob, 'PK', 'om_diff', 1e-4);

  % Drive forcing frequency difference lower
  bd = coco(prob, run_name, [], 1, {'om_diff', 'om', 'pk2.om', 'R.PK1', 'R.PK2'}, {[0.9e-4, 1.5e-1]});
else
  bd = coco_bd_read(run_name);
end

%% Step 4a
%  Collect PK1, PK2, OM1, OM2, OM3 in one problem and
%  reduce distance in Amplitude to a small value
last_run_name = run_name;
run_name = 'fold_find_top';

if not(coco_exist(run_name, 'run'))
  pk_labs = coco_bd_labs(bd, 'EP');

  swp_bd = coco_bd_read('sweep');
  om_labs = coco_bd_labs(swp_bd, 'OM');

  prob = ode_bvp2bvp(prob_init, 'PK1', last_run_name, pk_labs(1));
  bvp_data = coco_get_func_data(prob, 'PK1.bvp', 'data');

  [pk1_data, pk1_uidx] = coco_get_func_data(prob, 'PK1.bvp.seg1.coll', 'data', 'uidx');
  pk1_maps = pk1_data.coll_seg.maps;
  pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

  prob = coco_add_pars(prob, 'PK1.R', pk1_x0(1), 'PK1.R', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'PK1.v0', pk1_x0(2), 'PK1.v0', 'inactive');

  prob = ode_coll2coll(prob, 'PK2', last_run_name, pk_labs(1));

  % Boundary conditions for PK2
  [pk2_data, pk2_uidx] = coco_get_func_data(prob, 'PK2.coll', 'data', 'uidx');
  pk2_maps = pk2_data.coll_seg.maps;

  args = pk2_uidx([pk2_maps.T0_idx; pk2_maps.T_idx; pk2_maps.x0_idx; ...
    pk2_maps.x1_idx; pk2_maps.p_idx]);
  prob = coco_add_func(prob, 'PK2.bvp', @duff_bc_caf, ...
    @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
    'zero', 'uidx', args);

  % Give PK2 Frequency a name.
  prob = coco_add_pars(prob, 'pk2.om', pk2_uidx(pk2_maps.p_idx(end)), 'pk2.om', 'inactive');

  pk2_x0 = pk2_uidx(pk2_maps.x0_idx);
  prob = coco_add_pars(prob, 'PK2.R', pk2_x0(1), 'PK2.R', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'PK2.v0', pk2_x0(2), 'PK2.v0', 'inactive');

  % Glue Parameters together (except forcing frequency)
  prob = coco_add_glue(prob, 'par_glue', pk2_uidx(pk2_maps.p_idx(1:5)), ...
    pk1_uidx(pk1_maps.p_idx(1:5)));
  prob = coco_add_glue(prob, 'PK.R.glue', pk2_x0(1), pk1_x0(1));

  % Parameterize frequency difference
  prob = coco_add_func(prob, 'om_diff', @udiff, @udiff_dU, @udiff_dUdU, ...
    [], 'inactive', 'om_diff', 'uidx', [pk2_uidx(pk2_maps.p_idx(6)), ...
    pk1_uidx(pk1_maps.p_idx(6))]);

  for i=1:3
    prob = ode_coll2coll(prob, strcat('OM', int2str(i)), 'sweep', 'bvp.seg1', om_labs(i));
    % Boundary conditions for OMi
    [om_data{i}, om_uidx{i}] = coco_get_func_data(prob, ...
      coco_get_id(strcat('OM', int2str(i)),'coll'), 'data', 'uidx');
    om_maps{i} = om_data{i}.coll_seg.maps;

    args = om_uidx{i}([om_maps{i}.T0_idx; om_maps{i}.T_idx; om_maps{i}.x0_idx; ...
      om_maps{i}.x1_idx; om_maps{i}.p_idx]);
    om_bvp_id = coco_get_id(strcat('OM', int2str(i)),'bvp');
    prob = coco_add_func(prob, om_bvp_id, @duff_bc_caf, ...
      @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
      'zero', 'uidx', args);

    % Maintain velocity constraint
    Rname = coco_get_id(strcat('OM', int2str(i)),'R');
    v0name = coco_get_id(strcat('OM', int2str(i)),'v0');
    om_x0{i} = om_uidx{i}(om_maps{i}.x0_idx);
    prob = coco_add_pars(prob, Rname, om_x0{i}(1), Rname, 'inactive');
    prob = coco_add_pars(prob, v0name, om_x0{i}(2), v0name, 'inactive');

    % Glue Parameters together
    if i > 1
      % The fold approximants share all parameter values.
      % They differ only in response amplitude 
      % {'OM1.R', 'OM2.R', 'OM3.R'}

      gluename = ['OM', int2str(i), '.to.OM1'];
      prob = coco_add_glue(prob, gluename, ...
        om_uidx{1}(om_maps{1}.p_idx), ...
        om_uidx{i}(om_maps{i}.p_idx));

      % Parameterize Response Amplitude Difference
      rdiffname = ['OM', int2str(i-1), 'R.minus.OM', int2str(i), 'R'];
      prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
        [], 'inactive', rdiffname, 'uidx', [om_x0{i-1}(1), om_x0{i}(1)]);

    else
      gluename = ['OM', int2str(i), '.to.PK1'];
      prob = coco_add_glue(prob, gluename, ...
        pk2_uidx(pk2_maps.p_idx(1:5)), ...
        om_uidx{1}(om_maps{1}.p_idx(1:5)));
    end
  end
  prob = coco_add_pars(prob, 'OM1.om', om_uidx{1}(om_maps{1}.p_idx(6)), 'OM1.om', 'inactive');
  
  bd = coco(prob, run_name, [], 1, {'OM1R.minus.OM2R', 'OM1.om', 'OM2R.minus.OM3R', 'om', 'pk2.om', 'PK1.R', 'PK2.R', 'OM1.R', 'OM2.R', 'OM3.R'}, [1e-4, 3]);
else
  bd = coco_bd_read(run_name);
end

%% Step 4b
%  Collect PK1, PK2, OM1, OM2, OM3 in one problem and
%  reduce distance in Amplitude to a small value
last_run_name = run_name;
run_name = 'fold_clamp';

if not(coco_exist(run_name, 'run'))
  labs = coco_bd_labs(bd, 'EP');
  prob = coco_set(prob_init, 'cont', 'NAdapt', 1);
  prob = ode_bvp2bvp(prob, 'PK1', last_run_name, labs(1));
  bvp_data = coco_get_func_data(prob, 'PK1.bvp', 'data');

  [pk1_data, pk1_uidx] = coco_get_func_data(prob, 'PK1.bvp.seg1.coll', 'data', 'uidx');
  pk1_maps = pk1_data.coll_seg.maps;
  pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

  prob = coco_add_pars(prob, 'PK1.R', pk1_x0(1), 'PK1.R', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'PK1.v0', pk1_x0(2), 'PK1.v0', 'inactive');

  prob = ode_coll2coll(prob, 'PK2', last_run_name, labs(1));

  % Boundary conditions for PK2
  [pk2_data, pk2_uidx] = coco_get_func_data(prob, 'PK2.coll', 'data', 'uidx');
  pk2_maps = pk2_data.coll_seg.maps;

  args = pk2_uidx([pk2_maps.T0_idx; pk2_maps.T_idx; pk2_maps.x0_idx; ...
    pk2_maps.x1_idx; pk2_maps.p_idx]);
  prob = coco_add_func(prob, 'PK2.bvp', @duff_bc_caf, ...
    @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
    'zero', 'uidx', args);

  % Give PK2 Frequency a name.
  prob = coco_add_pars(prob, 'pk2.om', pk2_uidx(pk2_maps.p_idx(end)), 'pk2.om', 'inactive');

  pk2_x0 = pk2_uidx(pk2_maps.x0_idx);
  prob = coco_add_pars(prob, 'PK2.R', pk2_x0(1), 'PK2.R', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'PK2.v0', pk2_x0(2), 'PK2.v0', 'inactive');

  % Glue Parameters together (except forcing frequency)
  prob = coco_add_glue(prob, 'par_glue', pk2_uidx(pk2_maps.p_idx(1:5)), ...
    pk1_uidx(pk1_maps.p_idx(1:5)));
  prob = coco_add_glue(prob, 'PK.R.glue', pk2_x0(1), pk1_x0(1));

  % Parameterize frequency difference
  prob = coco_add_func(prob, 'om_diff', @udiff, @udiff_dU, @udiff_dUdU, ...
    [], 'inactive', 'om_diff', 'uidx', [pk2_uidx(pk2_maps.p_idx(6)), ...
    pk1_uidx(pk1_maps.p_idx(6))]);

  for i=1:3
    prob = ode_coll2coll(prob, strcat('OM', int2str(i)), ...
      last_run_name, labs(1));
    % Boundary conditions for OMi
    [om_data{i}, om_uidx{i}] = coco_get_func_data(prob, ...
      coco_get_id(strcat('OM', int2str(i)),'coll'), 'data', 'uidx');
    om_maps{i} = om_data{i}.coll_seg.maps;

    args = om_uidx{i}([om_maps{i}.T0_idx; om_maps{i}.T_idx; om_maps{i}.x0_idx; ...
      om_maps{i}.x1_idx; om_maps{i}.p_idx]);
    om_bvp_id = coco_get_id(strcat('OM', int2str(i)),'bvp');
    prob = coco_add_func(prob, om_bvp_id, @duff_bc_caf, ...
      @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
      'zero', 'uidx', args);

    % Maintain velocity constraint
    Rname = coco_get_id(strcat('OM', int2str(i)),'R');
    v0name = coco_get_id(strcat('OM', int2str(i)),'v0');
    om_x0{i} = om_uidx{i}(om_maps{i}.x0_idx);
    prob = coco_add_pars(prob, Rname, om_x0{i}(1), Rname, 'inactive');
    prob = coco_add_pars(prob, v0name, om_x0{i}(2), v0name, 'inactive');

    % Glue Parameters together
    if i > 1
      % The fold approximants share all parameter values.
      % They differ only in response amplitude 
      % {'OM1.R', 'OM2.R', 'OM3.R'}

      gluename = ['OM', int2str(i), '.to.OM1'];
      prob = coco_add_glue(prob, gluename, ...
        om_uidx{1}(om_maps{1}.p_idx), ...
        om_uidx{i}(om_maps{i}.p_idx));

      % Parameterize Response Amplitude Difference
      rdiffname = ['OM', int2str(i-1), 'R.minus.OM', int2str(i), 'R'];
      prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
        [], 'inactive', rdiffname, 'uidx', [om_x0{i-1}(1), om_x0{i}(1)]);

    else
      gluename = ['OM', int2str(i), '.to.PK1'];
      prob = coco_add_glue(prob, gluename, ...
        pk2_uidx(pk2_maps.p_idx(1:5)), ...
        om_uidx{1}(om_maps{1}.p_idx(1:5)));
    end
  end
  prob = coco_add_pars(prob, 'OM1.om', om_uidx{1}(om_maps{1}.p_idx(6)), 'OM1.om', 'inactive');
  
  bd = coco(prob, run_name, [], 1, {'OM2R.minus.OM3R', 'A', 'OM1.om', 'om', 'pk2.om', 'PK1.R', 'PK2.R', 'OM1.R', 'OM2.R', 'OM3.R', 'OM1R.minus.OM2R'}, [1e-2, 3]);

else
  bd = coco_bd_read(run_name);
end

%% Step 4c
%  Confirmation of the near-linear character of the
%  frequency-response cuve

last_run_name = run_name;
swp_run_name = 'sweep2';
if not(coco_exist(swp_run_name, 'run'))
  labs = coco_bd_labs(bd, 'EP');
  prob = ode_bvp2bvp(prob_init, '', last_run_name, 'PK1', labs(1));
  
  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  maps = data.coll_seg.maps;
  x0 = uidx(maps.x0_idx);
  prob = coco_add_pars(prob, 'R', x0(1), 'R', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'v0', x0(2), 'v0', 'inactive');
  
  bd_swp = coco(prob, swp_run_name, [], 1, {'om', 'R'}, {[0.1, 5]});
  semilogx(cell2mat(bd_swp(2:end, 16)), cell2mat(bd_swp(2:end, 17)));
else

end

%% Step 5
last_run_name = run_name;
run_name = 'pk_sweep';

if not(coco_exist(run_name, 'run')) || true
  labs = coco_bd_labs(bd, 'EP');
  prob = coco_set(prob_init, 'cont', 'NAdapt', 1);
  prob = coco_set(prob, 'cont', 'PtMX', 100);
  prob = ode_bvp2bvp(prob, 'PK1', last_run_name, labs(1));
  bvp_data = coco_get_func_data(prob, 'PK1.bvp', 'data');

  [pk1_data, pk1_uidx] = coco_get_func_data(prob, 'PK1.bvp.seg1.coll', 'data', 'uidx');
  pk1_maps = pk1_data.coll_seg.maps;
  pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

  prob = coco_add_pars(prob, 'PK1.R', pk1_x0(1), 'PK1.R', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'PK1.v0', pk1_x0(2), 'PK1.v0', 'inactive');

  prob = ode_coll2coll(prob, 'PK2', last_run_name, labs(1));

  % Boundary conditions for PK2
  [pk2_data, pk2_uidx] = coco_get_func_data(prob, 'PK2.coll', 'data', 'uidx');
  pk2_maps = pk2_data.coll_seg.maps;

  args = pk2_uidx([pk2_maps.T0_idx; pk2_maps.T_idx; pk2_maps.x0_idx; ...
    pk2_maps.x1_idx; pk2_maps.p_idx]);
  prob = coco_add_func(prob, 'PK2.bvp', @duff_bc_caf, ...
    @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
    'zero', 'uidx', args);

  % Give PK2 Frequency a name.
  prob = coco_add_pars(prob, 'PK2.om', pk2_uidx(pk2_maps.p_idx(end)), 'pk2.om', 'inactive');

  pk2_x0 = pk2_uidx(pk2_maps.x0_idx);
  prob = coco_add_pars(prob, 'PK2.R', pk2_x0(1), 'PK2.R', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'PK2.v0', pk2_x0(2), 'PK2.v0', 'inactive');

  % Glue Parameters together (except forcing frequency)
  prob = coco_add_glue(prob, 'par_glue', pk2_uidx(pk2_maps.p_idx(1:5)), ...
    pk1_uidx(pk1_maps.p_idx(1:5)));
  prob = coco_add_glue(prob, 'PK.R.glue', pk2_x0(1), pk1_x0(1));

  % Parameterize frequency difference
  prob = coco_add_func(prob, 'om_diff', @udiff, @udiff_dU, @udiff_dUdU, ...
    [], 'inactive', 'om_diff', 'uidx', [pk2_uidx(pk2_maps.p_idx(6)), ...
    pk1_uidx(pk1_maps.p_idx(6))]);

  for i=1:3
    prob = ode_coll2coll(prob, strcat('OM', int2str(i)), ...
      last_run_name, labs(1));
    % Boundary conditions for OMi
    [om_data{i}, om_uidx{i}] = coco_get_func_data(prob, ...
      coco_get_id(strcat('OM', int2str(i)),'coll'), 'data', 'uidx');
    om_maps{i} = om_data{i}.coll_seg.maps;

    args = om_uidx{i}([om_maps{i}.T0_idx; om_maps{i}.T_idx; om_maps{i}.x0_idx; ...
      om_maps{i}.x1_idx; om_maps{i}.p_idx]);
    om_bvp_id = coco_get_id(strcat('OM', int2str(i)),'bvp');
    prob = coco_add_func(prob, om_bvp_id, @duff_bc_caf, ...
      @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
      'zero', 'uidx', args);

    % Maintain velocity constraint
    Rname = coco_get_id(strcat('OM', int2str(i)),'R');
    v0name = coco_get_id(strcat('OM', int2str(i)),'v0');
    om_x0{i} = om_uidx{i}(om_maps{i}.x0_idx);
    prob = coco_add_pars(prob, Rname, om_x0{i}(1), Rname, 'inactive');
    prob = coco_add_pars(prob, v0name, om_x0{i}(2), v0name, 'inactive');

    % Glue Parameters together
    if i > 1
      % The fold approximants share all parameter values.
      % They differ only in response amplitude 
      % {'OM1.R', 'OM2.R', 'OM3.R'}

      gluename = ['OM', int2str(i), '.to.OM1'];
      prob = coco_add_glue(prob, gluename, ...
        om_uidx{1}(om_maps{1}.p_idx), ...
        om_uidx{i}(om_maps{i}.p_idx));

      % Parameterize Response Amplitude Difference
      rdiffname = ['OM', int2str(i-1), 'R.minus.OM', int2str(i), 'R'];
      prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
        [], 'inactive', rdiffname, 'uidx', [om_x0{i-1}(1), om_x0{i}(1)]);

    else
      gluename = ['OM', int2str(i), '.to.PK1'];
      prob = coco_add_glue(prob, gluename, ...
        pk2_uidx(pk2_maps.p_idx(1:5)), ...
        om_uidx{1}(om_maps{1}.p_idx(1:5)));
    end
  end
  prob = coco_add_pars(prob, 'OM1.om', om_uidx{1}(om_maps{1}.p_idx(6)), 'OM1.om', 'inactive');
  
  [uidx, data] = coco_get_func_data(prob, 'PK1.bvp.seg1.coll', 'uidx', 'data');
  mk_idx = uidx(data.coll_seg.maps.p_idx([1,3]));
%   prob = coco_add_func(prob, 'km_constraint', ...
%     @km_const, @km_const_du, @km_const_dudu, [], ...
%     'zero', 'uidx', mk_idx);
  
  bd = coco(prob, run_name, [], 1, {'PK1.R', 'om', 'A', 'OM1.om', 'PK2.om', 'PK2.R', 'OM1.R', 'OM2.R', 'OM3.R'});

else
  bd = coco_bd_read(run_name);
end