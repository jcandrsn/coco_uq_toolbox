figure2 = figure(2);
clf

figname = 'curve_build_btm_fold.eps';
fp = strcat(img_path, figname);

if ~coco_exist('uq_sweep_2', 'run')
  addpath('../../duff')
  addpath('../../utils')  
  last_run_name = 'fold_find_btm';

  bd = coco_bd_read(last_run_name);
  labs = coco_bd_labs(bd, 'UZ');
  lab = labs(1);
  prob = coco_prob();
  prob = coco_set(prob, 'coll', 'NTST', 30);
  
  prob = ode_bvp2bvp(prob, 'PK1', last_run_name, lab);
  bvp_data = coco_get_func_data(prob, 'PK1.bvp', 'data');

  [pk1_data, pk1_uidx] = coco_get_func_data(prob, ...
    'PK1.bvp.seg1.coll', 'data', 'uidx');
  pk1_maps = pk1_data.coll_seg.maps;
  pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

  % Maintain velocity constraint (could also be done with a
  % boundary condition)
  prob = coco_add_pars(prob, 'PK1.v0', pk1_x0(2), ...
    'PK1.v0', 'inactive');
  prob = coco_add_pars(prob, 'R', pk1_x0(1), ...
    'R', 'inactive');
  bd = coco(prob, 'uq_sweep_2', [], {'om', 'R'});
else
  bd = coco_bd_read('uq_sweep_2');
end

% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
hold on; grid on; box on;
xlim([1.25,1.8]);
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)

semilogx(coco_bd_col(bd, 'om'), coco_bd_col(bd, 'R'), 'k-', 'LineWidth', 1)
% OM points
top_fold_bd = coco_bd_read('fold_find_top');
om34_idx = strcmpi(coco_bd_col(top_fold_bd, 'TYPE'), 'UZ');
idx = 1:numel(om34_idx);
om34_idx = idx(om34_idx);

om =  coco_bd_col(top_fold_bd, 'om');
pk1r = coco_bd_col(top_fold_bd, 'R');
om1 = coco_bd_col(top_fold_bd, 'OM1.om');
rom1 = coco_bd_col(top_fold_bd, 'OM1.R');
rom2 = coco_bd_col(top_fold_bd, 'OM2.R');
rom3 = coco_bd_col(top_fold_bd, 'OM3.R');
om = [om1(om34_idx(1)), om1(om34_idx(1)), ...
  om1(om34_idx(1))];
R = [rom1(om34_idx(1)), rom2(om34_idx(1)), ...
  rom3(om34_idx(1))];
semilogx(om, R, 'ko', 'MarkerSize', marker_size_2, 'MarkerFaceColor', [0.5,0.5,0.5])

semilogx([om(1), om(1)], [min(R)+0.01,max(R)-0.05], 'k--')

dim = [1.26, 0.87, 0.155, 0.23];
bx = xy_annotation(gca, 'rectangle', dim, ...
  'LineWidth', 1, 'LineStyle', 'none');

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;

% % Bottom Fold Box
% dim = [1.48, 0.77, ...
%         0.15, 0.14];
% str = {'A \downarrow , \epsilon_{\omega,2} \downarrow'};
% xy_annotation(gca, 'textbox', dim, ...
%   'LineStyle', '-', 'String', str, ...
%   'FitBoxToText', 'off', 'FontSize', font_size, ...
%   'BackgroundColor', 'white', ...
%   'VerticalAlignment', 'middle', ...
%   'HorizontalAlignment', 'center');

xy_annotation(gca, 'line', ...
  [om(2)+0.007, om(2)+0.015], ...
  [R(1), R(1)], ...
  'LineWidth', 1);

xy_annotation(gca, 'line', ...
  [om(2)+0.007, om(2)+0.015], ...
  [R(2), R(2)], ...
  'LineWidth', 1);

xy_annotation(gca, 'line', ...
  [om(2)+0.007, om(2)+0.015], ...
  [R(3), R(3)], ...
  'LineWidth', 1);

xy_annotation(gca, 'arrow', ...
  [om(2)+0.01, om(2)+0.01], ...
  [R(1)+0.05, R(1)], ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xy_annotation(gca, 'doublearrow', ...
  [om(2)+0.01, om(2)+0.01], ...
  [R(2), R(3)], ...
  'LineWidth', 1, 'HeadStyle', 'cback1', ...
  'Head1Width', 6, 'Head2Width', 6);

xy_annotation(gca, 'textarrow', ...
  [1.75, 1.75], ...
  [rom2(fake_om12_idx(1))-0.02, rom2(fake_om12_idx(1))-0.02], ...
  'String', '$\epsilon_{\omega,1}$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'none', 'FontSize', font_size);
clear mean
xy_annotation(gca, 'textarrow', ...
  [1.73, om(2)+0.01], ...
  [mean(R(2:3)), mean(R(2:3))], ...
  'String', '$\epsilon_{\omega,2}$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'none', 'FontSize', font_size);

btm_fold_bd = coco_bd_read('fold_find_btm');
idx = strcmpi(coco_bd_col(btm_fold_bd, 'TYPE'), 'UZ');
idx2 = 1:numel(idx);
idx = idx2(idx);

ompk1 = coco_bd_col(btm_fold_bd, 'om');
rpk1 = coco_bd_col(btm_fold_bd, 'R');
omom = coco_bd_col(btm_fold_bd, 'OM.om');
om1r = coco_bd_col(btm_fold_bd, 'OM1.R');

semilogx([ompk1(idx), ompk1(idx)+0.01, omom(idx), omom(idx), omom(idx)], ...
  [rpk1(idx), rpk1(idx), om1r(idx), om1r(idx)-0.01, om1r(idx)-0.02], ...
  'ko', 'MarkerSize', marker_size_2, 'MarkerFaceColor', [0.5,0.5,0.5]);

xy_annotation(ax, 'arrow', [1.68, 1.6], [1.45, 1.2], ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);
xy_annotation(ax, 'textbox', [1.59, 1.3, 0.05, 0.1], ...
  'String', 'A \downarrow', 'FontSize', font_size, ...
  'FitBoxToText', 'on', 'LineStyle', 'none');

saveas(figure2, fp, 'epsc');