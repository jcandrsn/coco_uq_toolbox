
last_run_name = 'fold_find_btm';
run_name = 'uq_nds';
bd = coco_bd_read(last_run_name);
labs = coco_bd_labs(bd, 'EP');
prob = coco_prob();
prob = coco_set(prob, 'coll', 'NTST', 20);
prob = ode_bvp2bvp(prob, 'PK1', last_run_name, labs(1));
bvp_data = coco_get_func_data(prob, 'PK1.bvp', 'data');

[pk1_data, pk1_uidx] = coco_get_func_data(prob, ...
  'PK1.bvp.seg1.coll', 'data', 'uidx');
pk1_maps = pk1_data.coll_seg.maps;
pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

prob = coco_add_pars(prob, 'PK1.R', pk1_x0(1), ...
  'PK1.R', 'inactive');
% Maintain velocity constraint (could also be done with a
% boundary condition)
prob = coco_add_pars(prob, 'PK1.v0', pk1_x0(2), ...
  'PK1.v0', 'inactive');

prob = ode_coll2coll(prob, 'PK2', last_run_name, labs(1));

% Boundary conditions for PK2
[pk2_data, pk2_uidx] = coco_get_func_data(prob, 'PK2.coll', 'data', 'uidx');
pk2_maps = pk2_data.coll_seg.maps;

args = pk2_uidx([pk2_maps.T0_idx; pk2_maps.T_idx; pk2_maps.x0_idx; ...
  pk2_maps.x1_idx; pk2_maps.p_idx]);
prob = coco_add_func(prob, 'PK2.bvp', @duff_bc_caf, ...
  @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
  'zero', 'uidx', args);

% Give PK2 Frequency a name.
pidx = pk2_maps.p_idx(end);
prob = coco_add_pars(prob, 'pk2.om', pk2_uidx(pidx), ...
  'pk2.om', 'inactive');

pk2_x0 = pk2_uidx(pk2_maps.x0_idx);
prob = coco_add_pars(prob, 'PK2.R', pk2_x0(1), ...
  'PK2.R', 'inactive');

% Maintain velocity constraint (could also be done with a
% boundary condition)
prob = coco_add_pars(prob, 'PK2.v0', pk2_x0(2), ...
  'PK2.v0', 'inactive');

% Glue Parameters together (except forcing frequency)
prob = coco_add_glue(prob, 'par_glue', pk2_uidx(pk2_maps.p_idx(1:5)), ...
  pk1_uidx(pk1_maps.p_idx(1:5)));
prob = coco_add_glue(prob, 'PK.R.glue', pk2_x0(1), pk1_x0(1));

% Parameterize frequency difference
prob = coco_add_func(prob, 'om_diff', @udiff, @udiff_dU, @udiff_dUdU, ...
  [], 'inactive', 'om_diff', 'uidx', [pk2_uidx(pk2_maps.p_idx(6)), ...
  pk1_uidx(pk1_maps.p_idx(6))]);

% Reconstruct top approximate fold
top_fold_name = 'fold_find_top';
top_fold_bd = coco_bd_read(top_fold_name);
top_labs = coco_bd_labs(top_fold_bd);

for i=1:2
  prob = ode_coll2coll(prob, strcat('OM', int2str(i)), ...
    top_fold_name, top_labs(1));
  % Boundary conditions for OMi
  [om_data{i}, om_uidx{i}] = coco_get_func_data(prob, ...
    coco_get_id(strcat('OM', int2str(i)),'coll'), 'data', 'uidx');
  om_maps{i} = om_data{i}.coll_seg.maps;

  args = om_uidx{i}([om_maps{i}.T0_idx; om_maps{i}.T_idx; om_maps{i}.x0_idx; ...
    om_maps{i}.x1_idx; om_maps{i}.p_idx]);
  om_bvp_id = coco_get_id(strcat('OM', int2str(i)),'bvp');
  prob = coco_add_func(prob, om_bvp_id, @duff_bc_caf, ...
    @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
    'zero', 'uidx', args);

  % Maintain velocity constraint
  Rname = coco_get_id(strcat('OM', int2str(i)),'R');
  v0name = coco_get_id(strcat('OM', int2str(i)),'v0');
  om_x0{i} = om_uidx{i}(om_maps{i}.x0_idx);
  prob = coco_add_pars(prob, Rname, om_x0{i}(1), Rname, 'inactive');
  prob = coco_add_pars(prob, v0name, om_x0{i}(2), v0name, 'inactive');

  % Glue Parameters together
  if i > 1
    % The fold approximants share all parameter values.
    % They differ only in response amplitude 
    % {'OM1.R', 'OM2.R', 'OM3.R'}

    gluename = ['OM', int2str(i), '.to.OM1'];
    prob = coco_add_glue(prob, gluename, ...
      om_uidx{1}(om_maps{1}.p_idx), ...
      om_uidx{i}(om_maps{i}.p_idx));

    % Parameterize Response Amplitude Difference
    rdiffname = ['OM', int2str(i-1), 'R.minus.OM', int2str(i), 'R'];
    prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
      [], 'inactive', rdiffname, 'uidx', [om_x0{i-1}(1), om_x0{i}(1)]);

  else
    gluename = ['OM', int2str(i), '.to.PK1'];
    prob = coco_add_glue(prob, gluename, ...
      pk2_uidx(pk2_maps.p_idx(1:5)), ...
      om_uidx{1}(om_maps{1}.p_idx(1:5)));
  end
end

% Reconstruct bottom approximate fold
btm_fold_name = 'fold_find_btm';
btm_fold_bd = coco_bd_read(btm_fold_name);

for i=3:4
  prob = ode_coll2coll(prob, strcat('OM', int2str(i)), ...
    btm_fold_name, strcat('OM', int2str(i-1)), labs(1));
  % Boundary conditions for OMi
  [om_data{i}, om_uidx{i}] = coco_get_func_data(prob, ...
    coco_get_id(strcat('OM', int2str(i)),'coll'), 'data', 'uidx');
  om_maps{i} = om_data{i}.coll_seg.maps;

  args = om_uidx{i}([om_maps{i}.T0_idx; om_maps{i}.T_idx; om_maps{i}.x0_idx; ...
    om_maps{i}.x1_idx; om_maps{i}.p_idx]);
  om_bvp_id = coco_get_id(strcat('OM', int2str(i)),'bvp');
  prob = coco_add_func(prob, om_bvp_id, @duff_bc_caf, ...
    @duff_bc_caf_du, @duff_bc_caf_dudu, bvp_data, ...
    'zero', 'uidx', args);

  % Maintain velocity constraint
  Rname = coco_get_id(strcat('OM', int2str(i)),'R');
  v0name = coco_get_id(strcat('OM', int2str(i)),'v0');
  om_x0{i} = om_uidx{i}(om_maps{i}.x0_idx);
  prob = coco_add_pars(prob, Rname, om_x0{i}(1), Rname, 'inactive');
  prob = coco_add_pars(prob, v0name, om_x0{i}(2), v0name, 'inactive');

  % Glue Parameters together
  if i == 4
    gluename = ['OM', int2str(i), '.to.OM1'];
    prob = coco_add_glue(prob, gluename, ...
      om_uidx{i-1}(om_maps{1}.p_idx), ...
      om_uidx{i}(om_maps{i}.p_idx));

    % Parameterize Response Amplitude Difference
    rdiffname = ['OM', int2str(i-1), 'R.minus.OM', int2str(i), 'R'];
    prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
      [], 'inactive', rdiffname, 'uidx', [om_x0{i-1}(1), om_x0{i}(1)]);
  else
    gluename = ['OM', int2str(i), '.to.PK1'];
    prob = coco_add_glue(prob, gluename, ...
      pk2_uidx(pk2_maps.p_idx(1:5)), ...
      om_uidx{i}(om_maps{i}.p_idx(1:5)));
  end
end

prob = coco_add_pars(prob, 'OM1.om', om_uidx{1}(om_maps{1}.p_idx(6)), 'OM1.om', 'inactive');
prob = coco_add_pars(prob, 'OM3.om', om_uidx{3}(om_maps{3}.p_idx(6)), 'OM3.om', 'inactive');
prob = coco_add_func(prob, 'hyst', @udiff, @udiff_dU, @udiff_dUdU, ...
      [], 'inactive', 'hyst', 'uidx', ...
      [om_uidx{1}(om_maps{1}.p_idx(6)), ...
       om_uidx{3}(om_maps{3}.p_idx(6))]);
pars_to_release = {'hyst', 'PK1.R', 'PK2.R',  'pk2.om', 'OM1.om', 'OM1.R', 'OM2.R', 'OM3.om', 'OM3.R', 'OM4.R', 'om_diff' 'OM1R.minus.OM2R', 'OM3R.minus.OM4R'};
%% UQ Toolbox Arguments
args = struct(); opts=struct();
args.spnames = {'alpha'};
args.spdists = {'Uniform'};
args.spdp = [0.3, 0.7];
args.dpdtpars = {'om'};
opts.addadjt = 1;
%% Initialize UQ Data Structure
bvp_id = coco_get_id('PK1', 'bvp');
bc_data = coco_get_func_data(prob, bvp_id, 'data');  
uq_data = bvp_uq_init_data(bc_data, 'PK1');
% Calculates Integration nodes and weights for the
% continuation problem.
uq_data = uq_init_data(prob, uq_data, args, opts);

% Mark the integration node locations with the label 'UQ'
prob = coco_add_event(prob, 'UQ', 'alpha', uq_data.nds);
bd = coco(prob, run_name, [], 1, ...
  {'alpha', 'om', pars_to_release{:}, 'k', 'm'},[0.3,0.7]);