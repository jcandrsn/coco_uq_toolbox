function [data, y] = resp_pce(prob, data, u)
%UQ_PCE_COEFFICIENTS Summary of this function goes here
%   Evaluate Response function and calculate coefficients of polynomial
%   chaos expansion.

x = reshape(u(1:end-data.Nt), [], data.nsamples);

r = (x(1,:) - 5*abs(x(2,:) - x(3,:)))';

alphas = u(end-data.Nt+1:end);
R = data.wtd_psi_mat*r;

y = alphas - R;

end