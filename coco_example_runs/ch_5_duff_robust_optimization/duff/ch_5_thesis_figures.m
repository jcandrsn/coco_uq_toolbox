%% Duffing Hysteresis
%  Plot the frequency sweep.  The unstable segment plotted
%  with dashed lines, the stable segments with solid lines,
%  the hysteresis transitions shown with arrows.  The
%  forward sweep plotted with a red line, the reverse sweep
%  plotted with a blue segment.  Remove axis numbers.

addpath('../../utils');

img_path = ['/Users/jcoleanderson/Google Drive/',...
 'Grad School/00_Thesis Research/00_Document/Images/'];
font_size = 15;
swp_bd = coco_bd_read('sweep');

om_vals = coco_bd_col(swp_bd, 'om');
R_vals  = coco_bd_col(swp_bd, 'R');
fold_idx = strcmpi(coco_bd_col(swp_bd, 'TYPE'), 'FP');
idx = 1:numel(fold_idx);
fold_idx = idx(fold_idx);
om_idx_left = om_vals <= om_vals(fold_idx(2));
om_idx_right = om_vals >= om_vals(fold_idx(1));

[PK_max, PK_idx] = max(R_vals);
up_idx = (R_vals < PK_max);

figure1 = figure(1);
hold on;grid on;box on;

% Primary frequency response curve.
% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)
% Fold points
% semilogx(om_vals(fold_idx), R_vals(fold_idx), 'ko', ...
%   'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size)
xlim([1.25,1.8]);
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;
ax.XScale = 'log';

xx = [om_vals(fold_idx(1)),om_vals(fold_idx(1))];
yy = [R_vals(fold_idx(1))-0.05, 0.68];
xy_annotation(gca, 'arrow', xx, yy, ...
  'Color', [1,0,0], 'LineStyle', ':', 'LineWidth', 1, ...
  'HeadStyle', 'cback1', 'HeadWidth', 6);

xx = [1.71, 1.78];
yy = [0.6, 0.45];
xy_annotation(gca, 'arrow', xx, yy, ...
  'Color', [1,0,0], 'LineStyle', ':', 'LineWidth', 1, ...
  'HeadStyle', 'cback1', 'HeadWidth', 6);

xx = [1.77, 1.7];
yy = [0.42, 0.57];
xy_annotation(gca, 'arrow', xx, yy, ...
  'Color', [0,0,1], 'LineStyle', '-.', 'LineWidth', 1, ...
  'HeadStyle', 'cback1', 'HeadWidth', 6);

xx = [1.39,1.48];
yy = [1.005,1.19];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'Color', [1,0,0], 'LineStyle', ':', ...
  'String', 'Sweep Up', ...
  'TextColor', [0,0,0], ...
  'VerticalAlignment', 'baseline', ...
  'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.48,1.39];
yy = [1.14,0.96];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'Color', [0,0,1], 'LineStyle', '-.', ...
  'String', 'Sweep Down', ...
  'TextColor', [0,0,0], ...
  'VerticalAlignment', 'top', ...
  'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [om_vals(fold_idx(2)),om_vals(fold_idx(2))];
yy = [R_vals(fold_idx(2))+0.1, 1.47];
xy_annotation(gca, 'arrow', xx, yy, ...
  'Color', [0,0,1], 'LineStyle', '-.', 'LineWidth', 1, ...
  'HeadStyle', 'cback1', 'HeadWidth', 6);

xx = [om_vals(fold_idx(2))-0.05,om_vals(fold_idx(2))];
yy = [1.3,1.3];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'String', '$\Delta \omega$ ', 'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [om_vals(fold_idx(1))+0.05,om_vals(fold_idx(1))];
yy = [1.3,1.3];
xy_annotation(gca, 'arrow', xx, yy, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

figname = 'duffing_hysteresis.eps';
fp = strcat(img_path, figname);

saveas(figure1, fp, 'epsc');

%% Duffing Multiple Orbits
%  Plot the frequency sweep showing the actual fold points
%  and the approximate fold points.  Show the
%  approximations as insets of the larger figure.
font_size = 20;
marker_size_1 = 10;
marker_size_2 = 7;
figname = 'curve_build_initial_phase.eps';
fp = strcat(img_path, figname);
figure2 = figure(2);
xlim([0, 2*pi])
hold on; grid on; box on;
[sol, ~] = coll_read_solution('bvp.seg1', 'set_v0', 1);
pos = plot(sol.tbp, sol.xbp(:,1), 'k', 'DisplayName','Position', 'LineWidth', 1);
vel = plot(sol.tbp, sol.xbp(:,2), 'k--', 'DisplayName','Velocity', 'LineWidth', 1);

lgd = legend([pos, vel]);
lgd.Interpreter = 'latex';
lgd.Location = 'southeast';

xlim([0, 2*pi])
xlabel('Time, $t$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.XTick = [0,pi/2, pi, 3*pi/2, 2*pi];
ax.TickLabelInterpreter = 'latex';
ax.XTickLabel = {'$0$', '$\frac{\pi}{2}$', '$\pi$', '$\frac{3\pi}{2}$', '$2\pi$'};
ax.FontSize = font_size;

saveas(figure2, fp, 'epsc');
clf

bd = coco_bd_read('set_v0');
labs = coco_bd_labs(bd, 'MAX');
[sol, ~] = coll_read_solution('bvp.seg1', 'set_v0', labs(2));

figname = 'curve_build_max_phase.eps';
fp = strcat(img_path, figname);

pos = plot(sol.tbp, sol.xbp(:,1), 'k', ...
  'DisplayName','Position', 'LineWidth', 1);
hold on; grid on; box on;
vel = plot(sol.tbp, sol.xbp(:,2), 'k--', ...
  'DisplayName','Velocity', 'LineWidth', 1);

lgd = legend([pos, vel]);
lgd.Interpreter = 'latex';
lgd.Location = 'southeast';

xlim([0, 2*pi])

xlabel('Time, $t$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.XTick = [0,pi/2, pi, 3*pi/2, 2*pi];
ax.TickLabelInterpreter = 'latex';
ax.XTickLabel = {'$0$', '$\frac{\pi}{2}$', '$\pi$', '$\frac{3\pi}{2}$', '$2\pi$'};
ax.FontSize =  font_size;

saveas(figure2, fp, 'epsc');
clf
% Primary frequency response curve.
figname = 'curve_build_freq_sweep.eps';
fp = strcat(img_path, figname);

% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
hold on; grid on; box on;
xlim([1.25,1.8]);
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

pk_idx = strcmpi(coco_bd_col(swp_bd, 'TYPE'), 'PK');
om_idx = strcmpi(coco_bd_col(swp_bd, 'TYPE'), 'OM');
idx = 1:numel(pk_idx);
pk_idx = idx(pk_idx);
om_idx = idx(om_idx);

% PK points
semilogx([om_vals(pk_idx(1)),om_vals(pk_idx(2))],[1.4,1.4], 'k--')
semilogx(om_vals(pk_idx), R_vals(pk_idx), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)

% OM points
semilogx([1.67,1.67],[min(R_vals(om_idx)),max(R_vals(om_idx))], 'k--')
semilogx(om_vals(om_idx), R_vals(om_idx), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)
xx = [om_vals(fold_idx(1))+0.05,om_vals(fold_idx(1))];
yy = [1.3,1.39];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'String', 'PK', 'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.55, 1.59];
yy = [1.4, 1.4];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'String', 'PK', 'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.62, 1.66];
yy = [R_vals(om_idx(1)), R_vals(om_idx(1))];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'String', 'OM', 'Interpreter', 'latex', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.74, 1.675];
yy = [1, R_vals(om_idx(2))-0.01];
xy_annotation(gca, 'textarrow', xx, yy, ...
  'String', 'OM', 'Interpreter', 'latex', ...
  'VerticalAlignment', 'middle', ...
  'FontSize', font_size, 'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.74, 1.675];
yy = [1, R_vals(om_idx(3))+0.01];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;

saveas(figure2, fp, 'epsc');
clf

figname = 'curve_build_peak.eps';
fp = strcat(img_path, figname);

% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
hold on; grid on; box on;
xlim([1.25,1.8]);
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)

% PK points
semilogx([om_vals(pk_idx(1)),om_vals(pk_idx(2))],[1.4,1.4], 'k--')
semilogx(om_vals(pk_idx), R_vals(pk_idx), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)

xx = [1.59, 1.63];
yy = [1.4, 1.47];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.695, 1.7];
yy = [1.4, 1.47];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;

dim = [1.26, 1.35, 0.155, 0.23];
bx = xy_annotation(gca, 'rectangle', dim, ...
  'LineWidth', 1, 'LineStyle', 'none');

pk_bd = coco_bd_read('pk');
pk_idx = strcmpi(coco_bd_col(pk_bd, 'TYPE'), 'PK');
idx = 1:numel(pk_idx);
pk_idx = idx(pk_idx);
fake_pk_idx = idx(pk_idx)+2;
semilogx(om_vals(pk_idx), R_vals(pk_idx), 'ko', 'MarkerSize', marker_size_2)

ompk1 = coco_bd_col(pk_bd, 'om');
rpk1 = coco_bd_col(pk_bd, 'R.PK1');
ompk2 = coco_bd_col(pk_bd, 'pk2.om');
rpk2 = coco_bd_col(pk_bd, 'R.PK2');
om = [ompk1(fake_pk_idx),ompk2(fake_pk_idx)];
R = [rpk1(fake_pk_idx), rpk2(fake_pk_idx)];
semilogx(ompk1(pk_idx), rpk1(pk_idx), 'ko', 'MarkerSize', marker_size_2)

rect_width = 0.03;
rect_height = 0.06;
% Peak Box
dim = [ompk1(pk_idx)-rect_width/2, ...
  rpk1(pk_idx) - rect_height/2, ...
  rect_width, rect_height];
xy_annotation(gca, 'rectangle', dim, ...
  'LineStyle', '--');

axes('Position', bx.Position);
hold on; box on; grid on;
set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
xlim([1.64, 1.7]);
ylim([1.475, 1.535]);

semilogx(om, R, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_1)
semilogx(ompk1(pk_idx), rpk1(pk_idx), 'ko', 'MarkerSize', marker_size_1)

xy_annotation(gca, 'line', ...
  [ompk1(fake_pk_idx),ompk1(fake_pk_idx)], ...
  [rpk1(fake_pk_idx)+0.005, rpk1(fake_pk_idx)+0.025], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'line', ...
  [ompk2(fake_pk_idx),ompk2(fake_pk_idx)], ...
  [rpk2(fake_pk_idx)+0.005, rpk2(fake_pk_idx)+0.025], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'doublearrow', ...
  [ompk1(fake_pk_idx),ompk2(fake_pk_idx)], ...
  [rpk1(fake_pk_idx)+0.02, rpk2(fake_pk_idx)+0.02], ...
  'LineWidth', 1, 'HeadStyle', 'cback1', ...
  'Head1Width', 6, 'Head2Width', 6);

xy_annotation(gca, 'textarrow', ...
  [ompk1(fake_pk_idx)-0.01,ompk1(fake_pk_idx)], ...
  [rpk1(fake_pk_idx)+0.02, rpk2(fake_pk_idx)+0.02], ...
  'String', '$\epsilon_\omega$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'none', 'FontSize', font_size);

saveas(figure2, fp, 'epsc');
clf

figname = 'curve_build_top_fold.eps';
fp = strcat(img_path, figname);

% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
hold on; grid on; box on;
xlim([1.25,1.8]);
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)

% OM points
semilogx([1.67,1.67],[min(R_vals(om_idx(1:2))),max(R_vals(om_idx(1:2)))], 'k--')
semilogx(om_vals(om_idx(1:2)), R_vals(om_idx(1:2)), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)


xx = [1.68, 1.695];
yy = [1.2, 1.35];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.67, 1.7];
yy = [1.52, 1.51];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

dim = [1.26, 1.11, 0.155, 0.23];
bx = xy_annotation(gca, 'rectangle', dim, ...
  'LineWidth', 1, 'LineStyle', 'none');

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;

om12_bd = coco_bd_read('fold_find_top');
om12_idx = strcmpi(coco_bd_col(om12_bd, 'TYPE'), 'EP');
idx = 1:numel(om12_idx);
om12_idx = idx(om12_idx);
fake_om12_idx = idx(om12_idx)+2;

om1 = coco_bd_col(om12_bd, 'OM1.om');
rom1 = coco_bd_col(om12_bd, 'OM1.R');
rom2 = coco_bd_col(om12_bd, 'OM2.R');
om = [om1(fake_om12_idx(1)),om1(fake_om12_idx(1))];
R = [rom1(fake_om12_idx(1)), rom2(fake_om12_idx(1))];
semilogx(om1(om12_idx(1)), rom1(om12_idx(1)), 'ko', 'MarkerSize', marker_size_2)

% Top Fold Box
dim = [om1(om12_idx(1))-rect_width/2, ...
  rom1(om12_idx(1)) - rect_height/2, ...
  rect_width, rect_height];
xy_annotation(gca, 'rectangle', dim, ...
  'LineStyle', '--');

axes('Position', bx.Position);
hold on; box on; grid on;
set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)
xlim([1.685, 1.715]);
ylim([1.42, 1.48]);

semilogx(om, R, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_1)
semilogx(om1(om12_idx), rom1(om12_idx), 'ko', 'MarkerSize', marker_size_1)

xy_annotation(gca, 'line', ...
  [om1(fake_om12_idx(1))+0.001,om1(fake_om12_idx(1))+0.01], ...
  [rom1(fake_om12_idx(1)), rom1(fake_om12_idx(1))], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'line', ...
  [om1(fake_om12_idx(1))+0.001,om1(fake_om12_idx(1))+0.01], ...
  [rom2(fake_om12_idx(1)), rom2(fake_om12_idx(1))], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'doublearrow', ...
  [om1(fake_om12_idx(1))+0.007, om1(fake_om12_idx(1))+0.007], ...
  [rom1(fake_om12_idx(1)), rom2(fake_om12_idx(1))], ...
  'LineWidth', 1, 'HeadStyle', 'cback1', ...
  'Head1Width', 6, 'Head2Width', 6);

xy_annotation(gca, 'line', ...
  [om1(fake_om12_idx(1))+0.007, om1(fake_om12_idx(1))+0.007], ...
  [rom2(fake_om12_idx(1))-0.005, rom2(fake_om12_idx(1))], ...
  'LineWidth', 1);

xy_annotation(gca, 'textarrow', ...
  [om1(fake_om12_idx(1))+0.012, om1(fake_om12_idx(1))+0.007], ...
  [rom2(fake_om12_idx(1))-0.005, rom2(fake_om12_idx(1))-0.005], ...
  'String', '$\epsilon_R$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'none', 'FontSize', font_size);

saveas(figure2, fp, 'epsc');
clf

figname = 'curve_build_btm_fold.eps';
fp = strcat(img_path, figname);

% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
hold on; grid on; box on;
xlim([1.25,1.8]);
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)

% OM points
semilogx([1.67,1.67],[min(R_vals(om_idx(2:3))),max(R_vals(om_idx(2:3)))], 'k--')
semilogx(om_vals(om_idx(2:3)), R_vals(om_idx(2:3)), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)

xx = [1.66, 1.65];
yy = [1.2, 1.05];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

xx = [1.66, 1.65];
yy = [0.75, 0.9];
xy_annotation(gca, 'arrow', xx, yy, ...
  'LineWidth', 1, ...
  'HeadStyle', 'cback1','HeadWidth', 6);

dim = [1.26, 0.87, 0.155, 0.23];
bx = xy_annotation(gca, 'rectangle', dim, ...
  'LineWidth', 1, 'LineStyle', 'none');

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;

om34_bd = coco_bd_read('fold_find_btm');
om34_idx = strcmpi(coco_bd_col(om34_bd, 'TYPE'), 'EP');
idx = 1:numel(om34_idx);
om34_idx = idx(om34_idx);
fake_om34_idx = idx(om34_idx)+2;

om3 = coco_bd_col(om34_bd, 'OM2.om');
rom3 = coco_bd_col(om34_bd, 'OM2.R');
rom4 = coco_bd_col(om34_bd, 'OM3.R');
om = [om3(fake_om34_idx(1)),om3(fake_om34_idx(1))];
R = [rom3(fake_om34_idx(1)), rom4(fake_om34_idx(1))];
semilogx(om3(om34_idx(1)), rom3(om34_idx(1)), 'ko', 'MarkerSize', marker_size_2)

% Bottom Fold Box
dim = [om3(om34_idx(1))-rect_width/2, ...
  rom3(om34_idx(1)) - rect_height/2, ...
  rect_width, rect_height];
xy_annotation(gca, 'rectangle', dim, ...
  'LineStyle', '--');

axes('Position', bx.Position);
hold on; box on; grid on;
set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)
xlim([1.645, 1.675]);
ylim([0.91, 0.99]);

semilogx(om, R, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_1)
semilogx(om3(om34_idx), rom3(om34_idx), 'ko', 'MarkerSize', marker_size_1)

xy_annotation(gca, 'line', ...
  [om3(fake_om34_idx(1))+0.001,om3(fake_om34_idx(1))+0.01], ...
  [rom3(fake_om34_idx(1)), rom3(fake_om34_idx(1))], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'line', ...
  [om3(fake_om34_idx(1))+0.001,om3(fake_om34_idx(1))+0.01], ...
  [rom4(fake_om34_idx(1)), rom4(fake_om34_idx(1))], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'doublearrow', ...
  [om3(fake_om34_idx(1))+0.007, om3(fake_om34_idx(1))+0.007], ...
  [rom3(fake_om34_idx(1)), rom4(fake_om34_idx(1))], ...
  'LineWidth', 1, 'HeadStyle', 'cback1', ...
  'Head1Width', 6, 'Head2Width', 6);

xy_annotation(gca, 'line', ...
  [om3(fake_om34_idx(1))+0.007, om3(fake_om34_idx(1))+0.007], ...
  [rom4(fake_om34_idx(1))-0.005, rom4(fake_om34_idx(1))], ...
  'LineWidth', 1);

xy_annotation(gca, 'textarrow', ...
  [om3(fake_om34_idx(1))+0.01, om3(fake_om34_idx(1))+0.007], ...
  [rom4(fake_om34_idx(1))-0.005, rom4(fake_om34_idx(1))-0.005], ...
  'String', '$\epsilon_R$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'none', 'FontSize', font_size);

saveas(figure2, fp, 'epsc');
clf

figname = 'curve_build_final.eps';
fp = strcat(img_path, figname);

% Stable branches
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
hold on; grid on; box on;
xlim([1.25,1.8]);
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)
main_axis = gca;


om = [om1(fake_om12_idx(1)),om1(fake_om12_idx(1))];
R = [rom1(fake_om12_idx(1)), rom2(fake_om12_idx(1))];

xlabel('Forcing Frequency, $\omega$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Response Amplitude, $R$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize =  font_size;

dim = [1.26, 1.35, 0.155, 0.23];
bx = xy_annotation(main_axis, 'rectangle', dim, ...
  'LineWidth', 1, 'LineStyle', 'none');

pk_bd = coco_bd_read('pk');
pk_idx = strcmpi(coco_bd_col(pk_bd, 'TYPE'), 'PK');
idx = 1:numel(pk_idx);
pk_idx = idx(pk_idx);
fake_pk_idx = idx(pk_idx)+2;

ompk1 = coco_bd_col(pk_bd, 'om');
rpk1 = coco_bd_col(pk_bd, 'R.PK1');
ompk2 = coco_bd_col(pk_bd, 'pk2.om');
rpk2 = coco_bd_col(pk_bd, 'R.PK2');

om12_bd = coco_bd_read('fold_find_top');
om12_idx = strcmpi(coco_bd_col(om12_bd, 'TYPE'), 'EP');
idx = 1:numel(om12_idx);
om12_idx = idx(om12_idx);
fake_om12_idx = idx(om12_idx)+2;

om1 = coco_bd_col(om12_bd, 'OM1.om');
rom1 = coco_bd_col(om12_bd, 'OM1.R');
rom2 = coco_bd_col(om12_bd, 'OM2.R');

om34_bd = coco_bd_read('fold_find_btm');
om34_idx = strcmpi(coco_bd_col(om34_bd, 'TYPE'), 'EP');
idx = 1:numel(om34_idx);
om34_idx = idx(om34_idx);
fake_om34_idx = idx(om34_idx)+2;

om3 = coco_bd_col(om34_bd, 'OM2.om');
rom3 = coco_bd_col(om34_bd, 'OM2.R');
rom4 = coco_bd_col(om34_bd, 'OM3.R');

semilogx(ompk1(pk_idx), rpk1(pk_idx), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)
semilogx(om1(om12_idx(1)), rom1(om12_idx(1)), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)
semilogx(om3(om34_idx(1)), rom3(om34_idx(1)), 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_2)

rect_width = 0.03;
rect_height = 0.06;
% Peak Box
dim = [ompk1(pk_idx)-rect_width/2, ...
  rpk1(pk_idx) - rect_height/2, ...
  rect_width, rect_height];
xy_annotation(gca, 'rectangle', dim, ...
  'LineStyle', '--');
% Top Fold Box
dim = [om1(om12_idx(1))-rect_width/2, ...
  rom1(om12_idx(1)) - rect_height/2, ...
  rect_width, rect_height];
xy_annotation(gca, 'rectangle', dim, ...
  'LineStyle', '--');
% Bottom Fold Box
dim = [om3(om34_idx(1))-rect_width/2, ...
  rom3(om34_idx(1)) - rect_height/2, ...
  rect_width, rect_height];
xy_annotation(gca, 'rectangle', dim, ...
  'LineStyle', '--');

axes('Position', bx.Position);
hold on; box on; grid on;
set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
xlim([1.64, 1.7]);
ylim([1.475, 1.535]);

om = [ompk1(fake_pk_idx),ompk2(fake_pk_idx)];
R = [rpk1(fake_pk_idx), rpk2(fake_pk_idx)];

semilogx(om, R, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_1)
semilogx(ompk1(pk_idx), rpk1(pk_idx), 'ko', 'MarkerSize', marker_size_1)

xy_annotation(gca, 'line', ...
  [ompk1(fake_pk_idx),ompk1(fake_pk_idx)], ...
  [rpk1(fake_pk_idx)+0.005, rpk1(fake_pk_idx)+0.025], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'line', ...
  [ompk2(fake_pk_idx),ompk2(fake_pk_idx)], ...
  [rpk2(fake_pk_idx)+0.005, rpk2(fake_pk_idx)+0.025], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'doublearrow', ...
  [ompk1(fake_pk_idx),ompk2(fake_pk_idx)], ...
  [rpk1(fake_pk_idx)+0.02, rpk2(fake_pk_idx)+0.02], ...
  'LineWidth', 1, 'HeadStyle', 'cback1', ...
  'Head1Width', 6, 'Head2Width', 6);

xy_annotation(gca, 'textarrow', ...
  [ompk1(fake_pk_idx)-0.01,ompk1(fake_pk_idx)], ...
  [rpk1(fake_pk_idx)+0.02, rpk2(fake_pk_idx)+0.02], ...
  'String', '$\epsilon_\omega$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'none', 'FontSize', font_size);

dim = [1.26, 1.11, 0.155, 0.23];
bx = xy_annotation(main_axis, 'rectangle', dim, ...
  'LineWidth', 1, 'LineStyle', 'none');

axes('Position', bx.Position);
hold on; box on; grid on;
set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
semilogx(om_vals(1:fold_idx(1)), R_vals(1:fold_idx(1)), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)
xlim([1.685, 1.715]);
ylim([1.42, 1.48]);

om = [om1(fake_om12_idx(1)),om1(fake_om12_idx(1))];
R = [rom1(fake_om12_idx(1)), rom2(fake_om12_idx(1))];

semilogx(om, R, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_1)
semilogx(om1(om12_idx), rom1(om12_idx), 'ko', 'MarkerSize', marker_size_1)

xy_annotation(gca, 'line', ...
  [om1(fake_om12_idx(1))+0.001,om1(fake_om12_idx(1))+0.01], ...
  [rom1(fake_om12_idx(1)), rom1(fake_om12_idx(1))], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'line', ...
  [om1(fake_om12_idx(1))+0.001,om1(fake_om12_idx(1))+0.01], ...
  [rom2(fake_om12_idx(1)), rom2(fake_om12_idx(1))], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'doublearrow', ...
  [om1(fake_om12_idx(1))+0.007, om1(fake_om12_idx(1))+0.007], ...
  [rom1(fake_om12_idx(1)), rom2(fake_om12_idx(1))], ...
  'LineWidth', 1, 'HeadStyle', 'cback1', ...
  'Head1Width', 6, 'Head2Width', 6);

xy_annotation(gca, 'line', ...
  [om1(fake_om12_idx(1))+0.007, om1(fake_om12_idx(1))+0.007], ...
  [rom2(fake_om12_idx(1))-0.005, rom2(fake_om12_idx(1))], ...
  'LineWidth', 1);

xy_annotation(gca, 'textarrow', ...
  [om1(fake_om12_idx(1))+0.012, om1(fake_om12_idx(1))+0.007], ...
  [rom2(fake_om12_idx(1))-0.005, rom2(fake_om12_idx(1))-0.005], ...
  'String', '$\epsilon_R$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'none', 'FontSize', font_size);

dim = [1.26, 0.87, 0.155, 0.23];
bx = xy_annotation(main_axis, 'rectangle', dim, ...
  'LineWidth', 1, 'LineStyle', 'none');

axes('Position', bx.Position);
hold on; box on; grid on;
set(gca, 'xticklabel', {[]}, 'yticklabel', {[]})
semilogx(om_vals(fold_idx(2):end), R_vals(fold_idx(2):end), 'k-', 'LineWidth', 1)
% Unstable Branch
semilogx(om_vals(fold_idx(1):fold_idx(2)), R_vals(fold_idx(1):fold_idx(2)), 'k--', 'LineWidth', 1)

xlim([1.645, 1.675]);
ylim([0.91, 0.99]);

om = [om3(fake_om34_idx(1)),om3(fake_om34_idx(1))];
R = [rom3(fake_om34_idx(1)), rom4(fake_om34_idx(1))];

semilogx(om, R, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', marker_size_1)
semilogx(om3(om34_idx), rom3(om34_idx), 'ko', 'MarkerSize', marker_size_1)

xy_annotation(gca, 'line', ...
  [om3(fake_om34_idx(1))+0.001,om3(fake_om34_idx(1))+0.01], ...
  [rom3(fake_om34_idx(1)), rom3(fake_om34_idx(1))], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'line', ...
  [om3(fake_om34_idx(1))+0.001,om3(fake_om34_idx(1))+0.01], ...
  [rom4(fake_om34_idx(1)), rom4(fake_om34_idx(1))], ...
  'LineStyle', '--', 'LineWidth', 1);

xy_annotation(gca, 'doublearrow', ...
  [om3(fake_om34_idx(1))+0.007, om3(fake_om34_idx(1))+0.007], ...
  [rom3(fake_om34_idx(1)), rom4(fake_om34_idx(1))], ...
  'LineWidth', 1, 'HeadStyle', 'cback1', ...
  'Head1Width', 6, 'Head2Width', 6);

xy_annotation(gca, 'line', ...
  [om3(fake_om34_idx(1))+0.007, om3(fake_om34_idx(1))+0.007], ...
  [rom4(fake_om34_idx(1))-0.005, rom4(fake_om34_idx(1))], ...
  'LineWidth', 1);

xy_annotation(gca, 'textarrow', ...
  [om3(fake_om34_idx(1))+0.01, om3(fake_om34_idx(1))+0.007], ...
  [rom4(fake_om34_idx(1))-0.005, rom4(fake_om34_idx(1))-0.005], ...
  'String', '$\epsilon_R$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'none', 'FontSize', font_size);

saveas(figure2, fp, 'epsc');

%% Duffing Optimal Continuation Run

font_size = 20;
figname = 'uq_opt_k-m-obj.eps';
fp = strcat(img_path, figname);
uq_swp_bd = coco_bd_read('uq_sweep');

A = coco_bd_col(uq_swp_bd, 'A');
k = coco_bd_col(uq_swp_bd, 'k');
m = coco_bd_col(uq_swp_bd, 'm');
obj = coco_bd_col(uq_swp_bd, 'obj');

labs = coco_bd_labs(uq_swp_bd, 'BP');
lab = labs(2);
chart = coco_read_solution('s1.pars', 'uq_sweep', lab, 'chart');
m_bp = chart.x(1);
k_bp = chart.x(3);

chart = coco_read_solution('obj', 'uq_sweep', lab, 'chart');
obj_bp = chart.x(1) - sqrt(chart.x(2));

labs = coco_bd_labs(uq_swp_bd, 'FP');
lab = labs(2);
chart = coco_read_solution('s1.pars', 'uq_sweep', lab, 'chart');
m_fp = chart.x(1);
k_fp = chart.x(3);

chart = coco_read_solution('obj', 'uq_sweep', lab, 'chart');
obj_fp = chart.x(1) - sqrt(chart.x(2));

opt_bd = coco_bd_read('uq_dA_to_0');
k_opt = coco_bd_col(opt_bd, 'k');
m_opt = coco_bd_col(opt_bd, 'm');
obj_opt = coco_bd_col(opt_bd, 'obj');

figure3 = figure(3);
hold on; grid on
plot3(k, m, obj, 'k', 'LineWidth', 1)
plot3(k_bp, m_bp, obj_bp, 'ko', 'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)

xlim([1,3])
ylim([1,3])
zlim([0.85,1.3])
xlabel('Stiffness, $k$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Mass, $m$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
zlabel('Objective', 'Interpreter', ...
  'latex', 'FontSize', font_size)
view([-36,6])
ax = gca;
ax.XTick = 1:0.5:3;
ax.YTick = 1.5:0.5:3;
ax.ZTick = 0.8:0.05:1.3;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
saveas(figure3, fp, 'epsc');
clf

figname = 'uq_opt_k-m.eps';
fp = strcat(img_path, figname);

xlim([0.9,3.1])
ylim([0.9,3.1])

plot(k, m, 'k-', 'LineWidth', 1);
hold on; grid on; box on;
plot(k_bp, m_bp, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)
xlabel('Stiffness, $k$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Mass, $m$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.XTick = 1:0.5:3;
ax.YTick = 1:0.5:3;
ax.ZTick = 0.8:0.05:1.3;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
saveas(figure3, fp, 'epsc');
clf

figname = 'uq_opt_k-obj.eps';
fp = strcat(img_path, figname);

plot(k, obj, 'k-', 'LineWidth', 1);
hold on; grid on; box on;
xlabel('Stiffness, $k$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Objective', 'Interpreter', ...
  'latex', 'FontSize', font_size)
plot(k_bp, obj_bp, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)

ax = gca;
ax.XTick = 1:0.5:3;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
saveas(figure3, fp, 'epsc');
clf

figname = 'uq_opt_obj-m.eps';
fp = strcat(img_path, figname);

plot(obj, m, 'k-', 'LineWidth', 1);
hold on; grid on; box on;
xlabel('Objective', 'Interpreter', ...
  'latex', 'FontSize', font_size);
ylabel('Mass, $m$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
plot(obj_bp, m_bp, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)

ax = gca;
ax.YTick = 1:0.5:3;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
saveas(figure3, fp, 'epsc');
clf

font_size = 15;
figname = 'opt_secondary_branch_zoom.eps';
fp = strcat(img_path, figname);

hold on; grid on
plot3(k, m, obj, 'k', 'LineWidth', 1)
plot3(k_bp, m_bp, obj_bp, 'ko', 'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 10)

plot3(k_opt, m_opt, obj_opt, 'r:', 'LineWidth', 2)
plot3(k_opt(end), m_opt(end), obj_opt(end), 'ko', 'MarkerSize', 10)

xlim([2.5,3])
ylim([2.5,3])
zlim([1.2,1.35])
xlabel('Stiffness, $k$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Mass, $m$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
zlabel('Objective', 'Interpreter', ...
  'latex', 'FontSize', font_size)
view([-36,6])
ax = gca;
ax.XTick = 2.5:0.1:3;
ax.YTick = 2.6:0.1:3;
ax.ZTick = 1.2:0.03:1.35;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
ax.XLabel.Rotation = 4;
ax.YLabel.Rotation = -4;

saveas(figure3, fp, 'epsc');

font_size = 15;
figname = 'opt_secondary_branch.eps';
fp = strcat(img_path, figname);

xlim([1,3])
ylim([1,3])
zlim([0.85,1.35])
xlabel('Stiffness, $k$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Mass, $m$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
zlabel('Objective', 'Interpreter', ...
  'latex', 'FontSize', font_size)
view([-36,6])
ax = gca;
ax.XTick = 1:0.5:3;
ax.YTick = 1.5:0.5:3;
ax.ZTick = 0.8:0.05:1.35;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
ax.XLabel.Rotation = 4;
ax.YLabel.Rotation = -4;

annotation(figure3,'rectangle',...
  [0.542, 0.788, 0.13, 0.09], 'LineStyle', '--');

saveas(figure3, fp, 'epsc');

figure4 = figure(4);
font_size = 15;
figname = 'opt_change_w_n_stdev.eps';
fp = strcat(img_path, figname);
mu = coco_bd_col(uq_swp_bd, 'resp.mean');
var = coco_bd_col(uq_swp_bd, 'resp.var');
n = [0:6];
k_max = zeros(1,numel(n));
m_max = zeros(1,numel(n));
obj_max = zeros(1,numel(n));
for i=1:numel(n)
  [val, idx] = max(mu - n(i)*sqrt(var));
  k_max(i) = k(idx);
  m_max(i) = m(idx);
  obj_max(i) = mu(i) - n(i)*sqrt(var(i));
end

plot(k, m, 'k-', 'LineWidth', 1);
hold on; grid on; box on;
plot(k_max, m_max, 'ko', ...
  'MarkerFaceColor', [0.5,0.5,0.5], ...
  'MarkerSize', 7);
[v, i] = min(var);
plot(k(i), m(i), 'ko', ...
  'MarkerSize', 7);
r = 2.4;
angles = [50, 30, 5, -20, -50, -70, -90]; 
x = 0.6*cos(deg2rad(angles))+2;
y = 0.6*sin(deg2rad(angles))+2;
labs = {'$\mu$', '$\mu - \sigma$', '$\mu - 2\sigma$', ...
  '$\mu - 3\sigma$', '$\mu - 4\sigma$', '$\mu - 5\sigma$', ...
  '$\mu - 6\sigma$'};
for n=0:6
  xy_annotation(gca, 'textarrow', ...
    [x(n+1), k_max(n+1)], ...
    [y(n+1), m_max(n+1)], ...
    'String', labs{n+1}, ...
    'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
    'cback1', 'FontSize', font_size, 'HeadWidth', 6);
end

xy_annotation(gca, 'textarrow', ...
  [2, k(i)], ...
  [1.3, m(i)], ...
  'String', '$-\sigma$', ...
  'Interpreter', 'latex', 'LineWidth', 1,'HeadStyle', ...
  'cback1', 'FontSize', font_size, 'HeadWidth', 6);

xlabel('Stiffness, $k$', 'Interpreter', ...
  'latex', 'FontSize', font_size)
ylabel('Mass, $m$', 'Interpreter', ...
  'latex', 'FontSize', font_size)

ax = gca;
ax.XTick = 1:0.5:3;
ax.YTick = 1:0.5:3;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
saveas(figure4, fp, 'epsc');

%% Convergence plots:
figname = 'uq_pce_var_convergence.eps';
fp = strcat(img_path, figname);
figure5 = figure(5);

font_size = 15;
mean = zeros(6,5);
var  = zeros(6,5);

for M=1:6
  for P=2:6
    run_name = ['pce_convergence_M_', int2str(M), '_P_', int2str(P)];
    bd = coco_bd_read(run_name);
    mu = coco_bd_col(bd, 'resp.mean');
    sigsq = coco_bd_col(bd, 'resp.var');
    mean(M, P-1) = mu;
    var(M, P-1) = sigsq;
  end
end

M = repmat(1:6, 5, 1)';
P = repmat(2:6, 6, 1);

mesh(M, P, var)
colormap(gray)
hold on
plot3(M(4,3), P(4,3), var(4,3), 'ko', 'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 7)

shading interp
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
ax.View = [30.5, 29.2];
ax.XLabel.Position = [5,1];
ax.XTick = 1:6;
ax.XLabel.Position = [3.5, 1,0];
ax.XLabel.Rotation = -11;
ax.YLabel.Position = [6.75, 3.5, 0];
ax.YLabel.Rotation = 32;
ax.YTick = 2:6;
xlabel('Integration Order, $M$', 'Interpreter', 'latex');
ylabel('Polynomial Degree, $P$', 'Interpreter', 'latex');
zlabel('Variance, $\sigma^{2}$', 'Interpreter', 'latex');

saveas(figure5, fp, 'epsc');

figname = 'uq_pce_mean_convergence.eps';
fp = strcat(img_path, figname);
figure6 = figure(6);
M = repmat(1:6, 5, 1)';
P = repmat(2:6, 6, 1);

mesh(M, P, mean)
colormap(gray)
hold on
plot3(M(4,3), P(4,3), mean(4,3), 'ko', 'MarkerFaceColor', [0.5,0.5,0.5], 'MarkerSize', 7)

shading interp
ax = gca;
ax.TickLabelInterpreter = 'latex';
ax.FontSize = font_size;
ax.View = [30.5, 29.2];
ax.XTick = 1:6;
ax.XLabel.Position = [3.5,1,1.3335];
ax.XLabel.Rotation = -11;
ax.YLabel.Position = [6.8,3.5,1.3335];
ax.YLabel.Rotation = 33;
ax.YTick = 2:6;
xlabel('Integration Order, $M$', 'Interpreter', 'latex');
ylabel('Polynomial Degree, $P$', 'Interpreter', 'latex');
zlabel('Mean, $\mu$', 'Interpreter', 'latex');

saveas(figure6, fp, 'epsc');
