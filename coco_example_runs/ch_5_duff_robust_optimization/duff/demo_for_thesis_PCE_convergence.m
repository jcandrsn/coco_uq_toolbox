%% Starting Point Build
% Generate the starting point for uncertainty
% quantification.  Result is six orbits, two on either side
% of the maximum frequency response, two on either side of
% the top fold point and two on either side of the bottom
% fold point.
demo_curve_build

%% PCE Integration Nodes
% Find integration nodes used in the PCE

for M=1:6
  for Pt=2:6
uq_data = demo_uq_nds_func(M,Pt);

last_run_name = 'uq_nds_M_P_vary';
run_name = 'pce_convergence';
add_adjt=0;

prob = coco_prob();
prob = coco_set(prob, 'coll', 'NTST', 30);
prob = coco_set(prob, 'cont', 'PtMX', 120);

bd = coco_bd_read(last_run_name);
labs = coco_bd_labs(bd, 'UQ');

j = 0;
s_idx = cell(1, uq_data.nsamples);
r_igs = cell(1, uq_data.nsamples);
r_idx = cell(1, uq_data.nsamples);
adj_s_idx = cell(1, uq_data.nsamples);
r_aidx = cell(1, uq_data.nsamples);
for lab=labs
  j = j+1;
  sample_id = strcat('s', int2str(j));
  pk1_sname = coco_get_id(sample_id, 'PK1');
  pk1_bvp_sname = coco_get_id(pk1_sname, 'bvp');
  pk1_coll_sname = coco_get_id(pk1_bvp_sname, 'seg1.coll');
  coll_name = 'PK1.bvp.seg1';
  sol = coll_read_solution(coll_name, last_run_name, lab);

  coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};

  if j == 1
    % bvp_args defined in demo_curve_build
    % alpha is stochastic and om will vary between
    % samples, so they're labeled with a sample integer
    s_pnames = {pnames{1:3}, 'alpha.1', pnames{5}, 'om.1'};
    prob = ode_isol2bvp(prob, pk1_sname, ...
      coll_args{:}, {}, bvp_args{:});

    [pk1_s1_data, pk1_s1_uidx, pk1_s1_u0] = ...
      coco_get_func_data(prob, pk1_coll_sname, ...
      'data', 'uidx', 'u0');
    pk1_s1_maps = pk1_s1_data.coll_seg.maps;

    si_par_fname = coco_get_id(sample_id, 'pars');
    si_par_pname = s_pnames([1:3, 5, 6]);
    si_par_idx = pk1_s1_uidx(pk1_s1_maps.p_idx([1:3, 5, 6]));
    prob = coco_add_pars(prob, si_par_fname, ...
      si_par_idx, si_par_pname);

    s_idx{j} = pk1_s1_uidx(pk1_s1_maps.p_idx);
    r_igs{j} = pk1_s1_u0(pk1_s1_maps.x0_idx(1));
    r_idx{j} = pk1_s1_uidx(pk1_s1_maps.x0_idx(1));

    % Adjoints
    if add_adjt
      prob = adjt_isol2bvp(prob, pk1_sname);
      [s1_pk1_fdata, s1_pk1_aidx] = coco_get_adjt_data(prob, ...
        pk1_coll_sname, 'data', 'axidx');
      s1_pk1_opt = s1_pk1_fdata.coll_opt;

      prob = coco_add_adjt(prob, si_par_fname, ...
        coco_get_id('d', si_par_pname), ...
        'aidx', s1_pk1_aidx(s1_pk1_opt.p_idx([1:3, 5, 6])), ...
        'l0', zeros(5,1));
      adj_s_idx{j} = s1_pk1_aidx(s1_pk1_opt.p_idx);
      r_aidx{j} = s1_pk1_aidx(s1_pk1_opt.x0_idx(1));
    end
  else
    % 'bvp'-toolbox arguments
    prob = ode_isol2bvp(prob, pk1_sname, ...
      coll_args{:}, {}, bvp_args{:});

    % Glue deterministic parameters together
    [pk1_sj_data, pk1_sj_uidx, pk1_sj_u0] = ...
      coco_get_func_data(prob, pk1_coll_sname, ...
                         'data', 'uidx', 'u0');
    pk1_sj_maps = pk1_sj_data.coll_seg.maps;
    s1_p_idx = pk1_s1_uidx(pk1_s1_maps.p_idx([1:3,5]));
    si_p_idx = pk1_sj_uidx(pk1_sj_maps.p_idx([1:3,5]));

    glue_name = coco_get_id(sample_id, 'glue_to_s1');
    prob = coco_add_glue(prob, glue_name, s1_p_idx, ...
      si_p_idx);

    si_om_fname = coco_get_id(sample_id, 'om');
    si_om_pname = coco_get_id('om', int2str(j));
    si_om_idx = pk1_sj_uidx(pk1_sj_maps.p_idx(6));
    prob = coco_add_pars(prob, si_om_fname, ...
      si_om_idx, si_om_pname);
    s_idx{j} = pk1_sj_uidx(pk1_sj_maps.p_idx);
    r_igs{j} = pk1_sj_u0(pk1_sj_maps.x0_idx(1));
    r_idx{j} = pk1_sj_uidx(pk1_sj_maps.x0_idx(1));

    % Adjoints
    if add_adjt
      % For ode
      prob = adjt_isol2bvp(prob, pk1_sname);
      [sj_pk1_fdata, sj_pk1_aidx] = coco_get_adjt_data(prob, ...
        pk1_coll_sname, 'data', 'axidx');
      sj_pk1_opt = sj_pk1_fdata.coll_opt;

      % For glue
      s1_p_aidx = s1_pk1_aidx(s1_pk1_opt.p_idx([1:3,5]));
      si_p_aidx = sj_pk1_aidx(sj_pk1_opt.p_idx([1:3,5]));
      prob = coco_add_adjt(prob, glue_name, ...
        'aidx', [s1_p_aidx; si_p_aidx]);

      % For frequency
      prob = coco_add_adjt(prob, si_om_fname, ...
        coco_get_id('d', si_om_pname), ...
        'aidx', sj_pk1_aidx(sj_pk1_opt.p_idx(6)), ...
        'l0', 0);
      adj_s_idx{j} = sj_pk1_aidx(sj_pk1_opt.p_idx);
      r_aidx{j} = sj_pk1_aidx(sj_pk1_opt.x0_idx(1));
    end
  end

  [pk1_data, pk1_uidx] = coco_get_func_data(prob, pk1_coll_sname, 'data', 'uidx');
  pk1_maps = pk1_data.coll_seg.maps;
  pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

  % Parameterize Amplitude Peak (Is this necessary?)
  pk1_r_sname = coco_get_id(pk1_sname, 'R');
  prob = coco_add_pars(prob, pk1_r_sname, pk1_x0(1), ...
    pk1_r_sname, 'active');

  % Adjoint
  if add_adjt
    [sj_pk1_fdata, sj_pk1_aidx] = coco_get_adjt_data(prob, ...
      pk1_coll_sname, 'data', 'axidx');
    sj_pk1_opt = sj_pk1_fdata.coll_opt;

    prob = coco_add_adjt(prob, pk1_r_sname, ...
      coco_get_id('d', pk1_r_sname), ...
      'aidx', sj_pk1_aidx(sj_pk1_opt.x0_idx(1)), ...
      'l0', 0);
  end

  % Maintain velocity constraint (could also be done with a
  % boundary condition)
  pk1_v0_sname = coco_get_id(pk1_sname, 'v0');
  prob = coco_add_pars(prob, pk1_v0_sname, pk1_x0(2), ...
    pk1_v0_sname, 'inactive');

  % Adjoint
  if add_adjt
    dv10_sname = coco_get_id('d', pk1_v0_sname);
    prob = coco_add_adjt(prob, pk1_v0_sname, ...
      dv10_sname, ...
      'aidx', sj_pk1_aidx(sj_pk1_opt.x0_idx(2)), ...
      'l0', 0);
  end

  pk2_sname = coco_get_id(sample_id, ...
    'PK2');
  pk2_coll_sname = coco_get_id(pk2_sname, 'bvp', 'seg1', 'coll');

  coll_name = 'PK2';
  sol = coll_read_solution(coll_name, last_run_name, lab);

  coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};
  prob = ode_isol2bvp(prob, pk2_sname, ...
    coll_args{:}, {}, bvp_args{:});

  [pk2_data, pk2_uidx] = coco_get_func_data(prob, ...
    pk2_coll_sname, 'data', 'uidx');
  pk2_maps = pk2_data.coll_seg.maps;
  pk2_x0 = pk2_uidx(pk2_maps.x0_idx);

  % Maintain velocity constraint (could also be done with a
  % boundary condition)
  pk2_v0_sname = coco_get_id(pk2_sname, 'v0');
  prob = coco_add_pars(prob, pk2_v0_sname, pk2_x0(2), ...
    pk2_v0_sname, 'inactive');

  % Glue Parameters together (except forcing frequency)
  parglue_sname = coco_get_id(sample_id, ...
    'par_glue_PK2_to_PK1');
  prob = coco_add_glue(prob, parglue_sname, ...
    pk2_uidx(pk2_maps.p_idx(1:5)), ...
    pk1_uidx(pk1_maps.p_idx(1:5)));

  pk_r_glue_sname = coco_get_id(sample_id, ...
    'R_glue_to_PK1');
  prob = coco_add_glue(prob, pk_r_glue_sname, ...
    pk2_x0(1), pk1_x0(1));

  % Lock in the frequency difference
  pk_om_diff_sname = coco_get_id(sample_id, ...
    'PK_om_diff');
  prob = coco_add_func(prob, pk_om_diff_sname, ...
    @udiff, @udiff_dU, @udiff_dUdU, [], ...
    'inactive', pk_om_diff_sname, 'uidx', ...
    [pk2_uidx(pk2_maps.p_idx(6)), ...
     pk1_uidx(pk1_maps.p_idx(6))]);

  % Adjoints
  if add_adjt
    % ode
    prob = adjt_isol2bvp(prob, pk2_sname);
    [sj_pk2_fdata, sj_pk2_aidx] = coco_get_adjt_data(prob, ...
      pk2_coll_sname, 'data', 'axidx');
    sj_pk2_opt = sj_pk2_fdata.coll_opt;

    % velocity constraint
    dv20_sname = coco_get_id('d', pk2_v0_sname);
    prob = coco_add_adjt(prob, pk2_v0_sname, ...
      dv20_sname, ...
      'aidx', sj_pk2_aidx(sj_pk2_opt.x0_idx(2)));

    % glue
    prob = coco_add_adjt(prob, parglue_sname, ...
      'aidx', [sj_pk2_aidx(sj_pk2_opt.p_idx(1:5)); ...
      sj_pk1_aidx(sj_pk1_opt.p_idx(1:5))]);

    prob = coco_add_adjt(prob, pk_r_glue_sname, ...
      'aidx', [sj_pk2_aidx(sj_pk2_opt.x0_idx(1)); ...
      sj_pk1_aidx(sj_pk1_opt.x0_idx(1))]);

    % om constraint
    prob = coco_add_adjt(prob, pk_om_diff_sname, ...
      coco_get_id('d', pk_om_diff_sname), ...
      'aidx', [sj_pk2_aidx(sj_pk2_opt.p_idx(6)), ...
      sj_pk1_aidx(sj_pk1_opt.p_idx(6))]);

  end     

for i=1:2
  om_id = strcat('OM', int2str(i));
  om12_sname = coco_get_id(sample_id, om_id);

  om12_coll_sname = coco_get_id(om12_sname, 'bvp', 'seg1', 'coll');

  sol = coll_read_solution(om_id, last_run_name, lab);

  coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};
  prob = ode_isol2bvp(prob, om12_sname, ...
    coll_args{:}, {}, bvp_args{:});    

  [om_data{i}, om_uidx{i}] = coco_get_func_data(prob, ...
    om12_coll_sname, 'data', 'uidx');
  om_maps{i} = om_data{i}.coll_seg.maps;

  % Maintain velocity constraint
  v0sname = coco_get_id(sample_id, ['OM', int2str(i)],'v0');
  om_x0{i} = om_uidx{i}(om_maps{i}.x0_idx);
  prob = coco_add_pars(prob, v0sname, om_x0{i}(2), v0sname, 'inactive');
  if add_adjt

    prob = adjt_isol2bvp(prob, om12_sname);
    [sj_om_fdata{i}, sj_om_aidx{i}] = coco_get_adjt_data(prob, ...
      om12_coll_sname, 'data', 'axidx');
    sj_om_opt{i} = sj_om_fdata{i}.coll_opt;    

    om_x0a{i} = sj_om_aidx{i}(sj_om_opt{i}.x0_idx);
    dv0sname = coco_get_id('d', v0sname);
    prob = coco_add_adjt(prob, v0sname, dv0sname, ...
      'aidx', om_x0a{i}(2));
  end

  % Glue Parameters together
  if i > 1
    % The fold approximants share all parameter values.
    % They differ only in response amplitude 

    gluename = coco_get_id(sample_id, ['par_glue_OM', ...
      int2str(i), '_to_OM1']);
    prob = coco_add_glue(prob, gluename, ...
      om_uidx{1}(om_maps{1}.p_idx), ...
      om_uidx{i}(om_maps{i}.p_idx));

    % Parameterize Response Amplitude Difference
    rdiffname = ['OM', int2str(i-1), 'R_minus_OM', int2str(i), 'R'];
    rdiffname = coco_get_id(sample_id, rdiffname);
    prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
      [], 'inactive', rdiffname, 'uidx', [om_x0{i-1}(1), om_x0{i}(1)]);

    if add_adjt
      prob = coco_add_adjt(prob, gluename, ...
        'aidx', [sj_om_aidx{1}(sj_om_opt{1}.p_idx), ...
        sj_om_aidx{i}(sj_om_opt{i}.p_idx)]);

      drdiffname = coco_get_id('d', rdiffname);
      drdiffnames12{j} = drdiffname;
      prob = coco_add_adjt(prob, rdiffname, drdiffname, ...
        'aidx', [om_x0a{i-1}(1), om_x0a{i}(1)]);

    end

  else
    gluename = coco_get_id(sample_id, ['par_glue_OM', ...
      int2str(i), '_to_PK1']);
    prob = coco_add_glue(prob, gluename, ...
      pk2_uidx(pk2_maps.p_idx(1:5)), ...
      om_uidx{1}(om_maps{1}.p_idx(1:5)));
    if add_adjt
      prob = coco_add_adjt(prob, gluename, ...
        'aidx', [sj_pk1_aidx(sj_pk1_opt.p_idx(1:5)), ...
        sj_om_aidx{1}(sj_om_opt{1}.p_idx(1:5))]);
    end
  end
end

for i=3:4
  om_id = strcat('OM', int2str(i));
  om34_sname = coco_get_id(sample_id, om_id);

  om34_coll_sname = coco_get_id(om34_sname, 'bvp', 'seg1', 'coll');

  sol = coll_read_solution(om_id, last_run_name, lab);

  coll_args = {coll_args{1:6}, sol.tbp-sol.tbp(1), sol.xbp, sol.p};
  prob = ode_isol2bvp(prob, om34_sname, ...
    coll_args{:}, {}, bvp_args{:});  

  [om_data{i}, om_uidx{i}] = coco_get_func_data(prob, ...
    om34_coll_sname, 'data', 'uidx');
  om_maps{i} = om_data{i}.coll_seg.maps;

  % Maintain velocity constraint
  v0sname = coco_get_id(sample_id, ['OM', int2str(i)],'v0');
  om_x0{i} = om_uidx{i}(om_maps{i}.x0_idx);
  prob = coco_add_pars(prob, v0sname, om_x0{i}(2), v0sname, 'inactive');
  if add_adjt

    prob = adjt_isol2bvp(prob, om34_sname);
    [sj_om_fdata{i}, sj_om_aidx{i}] = coco_get_adjt_data(prob, ...
      om34_coll_sname, 'data', 'axidx');
    sj_om_opt{i} = sj_om_fdata{i}.coll_opt;    

    om_x0a{i} = sj_om_aidx{i}(sj_om_opt{i}.x0_idx);
    dv0sname = coco_get_id('d', v0sname);
    prob = coco_add_adjt(prob, v0sname, dv0sname, ...
      'aidx', om_x0a{i}(2));
  end

  % Glue Parameters together
  if i == 4
    gluename = coco_get_id(sample_id, ['par_glue_OM', ...
      int2str(i), '_to_OM1']);
    prob = coco_add_glue(prob, gluename, ...
      om_uidx{i-1}(om_maps{i-1}.p_idx), ...
      om_uidx{i}(om_maps{i}.p_idx));

    % Parameterize Response Amplitude Difference
    rdiffname = ['OM', int2str(i-1), 'R_minus_OM', int2str(i), 'R'];
    rdiffname = coco_get_id(sample_id, rdiffname);      
    prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
      [], 'inactive', rdiffname, 'uidx', [om_x0{i-1}(1), om_x0{i}(1)]);
    if add_adjt
      prob = coco_add_adjt(prob, gluename, ...
        'aidx', [sj_om_aidx{i-1}(sj_om_opt{i-1}.p_idx), ...
        sj_om_aidx{i}(sj_om_opt{i}.p_idx)]);

      drdiffname = coco_get_id('d', rdiffname);
      drdiffnames34{j} = drdiffname;
      prob = coco_add_adjt(prob, rdiffname, drdiffname, ...
        'aidx', [om_x0a{i-1}(1), om_x0a{i}(1)]);

    end      
  else
    gluename = coco_get_id(sample_id, ['par_glue_OM', ...
      int2str(i), '_to_PK1']);
    prob = coco_add_glue(prob, gluename, ...
      pk1_uidx(pk1_maps.p_idx(1:5)), ...
      om_uidx{i}(om_maps{i}.p_idx(1:5)));
    if add_adjt
      prob = coco_add_adjt(prob, gluename, ...
        'aidx', [sj_pk1_aidx(sj_pk1_opt.p_idx(1:5)), ...
        sj_om_aidx{i}(sj_om_opt{i}.p_idx(1:5))]);
    end      
  end
end

hyst_fname = coco_get_id(sample_id, 'hyst');
hyst_pname = coco_get_id('hyst', int2str(j));

prob = coco_add_func(prob, hyst_fname, @udiff, ...
  @udiff_dU, @udiff_dUdU, [], 'active', hyst_pname, ...
  'uidx', [om_uidx{1}(om_maps{1}.p_idx(6)), ...
           om_uidx{3}(om_maps{3}.p_idx(6))]);

[hyst_idx{j}, oms{j}] = coco_get_func_data(prob, hyst_fname, 'uidx', 'u0');

if add_adjt
  dhyst_pname = coco_get_id('d', hyst_pname);
  prob = coco_add_adjt(prob, hyst_fname, dhyst_pname, ...
    'aidx', [sj_om_aidx{1}(sj_om_opt{1}.p_idx(6)), ...
    sj_om_aidx{3}(sj_om_opt{3}.p_idx(6))]);

  hyst_aidx{j} = coco_get_adjt_data(prob, hyst_fname, 'axidx');
end

end

psi_mat = uq_make_psi_mat(uq_data.nds_grid, uq_data.uq.Pt, uq_data.spdists);
uq_data.wtd_psi_mat = psi_mat*diag(uq_data.wts);
uq_data.s_idx = s_idx;
if add_adjt
  uq_data.adj_s_idx = adj_s_idx;
end
uq_data.addadjt=add_adjt;
[prob, uq_data] = uq_add_sample_nodes(prob, uq_data);

igs = zeros(uq_data.nsamples,1);
idx = zeros(3, uq_data.nsamples);
for i=1:uq_data.nsamples
  omi = oms{i};
  igs(i) = r_igs{i} - 5*abs(omi(1) - omi(2));
  idx(:,i) = [r_idx{i}; hyst_idx{i}];
end

alpha_ig = uq_data.wtd_psi_mat*igs;
response_id = 'resp';
prob = coco_add_func(prob, response_id, ...
  @resp_pce, @resp_pce_dU, @resp_pce_dUdU, ...
  uq_data, 'zero', 'uidx', idx(:), 'u0', alpha_ig);

if add_adjt
  aidx = zeros(3, uq_data.nsamples);
  for i=1:uq_data.nsamples
    aidx(:,i) = [r_aidx{i}; hyst_aidx{i}];
  end

  prob = coco_add_adjt(prob, response_id, ...
    'aidx', aidx(:), 'l0', zeros(size(alpha_ig)), ...
    'tl0', zeros(size(alpha_ig)));
end

alpha_idx = coco_get_func_data(prob, response_id, 'uidx');

% Grab the last Nt parameters. This will only work for single output
% response function.  Leaving it for now, would like to generalize this to
% multiple output response functions.
alpha_idx = alpha_idx(end-uq_data.Nt+1:end);
if add_adjt
  alpha_aidx = coco_get_adjt_data(prob, response_id, 'axidx');
  alpha_aidx = alpha_aidx(end-uq_data.Nt+1:end);
end

% Calculate statistics from the PCE
% Add zero functions for the mean and variance along with
% inactive continuation parameters for tracking their
% values.

% Mean Zero Function
mean_id = coco_get_id(response_id, 'pce_mean');
prob = coco_add_func(prob, mean_id, ...
  @uq_pce_mean, @uq_pce_mean_dU, @uq_pce_mean_dUdU, ...
  uq_data, 'zero', 'uidx', alpha_idx, 'u0', alpha_ig(1));

% Mean Continuation Parameter
mean_par_id = coco_get_id(response_id, 'mean');
mean_idx = coco_get_func_data(prob, mean_id, 'uidx');
mean_name = coco_get_id(response_id, 'mean');
prob = coco_add_pars(prob, mean_par_id, mean_idx(end), mean_name);

if add_adjt  

  prob = coco_add_adjt(prob, mean_id, 'aidx', ...
    alpha_aidx, ...
    'l0', 0);

  mean_aidx = coco_get_adjt_data(prob, mean_id, 'axidx');
  dmean_name = coco_get_id('d', mean_name);
  prob = coco_add_adjt(prob, mean_par_id, dmean_name, ...
    'aidx', mean_aidx(end), ...
    'l0', 0);
end

% Variance Zero Function
var_id = coco_get_id(response_id, 'pce_variance');
prob = coco_add_func(prob, var_id, ...
  @uq_pce_variance, @uq_pce_variance_dU, @uq_pce_variance_dUdU,...
  uq_data, 'zero', 'uidx', alpha_idx, 'u0', sum(alpha_ig(2:end).^2));

% Variance Parameter
var_par_id = coco_get_id(response_id, 'variance');
var_idx = coco_get_func_data(prob, var_id, 'uidx');
var_name = coco_get_id(response_id, 'var');
prob = coco_add_pars(prob, var_par_id, var_idx(end), var_name);

if add_adjt
  prob = coco_add_adjt(prob, var_id, 'aidx', ...
    alpha_aidx, ...
    'l0', 0);

  var_aidx = coco_get_adjt_data(prob, var_id, 'axidx');
  dvar_name = coco_get_id('d', var_name);
  prob = coco_add_adjt(prob, var_par_id, dvar_name, ...
    'aidx', var_aidx(end), ...
    'l0', 0);
end

obj_id = 'obj';
mean_idx = coco_get_func_data(prob, mean_id, 'uidx');
var_idx = coco_get_func_data(prob, var_id, 'uidx');

prob = coco_add_func(prob, obj_id, ...
  @obj, @obj_du, @obj_dudu, [], ...
  'inactive', obj_id, 'uidx', ...
  [mean_idx(end); var_idx(end)]);

if add_adjt
  dobj_id = coco_get_id('d', obj_id);
  prob = coco_add_adjt(prob, obj_id, dobj_id, ...
    'aidx', [mean_aidx(end); var_aidx(end)]);
end

[uidx, data] = coco_get_func_data(prob, 's1.PK1.bvp.seg1.coll', 'uidx', 'data');
mk_idx = uidx(data.coll_seg.maps.p_idx([1,3]));

prob = coco_add_func(prob, 'km_constraint', ...
  @km_const, @km_const_du, @km_const_dudu, [], ...
  'zero', 'uidx', mk_idx);

if add_adjt
  [aidx, fdata] = coco_get_adjt_data(prob, ...
    's1.PK1.bvp.seg1.coll', 'axidx', 'data');
  mk_aidx = aidx(fdata.coll_opt.p_idx([1,3]));

  prob = coco_add_adjt(prob, 'km_constraint', 'aidx', ...
    mk_aidx);
end   

dnames = coco_get_id('d', s_pnames([1:3,5]));
alnames = uq_get_sample_par_names('alpha', 1:uq_data.nsamples);
dalnames = coco_get_id('d', alnames);
omnames = uq_get_sample_par_names('om', 1:uq_data.nsamples);
domnames = coco_get_id('d', omnames);
hyst_names = uq_get_sample_par_names('hyst', 1:uq_data.nsamples);
pk1_ids = cell(1,uq_data.nsamples);
pk2_ids = cell(1,uq_data.nsamples);
om1_ids = cell(1,uq_data.nsamples);
om2_ids = cell(1,uq_data.nsamples);
om3_ids = cell(1,uq_data.nsamples);
om4_ids = cell(1,uq_data.nsamples);

for i=1:uq_data.nsamples
  pk1_ids{i} = coco_get_id(['s', int2str(i)], 'PK1', 'v0');
  pk2_ids{i} = coco_get_id(['s', int2str(i)], 'PK2', 'v0');
  om1_ids{i} = coco_get_id(['s', int2str(i)], 'OM1', 'v0');
  om2_ids{i} = coco_get_id(['s', int2str(i)], 'OM2', 'v0');
  om3_ids{i} = coco_get_id(['s', int2str(i)], 'OM3', 'v0');
  om4_ids{i} = coco_get_id(['s', int2str(i)], 'OM4', 'v0');    
end

dv10_snames = coco_get_id('d', pk1_ids);
dv20_snames = coco_get_id('d', pk2_ids);
dv30_snames = coco_get_id('d', om1_ids);
dv40_snames = coco_get_id('d', om2_ids);
dv50_snames = coco_get_id('d', om3_ids);
dv60_snames = coco_get_id('d', om4_ids);
if not(coco_exist(run_name, 'run'))
if add_adjt
  adj_pars2free = {dnames{[2,4]}, domnames{:}, dv10_snames{:}, dv20_snames{:}, dv30_snames{:}, dv40_snames{:}, drdiffnames12{:}, dv50_snames{:}, dv60_snames{:},  drdiffnames34{:}, 'd.lo.alpha', 'd.up.alpha'};
  bd = coco(prob, run_name, [], 1, ...
    {obj_id, 'k', 'm', dobj_id, mean_name, var_name, omnames{:}, adj_pars2free{:}});
else
  bd = coco(prob, ['pce_convergence_M_',int2str(M),'_P_',int2str(Pt)], [], 0, {obj_id, 'k', 'm', mean_name, var_name, omnames{:}, alnames{:}});
end

else
bd = coco_bd_read(run_name);
end
  end
end