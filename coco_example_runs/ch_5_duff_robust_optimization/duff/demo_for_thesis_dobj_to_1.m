last_run_name = 'uq_sweep';
run_name = 'uq_dobj_to_1';
add_adjt=1;

% Initialize cell arrays
s_idx = cell(1, uq_data.nsamples);
r_igs = cell(1, uq_data.nsamples);
r_idx = cell(1, uq_data.nsamples);

if not(coco_exist(run_name, 'run'))
prob = prob_init;
prob = coco_set(prob, 'cont', 'h', 5, 'h_min', 1e-5, 'h_max', 100);
prob = coco_set(prob, 'coll', 'NTST', 30);
bd = coco_bd_read(last_run_name);
BPlabs = coco_bd_labs(bd, 'BP');
BPlab = BPlabs(1);

for j=1:uq_data.nsamples

  sample_id = strcat('s', int2str(j));
  pk1_sname = coco_get_id(sample_id, 'PK1');
  pk2_sname = coco_get_id(sample_id, 'PK2');
  om_sname{1} = coco_get_id(sample_id, 'OM1');
  om_sname{2} = coco_get_id(sample_id, 'OM2');
  om_sname{3} = coco_get_id(sample_id, 'OM3');
  om_sname{4} = coco_get_id(sample_id, 'OM4');

  prob = ode_BP2bvp(prob, pk1_sname, last_run_name, BPlab);
  prob = ode_BP2bvp(prob, pk2_sname, last_run_name, BPlab);
  prob = ode_BP2bvp(prob, om_sname{1}, last_run_name, BPlab);
  prob = ode_BP2bvp(prob, om_sname{2}, last_run_name, BPlab);
  prob = ode_BP2bvp(prob, om_sname{3}, last_run_name, BPlab);
  prob = ode_BP2bvp(prob, om_sname{4}, last_run_name, BPlab);

  adjt_args = struct();
  if add_adjt
    adjt_args.run = last_run_name;
    adjt_args.lab = BPlab;
    adjt_chart = coco_read_solution(last_run_name, BPlab, 'chart');
    cdata = coco_get_chart_data(adjt_chart, 'lsol');

    prob = adjt_BP2bvp(prob, pk1_sname, last_run_name, BPlab);
    prob = adjt_BP2bvp(prob, pk2_sname, last_run_name, BPlab);
    prob = adjt_BP2bvp(prob, om_sname{1}, last_run_name, BPlab);
    prob = adjt_BP2bvp(prob, om_sname{2}, last_run_name, BPlab);
    prob = adjt_BP2bvp(prob, om_sname{3}, last_run_name, BPlab);
    prob = adjt_BP2bvp(prob, om_sname{4}, last_run_name, BPlab);
  end    

  pk1_coll_sname = coco_get_id(pk1_sname, 'bvp', ...
    'seg1', 'coll');
  pk2_coll_sname = coco_get_id(pk2_sname, 'bvp', ...
    'seg1', 'coll');

  % Peak 1 Information
  [pk1_data, pk1_uidx, pk1_u0] = ...
    coco_get_func_data(prob, pk1_coll_sname, ...
    'data', 'uidx', 'u0');
  pk1_maps = pk1_data.coll_seg.maps;
  pk1_x0 = pk1_uidx(pk1_maps.x0_idx);

  % Peak 2 Information
  [pk2_data, pk2_uidx] = coco_get_func_data(prob, ...
    pk2_coll_sname, 'data', 'uidx');
  pk2_maps = pk2_data.coll_seg.maps;
  pk2_x0 = pk2_uidx(pk2_maps.x0_idx);

  if add_adjt
      [pk1_fdata, pk1_aidx] = coco_get_adjt_data(prob, ...
        pk1_coll_sname, 'data', 'axidx');
      pk1_opt = pk1_fdata.coll_opt;

      [pk2_fdata, pk2_aidx] = coco_get_adjt_data(prob, ...
        pk2_coll_sname, 'data', 'axidx');
      pk2_opt = pk2_fdata.coll_opt;
  end

  if j == 1
    % Name the parameters
    par_fname = coco_get_id(sample_id, 'pars');
    par_pname = pnames([1:3,5]);
    par_pname = {par_pname{:},'om.1'};
    par_idx = pk1_uidx(pk1_maps.p_idx([1:3,5,6]));
    prob = coco_add_pars(prob, par_fname, ...
      par_idx, par_pname);

    % Store first sample's info for glue
    s1_pk1_data = pk1_data;
    s1_pk1_uidx = pk1_uidx;
    s1_pk1_maps = pk1_maps;

    % Adjoints
    if add_adjt
      [chart, lidx] = coco_read_adjoint(par_fname, ...
        last_run_name, BPlab, 'chart', 'lidx');

      prob = coco_add_adjt(prob, par_fname, ...
        coco_get_id('d', par_pname), ...
        'aidx', pk1_aidx(pk1_opt.p_idx([1:3,5,6])), ...
        'l0', chart.x, 'tl0', cdata.v(lidx));

      s1_pk1_fdata = pk1_fdata;
      s1_pk1_aidx  = pk1_aidx;
      s1_pk1_opt   = pk1_opt;

      adj_s_idx{j} = pk1_aidx(pk1_opt.p_idx);
      r_aidx{j} = pk1_aidx(pk1_opt.x0_idx(1));
    end
  else
    % Glue deterministic parameters together
    s1_p_idx = s1_pk1_uidx(s1_pk1_maps.p_idx([1:3,5]));
    p_idx = pk1_uidx(pk1_maps.p_idx([1:3,5]));

    glue_name = coco_get_id(sample_id, 'glue_to_s1');
    prob = coco_add_glue(prob, glue_name, s1_p_idx, ...
      p_idx);

    % Frequency Names
    om_fname = coco_get_id(sample_id, 'om');
    om_pname = coco_get_id('om', int2str(j));
    om_idx = pk1_uidx(pk1_maps.p_idx(6));
    prob = coco_add_pars(prob, om_fname, ...
      om_idx, om_pname);      

    % Adjoints
    if add_adjt
      % For glue
      [chart, lidx] = coco_read_adjoint(glue_name, ...
        last_run_name, BPlab, 'chart', 'lidx');        
      s1_p_aidx = s1_pk1_aidx(s1_pk1_opt.p_idx([1:3,5]));
      p_aidx = pk1_aidx(pk1_opt.p_idx([1:3,5]));
      prob = coco_add_adjt(prob, glue_name, ...
        'aidx', [s1_p_aidx; p_aidx], 'l0', chart.x, ...
        'tl0', cdata.v(lidx));

      % For frequency
      [chart, lidx] = coco_read_adjoint(om_fname, ...
        last_run_name, BPlab, 'chart', 'lidx'); 

      prob = coco_add_adjt(prob, om_fname, ...
        coco_get_id('d', om_pname), ...
        'aidx', pk1_aidx(pk1_opt.p_idx(6)), ...
        'l0', chart.x, 'tl0', cdata.v(lidx));

      adj_s_idx{j} = pk1_aidx(sj_pk1_opt.p_idx);
      r_aidx{j} = pk1_aidx(sj_pk1_opt.x0_idx(1));        
    end
  end

  % Parameterize Amplitude Peak (Is this necessary?)
  pk1_r_sname = coco_get_id(pk1_sname, 'R');
  prob = coco_add_pars(prob, pk1_r_sname, pk1_x0(1), ...
    pk1_r_sname, 'active');

  % Adjoint
  if add_adjt
    [chart, lidx] = coco_read_adjoint(pk1_r_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');
    [pk1_fdata, pk1_aidx] = coco_get_adjt_data(prob, ...
      pk1_coll_sname, 'data', 'axidx');
    sj_pk1_opt = pk1_fdata.coll_opt;

    prob = coco_add_adjt(prob, pk1_r_sname, ...
      coco_get_id('d', pk1_r_sname), ...
      'aidx', pk1_aidx(sj_pk1_opt.x0_idx(1)), ...
      'l0', chart.x, 'tl0', cdata.v(lidx));
  end

  % Maintain velocity constraint (could also be done with a
  % boundary condition)
  pk1_v0_sname = coco_get_id(pk1_sname, 'v0');
  prob = coco_add_pars(prob, pk1_v0_sname, pk1_x0(2), ...
    pk1_v0_sname, 'inactive');

  % Adjoint
  if add_adjt
    [chart, lidx] = coco_read_adjoint(pk1_v0_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');
    dv10_sname = coco_get_id('d', pk1_v0_sname);
    prob = coco_add_adjt(prob, pk1_v0_sname, ...
      dv10_sname, ...
      'aidx', pk1_aidx(pk1_opt.x0_idx(2)), ...
      'l0', chart.x, 'tl0', cdata.v(lidx));
  end


  % Maintain velocity constraint (could also be done with a
  % boundary condition)
  pk2_v0_sname = coco_get_id(pk2_sname, 'v0');
  prob = coco_add_pars(prob, pk2_v0_sname, pk2_x0(2), ...
    pk2_v0_sname, 'inactive');

  % Glue Parameters together (except forcing frequency)
  parglue_sname = coco_get_id(sample_id, ...
    'par_glue_PK2_to_PK1');
  prob = coco_add_glue(prob, parglue_sname, ...
    pk2_uidx(pk2_maps.p_idx(1:5)), ...
    pk1_uidx(pk1_maps.p_idx(1:5)));

  pk_r_glue_sname = coco_get_id(sample_id, ...
    'R_glue_to_PK1');
  prob = coco_add_glue(prob, pk_r_glue_sname, ...
    pk2_x0(1), pk1_x0(1));

  % Lock in the frequency difference
  pk_om_diff_sname = coco_get_id(sample_id, ...
    'PK_om_diff');
  prob = coco_add_func(prob, pk_om_diff_sname, ...
    @udiff, @udiff_dU, @udiff_dUdU, [], ...
    'inactive', pk_om_diff_sname, 'uidx', ...
    [pk2_uidx(pk2_maps.p_idx(6)), ...
     pk1_uidx(pk1_maps.p_idx(6))]);

  % Adjoints
  if add_adjt
    % velocity constraint
    [chart, lidx] = coco_read_adjoint(pk2_v0_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');

    dv20_sname = coco_get_id('d', pk2_v0_sname);
    prob = coco_add_adjt(prob, pk2_v0_sname, ...
      dv20_sname, ...
      'aidx', pk2_aidx(pk2_opt.x0_idx(2)),...
      'l0', chart.x, 'tl0', cdata.v(lidx));

    % glue
    [chart, lidx] = coco_read_adjoint(parglue_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');

    prob = coco_add_adjt(prob, parglue_sname, ...
      'aidx', [pk2_aidx(pk2_opt.p_idx(1:5)); ...
      pk1_aidx(pk1_opt.p_idx(1:5))], ...
      'l0', chart.x, 'tl0', cdata.v(lidx));

    [chart, lidx] = coco_read_adjoint(pk_r_glue_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');

    prob = coco_add_adjt(prob, pk_r_glue_sname, ...
      'aidx', [pk2_aidx(pk2_opt.x0_idx(1)); ...
      pk1_aidx(pk1_opt.x0_idx(1))], ...
      'l0', chart.x, 'tl0', cdata.v(lidx));

    % om constraint
    [chart, lidx] = coco_read_adjoint(pk_om_diff_sname, ...
      last_run_name, BPlab, 'chart', 'lidx');

    prob = coco_add_adjt(prob, pk_om_diff_sname, ...
      coco_get_id('d', pk_om_diff_sname), ...
      'aidx', [pk2_aidx(pk2_opt.p_idx(6)), ...
      pk1_aidx(pk1_opt.p_idx(6))], ...
      'l0', chart.x, 'tl0', cdata.v(lidx));

  end

  for i=1:4
    om_coll_sname = coco_get_id(om_sname{i}, 'bvp', ...
      'seg1', 'coll');
    [om_data{i}, om_uidx{i}] = coco_get_func_data(prob, ...
      om_coll_sname, 'data', 'uidx');
    om_maps{i} = om_data{i}.coll_seg.maps;

    % Maintain velocity constraint
    v0sname = coco_get_id(om_sname{i},'v0');
    om_x0{i} = om_uidx{i}(om_maps{i}.x0_idx);
    prob = coco_add_pars(prob, v0sname, om_x0{i}(2), ...
      v0sname, 'inactive');    

    if add_adjt
      [om_fdata{i}, om_aidx{i}] = coco_get_adjt_data(prob, ...
        om_coll_sname, 'data', 'axidx');
      om_opt{i} = om_fdata{i}.coll_opt;
      om_x0a{i} = om_aidx{i}(om_opt{i}.x0_idx);

      [chart, lidx] = coco_read_adjoint(pk_om_diff_sname, ...
        last_run_name, BPlab, 'chart', 'lidx');

      dv0sname = coco_get_id('d', v0sname);
      prob = coco_add_adjt(prob, v0sname, dv0sname, ...
        'aidx', om_x0a{i}(2),...
        'l0', chart.x, 'tl0', cdata.v(lidx));
    end

    switch i
      case 1
        gluename = coco_get_id(sample_id, ['par_glue_OM', ...
          int2str(i), '_to_PK1']);
        prob = coco_add_glue(prob, gluename, ...
          pk2_uidx(pk2_maps.p_idx(1:5)), ...
          om_uidx{i}(om_maps{i}.p_idx(1:5)));

        if add_adjt
          [chart, lidx] = ...
            coco_read_adjoint(gluename, ...
            last_run_name, BPlab, 'chart', 'lidx');

          prob = coco_add_adjt(prob, gluename, ...
            'aidx', [pk1_aidx(pk1_opt.p_idx(1:5)), ...
            om_aidx{i}(om_opt{i}.p_idx(1:5))], ...
            'l0', chart.x, 'tl0', cdata.v(lidx));
        end          
      case 2
        % The fold approximants share all parameter values.
        % They differ only in response amplitude 

        gluename = coco_get_id(sample_id, ['par_glue_OM', ...
          int2str(i), '_to_OM1']);
        prob = coco_add_glue(prob, gluename, ...
          om_uidx{1}(om_maps{1}.p_idx), ...
          om_uidx{i}(om_maps{i}.p_idx));

        % Parameterize Response Amplitude Difference
        rdiffname = ['OM', int2str(i-1), 'R_minus_OM', int2str(i), 'R'];
        rdiffname = coco_get_id(sample_id, rdiffname);
        prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
          [], 'inactive', rdiffname, 'uidx', [om_x0{i-1}(1), om_x0{i}(1)]);

        if add_adjt
          [chart, lidx] = ...
            coco_read_adjoint(gluename, ...
            last_run_name, BPlab, 'chart', 'lidx');

          prob = coco_add_adjt(prob, gluename, ...
            'aidx', [om_aidx{1}(om_opt{1}.p_idx), ...
            om_aidx{i}(om_opt{i}.p_idx)], ...
            'l0', chart.x, 'tl0', cdata.v(lidx));

          drdiffname = coco_get_id('d', rdiffname);
          drdiffnames12{j} = drdiffname;

          [chart, lidx] = ...
            coco_read_adjoint(rdiffname, ...
            last_run_name, BPlab, 'chart', 'lidx');

          prob = coco_add_adjt(prob, rdiffname, drdiffname, ...
            'aidx', [om_x0a{i-1}(1), om_x0a{i}(1)], ...
            'l0', chart.x, 'tl0', cdata.v(lidx));
        end
      case 3
        gluename = coco_get_id(sample_id, ['par_glue_OM', ...
          int2str(i), '_to_PK1']);
        prob = coco_add_glue(prob, gluename, ...
          pk1_uidx(pk1_maps.p_idx(1:5)), ...
          om_uidx{i}(om_maps{i}.p_idx(1:5)));

        if add_adjt
          [chart, lidx] = ...
            coco_read_adjoint(gluename, ...
            last_run_name, BPlab, 'chart', 'lidx');

          prob = coco_add_adjt(prob, gluename, ...
            'aidx', [pk1_aidx(pk1_opt.p_idx(1:5)), ...
            om_aidx{i}(om_opt{i}.p_idx(1:5))], ...
            'l0', chart.x, 'tl0', cdata.v(lidx));
        end
      case 4
        gluename = coco_get_id(sample_id, ['par_glue_OM', ...
          int2str(i), '_to_OM1']);
        prob = coco_add_glue(prob, gluename, ...
          om_uidx{i-1}(om_maps{i-1}.p_idx), ...
          om_uidx{i}(om_maps{i}.p_idx));

        % Parameterize Response Amplitude Difference
        rdiffname = ['OM', int2str(i-1), 'R_minus_OM', int2str(i), 'R'];
        rdiffname = coco_get_id(sample_id, rdiffname);      
        prob = coco_add_func(prob, rdiffname, @udiff, @udiff_dU, @udiff_dUdU, ...
          [], 'inactive', rdiffname, 'uidx', [om_x0{i-1}(1), om_x0{i}(1)]);
        if add_adjt
          [chart, lidx] = ...
            coco_read_adjoint(gluename, ...
            last_run_name, BPlab, 'chart', 'lidx');

          prob = coco_add_adjt(prob, gluename, ...
            'aidx', [om_aidx{i-1}(om_opt{i-1}.p_idx), ...
            om_aidx{i}(om_opt{i}.p_idx)], ...
            'l0', chart.x, 'tl0', cdata.v(lidx));

          drdiffname = coco_get_id('d', rdiffname);
          drdiffnames34{j} = drdiffname;
          [chart, lidx] = ...
            coco_read_adjoint(rdiffname, ...
            last_run_name, BPlab, 'chart', 'lidx');

          prob = coco_add_adjt(prob, rdiffname, drdiffname, ...
            'aidx', [om_x0a{i-1}(1), om_x0a{i}(1)], ...
            'l0', chart.x, 'tl0', cdata.v(lidx));
        end          
    end
  end

  % Indices and Guesses for later functions
  s_idx{j} = pk1_uidx(pk1_maps.p_idx);
  r_igs{j} = pk1_u0(pk1_maps.x0_idx(1));
  r_idx{j} = pk1_uidx(pk1_maps.x0_idx(1));

hyst_fname = coco_get_id(sample_id, 'hyst');
hyst_pname = coco_get_id('hyst', int2str(j));

prob = coco_add_func(prob, hyst_fname, @udiff, ...
  @udiff_dU, @udiff_dUdU, [], 'active', hyst_pname, ...
  'uidx', [om_uidx{1}(om_maps{1}.p_idx(6)), ...
           om_uidx{3}(om_maps{3}.p_idx(6))]);

[hyst_idx{j}, oms{j}] = coco_get_func_data(prob, hyst_fname, 'uidx', 'u0');

if add_adjt
  [chart, lidx] = ...
    coco_read_adjoint(hyst_fname, ...
    last_run_name, BPlab, 'chart', 'lidx');

  dhyst_pname = coco_get_id('d', hyst_pname);
  prob = coco_add_adjt(prob, hyst_fname, dhyst_pname, ...
    'aidx', [om_aidx{1}(om_opt{1}.p_idx(6)), ...
    om_aidx{3}(om_opt{3}.p_idx(6))], ...
    'l0', chart.x, 'tl0', cdata.v(lidx));

  hyst_aidx{j} = coco_get_adjt_data(prob, hyst_fname, 'axidx');
end

end

psi_mat = uq_make_psi_mat(uq_data.nds_grid, uq_data.uq.Pt, uq_data.spdists);
uq_data.wtd_psi_mat = psi_mat*diag(uq_data.wts);

uq_data.s_idx = s_idx;
if add_adjt
  uq_data.adj_s_idx = adj_s_idx;
end
uq_data.addadjt=add_adjt;
[prob, uq_data] = uq_add_sample_nodes(prob, ...
  uq_data, adjt_args);

igs = zeros(uq_data.nsamples,1);
idx = zeros(3, uq_data.nsamples);
for i=1:uq_data.nsamples
  omi = oms{i};
  igs(i) = r_igs{i} - 5*abs(omi(1) - omi(2));
  idx(:,i) = [r_idx{i}; hyst_idx{i}];
end

alpha_ig = uq_data.wtd_psi_mat*igs;
response_id = 'resp';
prob = coco_add_func(prob, response_id, ...
  @resp_pce, @resp_pce_dU, @resp_pce_dUdU, ...
  uq_data, 'zero', 'uidx', idx(:), 'u0', alpha_ig);

if add_adjt
  aidx = zeros(3, uq_data.nsamples);
  for i=1:uq_data.nsamples
    aidx(:,i) = [r_aidx{i}; hyst_aidx{i}];
  end

  [chart, lidx] = ...
    coco_read_adjoint(response_id, ...
    last_run_name, BPlab, 'chart', 'lidx');

  prob = coco_add_adjt(prob, response_id, ...
    'aidx', aidx(:), ...
    'l0', chart.x, 'tl0', cdata.v(lidx));

end

alpha_idx = coco_get_func_data(prob, response_id, 'uidx');

% Grab the last Nt parameters. This will only work for single output
% response function.  Leaving it for now, would like to generalize this to
% multiple output response functions.
alpha_idx = alpha_idx(end-uq_data.Nt+1:end);
if add_adjt
  alpha_aidx = coco_get_adjt_data(prob, response_id, 'axidx');
  alpha_aidx = alpha_aidx(end-uq_data.Nt+1:end);
end

% Calculate statistics from the PCE
% Add zero functions for the mean and variance along with
% inactive continuation parameters for tracking their
% values.

% Mean Zero Function
mean_id = coco_get_id(response_id, 'pce_mean');
prob = coco_add_func(prob, mean_id, ...
  @uq_pce_mean, @uq_pce_mean_dU, @uq_pce_mean_dUdU, ...
  uq_data, 'zero', 'uidx', alpha_idx, 'u0', alpha_ig(1));

if add_adjt
  [chart, lidx] = ...
    coco_read_adjoint(mean_id, ...
    last_run_name, BPlab, 'chart', 'lidx');    

  prob = coco_add_adjt(prob, mean_id, 'aidx', ...
    alpha_aidx, ...
    'l0', chart.x, 'tl0', cdata.v(lidx));

  mean_aidx = coco_get_adjt_data(prob, mean_id, 'axidx');
end

% Variance Zero Function
var_id = coco_get_id(response_id, 'pce_variance');
prob = coco_add_func(prob, var_id, ...
  @uq_pce_variance, @uq_pce_variance_dU, @uq_pce_variance_dUdU,...
  uq_data, 'zero', 'uidx', alpha_idx, 'u0', sum(alpha_ig(2:end).^2));

if add_adjt
  [chart, lidx] = ...
    coco_read_adjoint(var_id, ...
    last_run_name, BPlab, 'chart', 'lidx');

  prob = coco_add_adjt(prob, var_id, 'aidx', ...
    alpha_aidx, ...
    'l0', chart.x, 'tl0', cdata.v(lidx));

  var_aidx = coco_get_adjt_data(prob, var_id, 'axidx');
end

obj_id = 'obj';
mean_idx = coco_get_func_data(prob, mean_id, 'uidx');
var_idx = coco_get_func_data(prob, var_id, 'uidx');

prob = coco_add_func(prob, obj_id, ...
  @obj, @obj_du, @obj_dudu, [], ...
  'inactive', obj_id, 'uidx', ...
  [mean_idx(end); var_idx(end)]);

if add_adjt
  [chart, lidx] = coco_read_adjoint(obj_id, ...
    last_run_name, BPlab, 'chart', 'lidx');

  dobj_id = coco_get_id('d', obj_id);
  prob = coco_add_adjt(prob, obj_id, dobj_id, ...
    'aidx', [mean_aidx(end); var_aidx(end)], ...
    'l0', chart.x, 'tl0', cdata.v(lidx));
end

[uidx, data] = coco_get_func_data(prob, 's1.PK1.bvp.seg1.coll', 'uidx', 'data');
mk_idx = uidx(data.coll_seg.maps.p_idx([1,3]));
prob = coco_add_func(prob, 'km_constraint', ...
  @km_const, @km_const_du, @km_const_dudu, [], ...
  'zero', 'uidx', mk_idx);

if add_adjt
  [aidx, fdata] = coco_get_adjt_data(prob, ...
    's1.PK1.bvp.seg1.coll', 'axidx', 'data');
  [chart, lidx] = coco_read_adjoint('km_constraint', ...
    last_run_name, BPlab, 'chart', 'lidx');

  mk_aidx = aidx(fdata.coll_opt.p_idx([1,3]));

  prob = coco_add_adjt(prob, 'km_constraint', 'aidx', ...
    mk_aidx, 'l0', chart.x, 'tl0', cdata.v(lidx));
end   

dnames = coco_get_id('d', pnames([1:3,5]));
alnames = uq_get_sample_par_names('alpha', 1:uq_data.nsamples);
dalnames = coco_get_id('d', alnames);

omnames = uq_get_sample_par_names('om', 1:uq_data.nsamples);
domnames = coco_get_id('d', omnames);
hyst_names = uq_get_sample_par_names('hyst', 1:uq_data.nsamples);
pk1_ids = cell(1,uq_data.nsamples);
pk2_ids = cell(1,uq_data.nsamples);
om1_ids = cell(1,uq_data.nsamples);
om2_ids = cell(1,uq_data.nsamples);
om3_ids = cell(1,uq_data.nsamples);
om4_ids = cell(1,uq_data.nsamples);

for i=1:uq_data.nsamples
  pk1_ids{i} = coco_get_id(['s', int2str(i)], 'PK1', 'v0');
  pk2_ids{i} = coco_get_id(['s', int2str(i)], 'PK2', 'v0');
  om1_ids{i} = coco_get_id(['s', int2str(i)], 'OM1', 'v0');
  om2_ids{i} = coco_get_id(['s', int2str(i)], 'OM2', 'v0');
  om3_ids{i} = coco_get_id(['s', int2str(i)], 'OM3', 'v0');
  om4_ids{i} = coco_get_id(['s', int2str(i)], 'OM4', 'v0');    
end

dv10_snames = coco_get_id('d', pk1_ids);
dv20_snames = coco_get_id('d', pk2_ids);
dv30_snames = coco_get_id('d', om1_ids);
dv40_snames = coco_get_id('d', om2_ids);
dv50_snames = coco_get_id('d', om3_ids);
dv60_snames = coco_get_id('d', om4_ids);

adj_pars2free = {dnames{[2,4]}, domnames{:}, dv10_snames{:}, dv20_snames{:}, dv30_snames{:}, dv40_snames{:}, drdiffnames12{:}, dv50_snames{:}, dv60_snames{:},  drdiffnames34{:}, 'd.lo.alpha', 'd.up.alpha'};
if add_adjt
  bd = coco(prob, run_name, [], 1, ...
    {dobj_id, 'k', 'm', obj_id, omnames{:}, adj_pars2free{:}}, [0,1]);
else
  bd = coco(prob, 'test', [], 0, {'obj', omnames{:}, 'resp.mean', 'resp.var'});
end

else
bd = coco_bd_read(run_name);
end