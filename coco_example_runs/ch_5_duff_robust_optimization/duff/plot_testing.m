figure(1)
hold on

for i=1:4
  bd = coco_bd_read(strcat('duff.', int2str(i)));
  plot(cell2mat(bd(2:end, 16)), cell2mat(bd(2:end, 17)));
end
legend('show')
axis([0.98 1.02 0 3])
set(gca, 'xscale', 'log')