function [data, J] = resp_pce_dU(prob, data, u)
% UQ_PCE_COEFFICIENTS_DU Jacobian of the Polynomial Chaos
% Expansion (PCE) coefficient Evaluation function

r = u(1:end-data.Nt);

dr = -data.wtd_psi_mat*eye(numel(r));

J = [dr, ...         % dr/du
     eye(data.Nt)];  % alphas are linear

%% Test Jacobian with following sequence:
% [data, Jd] = coco_ezDFDX('f(o,d,x)', prob, data, @resp_pce, u);
% diff_J = abs(J-Jd);
% max(max(diff_J))
% fprintf('test')
end