function [data, y] = obj(prob, data, u)

mu = u(1);
var = u(2);

y = mu - sqrt(var);

end