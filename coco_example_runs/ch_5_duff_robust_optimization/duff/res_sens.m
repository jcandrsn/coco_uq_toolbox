function dwdm = res_sens(m, F)

% -(k/(2 Sqrt[k/m] m^2)) + (3 F^2 Sqrt[k/m] \[Alpha] \[Epsilon])/(
%  8 c^2 k^2) - (3 F^2 \[Alpha] \[Epsilon])/(16 c^2 k Sqrt[k/m] m)

eps = 0.001;
c = 2;
k = 1;
alpha = 5;
F = 20;

dwdm = -k./(2*sqrt(k./m).*m.^2) + (3*F^2*sqrt(k./m)*alpha*eps)./(8*c^2*k^2) - ...
  3*F^2*alpha*eps./(16*c^2*k*sqrt(k./m).*m);

end