function j = dRdm(w,k,m,eps)

A = (w - sqrt(k./m))./((3/8).*eps*0.01.*sqrt(k./m));
B = (w.*(sqrt(m)).^(-1))./((3/8)*0.01.*eps.*sqrt(k));

j = (1/4).*((sqrt(A)).^1).*(B);

end