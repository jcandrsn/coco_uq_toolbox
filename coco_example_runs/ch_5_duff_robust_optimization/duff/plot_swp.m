
function plot_swp(run_name, lab)
  prob = coco_prob();
  prob = coco_set(prob, 'coll', 'NTST', 20);
  prob = ode_bvp2bvp(prob, '', run_name, 's1.PK1', lab);
  
  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  maps = data.coll_seg.maps;
  x0 = uidx(maps.x0_idx);
  pnames = {'m', 'c', 'k', 'alpha', 'A', 'om'};
  prob = coco_add_pars(prob, 's1.PK1.pars', maps.p_idx, pnames, 'inactive');
  prob = coco_add_pars(prob, 'R', x0(1), 'R', 'inactive');
  % Maintain velocity constraint
  prob = coco_add_pars(prob, 'v0', x0(2), 'v0', 'inactive');

  bd_swp = coco(prob, 'plot_swp', [], 1, {'om', 'R'}, {[0.01, 10]});
  om_idx = strcmpi('om', bd_swp(1,:));
  r_idx = strcmpi('R', bd_swp(1,:));
  semilogx(cell2mat(bd_swp(2:end, om_idx)), ...
           cell2mat(bd_swp(2:end, r_idx)));
  hold on
  bd = coco_bd_read(run_name);
  lab_idx = strcmpi('LAB', bd(1,:));
  om_idx = strcmpi('om', bd(1,:));
  pk1r_idx = strcmpi('PK1.R', bd(1,:));
  pk2r_idx = strcmpi('PK2.R', bd(1,:));
  omdiff_idx = strcmpi('om_diff', bd(1,:));
  tf = cellfun('isempty', bd(:, lab_idx));
  bd(tf, lab_idx)={0};
  lab_idx = find(cell2mat(bd(2:end,lab_idx))==lab)+1;
  om1 = cell2mat(bd(lab_idx, om_idx));
  pk1 = cell2mat(bd(lab_idx, pk1r_idx));
  om2 = om1 + cell2mat(bd(lab_idx, omdiff_idx));
  pk2 = cell2mat(bd(lab_idx, pk2r_idx));
  
  plot([om1, om2], [pk1, pk2],'ro')
end