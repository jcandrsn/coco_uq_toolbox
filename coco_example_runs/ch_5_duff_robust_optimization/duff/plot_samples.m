bd = coco_bd_read('uq_sweep');

A_idx      = strcmpi('A', bd(1,:));
k_idx      = strcmpi('k', bd(1,:));
m_idx      = strcmpi('m', bd(1,:));
s1pk1r_idx = strcmpi('s1.PK1.R', bd(1,:));
s2pk1r_idx = strcmpi('s2.PK1.R', bd(1,:));
s3pk1r_idx = strcmpi('s3.PK1.R', bd(1,:));
s4pk1r_idx = strcmpi('s4.PK1.R', bd(1,:));
hyst1_idx  = strcmpi('hyst.1', bd(1,:));
hyst2_idx  = strcmpi('hyst.2', bd(1,:));
hyst3_idx  = strcmpi('hyst.3', bd(1,:));
hyst4_idx  = strcmpi('hyst.4', bd(1,:));
om1_idx    = strcmpi('om.1', bd(1,:));

% mu_idx = strcmpi('resp.mean', bd(1,:));
% sig_idx = strcmpi('resp.var', bd(1,:));

A     = cell2mat(bd(2:end, A_idx));
k     = cell2mat(bd(2:end, k_idx));
m     = cell2mat(bd(2:end, m_idx));
om1   = cell2mat(bd(2:end, om1_idx));

o = coco_bd_col(bd, 'obj');
mag = 5;
mag2 = 1;
s1pk1 = cell2mat(bd(2:end, s1pk1r_idx));
hyst1 = cell2mat(bd(2:end, hyst1_idx));
s1 = s1pk1 - mag*abs(hyst1);

s2pk1 = cell2mat(bd(2:end, s2pk1r_idx));
hyst2 = cell2mat(bd(2:end, hyst2_idx));
s2 = s2pk1 - mag*abs(hyst2);

s3pk1 = cell2mat(bd(2:end, s3pk1r_idx));
hyst3 = cell2mat(bd(2:end, hyst3_idx));
s3 = s3pk1 - mag*abs(hyst3);

s4pk1 = cell2mat(bd(2:end, s4pk1r_idx));
hyst4 = cell2mat(bd(2:end, hyst4_idx));
s4 = s4pk1 - mag*abs(hyst4);
alphas = uq_data.wtd_psi_mat*[s1,s2,s3,s4]';

mu = alphas(1,:);
var = sum(alphas(2:end,:).^2);

% o = coco_bd_col(bd, 'resp.mean') - mag2*sqrt(coco_bd_col(bd, 'resp.var'));
o0 = mu;
o1 = mu - 1*sqrt(var);
o2 = mu - 2*sqrt(var);
o3 = mu - 3*sqrt(var);
[~, idx0] = max(o0);
[~, idx1] = max(o1);
[~, idx2] = max(o2);
[~, idx3] = max(o3);

figure(1)
plot(k, o1)
plot(A, s1, A, s2, A, s3, A, s4)
hold on
plot(A, o0, 'r--', A, o1, 'k--', A, o2, 'g--', A, o3, 'b--', 'LineWidth', 2)
plot(A(idx0), o0(idx0), 'ro', A(idx1), o1(idx1), 'ko', A(idx2), o2(idx2), 'go', A(idx3), o3(idx3), 'bo', 'LineWidth', 2)

figure(2)
plot([A,A,A,A], [s1pk1, s2pk1, s3pk1, s4pk1])

hold on
plot([A,A,A,A], mag*abs([hyst1, hyst2, hyst3, hyst4]))

figure(4)
plot(A, o)
