function [data, J] = obj_du(prob, data, u)

mu = u(1);
var = u(2);

J = [1, (-1/2)*(var)^(-1/2)];

end