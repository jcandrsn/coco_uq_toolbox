function [data, y] = obj(prob, data, u)
%OBJ Summary of this function goes here
%   Objective function for

x0 = u(1);
X0 = u(2);
Vrms = u(3);
T = u(4);

y = x0/X0 - sqrt(Vrms/T);

end

