% Potential Plot for Thesis
%% Illustration of clamping the response peaks of the frequency response 
%  diagram
swp_bd = coco_bd_read('sweep');
subplot(1,2,1)
plot(cell2mat(swp_bd(2:end, 20)), cell2mat(swp_bd(2:end, 21))./cell2mat(swp_bd(2:end, 11)))
xlim([0.5,1.5])

subplot(1,2,2)
T = 2*pi./(cell2mat(swp_bd(2:end, 20)));
plot(cell2mat(swp_bd(2:end, 20)), sqrt((1./T).*cell2mat(swp_bd(2:end, 23))))
xlim([0.5,1.5])

% pk_labs = coco_bd_labs(swp_bd, 'PK');
% for lab=pk_labs
%   sol = coll_read_solution('bvp.seg1', 'sweep', lab);
%   semilogx(sol.p(end), sol.xbp(1,1), 'ro');
% end

% bd = coco_bd_read('pk1');
% pk_labs = coco_bd_labs(bd, 'PK');
% sol = coll_read_solution('PK1.bvp.seg1', 'pk1', pk_labs(1));
% semilogx(sol.p(end), sol.xbp(1,1), 'ko');
% sol = coll_read_solution('PK2', 'pk1', pk_labs(1));
% semilogx(sol.p(end), sol.xbp(1,1), 'ko');
% 
% bd = coco_bd_read('pk2');
% pk_labs = coco_bd_labs(bd, 'PK');
% sol = coll_read_solution('PK3', 'pk2', pk_labs(1));
% semilogx(sol.p(end), sol.xbp(1,1), 'ko');
% sol = coll_read_solution('PK4', 'pk2', pk_labs(1));
% semilogx(sol.p(end), sol.xbp(1,1), 'ko');
% 
% set(gca, 'xlim', 10.^[0,2])

% JCA:  Add Suplots around peak estimatesto show it's not
%       just a single dot
% JCA:  Add Labels, change colors to match other plots in
%       document