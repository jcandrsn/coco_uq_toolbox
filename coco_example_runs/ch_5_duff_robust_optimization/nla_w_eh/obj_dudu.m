function [data, dJ] = obj_dudu(prob, data, u)

x0 = u(1);
X0 = u(2);
Vrms = u(3);
T = u(4);

% % y = x0/X0 - sqrt(Vrms/T);
% J = zeros(1,4);
% J(1) = 1/X0;
% J(2) = -x0/X0^2;
% J(3) = (1/(2*T))*(1/(sqrt(Vrms/T)));
% J(4) = -(Vrms/(2*T^2))*(1/(sqrt(Vrms/T)));
dJ = zeros(1,4,4);

dJ(1,1,2) = -1/X0^2;
dJ(1,2,1) = -1/X0^2;
dJ(1,2,2) = 2*x0/X0^3;
dJ(1,3,3) = -1/(4*(T^2)*(Vrms/T)^(3/2));
dJ(1,3,4) = Vrms/(4*(T^3)*(Vrms/T)^(3/2)) - 1/(2*(T^2)*sqrt(Vrms/T));
dJ(1,4,3) = dJ(1,3,4);
dJ(1,4,4) = -(Vrms^2)/(4*(T^4)*(Vrms/T)^(3/2)) + Vrms/((T^3)*sqrt(Vrms/T));

end

