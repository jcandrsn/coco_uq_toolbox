function [data, y] = rdo(prob, data, u)

rmax_mean = u(1);
rmax_var = u(2);
vrms_mean = u(3);
vrms_var = u(4);

y = (rmax_mean - vrms_mean) + 3*(rmax_var+vrms_var);

end