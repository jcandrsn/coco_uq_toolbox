function prob = lab2prob(prob, run_name, lab_name, lab_number, adjoint)

  if nargin < 5
    adjoint = 1;
  end
    
  bd = coco_bd_read(run_name);
  labs = coco_bd_labs(bd, lab_name);
  lab = labs(lab_number);
  
  prob = ode_bvp2bvp(prob, '', run_name, lab);
  
  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  
  maps = data.coll_seg.maps;
  x0 = uidx(maps.x0_idx);
  
  prob = coco_add_pars(prob, 'v0', x0(2), 'v0', 'inactive');
  data = data.pr;
  data.objhan = @rms_obj;
  data.objhan_dx = @rms_obj_dx;
  data.objhan_dp = @rms_obj_dp;
  
  % Provide an Exact Initial Guess
  maps = data.coll_seg.maps;
  int  = data.coll_seg.int;
  
  T   = prob.efunc.x0(uidx(maps.T_idx));    % Extract interval length
  x   = prob.efunc.x0(uidx(maps.xbp_idx));  % Extract basepoint values
  p   = prob.efunc.x0(uidx(maps.p_idx));    % Extract problem parameters
  xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
  pp  = repmat(p, maps.p_rep);

  objode = data.objhan(xx, pp);
  wts1 = repmat(int.wt',[maps.NTST,1]);

  u0 = T/(2*maps.NTST)*objode*wts1;

  prob = coco_add_func(prob, 'rms', ...
  @int_obj, @int_obj_du, @int_obj_dudu, data, ...
  'zero', 'uidx', uidx, 'u0', u0);

  rms_uidx = coco_get_func_data(prob, 'rms', 'uidx');

  prob = coco_add_func(prob, 'obj', ...
    @obj, @obj_du, @obj_dudu, [], ...
    'inactive', 'obj', 'uidx', ...
    [x0(1); uidx(maps.p_idx(1)); ...
     rms_uidx(end); uidx(maps.T_idx)]);

  % adjoints
  if adjoint
    prob = adjt_bvp2bvp(prob, '', run_name, lab);
    [adata, axidx] = coco_get_adjt_data(prob, 'bvp.seg1.coll', 'data', 'axidx');
    opt = adata.coll_opt;

    chart = coco_read_solution(run_name, lab, 'chart');
    cdata = coco_get_chart_data(chart, 'lsol');

    fid = 'v0';
    [chart, lidx] = coco_read_adjoint(fid, run_name, lab, ...
      'chart', 'lidx');
    l0 = chart.x;
    tl0 = cdata.v(lidx);  
    prob = coco_add_adjt(prob, 'v0', 'd.v0', ...
      'aidx', axidx(opt.x0_idx(2)), ...
      'l0', l0);

    adata = adata.pr;
    adata.objhan = @rms_obj;
    adata.objhan_dx = @rms_obj_dx;
    adata.objhan_dp = @rms_obj_dp;

    fid = 'rms';
    [chart, lidx] = coco_read_adjoint(fid, run_name, lab, ...
      'chart', 'lidx');
    l0 = chart.x;
    tl0 = cdata.v(lidx);

    prob = coco_add_adjt(prob, 'rms', @adj_int_obj, adata, ...
      'aidx', axidx([opt.xcn_idx; opt.T0_idx; opt.T_idx; opt.p_idx]), ...
       'l0', l0);

    fid = 'obj';
    [chart, lidx] = coco_read_adjoint(fid, run_name, lab, ...
      'chart', 'lidx');
    l0 = chart.x;
    tl0 = cdata.v(lidx);

    rms_axidx = coco_get_adjt_data(prob, 'rms', 'axidx'); 
    prob = coco_add_adjt(prob, 'obj', 'd.obj', 'aidx', ...
      [axidx(opt.x0_idx(1)); axidx(opt.p_idx(1)); ...
      rms_axidx(end); axidx(opt.T_idx)], 'l0', l0, 'tl0', tl0);
  end

end