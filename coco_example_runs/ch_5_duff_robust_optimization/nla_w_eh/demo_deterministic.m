addpath('../../nonlinear_absorber_w_energy_harvester')
addpath('../../utils')

% Initialize problem instance and set options
prob_init = coco_prob();

prob_init = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
prob_init = coco_set(prob_init, 'cont', 'PtMX', 100);%, 'h_max', 10);
% prob_init = coco_set(prob_init, 'cont', 'PtMX', 100, ...
%   'h', 0.5, 'almax', 30, 'h_max', 1000);
% prob_init = coco_set(prob_init, 'cont', 'NAdapt', 5, 'NPR', 10);

% % Temporarily allow really bad results for some checking of
% % Jacobians.
% prob_init = coco_set(prob_init, 'coll', 'TOL', 5);

run_name = 'det_setup';

% Parameters
X0 = 0.05;
z1 = 0.08;
mu = 0.1;
z2 = 0.1;
b  = 0.909;
al = 0.1;
Xi = 0.05;
la = 1;
ka = 0.05;
Om = 0.5;

p0 = [X0; z1; mu; z2; b; al; Xi; la; ka; Om];
% parameter names
pnames = {'X0', 'z1', 'mu', 'z2', 'b', 'al', 'Xi', 'la', 'ka', 'Om'};

if not(coco_exist(run_name, 'run'))

  x10 = 0;
  x20 = 0;
  x30 = 0;
  x40 = 0;
  x50 = 0;
  x60 = 0;

  x0 = [x10; x20; x30; x40; x50; x60];
  % Let Transients die out
  [~, x0] = ode45(@(t,x) nla(x, p0), [0, 50*pi], x0);

  % Initial Solution Guess
  [t0, x0_ig]   = ode45(@(t,x) nla(x, p0), ...
    linspace(0,2*pi/Om,300), [x0(end,1:6)']);
  x0_ig(:,6) = x0_ig(:,6) - x0_ig(1,6);  

  % 'coll'-toolbox arguments a;slkdjf;laksdjf;ljsadfdkj;kji
  coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
    @nla_dxdp, @nla_dpdp, t0, x0_ig, p0};

  % 'bvp'-toolbox arguments
  bvp_args = {@nla_bc, @nla_bc_du, @nla_bc_dudu};
   
  prob = ode_isol2bvp(prob_init, '', coll_args{:}, pnames, bvp_args{:});

  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  maps = data.coll_seg.maps;
  x0_coco = uidx(maps.x0_idx);
  prob = coco_add_pars(prob, 'R', x0_coco(1), 'R', 'inactive');
  prob = coco_add_pars(prob, 'v0', x0_coco(2), 'v0', 'inactive');
  
  prob = coco_add_event(prob, 'VMAX', 'v0', 0);
  
  bd = coco(prob, run_name, [], 1, {'v0', 'R'});
else
  bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'det_sweep';
if not(coco_exist(run_name, 'run')) || true
  labs = coco_bd_labs(bd, 'VMAX');
 
  prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
  prob = coco_set(prob, 'cont', 'PtMX', 300);
  prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10);   

  sol = coll_read_solution('bvp.seg1', 'uq_setup', labs(2));
  
  coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
    @nla_dxdp, @nla_dpdp, sol.tbp, sol.xbp, sol.p};
  
  % 'bvp'-toolbox arguments
  bvp_args = {@nla_bc, @nla_bc_du, @nla_bc_dudu};

  prob = ode_isol2bvp(prob, '', coll_args{:}, pnames, bvp_args{:});
  
  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  
  maps = data.coll_seg.maps;
  x0_idx = uidx(maps.x0_idx);

  prob = coco_add_pars(prob, 'v0', x0_idx(2), 'v0', 'inactive');
  data = data.pr;
  data.objhan = @rms_obj;
  data.objhan_dx = @rms_obj_dx;
  data.objhan_dp = @rms_obj_dp;
  
  prob = coco_add_func(prob, 'rms', ...
    @int_obj, @int_obj_du, data, ...
    'zero', 'uidx', uidx, 'u0', 6e-4);
  
  rms_uidx = coco_get_func_data(prob, 'rms', 'uidx');
  
  prob = coco_add_func(prob, 'obj', ...
    @obj, @obj_du, @obj_dudu, [], ...
    'inactive', 'obj', 'uidx', ...
    [x0_idx(1); uidx(maps.p_idx(1)); ...
    rms_uidx(end); uidx(maps.T_idx)]);
  
  % adjoints  
  prob = adjt_isol2bvp(prob, '');
  [adata, axidx] = coco_get_adjt_data(prob, 'bvp.seg1.coll', 'data', 'axidx');
  opt = adata.coll_opt;
%   prob = coco_add_adjt(prob, 'R', 'd.R', 'aidx', axidx(opt.x0_idx(1)));
  prob = coco_add_adjt(prob, 'v0', 'd.v0', 'aidx', axidx(opt.x0_idx(2)));
  
  adata = adata.pr;
  adata.objhan = @rms_obj;
  adata.objhan_dx = @rms_obj_dx;
  adata.objhan_dp = @rms_obj_dp;
  adata.objhan_dxdx = @rms_obj_dxdx;
  adata.objhan_dxdp = @rms_obj_dxdp;
  adata.objhan_dpdp = @rms_obj_dpdp;
  

  prob = coco_add_adjt(prob, 'rms', @adj_int_obj, ...
    @adj_int_obj_du, adata, 'aidx', ...
    axidx([opt.xcn_idx; opt.T0_idx; opt.T_idx; opt.p_idx]), ...
     'l0', 0);
  rms_axidx = coco_get_adjt_data(prob, 'rms', 'axidx'); 
  prob = coco_add_adjt(prob, 'obj', 'd.obj', 'aidx', ...
    [axidx(opt.x0_idx(1)); axidx(opt.p_idx(1)); ...
    rms_axidx(end); axidx(opt.T_idx)]);
  
  bd = coco(prob, run_name, [], 1, {'obj', 'Om', 'd.obj', 'd.X0', 'd.z1', 'd.mu', 'd.z2', 'd.b', 'd.al', 'd.Xi', 'd.la', 'd.ka', 'd.v0'}, {[0,10],[0.25, 2.5]});

else
  bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'det_eta1_to_1_FP2';

if not(coco_exist(run_name, 'run'))
  labs = coco_bd_labs(bd, 'BP');
  lab = labs(2);
  
  prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
  prob = coco_set(prob, 'cont', 'PtMX', 300);
  prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10);
  prob = coco_set(prob, 'cont', 'h', 0.5, 'almax', 30, 'h_max', 1000);

  prob = ode_BP2bvp(prob, '', last_run_name, lab);
  
  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  
  maps = data.coll_seg.maps;
  x0_idx = uidx(maps.x0_idx);
  
  prob = coco_add_pars(prob, 'v0', x0_idx(2), 'v0', 'inactive');
  data = data.pr;
  data.objhan = @rms_obj;
  data.objhan_dx = @rms_obj_dx;
  data.objhan_ddsp = @rms_obj_dp;
  
  % Provide an Exact Initial Guess
  maps = data.coll_seg.maps;
  int  = data.coll_seg.int;
  
  T   = prob.efunc.x0(uidx(maps.T_idx));    % Extract interval length
  x   = prob.efunc.x0(uidx(maps.xbp_idx));  % Extract basepoint values
  p   = prob.efunc.x0(uidx(maps.p_idx));    % Extract problem parameters
  xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
  pp  = repmat(p, maps.p_rep);

  objode = data.objhan(xx, pp);
  wts1 = repmat(int.wt',[maps.NTST,1]);

  u0 = T/(2*maps.NTST)*objode*wts1;

  prob = coco_add_func(prob, 'rms', ...
  @int_obj, @int_obj_du, @int_obj_dudu, data, ...
  'zero', 'uidx', uidx, 'u0', u0);

  rms_uidx = coco_get_func_data(prob, 'rms', 'uidx');

  prob = coco_add_func(prob, 'obj', ...
    @obj, @obj_du, @obj_dudu, [], ...
    'inactive', 'obj', 'uidx', ...
    [x0_idx(1); uidx(maps.p_idx(1)); ...
    rms_uidx(end); uidx(maps.T_idx)]);

  % adjoints
  prob = adjt_BP2bvp(prob, '', last_run_name, lab);
  [adata, axidx] = coco_get_adjt_data(prob, 'bvp.seg1.coll', 'data', 'axidx');
  opt = adata.coll_opt;

  chart = coco_read_solution(last_run_name, lab, 'chart');
  cdata = coco_get_chart_data(chart, 'lsol');

  fid = 'v0';
  [chart, lidx] = coco_read_adjoint(fid, last_run_name, lab, ...
    'chart', 'lidx');
  l0 = chart.x;
  tl0 = cdata.v(lidx);  
  prob = coco_add_adjt(prob, 'v0', 'd.v0', ...
    'aidx', axidx(opt.x0_idx(2)), ...
    'l0', l0, 'tl0', tl0);
  
  adata = adata.pr;
  adata.objhan = @rms_obj;
  adata.objhan_dx = @rms_obj_dx;
  adata.objhan_dp = @rms_obj_dp;
  
  fid = 'rms';
  [chart, lidx] = coco_read_adjoint(fid, last_run_name, lab, ...
    'chart', 'lidx');
  l0 = chart.x;
  tl0 = cdata.v(lidx);
  
  prob = coco_add_adjt(prob, 'rms', @adj_int_obj, adata, ...
    'aidx', axidx([opt.xcn_idx; opt.T0_idx; opt.T_idx; opt.p_idx]), ...
     'l0', l0, 'tl0', tl0);
   
  fid = 'obj';
  [chart, lidx] = coco_read_adjoint(fid, last_run_name, lab, ...
    'chart', 'lidx');
  l0 = chart.x;
  tl0 = cdata.v(lidx);
   
  rms_axidx = coco_get_adjt_data(prob, 'rms', 'axidx'); 
  prob = coco_add_adjt(prob, 'obj', 'd.obj', 'aidx', ...
    [axidx(opt.x0_idx(1)); axidx(opt.p_idx(1)); ...
    rms_axidx(end); axidx(opt.T_idx)], 'l0', l0, 'tl0', tl0);
  
   
  bd = coco(prob, run_name, [], 1, {'d.obj', 'obj', 'Om', 'd.X0', 'd.z1', 'd.mu', 'd.z2', 'd.b', 'd.al', 'd.Xi', 'd.la', 'd.ka', 'd.v0'}, {[0,1]});
else
  bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'det_d.b.to.0_FP2';

lab_name = 'EP';
lab_number = 2;

prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
prob = coco_set(prob, 'cont', 'PtMX', 600);
prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10);
prob = coco_set(prob, 'cont', 'h', 0.5, 'almax', 30, 'h_max', 1000);

if not(coco_exist(run_name, 'run'))
  labs = coco_bd_labs(bd, 'EP');
  lab = labs(2);
  
  prob = ode_bvp2bvp(prob, '', last_run_name, lab);
  
  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  
  maps = data.coll_seg.maps;
  x0_idx = uidx(maps.x0_idx);
  
  prob = coco_add_pars(prob, 'v0', x0_idx(2), 'v0', 'inactive');
  data = data.pr;
  data.objhan = @rms_obj;
  data.objhan_dx = @rms_obj_dx;
  data.objhan_dp = @rms_obj_dp;
  
  % Provide an Exact Initial Guess
  maps = data.coll_seg.maps;
  int  = data.coll_seg.int;
  
  T   = prob.efunc.x0(uidx(maps.T_idx));    % Extract interval length
  x   = prob.efunc.x0(uidx(maps.xbp_idx));  % Extract basepoint values
  p   = prob.efunc.x0(uidx(maps.p_idx));    % Extract problem parameters
  xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
  pp  = repmat(p, maps.p_rep);

  objode = data.objhan(xx, pp);
  wts1 = repmat(int.wt',[maps.NTST,1]);

  u0 = T/(2*maps.NTST)*objode*wts1;

  prob = coco_add_func(prob, 'rms', ...
  @int_obj, @int_obj_du, @int_obj_dudu, data, ...
  'zero', 'uidx', uidx, 'u0', u0);

  rms_uidx = coco_get_func_data(prob, 'rms', 'uidx');

  prob = coco_add_func(prob, 'obj', ...
    @obj, @obj_du, @obj_dudu, [], ...
    'inactive', 'obj', 'uidx', ...
    [x0_idx(1); uidx(maps.p_idx(1)); ...
     rms_uidx(end); uidx(maps.T_idx)]);

  % adjoints
  prob = adjt_bvp2bvp(prob, '', last_run_name, lab);
  [adata, axidx] = coco_get_adjt_data(prob, 'bvp.seg1.coll', 'data', 'axidx');
  opt = adata.coll_opt;

  chart = coco_read_solution(last_run_name, lab, 'chart');
  cdata = coco_get_chart_data(chart, 'lsol');

  fid = 'v0';
  [chart, lidx] = coco_read_adjoint(fid, last_run_name, lab, ...
    'chart', 'lidx');
  l0 = chart.x;
  tl0 = cdata.v(lidx);  
  prob = coco_add_adjt(prob, 'v0', 'd.v0', ...
    'aidx', axidx(opt.x0_idx(2)), ...
    'l0', l0);
  
  adata = adata.pr;
  adata.objhan = @rms_obj;
  adata.objhan_dx = @rms_obj_dx;
  adata.objhan_dp = @rms_obj_dp;
  
  fid = 'rms';
  [chart, lidx] = coco_read_adjoint(fid, last_run_name, lab, ...
    'chart', 'lidx');
  l0 = chart.x;
  tl0 = cdata.v(lidx);
  
  prob = coco_add_adjt(prob, 'rms', @adj_int_obj, adata, ...
    'aidx', axidx([opt.xcn_idx; opt.T0_idx; opt.T_idx; opt.p_idx]), ...
     'l0', l0);
   
  fid = 'obj';
  [chart, lidx] = coco_read_adjoint(fid, last_run_name, lab, ...
    'chart', 'lidx');
  l0 = chart.x;
  tl0 = cdata.v(lidx);
   
  rms_axidx = coco_get_adjt_data(prob, 'rms', 'axidx'); 
  prob = coco_add_adjt(prob, 'obj', 'd.obj', 'aidx', ...
    [axidx(opt.x0_idx(1)); axidx(opt.p_idx(1)); ...
    rms_axidx(end); axidx(opt.T_idx)], 'l0', l0, 'tl0', tl0);
  
  prob = coco_add_event(prob, 'OPT', 'd.b', 0);
  
  % Directly reducing d.b to zero achieves what I believe
  % is the desired point.  Increasing d.b encounters a fold
  % point and begins reducing again but along this branch,
  % b is increasing.  May look neat in a figure.
  bd = coco(prob, run_name, [], 1, {'d.b', 'obj', 'b', 'Om', 'd.X0', 'd.z1', 'd.mu', 'd.z2', 'd.al', 'd.Xi', 'd.la', 'd.ka', 'd.v0', 'd.obj'});
%   bd = coco(prob, run_name, [], 0, {'obj'});
else
  bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'det_d.Xi.to.0_FP2';

lab_name = 'OPT';
lab_number = 1;

prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
prob = coco_set(prob, 'cont', 'PtMX', 700);
prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10);
prob = coco_set(prob, 'cont', 'h', 0.5, 'almax', 30, 'h_max', 1000);

if not(coco_exist(run_name, 'run'))
  
  prob = lab2prob(prob, last_run_name, lab_name, lab_number);

  prob = coco_add_event(prob, 'OPT', 'd.Xi', 0);

  bd = coco(prob, run_name, [], 1, {'d.Xi', 'obj', 'Xi', 'b', 'Om', 'd.X0', 'd.z1', 'd.z2', 'd.mu', 'd.al', 'd.ka', 'd.la', 'd.v0', 'd.obj', 'd.b'}, [-.27, 0]);
%   bd = coco(prob, run_name, [], 0, {'obj'});
else
  bd = coco_bd_read(run_name);
end
