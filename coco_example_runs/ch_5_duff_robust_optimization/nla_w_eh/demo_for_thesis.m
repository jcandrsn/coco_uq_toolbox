addpath('../../nonlinear_absorber_w_energy_harvester')

% Initialize problem instance and set options
prob_init = coco_prob();

prob_init = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
prob_init = coco_set(prob_init, 'cont', 'h', 0.5);
% prob_init = coco_set(prob_init, 'cont', 'PtMX', 100, 'NPR', 1, ...
%   'h', 0.5, 'almax', 30, 'h_max', 1000);
% prob_init = coco_set(prob_init, 'cont', 'NAdapt', 5, 'NPR', 10);

run_name = 'uq_setup';

% Parameters
X0 = 0.05;
z1 = 0.08;
mu = 0.1;
z2 = 0.1;
b  = 0.909;
al = 0.1;
Xi = 0.05;
la = 1;
ka = 0.05;
Om = 0.5;

p0 = [X0; z1; mu; z2; b; al; Xi; la; ka; Om];
% 'coll'-toolbox arguments

% parameter names
pnames = {'X0', 'z1', 'mu', 'z2', 'b', 'al', 'Xi', ...
  'la', 'ka', 'Om'};

if not(coco_exist(run_name, 'run'))

  x1_0 = 0;
  x2_0 = 0;
  x3_0 = 0;
  x4_0 = 0;
  x5_0 = 0;
  x6_0 = 0;

  x0 = [x1_0; x2_0; x3_0; x4_0; x5_0; x6_0];
  % Let Transients die out
  [t, x0] = ode45(@(t,x) nla(x, p0), [0, 50*pi], x0);

  % Initial Solution Guess
  [t0, x0_ig]   = ode45(@(t,x) nla(x, p0), ...
    linspace(0,2*pi/Om,300), [x0(end,1:6)']);
  x0_ig(:,6) = x0_ig(:,6) - x0_ig(1,6);
  
  coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
    @nla_dxdp, @nla_dpdp, t0, x0_ig, p0};
  
  % 'bvp'-toolbox arguments
  bvp_args = {@nla_bc, @nla_bc_du, @nla_bc_dudu};

  prob = ode_isol2bvp(prob_init, '', coll_args{:}, pnames, bvp_args{:});
  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  maps = data.coll_seg.maps;
  x0_coco = uidx(maps.x0_idx);
  prob = coco_add_pars(prob, 'R', x0_coco(1), 'R', 'inactive');
  prob = coco_add_pars(prob, 'v0', x0_coco(2), 'v0', 'inactive');
  
  prob = coco_add_event(prob, 'VMAX', 'v0', 0);
  
  % Phase shift until v0 = 0
  bd = coco(prob, run_name, [], 1, {'v0', 'R'});
else
  bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'uq_sweep';

if not(coco_exist(run_name, 'run')) || true
  labs = coco_bd_labs(bd, 'VMAX');

  prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
  prob = coco_set(prob, 'cont', 'PtMX', 300, 'h_max', 1000);
  prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10);

  sol = coll_read_solution('bvp.seg1', 'uq_setup', labs(2));
  
  coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
    @nla_dxdp, @nla_dpdp, sol.tbp, sol.xbp, sol.p};
  
  % 'bvp'-toolbox arguments
  bvp_args = {@nla_bc_v0, @nla_bc_v0_du, @nla_bc_v0_dudu};
  
  % 'uq'-toolbox arguments
  uq_args = {
    6, ...                % Stochastic Parameter
    {'Uniform'}, ...           % Distribution Type
    [[0.01, 0.5]], ...           % [Mean, St.Dev]
    {},...
    '-add-adjt'};                 
  
  prob = uq_isol2bvp_sample(prob, 'orig', ...
  coll_args{:}, pnames, bvp_args{:}, uq_args{:});
  
  prob = uq_coll_add_response(prob, 'orig', 'rmax', ...
    'bv', @x10, @x10_du, @x10_dudu);
  prob = uq_coll_add_response_adjoint(prob, 'orig', 'rmax');
  
  prob = uq_coll_add_response(prob, 'orig', 'vrms', ...
    'int', @rms_obj, @rms_obj_dx, @rms_obj_dp, ...
    @rms_obj_dxdx, @rms_obj_dxdp, @rms_obj_dpdp);
  prob = uq_coll_add_response_adjoint(prob, 'orig', 'vrms');

  % Robust Design Optimization Objective Function
  response_id = coco_get_id('orig', 'uq', 'responses');

  rmax_mean_idx = coco_get_func_data(prob, ...
    coco_get_id(response_id, 'rmax', 'mean'), 'uidx');
  rmax_var_idx = coco_get_func_data(prob, ...
    coco_get_id(response_id, 'rmax', 'variance'), 'uidx');
  vrms_mean_idx = coco_get_func_data(prob, ...
    coco_get_id(response_id, 'vrms', 'mean'), 'uidx');
  vrms_var_idx = coco_get_func_data(prob, ...
    coco_get_id(response_id, 'vrms', 'variance'), 'uidx');

  prob = coco_add_func(prob, 'rdo', @rdo, @rdo_dU, ...
    @rdo_dUdU, [], 'inactive', 'rdo', 'uidx', ...
    [rmax_mean_idx;rmax_var_idx;vrms_mean_idx;vrms_var_idx]);

  rmax_mean_aidx = coco_get_adjt_data(prob, ...
    coco_get_id(response_id, 'rmax', 'mean'), 'axidx');
  rmax_var_aidx = coco_get_adjt_data(prob, ...
    coco_get_id(response_id, 'rmax', 'variance'), 'axidx');
  vrms_mean_aidx = coco_get_adjt_data(prob, ...
    coco_get_id(response_id, 'vrms', 'mean'), 'axidx');
  vrms_var_aidx = coco_get_adjt_data(prob, ...
    coco_get_id(response_id, 'vrms', 'variance'), 'axidx');  

  prob = coco_add_adjt(prob, 'rdo', 'd.rdo', 'aidx', ...
    [rmax_mean_aidx;rmax_var_aidx;vrms_mean_aidx;vrms_var_aidx]);
  
  bd = coco(prob, run_name, [], 1, ...
    {'rdo', 'b', 'd.rdo', 'orig.rmax.mean', 'orig.rmax.var', ...
    'orig.vrms.mean', 'orig.vrms.var', 'd.up.z2', ...
    'd.lo.z2', 'd.X0', 'd.z1', 'd.mu', ...
    'd.Xi', 'd.la', 'd.ka', 'd.al', 'd.Om'}, [1.39, 10]);

else
  bd = coco_bd_read(run_name);
end