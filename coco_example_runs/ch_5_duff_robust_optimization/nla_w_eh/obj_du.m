function [ data, J ] = obj_du(prob, data, u)

x0 = u(1);
X0 = u(2);
Vrms = u(3);
T = u(4);

% y = x0/X0 - sqrt(Vrms/T);
J = zeros(1,4);
J(1) = 1/X0;
J(2) = -x0/X0^2;
J(3) = -(1/(2*T))*(1/(sqrt(Vrms/T)));
J(4) = (Vrms/(2*T^2))*(1/(sqrt(Vrms/T)));

end

