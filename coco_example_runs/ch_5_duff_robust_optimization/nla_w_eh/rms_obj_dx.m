function Jx = rms_obj_dx(x, p)

V = x(5,:);
X0 = p(1,:);

Jx = zeros(1,6,numel(V));
Jx(1,5,:) = (2*V)./X0;

end