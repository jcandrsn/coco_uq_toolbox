function ddr = x10_dudu(data, T0, T, x0, x1, p)

  u_per_sample = 2 + 2*data.xdim + data.pdim;
  ddr = zeros(u_per_sample, u_per_sample, data.nsamples);
  
  p1_idx = 2+2*data.xdim+1;
  ddr(3,p1_idx,:) = -1./p(1,:).^2;
  ddr(p1_idx,3,:) = -1./p(1,:).^2;
  ddr(p1_idx,p1_idx,:) = (2*x0(1,:))./p(1,:).^3;

end