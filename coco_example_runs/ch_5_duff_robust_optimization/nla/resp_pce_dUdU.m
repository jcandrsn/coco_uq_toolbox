function [data, dJ] = resp_pce_dUdU(prob, data, u)
% UQ_PCE_COEFFICIENTS_DU Jacobian of the Polynomial Chaos
% Expansion (PCE) coefficient Evaluation function

dJ = zeros(1, numel(u), numel(u));
   
%% Test Jacobian with following sequence:
% [data, Jd] = coco_ezDFDX('f(o,d,x)', prob, data, @resp_pce_dU, u);
% diff_dJ = abs(dJ-dJd);
% max(max(diff_J))
% fprintf('test')
end