addpath('../../nonlinear_absorber_non_dim')
addpath('../../utils')

% Initialize problem instance and set options
prob_init = coco_prob();

prob_init = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
prob_init = coco_set(prob_init, 'cont', 'PtMX', 100);%, 'h_max', 10);
% prob_init = coco_set(prob_init, 'cont', 'PtMX', 100, ...
%   'h', 0.5, 'almax', 30, 'h_max', 1000);
% prob_init = coco_set(prob_init, 'cont', 'NAdapt', 5, 'NPR', 10);

% % Temporarily allow really bad results for some checking of
% % Jacobians.
% prob_init = coco_set(prob_init, 'coll', 'TOL', 5);

run_name = 'det_setup';

% Parameters
A = 0.05;
z1 = 0.08;
mu = 0.1;
z2 = 0.1;
b  = 0.909;
al = 0.5;%0.1;
Om = 10;

p0 = [A; z1; mu; z2; b; al; Om];
% parameter names
pnames = {'A', 'z1', 'mu', 'z2', 'b', 'al', 'Om'};

if not(coco_exist(run_name, 'run'))

  x10 = 0;
  x20 = 0;
  x30 = 0;
  x40 = 0;
  x50 = 0;

  x0 = [x10; x20; x30; x40; x50];
  % Let Transients die out
  [~, x0] = ode45(@(t,x) nla(x, p0), [0, 50*pi/Om], x0);

  % Initial Solution Guess
  [t0, x0_ig]   = ode45(@(t,x) nla(x, p0), ...
    linspace(0,2*pi/Om,300), [x0(end,1:5)']);
  
  x0_ig(:,5) = x0_ig(:,5) - x0_ig(1,5);  

  % 'coll'-toolbox arguments
  coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
    @nla_dxdp, @nla_dpdp, t0, x0_ig, p0};

  % 'bvp'-toolbox arguments
  bvp_args = {@nla_bc, @nla_bc_du, @nla_bc_dudu};
   
  prob = ode_isol2bvp(prob_init, '', coll_args{:}, pnames, bvp_args{:});

  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  maps = data.coll_seg.maps;
  x0_coco = uidx(maps.x0_idx);
  prob = coco_add_pars(prob, 'R', x0_coco(1), 'R', 'inactive');
  prob = coco_add_pars(prob, 'v0', x0_coco(2), 'v0', 'inactive');
  
  prob = coco_add_event(prob, 'VMAX', 'v0', 0);
  
  bd = coco(prob, run_name, [], 1, {'R', 'v0'});
else
  bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'det_sweep';
om_run_name = run_name;
if not(coco_exist(run_name, 'run'))
  labs = coco_bd_labs(bd, 'VMAX');
  lab = labs(1);
 
  prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
  prob = coco_set(prob, 'cont', 'PtMX', 300);
  prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10);   

  sol = coll_read_solution('bvp.seg1', last_run_name, lab);
  
  coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
    @nla_dxdp, @nla_dpdp, sol.tbp, sol.xbp, sol.p};
  
  % 'bvp'-toolbox arguments
  bvp_args = {@nla_bc_v0, @nla_bc_v0_du, @nla_bc_v0_dudu};

  prob = ode_isol2bvp(prob, '', coll_args{:}, pnames, bvp_args{:});
  
  [data, uidx] = coco_get_func_data(prob, 'bvp.seg1.coll', 'data', 'uidx');
  
  maps = data.coll_seg.maps;
  x0_idx = uidx(maps.x0_idx);
  prob = coco_add_pars(prob, 'R', x0_idx(1), 'R', 'inactive');
  prob = coco_add_pars(prob, 'v0', x0_idx(2), 'v0', 'inactive');
  
  prob = coco_add_event(prob, 'OM', 'R', 0.15);
  
  bd_om = coco(prob, run_name, [], 1, {'Om', 'R', 'v0'}, {[0.1,100]});

else
  bd_om = coco_bd_read(run_name);
end

run_name = 'trough';
if not(coco_exist(run_name, 'run'))
  labs = coco_bd_labs(bd_om, 'OM');
 
  prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
  prob = coco_set(prob, 'cont', 'PtMX', 100);
  prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10); 
  i=1;
  for lab=labs([2,3])
    segname = strcat('orb', int2str(i));
    sol = coll_read_solution('bvp.seg1', om_run_name, lab);

    coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
      @nla_dxdp, @nla_dpdp, sol.tbp, sol.xbp, sol.p};

    pnamesi = strcat(pnames, '.', int2str(i));
    x10_func_name = strcat('x0', int2str(i));
    x10_par_name  = coco_get_id('x0', int2str(i));
    % 'bvp'-toolbox arguments
    bvp_args = {@nla_bc_v0, @nla_bc_v0_du, @nla_bc_v0_dudu};

    if i==1 || i==3
      prob = ode_isol2bvp(prob, segname, coll_args{:}, pnamesi, bvp_args{:});
      [data1, uidx1] = coco_get_func_data(prob, coco_get_id(segname, 'bvp.seg1.coll'), 'data', 'uidx');
      maps1 = data1.coll_seg.maps;
      prob = coco_add_pars(prob, x10_func_name, uidx1(maps1.x0_idx(1)), x10_par_name, 'inactive');
    else
      prob = ode_isol2bvp(prob, segname, coll_args{:}, {}, bvp_args{:});
      [data2, uidx2] = coco_get_func_data(prob, coco_get_id(segname, 'bvp.seg1.coll'), 'data', 'uidx');
      maps2 = data2.coll_seg.maps;
      
      % Give freq a name
      freq_name = strcat('Om', int2str(i));
      freq_par  = coco_get_id('Om', int2str(i));
      prob = coco_add_pars(prob, freq_name, uidx2(maps2.p_idx(end)), freq_par, 'inactive');
      prob = coco_add_pars(prob, x10_func_name, uidx2(maps2.x0_idx(1)), x10_par_name, 'inactive');
      % Glue Parameters together (except forcing frequency)
      glue_name = strcat('par', int2str(i-1), int2str(i), '_glue');
      prob = coco_add_glue(prob, glue_name, uidx2([maps2.x0_idx(1);maps2.p_idx(1:6)]), ...
        uidx1([maps1.x0_idx(1);maps1.p_idx(1:6)]));

      prob = coco_add_func(prob, 'Om_diff', @udiff, @udiff_dU, @udiff_dUdU, ...
        [], 'inactive', 'Om_diff', 'uidx', [uidx2(maps2.p_idx(end)), ...
        uidx1(maps1.p_idx(end))]);
    end
    i=i+1;
  end

  prob = coco_add_event(prob, 'TR', 'Om_diff', 1e-4);
  bd_tr = coco(prob, run_name, [], 1, {'Om_diff', 'Om.1', 'Om.2', 'x0.1', 'x0.2'}, [0.99e-4, 0.3]);
else
  bd_tr = coco_bd_read(run_name);
end

run_name = 'peak';
if not(coco_exist(run_name, 'run'))
  labs = coco_bd_labs(bd_om, 'OM');
 
  prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
  prob = coco_set(prob, 'cont', 'PtMX', 100);
  prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10); 
  i = 3;
  for lab=labs([3,4])
    segname = strcat('orb', int2str(i));
    sol = coll_read_solution('bvp.seg1', om_run_name, lab);

    coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
      @nla_dxdp, @nla_dpdp, sol.tbp, sol.xbp, sol.p};

    pnamesi = strcat(pnames, '.', int2str(i));
    x10_func_name = strcat('x0', int2str(i));
    x10_par_name  = coco_get_id('x0', int2str(i));
    % 'bvp'-toolbox arguments
    bvp_args = {@nla_bc_v0, @nla_bc_v0_du, @nla_bc_v0_dudu};

    if i==1 || i==3
      prob = ode_isol2bvp(prob, segname, coll_args{:}, pnamesi, bvp_args{:});
      [data1, uidx1] = coco_get_func_data(prob, coco_get_id(segname, 'bvp.seg1.coll'), 'data', 'uidx');
      maps1 = data1.coll_seg.maps;
      prob = coco_add_pars(prob, x10_func_name, uidx1(maps1.x0_idx(1)), x10_par_name, 'inactive');
    else
      prob = ode_isol2bvp(prob, segname, coll_args{:}, {}, bvp_args{:});
      [data2, uidx2] = coco_get_func_data(prob, coco_get_id(segname, 'bvp.seg1.coll'), 'data', 'uidx');
      maps2 = data2.coll_seg.maps;
      
      % Give freq2 a name
      freq_name = strcat('Om', int2str(i));
      freq_par  = coco_get_id('Om', int2str(i));
      prob = coco_add_pars(prob, freq_name, uidx2(maps2.p_idx(end)), freq_par, 'inactive');
      prob = coco_add_pars(prob, x10_func_name, uidx2(maps2.x0_idx(1)), x10_par_name, 'inactive');
      % Glue Parameters together (except forcing frequency)
      glue_name = strcat('par', int2str(i-1), int2str(i), '_glue');
      prob = coco_add_glue(prob, glue_name, uidx2([maps2.x0_idx(1);maps2.p_idx(1:6)]), ...
        uidx1([maps1.x0_idx(1);maps1.p_idx(1:6)]));

      prob = coco_add_func(prob, 'Om_diff', @udiff, @udiff_dU, @udiff_dUdU, ...
        [], 'inactive', 'Om_diff', 'uidx', [uidx2(maps2.p_idx(end)), ...
        uidx1(maps1.p_idx(end))]);
    end
    i=i+1;
  end

  prob = coco_add_event(prob, 'PK', 'Om_diff', 1e-4);
  bd_pk = coco(prob, run_name, [], 1, {'Om_diff', 'Om.3', 'Om.4', 'x0.3', 'x0.4'}, [0.99e-4, 0.19]);
else
  bd_pk = coco_bd_read(run_name);
end

pk_name = 'peak';
tr_name = 'trough';
run_name = 'uq_nds';
if not(coco_exist(run_name, 'run')) || true
  prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
  prob = coco_set(prob, 'cont', 'PtMX', 100);
  prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10); 
  lab_names = {'TR', 'TR', 'PK', 'PK'};
  bds       = {bd_tr, bd_tr, bd_pk, bd_pk};
  run_names = {tr_name, tr_name, pk_name, pk_name};
   
  for i=1:4
    segname = strcat('orb', int2str(i));
    collname = coco_get_id(segname, 'bvp.seg1.coll');
    lab = coco_bd_labs(bds{i}, lab_names{i});

    x10_func_name = strcat('x0', int2str(i));
    x10_par_name  = coco_get_id('x0', int2str(i));

    switch i
      case 1
        prob = ode_bvp2bvp(prob, segname, run_names{i}, lab);
        [data1, uidx1] = coco_get_func_data(prob, coco_get_id(segname, 'bvp.seg1.coll'), 'data', 'uidx');
        maps1 = data1.coll_seg.maps;
        prob = coco_add_pars(prob, x10_func_name, uidx1(maps1.x0_idx(1)), x10_par_name, 'inactive');
        
        % Use UQ helper functions to manually set up a
        % sample
        bvp_id = coco_get_id(segname, 'bvp');
        bc_data = coco_get_func_data(prob, bvp_id, 'data');
        uq_data = bvp_uq_init_data(prob, bc_data, segname);
        
        args.spnames = {'al.1'};
        args.spdists = {'Uniform'};
        args.spdp = [0.01, 0.6];
        args.dpdtpars = {};
        opts.addadjt = 0;
        uq_data = uq_init_data(prob, uq_data, args, opts);
        
%         % adjoints
%         prob = adjt_isol2bvp(prob, segname);
%         dp1_names = strcat(coco_get_id('d', pnames), '.1');
%         [adata, axidx] = coco_get_adjt_data(prob, collname, 'data', 'axidx');
%         opt = adata.coll_opt;
%         dx10_func_name = coco_get_id('d', x10_par_name);
%         prob = coco_add_adjt(prob, x10_func_name, dx10_func_name, 'aidx', axidx(opt.x0_idx(1)));
        
      case 3
        sol = coll_read_solution(coco_get_id(segname, 'bvp.seg1'), run_names{i}, lab);

        coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
          @nla_dxdp, @nla_dpdp, sol.tbp, sol.xbp, sol.p};

        % 'bvp'-toolbox arguments
        bvp_args = {@nla_bc_v0, @nla_bc_v0_du, @nla_bc_v0_dudu};
        prob = ode_isol2bvp(prob, segname, coll_args{:}, {}, bvp_args{:});

        segname1 = strcat('orb', int2str(i-2));
        collname1 = coco_get_id(segname1, 'bvp.seg1.coll');
        [data_seg_1, uidx_seg_1] = coco_get_func_data(prob, collname1, 'data', 'uidx');
        maps_seg_1 = data_seg_1.coll_seg.maps;
        [data_seg_3, uidx_seg_3] = coco_get_func_data(prob, collname, 'data', 'uidx');
        maps_seg_3 = data_seg_3.coll_seg.maps;

        prob = coco_add_func(prob, 'vdiff', ...
          @udiff, @udiff_dU, @udiff_dUdU, [], 'inactive', ...
          'vdiff', 'uidx', ...
          [uidx_seg_3(maps_seg_3.x0_idx(1)), ...
           uidx_seg_1(maps_seg_1.x0_idx(1))]);        

        % Give freq a name
        freq_name = strcat('Om', int2str(i));
        freq_par  = coco_get_id('Om', int2str(i));
        dfreq_par = coco_get_id('d', freq_par);
        prob = coco_add_pars(prob, freq_name, ...
          uidx_seg_3(maps_seg_3.p_idx(end)), freq_par, 'inactive');

        glue_name = strcat('par', int2str(i-2), int2str(i), '_glue');
        prob = coco_add_glue(prob, glue_name, ...
          uidx_seg_3(maps_seg_3.p_idx(1:6)), ...
          uidx_seg_1(maps_seg_1.p_idx(1:6)));

        prob = coco_add_pars(prob, x10_func_name, ...
          uidx_seg_3(maps_seg_3.x0_idx(1)), ...
          x10_par_name, 'inactive');

%         % adjoints
%         prob = adjt_isol2bvp(prob, segname);
%         [adata_seg_1, axidx_seg_1] = coco_get_adjt_data(prob, collname1, 'data', 'axidx');
%         opt_seg_1 = adata_seg_1.coll_opt;
%         [adata_seg_3, axidx_seg_3] = coco_get_adjt_data(prob, collname, 'data', 'axidx');
%         opt_seg_3 = adata_seg_3.coll_opt;        
% 
%         prob = coco_add_adjt(prob, 'vdiff', ...
%           'd.vdiff', 'aidx', ...
%           [axidx_seg_3(opt_seg_3.x0_idx(1)), ...
%            axidx_seg_1(opt_seg_1.x0_idx(1))]);
% 
%         prob = coco_add_adjt(prob, freq_name, dfreq_par, 'aidx', ...
%           axidx_seg_3(opt_seg_3.p_idx(end)));
%         
%         prob = coco_add_adjt(prob, glue_name, 'aidx', ...
%           [axidx_seg_3(opt_seg_3.p_idx(1:6)), ...
%            axidx_seg_1(opt_seg_1.p_idx(1:6))]);
%         
%         dx10_par_name = coco_get_id('d', x10_par_name);
%         prob = coco_add_adjt(prob, x10_func_name, ...
%           dx10_par_name, 'aidx', ...
%           axidx_seg_3(opt_seg_3.x0_idx(1)));        
      otherwise
        prob = ode_bvp2bvp(prob, segname, run_names{i}, lab);
        [data2, uidx2] = coco_get_func_data(prob, coco_get_id(segname, 'bvp.seg1.coll'), 'data', 'uidx');
        maps2 = data2.coll_seg.maps;

        segname1 = strcat('orb', int2str(i-1));
        collname1 = coco_get_id(segname1, 'bvp.seg1.coll');
        [data1, uidx1] = coco_get_func_data(prob,  collname1, 'data', 'uidx');
        maps1 = data1.coll_seg.maps;

        % Glue Parameters together (except forcing frequency)
        glue_name = strcat('par', int2str(i-1), int2str(i), '_glue');
        prob = coco_add_glue(prob, glue_name, uidx2([maps2.x0_idx(1);maps2.p_idx(1:6)]), ...
          uidx1([maps1.x0_idx(1);maps1.p_idx(1:6)]));
        % Lock in delta between frequencies on either side
        % of the fold.
        om_diff_func_name = strcat('Om_diff', int2str(i-1), int2str(i));
        om_diff_par_name  = coco_get_id('Om_diff', int2str(i-1), int2str(i));
        prob = coco_add_func(prob, om_diff_func_name, ...
          @udiff, @udiff_dU, @udiff_dUdU, [], 'inactive', ...
          om_diff_par_name, 'uidx', ...
          [uidx2(maps2.p_idx(end)),uidx1(maps1.p_idx(end))]);

%         % adjoints
%         prob = adjt_isol2bvp(prob, segname);
%         [adata2, axidx2] = coco_get_adjt_data(prob, collname, 'data', 'axidx');
%         opt2 = adata2.coll_opt;
% 
%         [adata1, axidx1] = coco_get_adjt_data(prob, collname1, 'data', 'axidx');
%         opt1 = adata1.coll_opt;
% 
%         prob = coco_add_adjt(prob, glue_name, 'aidx', ...
%           [axidx2([opt2.x0_idx(1);opt2.p_idx(1:6)]), ...
%            axidx1([opt1.x0_idx(1);opt1.p_idx(1:6)])]);
% 
%         dom_diff_par_name = coco_get_id('d', om_diff_par_name);
%         prob = coco_add_adjt(prob, om_diff_func_name, ...
%           dom_diff_par_name, 'aidx', ...
%           [axidx2(opt2.p_idx(end)),axidx1(opt1.p_idx(end))]);
    end
  end
  prob = coco_add_event(prob, 'UQ', 'al.1', uq_data.nds);
  bd = coco(prob, run_name, [], 1, {'al.1', 'vdiff', 'x0.1', 'x0.3', 'Om.1', 'Om.3'}, [0.05, 0.56]);
%   bd = coco(prob, run_name, [], 1, {'vdiff', 'b.1', 'd.vdiff', 'x0.1', 'x0.3', 'Om.1', 'Om.3' dp1_names{[1:4,6]}, 'd.Om_diff.1.2', 'd.Om_diff.3.4'});
else
  bd = coco_bd_read(run_name);
end

last_run_name = run_name;
run_name = 'uq_sweep';
if not(coco_exist(run_name, 'run')) || true
  prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
  prob = coco_set(prob, 'cont', 'PtMX', 100);
  prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10);
  prob = coco_set(prob, 'cont', 'h_max', 1000);
  
  labs = coco_bd_labs(bd, 'UQ');
  j=0;
  x10_par_names = {};
  al_par_names = {};
  om_par_names = {};
  vdiff_func_names = {};
  s_idx = cell(1, uq_data.nsamples);
  for lab=labs
    j=j+1;
  for i=1:4
    segname = strcat('orb', int2str(i));
    sample_id = ['sample', int2str(j)];
    samplename = coco_get_id('uq', segname, sample_id);
    collname = coco_get_id(samplename, 'bvp.seg1.coll');

    x10_func_name = strcat('x0', int2str(i), sample_id);
    x10_par_name  = coco_get_id('x0', int2str(i), ['s', int2str(j)]);

    chart = coco_read_solution(last_run_name, lab, 'chart');
    cdata = coco_get_chart_data(chart, 'lsol');

    switch i
      case 1
        if j == 1
          sol = coll_read_solution(coco_get_id(segname, 'bvp.seg1'), last_run_name, lab);

          coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
            @nla_dxdp, @nla_dpdp, sol.tbp, sol.xbp, sol.p};
          
          % 'bvp'-toolbox arguments
          bvp_args = {@nla_bc_v0, @nla_bc_v0_du, @nla_bc_v0_dudu};
          prob = ode_isol2bvp(prob, samplename, coll_args{:}, {}, bvp_args{:});
          
          
          sample_id1 = ['sample', int2str(j)];
          samplename1 = coco_get_id('uq', segname, sample_id1);
          collname1 = coco_get_id(samplename1, 'bvp.seg1.coll');

          [data_seg_1, uidx_seg_1] = coco_get_func_data(prob, collname1, 'data', 'uidx');
          maps_seg_1 = data_seg_1.coll_seg.maps;
          s_idx{j} = uidx_seg_1(maps_seg_1.p_idx);
          prob = coco_add_pars(prob, ...
            coco_get_id(collname1, 'pars'), ...
            uidx_seg_1(maps_seg_1.p_idx([1:5,7])), ...
            {pnames{1:5}, 'Om.1.s.1'}, 'inactive');
        else
          sol = coll_read_solution(coco_get_id(segname, 'bvp.seg1'), last_run_name, lab);

          coll_args = {@nla, @nla_dx, @nla_dp, @nla_dxdx, ...
            @nla_dxdp, @nla_dpdp, sol.tbp, sol.xbp, sol.p};
          
          % 'bvp'-toolbox arguments
          bvp_args = {@nla_bc_v0, @nla_bc_v0_du, @nla_bc_v0_dudu};
          prob = ode_isol2bvp(prob, samplename, coll_args{:}, {}, bvp_args{:});

          [data_seg_1j, uidx_seg_1j] = coco_get_func_data(prob, collname, 'data', 'uidx');
          maps_seg_1j = data_seg_1j.coll_seg.maps;
          s_idx{j} = uidx_seg_1j(maps_seg_1j.p_idx);
          
          glue_name = ['uq.orb1.sample',int2str(j),'.to.sample1'];
          prob = coco_add_glue(prob, glue_name, ...
            uidx_seg_1j([maps_seg_1j.p_idx([1:5])]), ...
            uidx_seg_1([maps_seg_1.p_idx([1:5])]));
          
          om_func_name = coco_get_id('Om', ['sample',int2str(j)]);
          
          om_par_name = coco_get_id('Om', int2str(i),'s',int2str(j));
          om_par_names = {om_par_names{:}, om_par_name};
          prob = coco_add_pars(prob, om_func_name, ...
            uidx_seg_1j([maps_seg_1j.p_idx(6)]), ...
            {om_par_name}, 'inactive');
        end

%         [data1, uidx1] = coco_get_func_data(prob, coco_get_id(samplename, 'bvp.seg1.coll'), 'data', 'uidx');
%         maps1 = data1.coll_seg.maps;
%         x10_par_names = {x10_par_names{:}, x10_par_name};
%         prob = coco_add_pars(prob, x10_func_name, uidx1(maps1.x0_idx(1)), x10_par_name, 'inactive');
        
%         % adjoints
%         prob = adjt_BP2bvp(prob, segname, last_run_name, lab);
%         dp1_names = strcat(coco_get_id('d', pnames), '.1');
%         [adata, axidx] = coco_get_adjt_data(prob, collname, 'data', 'axidx');
%         opt = adata.coll_opt;
%         dx10_func_name = coco_get_id('d', x10_par_name);
% 
%         prob = coco_add_adjt(prob, x10_func_name, ...
%           dx10_func_name, 'aidx', axidx(opt.x0_idx(1)));

      case 3
        prob = ode_bvp2bvp(prob, samplename, last_run_name, segname, lab);

        segname1 = strcat('orb', int2str(i-2));
        samplename1 = coco_get_id('uq', segname1, sample_id);
        collname1 = coco_get_id(samplename1, 'bvp.seg1.coll');
        [data_seg_1, uidx_seg_1] = coco_get_func_data(prob, collname1, 'data', 'uidx');
        maps_seg_1 = data_seg_1.coll_seg.maps;

        [data_seg_3, uidx_seg_3] = coco_get_func_data(prob, collname, 'data', 'uidx');
        maps_seg_3 = data_seg_3.coll_seg.maps;
        
        vdiff_func_name = strcat('vdiff.sample', int2str(j));
        vdiff_par_name = coco_get_id('vdiff', int2str(j));
        vdiff_func_names = {vdiff_func_names{:}, vdiff_func_name};
        prob = coco_add_func(prob, vdiff_func_name, ...
          @udiff, @udiff_dU, @udiff_dUdU, [], 'inactive', ...
          vdiff_par_name, 'uidx', ...
          [uidx_seg_3(maps_seg_3.x0_idx(1)), ...
           uidx_seg_1(maps_seg_1.x0_idx(1))]);        

        % Give freq a name
        freq_name = strcat('Om', int2str(i), '.sample', int2str(j));
        freq_par  = coco_get_id('Om', int2str(i), 's', int2str(j));

        prob = coco_add_pars(prob, freq_name, ...
          uidx_seg_3(maps_seg_3.p_idx(end)), freq_par, 'inactive');

        glue_name = strcat('par', int2str(i-2), int2str(i), '_glue.sample', int2str(j));
        prob = coco_add_glue(prob, glue_name, ...
          uidx_seg_3(maps_seg_3.p_idx(1:6)), ...
          uidx_seg_1(maps_seg_1.p_idx(1:6)));

%         prob = coco_add_pars(prob, x10_func_name, ...
%           uidx_seg_3(maps_seg_3.x0_idx(1)), ...
%           x10_par_name, 'inactive');
% 
%         % adjoints
%         prob = adjt_BP2bvp(prob, segname, last_run_name, lab);
%         [adata_seg_1, axidx_seg_1] = coco_get_adjt_data(prob, collname1, 'data', 'axidx');
%         opt_seg_1 = adata_seg_1.coll_opt;
%         [adata_seg_3, axidx_seg_3] = coco_get_adjt_data(prob, collname, 'data', 'axidx');
%         opt_seg_3 = adata_seg_3.coll_opt;        
% 
%         prob = coco_add_adjt(prob, 'vdiff', ...
%           'd.vdiff', 'aidx', ...
%           [axidx_seg_3(opt_seg_3.x0_idx(1)), ...
%            axidx_seg_1(opt_seg_1.x0_idx(1))]);
%         dfreq_par = coco_get_id('d', freq_par);
%         prob = coco_add_adjt(prob, freq_name, dfreq_par, 'aidx', ...
%           axidx_seg_3(opt_seg_3.p_idx(end)));
%         
%         prob = coco_add_adjt(prob, glue_name, 'aidx', ...
%           [axidx_seg_3(opt_seg_3.p_idx(1:6)), ...
%            axidx_seg_1(opt_seg_1.p_idx(1:6))]);
%         
%         dx10_par_name = coco_get_id('d', x10_par_name);
%         prob = coco_add_adjt(prob, x10_func_name, ...
%           dx10_par_name, 'aidx', ...
%           axidx_seg_3(opt_seg_3.x0_idx(1)));        
      otherwise
        prob = ode_bvp2bvp(prob, samplename, last_run_name, segname, lab);

        [data2, uidx2] = coco_get_func_data(prob, coco_get_id(samplename, 'bvp.seg1.coll'), 'data', 'uidx');
        maps2 = data2.coll_seg.maps;

        segname1 = strcat('orb', int2str(i-1));
        samplename1 = coco_get_id('uq', segname1, sample_id);
        collname1 = coco_get_id(samplename1, 'bvp.seg1.coll');

        [data1, uidx1] = coco_get_func_data(prob,  collname1, 'data', 'uidx');
        maps1 = data1.coll_seg.maps;

        % Glue Parameters (except forcing frequency) and
        % Amplitude together
        glue_name = strcat('par', int2str(i-1), int2str(i), '_glue.sample', int2str(j));
        prob = coco_add_glue(prob, glue_name, uidx2([maps2.x0_idx(1);maps2.p_idx(1:6)]), ...
          uidx1([maps1.x0_idx(1);maps1.p_idx(1:6)]));
        % Lock in delta between frequencies on either side
        % of the fold.
        om_diff_func_name = strcat('Om_diff', ...
          int2str(i-1), int2str(i), '.sample', int2str(j));
        om_diff_par_name  = coco_get_id(['Om_diff', ...
          int2str(i-1), int2str(i)], 's', int2str(j));
        prob = coco_add_func(prob, om_diff_func_name, ...
          @udiff, @udiff_dU, @udiff_dUdU, [], 'inactive', ...
          om_diff_par_name, 'uidx', ...
          [uidx2(maps2.p_idx(end)),uidx1(maps1.p_idx(end))]);

%         % adjoints
%         prob = adjt_BP2bvp(prob, segname, last_run_name, lab);
%         [adata2, axidx2] = coco_get_adjt_data(prob, collname, 'data', 'axidx');
%         opt2 = adata2.coll_opt;
% 
%         [adata1, axidx1] = coco_get_adjt_data(prob, collname1, 'data', 'axidx');
%         opt1 = adata1.coll_opt;
% 
%         prob = coco_add_adjt(prob, glue_name, 'aidx', ...
%           [axidx2([opt2.x0_idx(1);opt2.p_idx(1:6)]), ...
%            axidx1([opt1.x0_idx(1);opt1.p_idx(1:6)])]);
% 
%         dom_diff_par_name = coco_get_id('d', om_diff_par_name);
%         prob = coco_add_adjt(prob, om_diff_func_name, ...
%           dom_diff_par_name, 'aidx', ...
%           [axidx2(opt2.p_idx(end)),axidx1(opt1.p_idx(end))]);
    end
  end
  end

  psi_mat = uq_make_psi_mat(uq_data.nds_grid, uq_data.uq.Pt, uq_data.spdists);
  uq_data.wtd_psi_mat = psi_mat*diag(uq_data.wts);
  uq_data.s_idx = s_idx;  
  prob = uq_add_sample_nodes(prob, uq_data);
  
  % Response and PCE Evaluation
  i = 1;
  r_ig = zeros(uq_data.nsamples, 1);
  r_idx = zeros(2, uq_data.nsamples);
  for vd=vdiff_func_names
    [u0, uidx] = coco_get_func_data(prob, vd, 'u0', 'uidx');
    r_ig(i) = u0(1) - u0(2);
    r_idx(:,i) = uidx;
    i = i + 1;
  end
  alpha_ig = uq_data.wtd_psi_mat*r_ig;
  response_id = 'resp.vdiff';
  prob = coco_add_func(prob, response_id, ...
    @resp_pce, uq_data, 'zero', ...
    'uidx', r_idx, 'u0', alpha_ig);

  alpha_idx = coco_get_func_data(prob, response_id, 'uidx');

  % Grab the last Nt parameters. This will only work for single output
  % response function.  Leaving it for now, would like to generalize this to
  % multiple output response functions.
  alpha_idx = alpha_idx(end-uq_data.Nt+1:end);

  % Calculate statistics from the PCE
  % Add zero functions for the mean and variance along with
  % inactive continuation parameters for tracking their
  % values.

  % Mean Zero Function
  mean_id = coco_get_id(response_id, 'pce_mean');
  prob = coco_add_func(prob, mean_id, ...
    @uq_pce_mean, @uq_pce_mean_dU, @uq_pce_mean_dUdU, ...
    uq_data, 'zero', 'uidx', alpha_idx, 'u0', alpha_ig(1));

  % Mean Continuation Parameter
  mean_par_id = coco_get_id(response_id, 'mean');
  mean_idx = coco_get_func_data(prob, mean_id, 'uidx');
  mean_name = coco_get_id(uq_data.oid, response_id, 'mean');
  prob = coco_add_pars(prob, mean_par_id, mean_idx(end), mean_name);

  % Variance Zero Function
  var_id = coco_get_id(response_id, 'pce_variance');
  prob = coco_add_func(prob, var_id, ...
    @uq_pce_variance, @uq_pce_variance_dU, @uq_pce_variance_dUdU,...
    uq_data, 'zero', 'uidx', alpha_idx, 'u0', sum(alpha_ig(2:end).^2));

  % Variance Parameter
  var_par_id = coco_get_id(response_id, 'variance');
  var_idx = coco_get_func_data(prob, var_id, 'uidx');
  var_name = coco_get_id(uq_data.oid, response_id, 'var');
  prob = coco_add_pars(prob, var_par_id, var_idx(end), var_name);  
  
  om_par_names = {'Om.1.s.1', om_par_names{:}};
  om_3_par_names = uq_get_sample_par_names('Om.3.s', 1:4);
  vdiffnames = uq_get_sample_par_names('vdiff',1:4);
  
  bd = coco(prob, run_name, [], 1, {'orb1.resp.vdiff.mean', 'up.al.1', om_par_names{:}, om_3_par_names{:}, vdiffnames{:}, 'orb1.resp.vdiff.var', 'al.1.1', 'al.1.2', 'al.1.3', 'al.1.4'});
%   bd = coco(prob, run_name, [], 1, {'d.vdiff', 'b.1', 'vdiff', 'x0.1', 'x0.3', 'Om.1', 'Om.3', dp1_names{[1:4,6]}, 'd.Om_diff.1.2', 'd.Om_diff.3.4'}, [0,1]);
else
  bd = coco_bd_read(run_name);
end

% last_run_name = run_name;
% run_name = 'd.vdiff.to.1';
% if not(coco_exist(run_name, 'run'))
%   prob = coco_set(prob_init, 'coll', 'NTST', 15, 'NCOL', 4);
%   prob = coco_set(prob, 'cont', 'PtMX', 100);
%   prob = coco_set(prob, 'cont', 'NAdapt', 0, 'NPR', 10);
%   prob = coco_set(prob, 'cont', 'h_max', 1000);
%   
%   lab = coco_bd_labs(bd, 'BP');
%   
%   i = 1;
%   for i=1:4
%     segname = strcat('orb', int2str(i));
%     collname = coco_get_id(segname, 'bvp.seg1.coll');
% 
%     x10_func_name = strcat('x0', int2str(i));
%     x10_par_name  = coco_get_id('x0', int2str(i));
% 
%     chart = coco_read_solution(last_run_name, lab, 'chart');
%     cdata = coco_get_chart_data(chart, 'lsol');
% 
%     switch i
%       case 1
%         prob = ode_BP2bvp(prob, segname, last_run_name, lab);
%         [data1, uidx1] = coco_get_func_data(prob, coco_get_id(segname, 'bvp.seg1.coll'), 'data', 'uidx');
%         maps1 = data1.coll_seg.maps;
%         prob = coco_add_pars(prob, x10_func_name, uidx1(maps1.x0_idx(1)), x10_par_name, 'inactive');
%         
%         % adjoints
%         prob = adjt_BP2bvp(prob, segname, last_run_name, lab);
%         dp1_names = strcat(coco_get_id('d', pnames), '.1');
%         [adata, axidx] = coco_get_adjt_data(prob, collname, 'data', 'axidx');
%         opt = adata.coll_opt;
%         dx10_func_name = coco_get_id('d', x10_par_name);
% 
%         prob = coco_add_adjt(prob, x10_func_name, ...
%           dx10_func_name, 'aidx', axidx(opt.x0_idx(1)));
% 
%       case 3
%         prob = ode_BP2bvp(prob, segname, last_run_name, lab);
% 
%         segname1 = strcat('orb', int2str(i-2));
%         collname1 = coco_get_id(segname1, 'bvp.seg1.coll');
%         [data_seg_1, uidx_seg_1] = coco_get_func_data(prob, collname1, 'data', 'uidx');
%         maps_seg_1 = data_seg_1.coll_seg.maps;
%         [data_seg_3, uidx_seg_3] = coco_get_func_data(prob, collname, 'data', 'uidx');
%         maps_seg_3 = data_seg_3.coll_seg.maps;
% 
%         prob = coco_add_func(prob, 'vdiff', ...
%           @udiff, @udiff_dU, @udiff_dUdU, [], 'inactive', ...
%           'vdiff', 'uidx', ...
%           [uidx_seg_3(maps_seg_3.x0_idx(1)), ...
%            uidx_seg_1(maps_seg_1.x0_idx(1))]);        
% 
%         % Give freq a name
%         freq_name = strcat('Om', int2str(i));
%         freq_par  = coco_get_id('Om', int2str(i));
%         dfreq_par = coco_get_id('d', freq_par);
%         prob = coco_add_pars(prob, freq_name, ...
%           uidx_seg_3(maps_seg_3.p_idx(end)), freq_par, 'inactive');
% 
%         glue_name = strcat('par', int2str(i-2), int2str(i), '_glue');
%         prob = coco_add_glue(prob, glue_name, ...
%           uidx_seg_3(maps_seg_3.p_idx(1:6)), ...
%           uidx_seg_1(maps_seg_1.p_idx(1:6)));
% 
%         prob = coco_add_pars(prob, x10_func_name, ...
%           uidx_seg_3(maps_seg_3.x0_idx(1)), ...
%           x10_par_name, 'inactive');
% 
%         % adjoints
%         prob = adjt_BP2bvp(prob, segname, last_run_name, lab);
%         [adata_seg_1, axidx_seg_1] = coco_get_adjt_data(prob, collname1, 'data', 'axidx');
%         opt_seg_1 = adata_seg_1.coll_opt;
%         [adata_seg_3, axidx_seg_3] = coco_get_adjt_data(prob, collname, 'data', 'axidx');
%         opt_seg_3 = adata_seg_3.coll_opt;        
% 
%         prob = coco_add_adjt(prob, 'vdiff', ...
%           'd.vdiff', 'aidx', ...
%           [axidx_seg_3(opt_seg_3.x0_idx(1)), ...
%            axidx_seg_1(opt_seg_1.x0_idx(1))]);
% 
%         prob = coco_add_adjt(prob, freq_name, dfreq_par, 'aidx', ...
%           axidx_seg_3(opt_seg_3.p_idx(end)));
%         
%         prob = coco_add_adjt(prob, glue_name, 'aidx', ...
%           [axidx_seg_3(opt_seg_3.p_idx(1:6)), ...
%            axidx_seg_1(opt_seg_1.p_idx(1:6))]);
%         
%         dx10_par_name = coco_get_id('d', x10_par_name);
%         prob = coco_add_adjt(prob, x10_func_name, ...
%           dx10_par_name, 'aidx', ...
%           axidx_seg_3(opt_seg_3.x0_idx(1)));        
%       otherwise
%         prob = ode_BP2bvp(prob, segname, last_run_name, lab);
%         [data2, uidx2] = coco_get_func_data(prob, coco_get_id(segname, 'bvp.seg1.coll'), 'data', 'uidx');
%         maps2 = data2.coll_seg.maps;
% 
%         segname1 = strcat('orb', int2str(i-1));
%         collname1 = coco_get_id(segname1, 'bvp.seg1.coll');
%         [data1, uidx1] = coco_get_func_data(prob,  collname1, 'data', 'uidx');
%         maps1 = data1.coll_seg.maps;
% 
%         % Glue Parameters together (except forcing frequency)
%         glue_name = strcat('par', int2str(i-1), int2str(i), '_glue');
%         prob = coco_add_glue(prob, glue_name, uidx2([maps2.x0_idx(1);maps2.p_idx(1:6)]), ...
%           uidx1([maps1.x0_idx(1);maps1.p_idx(1:6)]));
%         % Lock in delta between frequencies on either side
%         % of the fold.
%         om_diff_func_name = strcat('Om_diff', int2str(i-1), int2str(i));
%         om_diff_par_name  = coco_get_id('Om_diff', int2str(i-1), int2str(i));
%         prob = coco_add_func(prob, om_diff_func_name, ...
%           @udiff, @udiff_dU, @udiff_dUdU, [], 'inactive', ...
%           om_diff_par_name, 'uidx', ...
%           [uidx2(maps2.p_idx(end)),uidx1(maps1.p_idx(end))]);
% 
%         % adjoints
%         prob = adjt_BP2bvp(prob, segname, last_run_name, lab);
%         [adata2, axidx2] = coco_get_adjt_data(prob, collname, 'data', 'axidx');
%         opt2 = adata2.coll_opt;
% 
%         [adata1, axidx1] = coco_get_adjt_data(prob, collname1, 'data', 'axidx');
%         opt1 = adata1.coll_opt;
% 
%         prob = coco_add_adjt(prob, glue_name, 'aidx', ...
%           [axidx2([opt2.x0_idx(1);opt2.p_idx(1:6)]), ...
%            axidx1([opt1.x0_idx(1);opt1.p_idx(1:6)])]);
% 
%         dom_diff_par_name = coco_get_id('d', om_diff_par_name);
%         prob = coco_add_adjt(prob, om_diff_func_name, ...
%           dom_diff_par_name, 'aidx', ...
%           [axidx2(opt2.p_idx(end)),axidx1(opt1.p_idx(end))]);
%     end
%     i=i+1;
%   end
%   bd = coco(prob, run_name, [], 1, {'d.vdiff', 'b.1', 'vdiff', 'x0.1', 'x0.3', 'Om.1', 'Om.3', dp1_names{[1:4,6]}, 'd.Om_diff.1.2', 'd.Om_diff.3.4'}, [0,1]);
% else
%   bd = coco_bd_read(run_name);
% end