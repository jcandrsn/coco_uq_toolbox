function [data, J] = resp_pce_dU(prob, data, u)
% UQ_PCE_COEFFICIENTS_DU Jacobian of the Polynomial Chaos
% Expansion (PCE) coefficient Evaluation function

x = reshape(u(1:end-data.Nt), [], data.nsamples);

s = repmat([ones(data.nsamples,1), ...
  repmat(sign(x(2,:) - x(3,:))', 1,2)], 1, data.nsamples);
dr = -data.wtd_psi_mat*(s.*kron(eye(data.nsamples),[1, -5, 5]));

J = [dr, ...           % dr/du
     speye(data.Nt)];  % alphas are linear
   
%% Test Jacobian with following sequence:
% [data, Jd] = coco_ezDFDX('f(o,d,x)', prob, data, @resp_pce, u);
% diff_J = abs(J-Jd);
% max(max(diff_J))
% fprintf('test')
end