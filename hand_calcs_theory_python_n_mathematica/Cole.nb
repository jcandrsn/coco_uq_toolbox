(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     37940,       1161]
NotebookOptionsPosition[     35943,       1086]
NotebookOutlinePosition[     36298,       1102]
CellTagsIndexPosition[     36255,       1099]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"LaplaceTransform", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"x1", "'"}], "[", "t", "]"}], "==", 
      RowBox[{"x2", "[", "t", "]"}]}], ",", 
     RowBox[{
      RowBox[{
       RowBox[{"x2", "'"}], "[", "t", "]"}], "\[Equal]", 
      RowBox[{
       RowBox[{"-", 
        RowBox[{"x2", "[", "t", "]"}]}], "-", 
       RowBox[{"k", " ", 
        RowBox[{"x1", "[", "t", "]"}]}], "+", 
       RowBox[{"i", "[", "t", "]"}]}]}]}], "}"}], ",", "t", ",", "s"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.698510245844597*^9, 3.698510280542231*^9}, {
  3.698510823701652*^9, 3.698510825308506*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"s", " ", 
      RowBox[{"LaplaceTransform", "[", 
       RowBox[{
        RowBox[{"x1", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}]}], "-", 
     RowBox[{"x1", "[", "0", "]"}]}], "\[Equal]", 
    RowBox[{"LaplaceTransform", "[", 
     RowBox[{
      RowBox[{"x2", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}]}], ",", 
   RowBox[{
    RowBox[{
     RowBox[{"s", " ", 
      RowBox[{"LaplaceTransform", "[", 
       RowBox[{
        RowBox[{"x2", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}]}], "-", 
     RowBox[{"x2", "[", "0", "]"}]}], "\[Equal]", 
    RowBox[{
     RowBox[{"LaplaceTransform", "[", 
      RowBox[{
       RowBox[{"i", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}], "-", 
     RowBox[{"k", " ", 
      RowBox[{"LaplaceTransform", "[", 
       RowBox[{
        RowBox[{"x1", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}]}], "-", 
     RowBox[{"LaplaceTransform", "[", 
      RowBox[{
       RowBox[{"x2", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}]}]}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.698510281314377*^9, 3.698510825971846*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{"%", ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"LaplaceTransform", "[", 
      RowBox[{
       RowBox[{"x1", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}], ",", 
     RowBox[{"LaplaceTransform", "[", 
      RowBox[{
       RowBox[{"x2", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}]}], "}"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.6985102822931747`*^9, 3.698510293385322*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{
     RowBox[{"LaplaceTransform", "[", 
      RowBox[{
       RowBox[{"x1", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}], "\[Rule]", 
     
     RowBox[{"-", 
      FractionBox[
       RowBox[{
        RowBox[{"-", 
         RowBox[{"LaplaceTransform", "[", 
          RowBox[{
           RowBox[{"i", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}]}], "-", 
        RowBox[{"x1", "[", "0", "]"}], "-", 
        RowBox[{"s", " ", 
         RowBox[{"x1", "[", "0", "]"}]}], "-", 
        RowBox[{"x2", "[", "0", "]"}]}], 
       RowBox[{"k", "+", "s", "+", 
        SuperscriptBox["s", "2"]}]]}]}], ",", 
    RowBox[{
     RowBox[{"LaplaceTransform", "[", 
      RowBox[{
       RowBox[{"x2", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}], "\[Rule]", 
     
     RowBox[{"-", 
      FractionBox[
       RowBox[{
        RowBox[{
         RowBox[{"-", "s"}], " ", 
         RowBox[{"LaplaceTransform", "[", 
          RowBox[{
           RowBox[{"i", "[", "t", "]"}], ",", "t", ",", "s"}], "]"}]}], "+", 
        RowBox[{"k", " ", 
         RowBox[{"x1", "[", "0", "]"}]}], "-", 
        RowBox[{"s", " ", 
         RowBox[{"x2", "[", "0", "]"}]}]}], 
       RowBox[{"k", "+", "s", "+", 
        SuperscriptBox["s", "2"]}]]}]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.698510293796464*^9, 3.698510827702176*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     FractionBox["s", 
      RowBox[{"k", "+", "s", "+", 
       SuperscriptBox["s", "2"]}]], "/.", 
     RowBox[{"s", "\[Rule]", "\[ImaginaryI]"}]}], ")"}], 
   RowBox[{"Exp", "[", 
    RowBox[{"\[ImaginaryI]", 
     RowBox[{"(", 
      RowBox[{"t", "+", "\[Phi]"}], ")"}]}], "]"}]}], "//", 
  "ComplexExpand"}]], "Input",
 CellChangeTimes->{{3.698510307673194*^9, 3.698510340511284*^9}, {
   3.6985104125834312`*^9, 3.69851041346163*^9}, {3.69851046277678*^9, 
   3.698510472952776*^9}, {3.69851051672504*^9, 3.6985105168993483`*^9}, 
   3.6985105471735067`*^9, 3.6985106654460297`*^9, {3.69851072623254*^9, 
   3.698510726302195*^9}, {3.698510836360125*^9, 3.698510844077292*^9}, {
   3.6985108839987497`*^9, 3.698510884055128*^9}, {3.698510942155039*^9, 
   3.698510965328397*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{"Cos", "[", 
    RowBox[{"t", "+", "\[Phi]"}], "]"}], 
   RowBox[{"1", "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]], "+", 
  FractionBox[
   RowBox[{"Sin", "[", 
    RowBox[{"t", "+", "\[Phi]"}], "]"}], 
   RowBox[{"1", "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]], "-", 
  FractionBox[
   RowBox[{"k", " ", 
    RowBox[{"Sin", "[", 
     RowBox[{"t", "+", "\[Phi]"}], "]"}]}], 
   RowBox[{"1", "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]], "+", 
  RowBox[{"\[ImaginaryI]", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", 
      FractionBox[
       RowBox[{"Cos", "[", 
        RowBox[{"t", "+", "\[Phi]"}], "]"}], 
       RowBox[{"1", "+", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]]}], "+", 
     FractionBox[
      RowBox[{"k", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{"t", "+", "\[Phi]"}], "]"}]}], 
      RowBox[{"1", "+", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]], "+", 
     FractionBox[
      RowBox[{"Sin", "[", 
       RowBox[{"t", "+", "\[Phi]"}], "]"}], 
      RowBox[{"1", "+", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]]}], 
    ")"}]}]}]], "Output",
 CellChangeTimes->{
  3.698510847428986*^9, 3.69851088433594*^9, {3.698510942469496*^9, 
   3.6985109657777*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   FractionBox[
    RowBox[{"Cos", "[", 
     RowBox[{"t", "+", "\[Phi]"}], "]"}], 
    RowBox[{"1", "+", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]], "+", 
   FractionBox[
    RowBox[{"Sin", "[", 
     RowBox[{"t", "+", "\[Phi]"}], "]"}], 
    RowBox[{"1", "+", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]], "-", 
   FractionBox[
    RowBox[{"k", " ", 
     RowBox[{"Sin", "[", 
      RowBox[{"t", "+", "\[Phi]"}], "]"}]}], 
    RowBox[{"1", "+", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]]}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.698510352890931*^9, 3.698510356626205*^9}, 
   3.698510488229141*^9, 3.698510520734687*^9, 3.6985105509929943`*^9, 
   3.698510853905521*^9, 3.6985108884350853`*^9}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"Cos", "[", 
    RowBox[{"t", "+", "\[Phi]"}], "]"}], "-", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", "k"}], ")"}], " ", 
    RowBox[{"Sin", "[", 
     RowBox[{"t", "+", "\[Phi]"}], "]"}]}]}], 
  RowBox[{"2", "-", 
   RowBox[{"2", " ", "k"}], "+", 
   SuperscriptBox["k", "2"]}]]], "Output",
 CellChangeTimes->{{3.698510353551186*^9, 3.6985103569704638`*^9}, 
   3.698510488743668*^9, 3.69851052103611*^9, 3.698510551327306*^9, 
   3.698510854340784*^9, 3.698510888825716*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"-", 
      FractionBox[
       RowBox[{"1", "+", "k"}], 
       RowBox[{"2", "+", 
        RowBox[{"2", " ", "k"}], "+", 
        SuperscriptBox["k", "2"]}]]}], 
     RowBox[{"Cos", "[", 
      RowBox[{"t", "+", "\[Phi]"}], "]"}]}], ",", "t"}], "]"}], 
  "="}]], "Input",
 CellChangeTimes->{{3.698510399244975*^9, 3.6985104067826843`*^9}, {
  3.698510445943458*^9, 3.698510447328661*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", 
   RowBox[{"1", "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"1", "+", "k"}], ")"}], "2"]}]], 
  RowBox[{"Cos", "[", 
   RowBox[{"t", "+", "\[Phi]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.698510421167679*^9, 3.6985104249674788`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{
    RowBox[{"Cos", "[", 
     RowBox[{"t", "+", "\[Phi]"}], "]"}], "-", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "k"}], ")"}], " ", 
     RowBox[{"Sin", "[", 
      RowBox[{"t", "+", "\[Phi]"}], "]"}]}]}], 
   RowBox[{"2", "-", 
    RowBox[{"2", " ", "k"}], "+", 
    SuperscriptBox["k", "2"]}]], "/.", 
  RowBox[{"t", "\[Rule]", "0"}]}]], "Input",
 CellChangeTimes->{{3.6985105809430523`*^9, 3.698510582726795*^9}, 
   3.6985109207646313`*^9}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"Cos", "[", "\[Phi]", "]"}], "-", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", "k"}], ")"}], " ", 
    RowBox[{"Sin", "[", "\[Phi]", "]"}]}]}], 
  RowBox[{"2", "-", 
   RowBox[{"2", " ", "k"}], "+", 
   SuperscriptBox["k", "2"]}]]], "Output",
 CellChangeTimes->{3.698510583118286*^9, 3.698510921147325*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       FractionBox[
        RowBox[{"Cos", "[", 
         RowBox[{"t", "+", "\[Phi]"}], "]"}], 
        RowBox[{"1", "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]], "+", 
       FractionBox[
        RowBox[{"Sin", "[", 
         RowBox[{"t", "+", "\[Phi]"}], "]"}], 
        RowBox[{"1", "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]], "-", 
       FractionBox[
        RowBox[{"k", " ", 
         RowBox[{"Sin", "[", 
          RowBox[{"t", "+", "\[Phi]"}], "]"}]}], 
        RowBox[{"1", "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]]}], "//", 
      "TrigExpand"}], ")"}], "/.", 
    RowBox[{
     RowBox[{"Sin", "[", "\[Phi]", "]"}], "\[Rule]", 
     RowBox[{"1", "/", 
      RowBox[{"Sqrt", "[", 
       RowBox[{"1", "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"k", "-", "1"}], ")"}], "^", "2"}]}], "]"}]}]}]}], "/.", 
   RowBox[{
    RowBox[{"Cos", "[", "\[Phi]", "]"}], "\[Rule]", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"k", "-", "1"}], ")"}], "/", 
     RowBox[{"Sqrt", "[", 
      RowBox[{"1", "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"k", "-", "1"}], ")"}], "^", "2"}]}], "]"}]}]}]}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.6985106694915667`*^9, 3.698510730746706*^9}, {
  3.698510945901229*^9, 3.698510969240089*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"Sin", "[", "t", "]"}], 
   SqrtBox[
    RowBox[{"2", "-", 
     RowBox[{"2", " ", "k"}], "+", 
     SuperscriptBox["k", "2"]}]]]}]], "Output",
 CellChangeTimes->{
  3.69851067104782*^9, {3.6985107042920923`*^9, 3.698510731114846*^9}, {
   3.698510955246695*^9, 3.6985109695804157`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{"1", "/", 
       SqrtBox[
        RowBox[{"1", "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "1"}], "+", "k"}], ")"}], "2"]}]]}], ",", "k"}], 
     "]"}], "\[Equal]", "0"}], ",", "k"}], "]"}]], "Input",
 CellChangeTimes->{{3.698510796682382*^9, 3.6985108033715477`*^9}, 
   3.6985109922244377`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"k", "\[Rule]", "1"}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.698510803709722*^9, 3.698510992529997*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sol", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"DSolve", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{"-", 
           RowBox[{
            RowBox[{"\[Lambda]1", "'"}], "[", "t", "]"}]}], "+", 
          RowBox[{"k", " ", 
           RowBox[{"\[Lambda]2", "[", "t", "]"}]}]}], "\[Equal]", "0"}], ",", 
        
        RowBox[{
         RowBox[{
          RowBox[{"-", 
           RowBox[{
            RowBox[{"\[Lambda]2", "'"}], "[", "t", "]"}]}], "+", 
          RowBox[{"\[Lambda]2", "[", "t", "]"}], "-", 
          RowBox[{"\[Lambda]1", "[", "t", "]"}]}], "\[Equal]", "0"}], ",", 
        RowBox[{
         RowBox[{"\[Lambda]1", "[", "0", "]"}], "==", 
         RowBox[{"\[Lambda]3", "+", "\[Eta]"}]}], ",", 
        RowBox[{
         RowBox[{"\[Lambda]2", "[", "0", "]"}], "==", 
         RowBox[{"\[Lambda]4", "+", "\[Lambda]5"}]}]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"\[Lambda]1", "[", "t", "]"}], ",", 
        RowBox[{"\[Lambda]2", "[", "t", "]"}]}], "}"}], ",", "t"}], "]"}], "//",
     "FullSimplify"}], "//", "Flatten"}]}]], "Input",
 CellChangeTimes->{{3.6985128766400137`*^9, 3.698512951115098*^9}, {
  3.6985130920640287`*^9, 3.698513107470861*^9}, {3.698542106640676*^9, 
  3.6985421085115232`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"\[Lambda]1", "[", "t", "]"}], "\[Rule]", 
    FractionBox[
     RowBox[{
      SuperscriptBox["\[ExponentialE]", 
       RowBox[{"t", "/", "2"}]], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         SqrtBox[
          RowBox[{"1", "-", 
           RowBox[{"4", " ", "k"}]}]], " ", 
         RowBox[{"(", 
          RowBox[{"\[Eta]", "+", "\[Lambda]3"}], ")"}], " ", 
         RowBox[{"Cosh", "[", 
          RowBox[{
           FractionBox["1", "2"], " ", 
           SqrtBox[
            RowBox[{"1", "-", 
             RowBox[{"4", " ", "k"}]}]], " ", "t"}], "]"}]}], "-", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"\[Eta]", "+", "\[Lambda]3", "-", 
           RowBox[{"2", " ", "k", " ", 
            RowBox[{"(", 
             RowBox[{"\[Lambda]4", "+", "\[Lambda]5"}], ")"}]}]}], ")"}], " ", 
         RowBox[{"Sinh", "[", 
          RowBox[{
           FractionBox["1", "2"], " ", 
           SqrtBox[
            RowBox[{"1", "-", 
             RowBox[{"4", " ", "k"}]}]], " ", "t"}], "]"}]}]}], ")"}]}], 
     SqrtBox[
      RowBox[{"1", "-", 
       RowBox[{"4", " ", "k"}]}]]]}], ",", 
   RowBox[{
    RowBox[{"\[Lambda]2", "[", "t", "]"}], "\[Rule]", 
    FractionBox[
     RowBox[{
      SuperscriptBox["\[ExponentialE]", 
       RowBox[{"t", "/", "2"}]], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         SqrtBox[
          RowBox[{"1", "-", 
           RowBox[{"4", " ", "k"}]}]], " ", 
         RowBox[{"(", 
          RowBox[{"\[Lambda]4", "+", "\[Lambda]5"}], ")"}], " ", 
         RowBox[{"Cosh", "[", 
          RowBox[{
           FractionBox["1", "2"], " ", 
           SqrtBox[
            RowBox[{"1", "-", 
             RowBox[{"4", " ", "k"}]}]], " ", "t"}], "]"}]}], "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{
            RowBox[{"-", "2"}], " ", "\[Eta]"}], "-", 
           RowBox[{"2", " ", "\[Lambda]3"}], "+", "\[Lambda]4", "+", 
           "\[Lambda]5"}], ")"}], " ", 
         RowBox[{"Sinh", "[", 
          RowBox[{
           FractionBox["1", "2"], " ", 
           SqrtBox[
            RowBox[{"1", "-", 
             RowBox[{"4", " ", "k"}]}]], " ", "t"}], "]"}]}]}], ")"}]}], 
     SqrtBox[
      RowBox[{"1", "-", 
       RowBox[{"4", " ", "k"}]}]]]}]}], "}"}]], "Output",
 CellChangeTimes->{{3.698512920175021*^9, 3.698512952264556*^9}, {
   3.6985131001968803`*^9, 3.698513109453755*^9}, 3.698542111417*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sub", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"Solve", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"\[Lambda]1", "[", "t", "]"}], "/.", "sol"}], "/.", 
            RowBox[{"t", "\[Rule]", 
             RowBox[{"2", "\[Pi]"}]}]}], ")"}], "-", "\[Lambda]3"}], 
         "\[Equal]", "0"}], ",", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"\[Lambda]2", "[", "t", "]"}], "/.", "sol"}], "/.", 
            RowBox[{"t", "\[Rule]", 
             RowBox[{"2", "\[Pi]"}]}]}], ")"}], "-", "\[Lambda]4"}], 
         "\[Equal]", "0"}]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"\[Lambda]3", ",", "\[Lambda]4"}], "}"}]}], "]"}], "//", 
    "FullSimplify"}], "//", "Flatten"}]}]], "Input",
 CellChangeTimes->{{3.698512954581552*^9, 3.698513001199861*^9}, {
  3.698513104992378*^9, 3.6985131292155457`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"\[Lambda]3", "\[Rule]", 
    FractionBox[
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        SuperscriptBox["\[ExponentialE]", "\[Pi]"]}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         RowBox[{"4", " ", "k"}]}], ")"}], " ", "\[Eta]"}], "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         RowBox[{"4", " ", "k"}]}], ")"}], " ", "\[Eta]", " ", 
       RowBox[{"Cosh", "[", 
        RowBox[{
         SqrtBox[
          RowBox[{"1", "-", 
           RowBox[{"4", " ", "k"}]}]], " ", "\[Pi]"}], "]"}]}], "+", 
      RowBox[{
       SqrtBox[
        RowBox[{"1", "-", 
         RowBox[{"4", " ", "k"}]}]], " ", 
       RowBox[{"(", 
        RowBox[{"\[Eta]", "-", 
         RowBox[{"2", " ", "k", " ", "\[Lambda]5"}]}], ")"}], " ", 
       RowBox[{"Sinh", "[", 
        RowBox[{
         SqrtBox[
          RowBox[{"1", "-", 
           RowBox[{"4", " ", "k"}]}]], " ", "\[Pi]"}], "]"}]}]}], 
     RowBox[{"2", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", 
        RowBox[{"4", " ", "k"}]}], ")"}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"Cosh", "[", "\[Pi]", "]"}], "-", 
        RowBox[{"Cosh", "[", 
         RowBox[{
          SqrtBox[
           RowBox[{"1", "-", 
            RowBox[{"4", " ", "k"}]}]], " ", "\[Pi]"}], "]"}]}], ")"}]}]]}], 
   ",", 
   RowBox[{"\[Lambda]4", "\[Rule]", 
    FractionBox[
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        SuperscriptBox["\[ExponentialE]", "\[Pi]"]}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         RowBox[{"4", " ", "k"}]}], ")"}], " ", "\[Lambda]5"}], "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         RowBox[{"4", " ", "k"}]}], ")"}], " ", "\[Lambda]5", " ", 
       RowBox[{"Cosh", "[", 
        RowBox[{
         SqrtBox[
          RowBox[{"1", "-", 
           RowBox[{"4", " ", "k"}]}]], " ", "\[Pi]"}], "]"}]}], "+", 
      RowBox[{
       SqrtBox[
        RowBox[{"1", "-", 
         RowBox[{"4", " ", "k"}]}]], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"2", " ", "\[Eta]"}], "-", "\[Lambda]5"}], ")"}], " ", 
       RowBox[{"Sinh", "[", 
        RowBox[{
         SqrtBox[
          RowBox[{"1", "-", 
           RowBox[{"4", " ", "k"}]}]], " ", "\[Pi]"}], "]"}]}]}], 
     RowBox[{"2", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", 
        RowBox[{"4", " ", "k"}]}], ")"}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"Cosh", "[", "\[Pi]", "]"}], "-", 
        RowBox[{"Cosh", "[", 
         RowBox[{
          SqrtBox[
           RowBox[{"1", "-", 
            RowBox[{"4", " ", "k"}]}]], " ", "\[Pi]"}], "]"}]}], ")"}]}]]}]}],
   "}"}]], "Output",
 CellChangeTimes->{{3.698512996224209*^9, 3.698513002867732*^9}, {
   3.6985131168917093`*^9, 3.6985131295965633`*^9}, 3.698542165940238*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Integrate", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"\[Lambda]2", "[", "t", "]"}], "/.", "sol"}], "/.", "sub"}], 
      ")"}], 
     FractionBox[
      RowBox[{"Cos", "[", "t", "]"}], 
      SqrtBox[
       RowBox[{"2", "-", 
        RowBox[{"2", " ", "k"}], "+", 
        SuperscriptBox["k", "2"]}]]]}], ",", 
    RowBox[{"{", 
     RowBox[{"t", ",", "0", ",", 
      RowBox[{"2", "\[Pi]"}]}], "}"}]}], "]"}], "//", 
  "FullSimplify"}]], "Input",
 CellChangeTimes->{{3.6985131314495687`*^9, 3.6985131540457897`*^9}, {
  3.6985131881726503`*^9, 3.698513215628029*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", "k"}], ")"}], " ", "\[Eta]"}], "+", 
    "\[Lambda]5"}], 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"2", "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "2"}], "+", "k"}], ")"}], " ", "k"}]}], ")"}], 
    RowBox[{"3", "/", "2"}]]]}]], "Output",
 CellChangeTimes->{{3.698513141599963*^9, 3.698513154513595*^9}, {
   3.698513208125683*^9, 3.698513222106154*^9}, 3.698542241627071*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Integrate", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"\[Lambda]2", "[", "t", "]"}], "/.", "sol"}], "/.", "sub"}], 
       ")"}], 
      RowBox[{"Sin", "[", 
       RowBox[{"t", "+", "\[Phi]"}], "]"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"t", ",", "0", ",", 
       RowBox[{"2", "\[Pi]"}]}], "}"}]}], "]"}], "/.", 
   RowBox[{"\[Lambda]5", "\[Rule]", 
    RowBox[{"\[Eta]", "-", 
     RowBox[{"k", " ", "\[Eta]"}]}]}]}], "//", "FullSimplify"}]], "Input",
 CellChangeTimes->{{3.6985132367336683`*^9, 3.698513241141652*^9}, {
   3.698541960058734*^9, 3.698541963080597*^9}, 3.698542288698227*^9}],

Cell[BoxData[
 RowBox[{"\[Eta]", " ", 
  RowBox[{"Cos", "[", "\[Phi]", "]"}]}]], "Output",
 CellChangeTimes->{3.698541907722028*^9, 3.698541971617675*^9, 
  3.6985423064021673`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Solve", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"-", 
      FractionBox[
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "1"}], "+", "k"}], ")"}], " ", "\[Eta]"}], "+", 
        "\[Lambda]5"}], 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"2", "+", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{
             RowBox[{"-", "2"}], "+", "k"}], ")"}], " ", "k"}]}], ")"}], 
        RowBox[{"3", "/", "2"}]]]}], "\[Equal]", "0"}], ",", "\[Lambda]5"}], 
   "]"}], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.69851325972742*^9, 3.698513262654182*^9}, {
   3.6985419146734324`*^9, 3.698541915847456*^9}, 3.6985422622915707`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"\[Lambda]5", "\[Rule]", 
    RowBox[{"\[Eta]", "-", 
     RowBox[{"k", " ", "\[Eta]"}]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.698513263108873*^9, 3.6985419162336617`*^9, 
  3.69854226274741*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"sub", "/.", 
      RowBox[{"k", "\[Rule]", "1"}]}], "/.", 
     RowBox[{"\[Eta]", "\[Rule]", "1"}]}], "/.", 
    RowBox[{"\[Phi]", "\[Rule]", 
     RowBox[{"\[Pi]", "/", "2"}]}]}], "/.", 
   RowBox[{"\[Lambda]5", "\[Rule]", "0"}]}], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.6985424358001623`*^9, 3.698542469750472*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"\[Lambda]3", "\[Rule]", 
    FractionBox[
     RowBox[{
      RowBox[{"3", " ", 
       SuperscriptBox["\[ExponentialE]", "\[Pi]"]}], "-", 
      RowBox[{"3", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{
         SqrtBox["3"], " ", "\[Pi]"}], "]"}]}], "+", 
      RowBox[{
       SqrtBox["3"], " ", 
       RowBox[{"Sin", "[", 
        RowBox[{
         SqrtBox["3"], " ", "\[Pi]"}], "]"}]}]}], 
     RowBox[{
      RowBox[{"6", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{
         SqrtBox["3"], " ", "\[Pi]"}], "]"}]}], "-", 
      RowBox[{"6", " ", 
       RowBox[{"Cosh", "[", "\[Pi]", "]"}]}]}]]}], ",", 
   RowBox[{"\[Lambda]4", "\[Rule]", 
    FractionBox[
     RowBox[{"Sin", "[", 
      RowBox[{
       SqrtBox["3"], " ", "\[Pi]"}], "]"}], 
     RowBox[{
      SqrtBox["3"], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"Cos", "[", 
         RowBox[{
          SqrtBox["3"], " ", "\[Pi]"}], "]"}], "-", 
        RowBox[{"Cosh", "[", "\[Pi]", "]"}]}], ")"}]}]]}]}], "}"}]], "Output",\

 CellChangeTimes->{{3.698542458655984*^9, 3.6985424701401987`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"\[Lambda]1", "[", "t", "]"}], "/.", "sol"}], "/.", "sub"}], "/.", 
      RowBox[{"k", "\[Rule]", "1"}]}], "/.", 
     RowBox[{"\[Eta]", "\[Rule]", "1"}]}], "/.", 
    RowBox[{"\[Phi]", "\[Rule]", 
     RowBox[{"\[Pi]", "/", "2"}]}]}], "/.", 
   RowBox[{"\[Lambda]5", "\[Rule]", "0"}]}], "//", "FullSimplify"}]], "Input",\

 CellChangeTimes->{{3.698542584476718*^9, 3.698542596729418*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{
     RowBox[{"-", "\[Pi]"}], "+", 
     FractionBox["t", "2"]}]], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"3", " ", 
      SuperscriptBox["\[ExponentialE]", "\[Pi]"], " ", 
      RowBox[{"Cos", "[", 
       RowBox[{
        FractionBox["1", "2"], " ", 
        SqrtBox["3"], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"2", " ", "\[Pi]"}], "-", "t"}], ")"}]}], "]"}]}], "-", 
     RowBox[{"3", " ", 
      RowBox[{"Cos", "[", 
       FractionBox[
        RowBox[{
         SqrtBox["3"], " ", "t"}], "2"], "]"}]}], "+", 
     RowBox[{
      SqrtBox["3"], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         SuperscriptBox["\[ExponentialE]", "\[Pi]"], " ", 
         RowBox[{"Sin", "[", 
          RowBox[{
           FractionBox["1", "2"], " ", 
           SqrtBox["3"], " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{"2", " ", "\[Pi]"}], "-", "t"}], ")"}]}], "]"}]}], "+", 
        RowBox[{"Sin", "[", 
         FractionBox[
          RowBox[{
           SqrtBox["3"], " ", "t"}], "2"], "]"}]}], ")"}]}]}], ")"}]}], 
  RowBox[{"6", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"Cos", "[", 
      RowBox[{
       SqrtBox["3"], " ", "\[Pi]"}], "]"}], "-", 
     RowBox[{"Cosh", "[", "\[Pi]", "]"}]}], ")"}]}]]], "Output",
 CellChangeTimes->{{3.698542593092677*^9, 3.698542598962145*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{"%", ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", 
     RowBox[{"2", "\[Pi]"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.698542603679488*^9, 3.698542609299268*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
    1.], LineBox[CompressedData["
1:eJwV1nk4VO8XAPAxm7El69xRZAmFvmSpvpH3qBSSEBJRRNoQKSIVofLNLiWl
0kKWLNnS1MXcCS1CEkqJkmwzRRTid39/3efz3Oc597znfc95r5p3oKMvlUKh
OIpQKP9/0nDx5n6TKPMBU+df8sxoXFVRVUslbTeCGHfn1R+j8F89yXZdqYFo
187LBRaVUXivRVNfZeppNO+13HDBvij87dGvhXdTk1DNRcw/jjiDt1wcl7me
egN5RZjk9/85hW/NDCeupBajTY9OJdlVncAvLnl6PSMVRw9zdBNbwB8fmmLO
WxxtQf9SrT8nyO9Ha/6m2D880onOlZy1yFOMQU0fXc9SNXrRLRPXVR+LUxCs
d5q55/0FfYp+ueN041XkdsRaWLFzEFX16cn++nsLPVXTia69O4Kw4LsjEU65
yOmKzu2eACG6mJQtselzIVrEeLIhed9P1I8cPvZrlKJzRvyy2vMTSNHMvL5S
uxxZbrbZcy9uEunxmO8P06oQ/8KEVkz4b2Q2FRJR3fIIDSXIcz9dmUY7e2UC
77ziIq/Q0iRh6izaFPxf/tBJHEWqEytPV80hS5WwB58W1aF/5iInqQcoUE7x
DWM61qNCv6AAwWEKvDC62AEu9Ui31Wfg/REK0HyJnjC3erTszpaO8jAKPP76
9ONn73qkZsOp3HeeAtpp6gGXj9Yj+YzyY89zKXB935r3ien1aHrF8ETKAAX0
bbiZN9rrUYOH67iqjwgUKWsde2nLQ41lg/u894uA9+ysRJE9Dz0XPdF9+7AI
yBSUro134qHm0sw6rWMisM1DxmntLh5qZ7xP0osTgWqhnGbQIR7qK/LQW5Mn
AuKK6aWrLvDQ7F9v320jInB88aU+yXoemnMc70wWioDapeMN4Xwems89a9s2
IQIp/RIDXxp5iOp4x8j5rwi85e8nil7zkNi9ryLuC6jwZm9AmUwPDyna7c/e
Z0CFSDOscskUD63M9n8XGUIF4/FiZ7SMQHMj4RXHw6gQGnu7YlaHQM9Nz6cF
nqQCQ3NxWOUKAnl35WzziqHC+huPlZYYEShdvrNhYzoVzkSGpxDrCDQZv75a
opwKXYdVlDwdCVQTys7MHKdCol2pQU0YgeKeLQ1NnaKC17i17Hw4gRwVDJ3/
m6ECM+cZEyIJNFxmKxNJowFX30elMopAi8eiLuyRo8HWzd/rguMJdNpn5IS2
EQ0W/aHn6mYRaKNDnVtFMA02Ug5tiH9E5vPfIWNaKA2aChfIBT8mUD9fYYFD
BA12e8Z9c3pCoOi1B+tGztIgoYGZKlFHoDoNuWVLL9GgFDWnmjUSyHzS51dq
FQ2kE24F5L8lUKKB9OvexzQQX+4ravGOQD0HH+X9U0uDM6EGQW2dBDr5SWrX
80YaKEx+zv/6nkCPGyvrRbppkHblifurzwRak8VKPjJLA2HHfHf2CIHOvy07
8JRCh83RJnO9owR6J+2xQZJBh1tFfk+VBQQ6HlM6mStFhyWeVaxzPwhU4e/m
8UmFDtqXyw5gkwSi59FXr9CgwyWFkxz9KQI59T1YGKFNxptfZgu/CTThQiMU
DehQukp5heM0gQyhUMfOgg5/VKaijObI9Ye70K9Z0qFEzndUeZ5AreWUj9+t
6WBzK7WOSuGjI8udU2Id6bD8FfvyYxE+qt07f/CNCx02VX1JSaLykXT2/Y1q
7nQof+M+6EHjoweyc1PcvXRob16Z9Z3OR3O2ea3i++nweoL+4x6Dj7aecyxw
PUwH/56DlzyZfDQyc89zIoQOsaiDyxXlI92v2/i6cXSItAvTMRbno84Dekm+
8XTw/dUnVkc6boy180YiHULLN2y3kuCj3qm6EdnLdHiYd5q9SZKPEk5mV9pm
0aGY0LrIJb1WJOJM3A06qDe6e66Q4qN0cWP56Vw6vExhSs+Rtkha+NGokA5R
lZu/eSzgozG50Vz/Ejo4RhUtrCaddaUpKLecDsmRyuclpfnISvme6edqOhQs
j7RxJ/3rVjRj0RM6iP8udbpDOkdr92unOjoMbbt1+xvpbQWmmYl8OugmGhhr
LuSjWX1sb2MTHQyZljQP0vfLJ/SozXSophJSSaRd/m2dNG2jw4NnqfaPSdOe
FtUe66CDQmJG42fSJevj44u76fA8sTyYKsNHHg37nL5/pEPYn9YtyqTFbTeo
aPTTwWv6jb0h6eqWJYO7vtFhQ82NUxakfZ1nSzOG6aC/W6nNhrRsd2dEi4AO
+6WWW9mRrvWssBSfoAMMlvRsIe3fnyK98TcdpmQvJ20grbQ/oCtylg7ppbVu
JqQbRmxuV1EY8OK7rJka6ZAgbf8fdAa09AcbiJJWm6St1hVjgPULvvE3Mv/m
8F6KrxQDsBahZR3piHnu82wZBmzR7tpzifSymMz0TgUG/JHwid5L+i3ruKes
EgPUao/n6pKOTnBcZqvCgPMJky9Hyfrqy+r/jFVnwN1njYL7pD9kSHBxLQaE
N7RJ7iG9+iaxzUifAVF8jeU15H5+WXqL42/EgOgyE3VP0qn3I/vvrWbAbSsF
sVnyPIyUrQpVAgY0979J0ySduVrOwmkjA+qtTq0sJc/TJq5APNGKASFYQskq
0jf5edkiDgwwPRq4SZ88j1ttYvebOjNAdnjBjhzyvE43exke28mAybx8M2nS
Tp1Kzwa9GBD3MDC8g8VHIh5TSer7yPgZvi8NSD/4/GbnroMM+DsmHIgh+0Fs
+OLo62AGtK/yPqBE9ktl4IEqsVAGiMQovHAn+8lnwjJqQwQDdGl/BjPIfnv6
d06+6iwDXONGbKfJ/gxeGGSWnc4Al+bXnsfI/h5KfH4k6QoDNBuWFKST/e8t
tfTumWsMmD6FAh78JZCD+DupvXcYEEPrs2udIZABfd0n7XIGPI/xMWon50te
dIYcp5oBHb19gmfk/FEVEW4W5zIg8+4i6fJfBJKeyykZ4TEASZUfihwn0NgU
K7r0DQPWm27d8XKMQPuOe1fmvGPA8i8H+1PIefdx4vFQ2nsGBPFMnjqQ8/DV
j4Dtx/sZcMbitXPtdwIVDrcvNR0n87s298z1C4EOfrrZQMgywUx1fVQIOW/7
PKZnKhSZwPKTe9nTQSC3D9sNcpWY8GDnpXUW5Ly27mJmXlBnwl+PsvjJVgIt
e3P4oJ0hE2zyE84pvSDQwLM1Up0OTEj23fXRkksgT8tUiyZnJlSnnDhgX0Og
Dt7wsZqdTHj6PWLcqZpA/Nrsj9e8mBDNsJzeUk6gOzX0Eq8gJpxqOp/wo4BA
ex+0OA4nM0H9Te5N7CoZD2uIXnmJCdrxaijpMplf9JOy0Ewm6H6buTGXTiB7
l3xZeg4TrGc3XGtIIpDZ3Nm2xQ+ZoF/loE2NJZC83WpHu7dMiN1s/lwxgKxv
9Yro9C4mpEl+VcQOEUhcfWlZdw8Tzuv2jcvsJxBlcqGs3wATpk0OOwq8CDRy
faj19BQTWr+V2dg7E4gYve5QyhGFAinToLy1BMJ3pEdNKYuCbQzPbuVq8n6u
iy9dpy4K4ilS/5WR93nJpVCZ5zqioCyZJpetR6Br6xxa+0xFgbVk7QKOCoGO
JdAd5D1FQcrk9HTWPA9prThkH5ojCh5SlQ61XB4aSf44FHJPFMZTE5quVvNQ
2YRDbHC+KJwK/WwZUM5D67hravzLRGHRVpP1lEIectrCXOpTLwqrGLqMlqs8
FHXg1pR9nyjcVJpLXRzKQx/uvsvWUWeBH2Og11WHh3LEtvy7TIsF7UqGJts0
eWi//9M3mjosGCutBXNVHho3vstSM2RB6QvNEKYiD4kTwcHs9SxImasQQyI8
tKZPahPdmwWuWuiTXUc9SlPZONpziwXGuudbO8PrUeXL76ENd1ng/+NcynxI
PeoOT6KW3meB4sV4sSWB9Uj1XTc7ppR8/3EKs9pL/l8mBa3XqWMBRWn1ifVb
6tEzyo3Lx3tZcC2tancHh/xf7PtjIb1EDIZHzMcOldYh5ZQbL3+ri0GohV/i
3fw6BMhyR5+WGOhtE+a8u12HzmUlH674RwzaDftVNTPqkLyT9mV3czEwhOJV
vuF1aAXfaSTXQwxiuLfnQy3q0J7c4gy4JgbXGj70djXVIqbFz8Wnb4rBWNER
4lhdLSrqNr795I4Y+LrdCmc9qkXTUjXFa4vE4GpNt5NsXi1KDyEajZ+KwY+j
lIiq2FrUaNE1vaxXDHp6v/hYolpk8IG6W2apOATEgvMTFxxRZFy0+wrE4aUR
esFs5qKgOwpYZ7E4bPmo3fbzERf1rX7Lan4oDuLX07pe3eUiwtNp6NFjcThN
Ff9gH8lFFwodi1JeiMPfwjS7eF0ukrXaZmQxLA433loPN8Q8RktPW6GbOhKw
Y7gixUWnBlmPrt3heV8CWBirYIFFFTI/fHnD/SIJ0LlxmZ2uVYUMh8f1J0ol
YG5HXzZdsgopfS9kxddIQKOhn+BeRyUa+aJSU/FCAnwMN5kID1ai5A9UZakx
CQB73CY1qQJ1vHje99hQErbGRPzzt/Eh8sl3C+BwJeHUSHNx1FQx+jwnmdrS
JAVft/piDNVsxHz218mbvwC8fnT+6n4Zhs/7L5837JCGd2r/TJgdvo/bE9ZL
Aw8uBM2l3W7OO6txB2Wr1+u/LQT9I1bjxQH1OHegf7QiQAaKnYK5ga+b8Ijv
KrsUR2VAUfMDNGW24Ld3Ta1TPC4LVhdW/K7vasdVxQIDaZOyIPOkXnI/tQu3
EzHIDTgqB4c8C/5cXdODfz2Q6HhiRg40DzCfdwl78RaJi1uDwuWh90v+pf9U
+3FNFW0ui64AGcfyF78O/YrrnJRYqfKfAqStHfz0veQbHiOX9EtVTBEO/bvk
Vc+97/jmRSUreCmKsObcgh0ZmcO47eLXh+rSFEFixv904M1h3F557D5+SRHm
o2KNLXOHcdclelrcTEUYcvhn9UD5MH5QI0+l4pYiDJ4ONp97PYxf1L0pnVuq
CD/vejmeZYzgbabJP+JbFcHhtbNDk/8I7rnrSIWjDBuWDJyTO2M4iqdE5w1v
kmPDgZTZQxvWjOJEXq+aqQIbXAp0s6jmo7jOL/tEDQ4bPI+zj4RYj+JTCSv9
JlTZUKWcnq65ZxRPxn9iGQZs2GJyuKnm4iher37sZNc2NszYfnh+pW8U/2VV
VPbKgQ1HF4xtb/k2ii8L/DpYt50NV94EWtJGR/HEx87O+TvYsF+iZbv71Cju
7rTqn4jdbOj99C/3jcQYPhE79WlxIBuuvT9a0Gc0hmt9P7FhTyIbNnWNee85
M4Yvij0zdSqZDYOWrs7esWP4QrVzBddT2fAlYMLNM34M/+OaLvc+gw1aZhkJ
G9PH8JeND/qdb7Dh9mFpmabcMTw4r//slhI2bPs8nvrg1Rj+1G8rsaqNDa1p
QQEv5AX4Q5pTmHM7G7o05IaZHAGed8NNL6SDDaxhzqCpsgBPeeeXXtpNrvdN
i32qpgD32Rzto9fPBqcNCu5CEwEurl1FV59gw6j+2fX+zgJ8x4CqpZQiBhm6
WTXWSQLccN73gSiGwZl9r2KDUwW4JFbApiphUFpZpXXpkgCvtTYZmlTGIEvV
v7shS4AvK7JO6tXEYM1nh+y+XAH++2hw10MTDIpd48yHngrwtotV6x+sxmCb
AfvqgzoBXnh3tiDvXwyeYVjiYUKA73kXd+r6OgwshBIGb5sEeNPaLI1zlhg0
GCsKXdsF+FURwt/NGYPRvaeo2oMCPERJ7J3TDgzSH3mwzw4JcDsjO9i2E4PD
76XPdY4IcKpvp+xGDwzyueF+gT8E+KHGkaoVvhjIT5vX2k4LcLMkBarIMQw6
j95c7cUQ4op5bodmjpPr3VFusJMpxIW1N9p/hWEgV2xevVVUiN/5uTx36CQG
5nd/HdQTE+KSLua27TEY6DdkL6uVFOI9i/0yctMx8FKSd0RyQtxF0+zrhwwM
npTSvBXkhXjLChlj2UwMttoHU76Rrjd/3HryOgZWnOmek4pCPG+PtJTjPQzi
s4deB3OEuNqBL+7n8jDQMRRnaSoJ8atBj/K5+eT6XCmP2kgnRO+10i7G4G9L
ZrjqYiEefKfq7GwVBhIR1w1iVYT4cOHFtpU1GGhEJfbLLhHiPhVean5cDIzQ
vT9ZpHc8k8Bba8n6YEc23lQl82vulRLlYfBBseLoIjUhbvOuYpcZHwPd9E0z
KaTNBndP32vCIHnkKCtIXYhXCo2tP7zAoFE053wXaf0/YldkmjEQaFi4rNMQ
4uri5SYn2zDgLZ5sniSdJXshprQdA+mS6yG2S4W4/CLPNwMdZPyHXbuukxbV
YwU5vMfgQMfKIQNNIX7GuAeP68Fg0bpPF46SnjYrW8D9hIFzzgavUtJHLc95
/PiMAX4x13+I9MjWXYVaXzAQObG9UEVLiO9zWTnjPkDm252kZEf6kyfTJmWQ
3N/FdyvCSLv6vb/ybAgD+5z34dmk2wJLvs2MkPEaI/xw0lvCYletFJD9MPAq
7D1p4oxb7L4fGERuVSr8Sdr8gn571jgGzTb35+jaQrwqha7R+guDfct4/rKk
Da52BTF/YxC0omRSiXR+zoNa02kMLG9UZ6iQ1ig4Kx00i4Fh/dItyqSvPXT1
vDeHgUu3mQybtAJ3RdF7CgeC9awHJEgnEtTZhTQO1MylNMyQ32e9emezicGB
4szdZQOko94WZkaIcmDSavr2S9LTPVGDJWIcWL4rP7OIdMiAy+oBCQ6UaFcn
XyA9OqYbt2gBBy4zDsft+X+9pihv7RdyYJ/LgjBD0r3zbzXiZDkwE/XNe56s
705WQfBjeQ64/UYbG0i3LTxTJ1TkQJrhHk48aVuO80ItDgdSHiZ/2kyar6az
230RByZYSukU0uY680XJyhz48WKjcTm539WG7bP8JRzQztn5xJv0StP7W2bU
OHDa/JaeJOn/AXnk0As=
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None},
  PlotRange->
   NCache[{{0, 2 Pi}, {-1.0095506560846428`, 0.16459063075499647`}}, {{
     0, 6.283185307179586}, {-1.0095506560846428`, 0.16459063075499647`}}],
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{3.6985426096417017`*^9}]
}, Open  ]]
},
WindowSize->{808, 707},
WindowMargins->{{Automatic, 3}, {Automatic, 8}},
FrontEndVersion->"10.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (October 11, \
2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 665, 20, 28, "Input"],
Cell[1248, 44, 1136, 32, 63, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2421, 81, 435, 12, 28, "Input"],
Cell[2859, 95, 1382, 41, 86, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4278, 141, 850, 20, 48, "Input"],
Cell[5131, 163, 1687, 59, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6855, 227, 956, 31, 52, "Input"],
Cell[7814, 260, 559, 16, 48, "Output"]
}, Open  ]],
Cell[8388, 279, 472, 15, 49, "Input"],
Cell[8863, 296, 295, 9, 52, "Input"],
Cell[CellGroupData[{
Cell[9183, 309, 534, 17, 49, "Input"],
Cell[9720, 328, 387, 12, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10144, 345, 1647, 53, 71, "Input"],
Cell[11794, 400, 342, 10, 59, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12173, 415, 465, 15, 46, "Input"],
Cell[12641, 432, 167, 4, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12845, 441, 1337, 36, 63, "Input"],
Cell[14185, 479, 2507, 75, 114, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16729, 559, 1037, 30, 46, "Input"],
Cell[17769, 591, 3018, 96, 116, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20824, 692, 660, 21, 60, "Input"],
Cell[21487, 715, 558, 18, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22082, 738, 707, 20, 28, "Input"],
Cell[22792, 760, 181, 4, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23010, 769, 755, 23, 52, "Input"],
Cell[23768, 794, 263, 7, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[24068, 806, 404, 11, 28, "Input"],
Cell[24475, 819, 1138, 38, 95, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[25650, 862, 510, 15, 28, "Input"],
Cell[26163, 879, 1450, 47, 70, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[27650, 931, 230, 6, 28, "Input"],
Cell[27883, 939, 8044, 144, 230, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

