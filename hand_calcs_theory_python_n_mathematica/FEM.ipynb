{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Derivation of Adjoint Equations for Non-Linear Beam Equations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NOTE TO SELF:  MEHDI'S CODE FOR IMPCOLL WILL ALREADY ADD THE ADDITIONAL STATE VARIABLES TO THE BOTTOM OF THE IMPLICIT DE FORMULATION.  \n",
    "$ F(x,\\dot{x},p) = 0 $  \n",
    "will be converted to  \n",
    "$ \n",
    "\\begin{bmatrix}\n",
    "F(x,y,p) \\\\\n",
    "\\dot{x} - T y\n",
    "\\end{bmatrix} = 0\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook will work through the derivation of the adjoint equations for a bi-layered, non-linear beam model discussed in [cite!]. The spatial dimension, $ s $, specifies a point along the deformed midplane of the beam and is scaled by the overall length, $\\ell$ so that $ s \\in \\left[0,1\\right]$. Similarly, the temporal dimension, $ t $ is scaled by the period of oscillation, $ T $ so that $ t \\in \\left[0,1\\right] $\n",
    "\n",
    "$ 0 = \\left(\\bar{m}\\ddot{u}_{1}+c_{u}\\dot{u}_{1}-f_{u_{1}}\\right)\\cos{u_{3}} + \\left(\\bar{m}\\ddot{u}_{2}+c_{v}\\dot{u}_{2}-f_{u_{2}}\\right)\\sin{u_{3}} -\n",
    "\\bar{I}\\ddot{u}_{3} - A $\n",
    "\n",
    "$ 0 = -\\left(\\bar{m}\\ddot{u}_{1}+c_{u}\\dot{u}_{1}-f_{u_{1}}\\right)\\sin{u_{3}} + \\left(\\bar{m}\\ddot{u}_{2}+c_{v}\\dot{u}_{2}-f_{u_{2}}\\right)\\cos{u_{3}} - \\bar{I}\\dot{u}_{3}^{2} - B$\n",
    "\n",
    "$ 0 = u_{3} - \\arctan{\\dfrac{u_{2,s}}{1+u_{1,s}}} $\n",
    "\n",
    "$ 0 = u_{4} - k_{12}\\left(\\nu - 1\\right) - k_{22}u_{3,s} $\n",
    "\n",
    "$ 0 = u_{5} - \\dfrac{\\lambda \\ddot{u}_{3} - \\bar{I}\\left(\\ddot{u}_{1}\\cos{u_{3}}+\\ddot{u}_{2}\\sin{u_{3}}\\right) - u_{4,s} - c}{\\nu} $\n",
    "\n",
    "A unique solution will be achieved by applying the spatial and temporal boundary conditions on the state vector, $ \\boldsymbol{u} $, given by  \n",
    "\n",
    "<center>  \n",
    "$ \\Psi =\n",
    "\\begin{bmatrix}\n",
    "u(0,t) \\\\\n",
    "u(1,t)\\\\\n",
    "u(s,0) - u(s,1)\n",
    "\\end{bmatrix} = 0 $\n",
    "</center>  \n",
    "\n",
    "and by enforcing that the maximum deflection of midpoint of the beam occur at the end of the period of the oscillation:  \n",
    "\n",
    "<center>  \n",
    "$ \\dot{u}_{2}\\left(\\dfrac{1}{2}, 1\\right) = 0 $\n",
    "</center>  \n",
    "\n",
    "where\n",
    "\n",
    "$ A = \\left(k_{11} - \\dfrac{k_{12}^{2}}{k_{22}}\\right)\\nu_{,s} + \\dfrac{k_{12}}{k_{22}}u_{4,s} - \\left(\\dfrac{u_{4}}{k_{22}} - \\dfrac{k_{12}}{k_{22}}\\left(\\nu-1\\right)\\right)u_{5} $\n",
    "\n",
    "$ B = u_{5,s} + \\dfrac{1}{k_{22}}\\left(k_{11} - 2\\dfrac{k_{12}^{2}}{k_{22}}\\right)\\left(\\nu - 1 \\right)u_{4} - \\dfrac{k_{12}}{k_{22}}\\left(k_{11} - \\dfrac{k_{12}^{2}}{k_{22}}\\right)\\left(\\nu - 1\\right)^{2} + \\dfrac{k_{12}}{k_{22}^{2}}u_{4}^{2} $ \n",
    "\n",
    "$ \\nu = \\sqrt{\\left(1 + u_{1,s}\\right)^{2} + u_{2,s}^{2}} $\n",
    "\n",
    "and other terms will be discussed in detail below.\n",
    "\n",
    "Here the $ u_{i} $'s are state variables that depend on time, $ t $, and the location along the beam center line, $ s $. Letter subscripts preceded with a comma indicate partial differentiation with respect to the indicated variable and the overdot represents differentiation with respect to time.\n",
    "\n",
    "The state variables $ u_{1} $ and $ u_{2} $ are displacements in the lateral and transverse directions, respectively.  The state variable $ u_{3} $ represents the slope of the deformed beam baseline.  State variables $ u_{4} $ and $ u_{5} $ are the bending moment and shear force of the beam. In order to work with a first order system, define two additional states:\n",
    "<center>  \n",
    "$ \\begin{pmatrix}\n",
    "    u_{6} \\\\ \n",
    "    u_{7}\n",
    "  \\end{pmatrix} = \n",
    "  \\begin{pmatrix} \n",
    "    \\dot{u}_{1} \\\\ \n",
    "    \\dot{u}_{2}\n",
    "  \\end{pmatrix}\n",
    "$\n",
    "</center>\n",
    "\n",
    "and introduce variables, $ y_{i} $, such that $ \\dot{u}_{i} = y_{i} $."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import sympy as sp\n",
    "import numpy as np\n",
    "from sympy.physics.quantum import TensorProduct\n",
    "sp.init_printing()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Define Independent Variables\n",
    "t, s = sp.symbols('t s')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Define State Variables as functions of s and t\n",
    "u1 = sp.Function('u1')(s, t)  # u\n",
    "u2 = sp.Function('u2')(s, t)  # v\n",
    "u3 = sp.Function('u3')(s, t)  # theta\n",
    "u4 = sp.Function('u4')(s, t)  # M\n",
    "u5 = sp.Function('u5')(s, t)  # V\n",
    "u6 = sp.Function('u6')(s, t)  # du/dt\n",
    "u7 = sp.Function('u7')(s, t)  # dv/dt\n",
    "\n",
    "y1 = sp.Function('y1')(s, t)\n",
    "y2 = sp.Function('y2')(s, t)\n",
    "y3 = sp.Function('y3')(s, t)\n",
    "y4 = sp.Function('y4')(s, t)\n",
    "y5 = sp.Function('y5')(s, t)\n",
    "y6 = sp.Function('y6')(s, t)\n",
    "y7 = sp.Function('y7')(s, t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Define necessary derivatives\n",
    "u1s = u1.diff(s)\n",
    "u2s = u2.diff(s)\n",
    "u3s = u3.diff(s)\n",
    "u4s = u4.diff(s)\n",
    "u5s = u5.diff(s)\n",
    "\n",
    "u1t = u1.diff(t)\n",
    "u2t = u2.diff(t)\n",
    "u3t = u3.diff(t)\n",
    "u4t = u4.diff(t)\n",
    "u5t = u5.diff(t)\n",
    "u6t = u6.diff(t)\n",
    "u7t = u7.diff(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After substitution and appending the additional equations, the system becomes,  \n",
    "<center>  \n",
    "$ \\begin{bmatrix}\n",
    "\\left(\\bar{m}y_{6}+c_{u_{1}}u_{6}-f_{u_{1}}\\right)\\cos{u_{3}} + \\left(\\bar{m}y_{7}+c_{u_{2}}u_{7}-f_{u_{2}}\\right)\\sin{u_{3}} - \\bar{I} \\ddot{u}_{3} - A \\\\\n",
    "-\\left(\\bar{m}y_{6}+c_{u_{1}}u_{6}-f_{u_{1}}\\right)\\sin{u_{3}} + \\left(\\bar{m}y_{7}+c_{u_{2}}u_{7}-f_{u_{2}}\\right)\\cos{u_{3}} - \\bar{I} \\dot{u}_{3}^{2} - B \\\\\n",
    "u_{3} - \\arctan{\\dfrac{u_{2,s}}{1+u_{1,s}}} \\\\\n",
    "u_{4} - k_{12}\\left(\\nu - 1\\right) - k_{22}u_{3,s} \\\\\n",
    "u_{5} - \\dfrac{\\lambda \\ddot{u}_{3} - \\bar{I}\\left(y_{6}\\cos{u_{3}}+y_{7}\\sin{u_{3}}\\right) - u_{4,s} - c}{\\nu} \\\\\n",
    "y_{1} - u_{6} \\\\\n",
    "y_{2} - u_{7} \\\\\n",
    "\\dot{u}_{1} - T y_{1} \\\\\n",
    "\\dot{u}_{2} - T y_{2} \\\\\n",
    "\\dot{u}_{3} - T y_{3} \\\\\n",
    "\\dot{u}_{4} - T y_{4} \\\\\n",
    "\\dot{u}_{5} - T y_{5} \\\\\n",
    "\\dot{u}_{6} - T y_{6} \\\\\n",
    "\\dot{u}_{7} - T y_{7} \\\\\n",
    "\\end{bmatrix} = 0 $\n",
    "</center>  \n",
    "\n",
    "Where time, $ t $ has been rescaled using the substitution $ t = T \\tau $ where $ T $ is the period of oscillation and will be treated as an unknown. Note that $ \\dot{u}_{3} $ and $ \\ddot{u}_{3} $ are directly calculable in terms of $ u_{1} $ and $ u_{2} $ by using the third equation.\n",
    "\n",
    "The other, as yet undefined terms in the equations, are given as follows:\n",
    "\n",
    "$ k_{11} = 12\\left(\\dfrac{h_{1}}{\\ell}\\right)^{-2}\\left(1+\\dfrac{E_{2}}{E_{1}}\\dfrac{A_{2}}{A_{1}}\\right) $\n",
    "\n",
    "$ k_{12} = -6\\left(\\dfrac{h_{1}}{\\ell}\\right)^{-1}\\dfrac{E_{2}}{E_{1}}\\dfrac{A_{2}}{A_{1}}\\left(1+\\dfrac{h_{2}}{h_{1}}\\right) $\n",
    "\n",
    "$ k_{22} = 1+\\dfrac{E_{2}}{E_{1}}\\dfrac{A_{2}}{A_{1}}\\left[\\left(\\dfrac{h_{2}}{h_{1}}\\right)^{2} + 3\\left(1+\\dfrac{h_{2}}{h_{1}}\\right)^{2}\\right]$\n",
    "\n",
    "$ \\lambda = \\dfrac{1}{\\alpha^{2}}\\left[1+\\dfrac{\\rho_{2}}{\\rho_{1}}\\dfrac{A_{2}}{A_{1}}\\left(\\left(\\dfrac{h_{2}}{h_{1}}\\right)^{2} + 3\\left(1+\\dfrac{h_{2}}{h_{1}}\\right)^{2}\\right)\\right] $\n",
    "\n",
    "$ \\alpha = \\ell\\sqrt{\\dfrac{A_{1}}{J_{1}}} $, which becomes $ \\alpha = \\sqrt{12}\\dfrac{\\ell}{h_1} $ for the rectangular cross section beam under study\n",
    "\n",
    "$ \\bar{m} = 1 + \\dfrac{\\rho_{2}}{\\rho_{1}}\\dfrac{A_{2}}{A_{1}} $\n",
    "\n",
    "$ \\bar{I} = \\dfrac{1}{2}\\dfrac{\\rho_{2}}{\\rho_{1}}\\dfrac{A_{2}}{A_{1}}\\dfrac{h_{1}}{\\ell}\\left(1+\\dfrac{h_{2}}{h_{1}}\\right) $\n",
    "\n",
    "Where $ \\rho $, $ A $, $ h $, and $ E $ are the density, cross-sectional area, thickness, and elastic modulus of the beam layers, respectively.  The numerical subscript indicates the layer. The parameter $ \\ell $ is the overall beam length.  Also, note that the overall beam thickness is given by $ h = h_{1} + h_{2} $\n",
    "\n",
    "$ c_{u} $ and $ c_{v} $ are directional, linear damping coefficients where the letter subpscript indicates the direction. For this analysis, these coefficients are assumed to be equal: $ c_{u} = c_{v} = c_{1} $"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Define Parameters\n",
    "hl, hh, EE, rr, AA = sp.symbols('\\\\left(\\\\dfrac{h_1}{\\\\ell}\\\\right), \\\\left(\\\\dfrac{h_2}{h_1}\\\\right), \\\n",
    "                                        \\\\dfrac{E_2}{E_1}, \\\\dfrac{\\\\rho_2}{\\\\rho_1}, \\\\dfrac{A_2}{A_1}')\n",
    "c1, Nx, thk, l = sp.symbols('c_{1}, N_{x}, b, \\\\ell')\n",
    "T = sp.Symbol('T')\n",
    "\n",
    "# Define Parameter Agglomerations\n",
    "alpha = sp.Symbol('\\\\alpha')\n",
    "k11 = sp.Symbol('k11') \n",
    "k12 = sp.Symbol('k12')\n",
    "k22 = sp.Symbol('k22')\n",
    "la = sp.Symbol('\\\\lambda')\n",
    "mbar = sp.Symbol('\\\\bar{m}')\n",
    "Ibar = sp.Symbol('\\\\bar{I}')\n",
    "\n",
    "nu = sp.Function('\\\\nu')(s,t)\n",
    "nus = nu.diff(s)\n",
    "\n",
    "A = sp.Symbol('A')\n",
    "B = sp.Symbol('B')\n",
    "\n",
    "\n",
    "# Dictionary for symbol substitutions\n",
    "subsyms = {alpha : sp.sqrt(12)*(hl)**-1,\n",
    "           k11 : (1/hl**2)*(12+12*EE*hh),\n",
    "           k12 : ((-6*EE*hh)/hl)*(1+hh),\n",
    "           k22 : 1 + EE*hh*(hh**2 + 3*(1+hh)**2),\n",
    "           la : (alpha**-2)*(1+rr*hh*(hh**2 + 3*(1+hh)**2)),\n",
    "           mbar : 1 + rr*hh,\n",
    "           Ibar : (1/2)*rr*hh*hl*(1+hh)}\n",
    "\n",
    "subnu = {nu: sp.sqrt((1+u1s)**2 + u2s**2),\n",
    "         nus: sp.diff(sp.sqrt((1+u1s)**2 + u2s**2),s)}\n",
    "\n",
    "subAB = {A: ((k11 - k12**2/k22)*nus + (k12/k22)*u4s - ((u4/k22) - (k12/k22)*(nu-1))*u5), \n",
    "         B: (u5s + (1/k22)*(k11-2*(k12**2/k22))*(nu-1)*u4 - \\\n",
    "            (k12/k22)*(k11 - k12**2/k22)*(nu-1)**2 + (k12/k22**2)*u4**2)}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$ f_{u_{1}} $, $ f_{u_{2}} $ and $ c $ are  input forces (where, again, letter subscript indicates direction) and moments, respectively.  \n",
    "\n",
    "For the present study, we will investigate the case where the beam is driven by a periodic force in the transverse direction with no other external input.  That is, we set $ f_{u_{1}} = c = 0 $ and $ f_{u_{2}} = \\Lambda \\cos{\\omega t} $. In order to work with an autonomous system given this non-autonomous forcing function, we'll append two additional states to the system whose solution results in the desired periodic forcing term [cite! -> 47 in Mehdi RSPA paper].   \n",
    "\n",
    "<center>  \n",
    "$\n",
    "\\begin{bmatrix}\n",
    "\\dot{x}_{1} - \\left(x_{1} + \\omega x_{2} − x_{1} \\left(x_{1}^{2} + x_{2}^{2}\\right)\\right) \\\\\n",
    "\\dot{x}_{2} - \\left(x_{2} - \\omega x_{1} − x_{2} \\left(x_{1}^{2} + x_{2}^{2}\\right)\\right) \n",
    "\\end{bmatrix} = 0\n",
    "$\n",
    "</center>  \n",
    "Inserting these equations into the system and a pair of corresponding $y$ variables yields the system:  \n",
    "<center>  \n",
    "$ \\begin{bmatrix}\n",
    "\\left(\\bar{m}y_{6}+c_{u_{1}}u_{6}\\right)\\cos{u_{3}} + \\left(\\bar{m}y_{7}+c_{u_{2}}u_{7}-\\Lambda  u_{9}\\right)\\sin{u_{3}} - \\bar{I} \\ddot{u}_{3} - A \\\\\n",
    "-\\left(\\bar{m}y_{6}+c_{u_{1}}u_{6}\\right)\\sin{u_{3}} + \\left(\\bar{m}y_{7}+c_{u_{2}}u_{7}-\\Lambda  u_{9}\\right)\\cos{u_{3}} - \\bar{I} \\dot{u}_{3}^{2} - B \\\\\n",
    "u_{3} - \\arctan{\\dfrac{u_{2,s}}{1+u_{1,s}}} \\\\\n",
    "u_{4} - k_{12}\\left(\\nu - 1\\right) - k_{22}u_{3,s} \\\\\n",
    "u_{5} - \\dfrac{\\lambda \\ddot{u}_{3} - \\bar{I}\\left(y_{6}\\cos{u_{3}}+y_{7}\\sin{u_{3}}\\right) - u_{4,s}}{\\nu} \\\\\n",
    "y_{1} - u_{6} \\\\\n",
    "y_{2} - u_{7} \\\\\n",
    "y_{8} - \\left(u_{8} + \\omega u_{9} − u_{8} \\left(u_{8}^{2} + u_{9}^{2}\\right)\\right) \\\\\n",
    "y_{9} - \\left(u_{9} - \\omega u_{8} − u_{9} \\left(u_{8}^{2} + u_{9}^{2}\\right)\\right) \\\\\n",
    "\\dot{u}_{1} - T y_{1} \\\\\n",
    "\\dot{u}_{2} - T y_{2} \\\\\n",
    "\\dot{u}_{3} - T y_{3} \\\\\n",
    "\\dot{u}_{4} - T y_{4} \\\\\n",
    "\\dot{u}_{5} - T y_{5} \\\\\n",
    "\\dot{u}_{6} - T y_{6} \\\\\n",
    "\\dot{u}_{7} - T y_{7} \\\\\n",
    "\\dot{u}_{8} - T y_{8} \\\\\n",
    "\\dot{u}_{9} - T y_{9}\n",
    "\\end{bmatrix}=0 $\n",
    "</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Define Force and Moment Variables\n",
    "u8 = sp.Function('u8')(t)  # Oscillatory Forcing Function (sin(omega t))\n",
    "u9 = sp.Function('u9')(t)  # Oscillatory Forcing Function (cos(omega t))\n",
    "\n",
    "y8 = sp.Function('y8')(t)\n",
    "y9 = sp.Function('y9')(t)\n",
    "\n",
    "u8t = u8.diff(t)\n",
    "u9t = u9.diff(t)\n",
    "\n",
    "w, La = sp.symbols('\\omega, \\Lambda')\n",
    "c = 0\n",
    "fu1 = 0\n",
    "fu2 = La*u9"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Need to include Length Scaling in spatial derivatives"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Calculate the time derivatives for u3\n",
    "u3t = sp.diff(sp.atan(u2s/(1+u1s)),t)\n",
    "u3tt = sp.diff(sp.atan(u2s/(1+u1s)),t,2)\n",
    "\n",
    "u3t = u3t.subs({u1.diff(t,1):y1, u2.diff(t,1):y2})\n",
    "u3tt = u3tt.subs({u1.diff(t,2):y6, u2.diff(t,2):y7})\n",
    "u3tt = u3tt.subs({u1.diff(t,1):y1, u2.diff(t,1):y2})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Define Functions outlined above\n",
    "F1  = (mbar*y6 + c1*u6 - fu1)*sp.cos(u3) + (mbar*y7 + c1*u7 - fu2)*sp.sin(u3) - Ibar*u3tt - A\n",
    "F2  = -(mbar*y6 + c1*u6 - fu1)*sp.sin(u3) + (mbar*y7 + c1*u7 - fu2)*sp.cos(u3) - Ibar*u3t**2 - B\n",
    "F3  = u3 - sp.atan(u2s/(1+u1s))\n",
    "F4  = u4 - k12*(nu-1)-k22*u3s\n",
    "F5  = u5 - la*u3tt - Ibar*(y6*sp.cos(u3)+y7*sp.sin(u3)) - u4s - c\n",
    "F6  = y1 - u6\n",
    "F7  = y2 - u7\n",
    "F8  = y8 - (u8 + w*u9 - u8*(u8**2 + u9**2))\n",
    "F9  = y9 - (u9 - w*u8 - u9*(u8**2 + u9**2))\n",
    "F10 = u1t - T*y1\n",
    "F11 = u2t - T*y2\n",
    "F12 = u3t - T*y3\n",
    "F13 = u4t - T*y4\n",
    "F14 = u5t - T*y5\n",
    "F15 = u6t - T*y6\n",
    "F16 = u7t - T*y7\n",
    "F17 = u8t - T*y8\n",
    "F18 = u9t - T*y9"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Define these lists for later use\n",
    "FF  = [F1,  F2,  F3,  F4,  F5,  F6,  F7,  F8,  F9, F10, F11, F12, F13, F14, F15, F16, F17, F18]\n",
    "uu  = [u1, u2, u3, u4, u5, u6, u7, u8, u9]\n",
    "uus = [u1s, u2s, u3s, u4s, u5s]\n",
    "yy  = [y1, y2, y3, y4, y5, y6, y7, y8, y9]\n",
    "pp  = [hl, hh, EE, rr, AA, w, La, c1, Nx, thk, l]\n",
    "aa = [sp.Symbol('a{}'.format(i)) for i in np.arange(11)+1]\n",
    "suba = dict(zip(pp,aa))\n",
    "TT  = [T]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point, discretize the spatial domain according to the Galerkin finite element method.  To that end, the solution will be approximated by piecewise continuous, smooth Lagrange polynomials  \n",
    "\n",
    "### Need to explain the spatial mesh of $i=1 \\dots M$ elements and the coordinate transformation between $\\xi$ and $s$\n",
    "\n",
    "$ u\\left(s,t\\right) = u_{k}\\left(\\xi, t\\right) = \\sum\\limits_{l=1}^{p+1} \\mathcal{L}_{l}\\left( \\xi \\right)u_{k}\\left(\\xi_{l}, t\\right)  = \\mathcal{L} \\left(\\xi\\right)\\cdot u_{k}\\left(t\\right)$  \n",
    "\n",
    "$ y\\left(s,t\\right) = y_{k}\\left(\\xi, t\\right) = \\sum\\limits_{l=1}^{p+1} \\mathcal{L}_{l}\\left( \\xi \\right)y_{k}\\left(\\xi_{l}, t\\right)  = \\mathcal{L} \\left(\\xi\\right)\\cdot y_{k}\\left(t\\right)$  \n",
    "\n",
    "with corresponding weight functions  \n",
    "\n",
    "$ w\\left(s\\right) = w_{k}\\left(\\xi\\right) = \\sum\\limits_{l=1}^{p+1} \\mathcal{L}_{l}\\left( \\xi \\right)w_{k}\\left(\\xi_{l}\\right) = \\mathcal{L}\\left(\\xi\\right)\\cdot w_{k}$\n",
    "\n",
    "Where $\\mathcal{L}\\left(\\xi\\right)$ is a vector of Lagrange polynomials"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For each entry, $F_{i}$ with $i = 1 \\dots 5 $, the weak form is then given by  \n",
    "\n",
    "$ \\displaystyle{\\int_{0}^{1}}w_{i} \\cdot F_{i}d\\xi =  \\displaystyle{\\int_{0}^{1}}w_{k,i}^{T}\\mathcal{L} \\ F_{i}\\left(\\mathcal{L} \\cdot u_{k,i}, \\mathcal{L} \\cdot y_{k,i}, p \\right)d\\xi = 0 $  \n",
    "\n",
    "Equations for i=6,7 are given by  \n",
    "\n",
    "$ \\displaystyle{\\int_{0}^{1}}w_{i} \\cdot \\left(u_{i}-y_{i}\\right) d\\xi= \\displaystyle{\\int_{0}^{1}} w_{k,i}^{T} \\mathcal{L} \\mathcal{L}^{T} \\left(u_{k,i}\\left(t\\right)-y_{k,i}\\left(t\\right)\\right) d\\xi = \\left(\\displaystyle{\\int_{0}^{1}} B_{i}\\left(\\xi\\right) d\\xi \\right)\\left(u_{k,i}-y_{k,i}\\right) = B_{i} \\left(u_{k,i}-y_{k,i}\\right) = 0 $  \n",
    "\n",
    "Where $ B_{i}\\left(\\xi\\right) = w_{k,i}^{T} \\mathcal{L} \\mathcal{L}^{T} $ and $ B_{i} $ is the vector resulting from the integration of $ B_{i}\\left(\\xi\\right) $. Equations $F_{8}$ and $F_{9}$ remain unchanged as they are not spatially dependent.  \n",
    "\n",
    "The entire system of spatially discretized $F$'s is referred to as  \n",
    "\n",
    "<center>  \n",
    "$F_{k}\\left(u_{k},y_{k},p\\right)$.\n",
    "</center>  \n",
    "\n",
    "The linear partial differential equations in time that make up the expanded system (for $i=1\\dots7$) will have weak forms given by  \n",
    "\n",
    "$ \\displaystyle{\\int_{0}^{1}}w_{i} \\cdot \\left(\\dot{u}_{i}-Ty_{i}\\right) d\\xi= \\displaystyle{\\int_{0}^{1}} w_{k,i}^{T} \\mathcal{L} \\mathcal{L}^{T} \\left(\\dot{u}_{k,i}\\left(t\\right)-Ty_{k,i}\\left(t\\right)\\right) d\\xi = \\left(\\displaystyle{\\int_{0}^{1}} B_{i}\\left(\\xi\\right) d\\xi \\right)\\left(\\dot{u}_{k,i}-Ty_{k,i}\\right) = B_{i} \\left(\\dot{u}_{k,i}-Ty_{k,i}\\right) = 0 $  \n",
    "\n",
    "The set of $7p+2$ linear differential equations can be written as  \n",
    "<center>  \n",
    "$ B \\left(\\dot{u}_{k} - Ty_{k}\\right) = 0$\n",
    "</center>  \n",
    "\n",
    "where $B$ is a block diagonal matrix of the $B_{i}$'s and two ones in the last two diagonal locations to account for the forcing functions at the end.  For appropriately chosen weights, the matrix B will be invertible, and therefore, the system can be written simply as  \n",
    "\n",
    "<center>  \n",
    "$\\dot{u}_{k} - Ty_{k} = 0 $\n",
    "</center>\n",
    "\n",
    "### Need to Mention Handling of Spatial BCs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Analysis Parameters\n",
    "p = 3                                                 # Degree of Polynomial for spatial approximation\n",
    "nstates = 7                                           # Number of states (not including y's or u8, u9 defined above)\n",
    "M = 1                                                 # Number of Elements\n",
    "ntot = (p+1)*nstates                                  # Total number of nodes / element\n",
    "bcs = np.arange(M*(p+1)*nstates)+1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def element_node_ids(p,M,nstates):\n",
    "    \"\"\"\n",
    "    Returns the node ids by element for a given polynomial order, number of elements, and number of states\n",
    "    \n",
    "    :param p: integer value for the polynomial order\n",
    "    :param M: Number of Elements\n",
    "    :param nstates: number of variables in the Finite element formulation\n",
    "    \n",
    "    :return: a dictionary with element ids as keys and lists of node ids as items\n",
    "    \"\"\"\n",
    "    node_idx = (np.arange(nstates*(M*(p+1)-(M-1)))+1)     # Index Values for Nodes, M*(p+1) Nodes minus (M-1) duplicate nodes\n",
    "    node_idx = node_idx.reshape(nstates,-1)\n",
    "    element_nodes = []\n",
    "    element_names = []\n",
    "    for i in np.arange(M):\n",
    "        element_nodes.append(node_idx[:,i*(p):(i+1)*(p+1)].flatten())\n",
    "        element_names.append(i)\n",
    "        \n",
    "    return dict(zip(element_names,element_nodes))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def lagrange_poly(p,i,x=sp.Symbol('x'),xi=None):\n",
    "    \"\"\"\n",
    "    Returns a Lagrange interpolation polynomial of order, p, for the specified basis point, {i : 0 < i <= p}\n",
    "    \n",
    "    :param p: polynomial order\n",
    "    :param i: Basis point integer\n",
    "    :param x: Optional variable symbol, 'x' is used by default\n",
    "    :param xi: Optional numerical values for basis points.  If None, symbolic basis points are generated.\n",
    "    \n",
    "    :return: Returns a Lagrange interpolation polynomial of order, p, for the specified basis point, {i : 0 < i <= p}\n",
    "    \n",
    "    http://www.longqi.cf/python/2014/03/24/implement-of-lagrange-polynomial-in-sympy/\n",
    "    \"\"\"\n",
    "    from sympy import symbols, prod\n",
    "    \n",
    "    if xi is None:\n",
    "        xi = symbols('{}:{}'.format(str(x),p+1))\n",
    "    index = list(range(p + 1))\n",
    "    index.pop(i)\n",
    "    \n",
    "    return prod([(x-xi[j])/(xi[i]-xi[j]) for j in index])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Element Lagrange Basis Points\n",
    "elbp = [sp.nsimplify(i) for i in np.linspace(0,1,p+1)]\n",
    "z = sp.Symbol('z')\n",
    "lps = sp.Matrix([[lagrange_poly(p,i,x=s,xi=elbp) for i in range(p+1)]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Ne = TensorProduct(sp.eye(nstates),lps)\n",
    "Xe  = sp.Matrix([sp.Function('x{}'.format(i))(t) for i in np.arange((p+1)*nstates)+1])\n",
    "Xet = sp.Matrix([sp.Function('x{}'.format(i))(t) for i in np.arange(nstates*(p+1)+2,2*(nstates*(p+1))+2)+1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Substitution Dictionaries for for u_i(s,t) -> L(s)*x_i(t) and y_i(s,t) -> L(s)*x_i(t)\n",
    "uu_subs = dict(zip(uu,list(Ne*Xe) + [sp.Function('x{}'.format(nstates*(p+1)+1))(t), \n",
    "                                     sp.Function('x{}'.format(nstates*(p+1)+2))(t)]))\n",
    "yy_subs = dict(zip(yy,list(Ne*Xet) + [sp.Function('x{}'.format(2+2*nstates*(p+1)+1))(t), \n",
    "                                     sp.Function('x{}'.format(2+2*nstates*(p+1)+2))(t)]))\n",
    "# Append the forcing terms to the end of the discretized state vector\n",
    "Xe = sp.Matrix(list(Xe) + [sp.Function('x{}'.format(nstates*(p+1)+1))(t), \n",
    "                           sp.Function('x{}'.format(nstates*(p+1)+2))(t)])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Discretized State Vectors\n",
    "xoft = [sp.Function('x{}'.format(i))(t) for i in np.arange(nstates*(p+1)+2)+1]\n",
    "yoft = [sp.Function('x{}'.format(i))(t) for i in np.arange(nstates*(p+1)+2,2*(nstates*(p+1)+2))+1]\n",
    "xk = [sp.Symbol('x[{},k]'.format(i)) for i in np.arange(2*nstates*(p+1)+4)+1]\n",
    "# Derivatives of Discretized State Vectors\n",
    "dxk = [sp.Symbol('x[{},k]'.format(i)) for i in np.arange(nstates*(p+1)+2,2*(nstates*(p+1)+2))+1]\n",
    "\n",
    "# Substitution dictionaries for x_i(t) -> x[1, k] and (d/dt)x_i(t) -> x[i+(p*nstates+2), k]\n",
    "xk_subs = dict(zip(xoft,xk))\n",
    "dxk_subs = dict(zip(Xe.diff(t),dxk))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Weight Function times original function\n",
    "Bs = Ne.transpose()*sp.Matrix(FF[0:7]).subs(subAB)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Sub in for nu and nus since they depend on state vectors and \n",
    "# have partials that will need evaluated\n",
    "Bs = Bs.subs(subnu)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Appending Forcing Functions to end of weak form of equations\n",
    "B = sp.Matrix(list(Bs) + list(FF[7:9]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Convert u's to discretized x's\n",
    "B = B.subs(uu_subs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Convert y's to discretized x's\n",
    "B = B.subs(yy_subs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Evaluate partial derivatives w/r/t s\n",
    "B = B.doit()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Construct matrices of xk's, separated by x and y indices for Jacobians\n",
    "xtmat = sp.Matrix(xoft)\n",
    "ytmat = sp.Matrix(yoft)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Jx = B.jacobian(xtmat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Jy = B.jacobian(ytmat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "pmat = sp.Matrix(pp).subs(suba)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Bp = B.subs(subsyms).subs(suba)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Need to make the substitution that a2 = a2*a10 prior to evaluating Jacobian\n",
    "### Also need to do the sub for z = s/l (mentioned above) before evaluating"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "Jp = Bp.jacobian(pmat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sympy.matrices import SparseMatrix\n",
    "\n",
    "Jps = SparseMatrix(Jp)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAAoAAAAOBAMAAADkjZCYAAAAMFBMVEX///8AAAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAv3aB7AAAAD3RSTlMAEJmJZjLNVN0i77ur\nRHZ72Yd1AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAVElEQVQIHWNgEDIxZWBgSGeQmMDAsoCBOYGB\n+wAD+0cG/gMMvN8Z5BUYeP8xzDdgYP3MMF8BREJEgLLs3xm4NzCwfATpYkpgYGhnkApgYBB+d5QB\nAPogE3QldevOAAAAAElFTkSuQmCC\n",
      "text/latex": [
       "$$0$$"
      ],
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 47,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Jps[:,10].nnz()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now consider the optimality system defined by  \n",
    "\n",
    "<center>  \n",
    "max $ \\mathcal{L}\\left(\\frac{1}{2}\\right) \\cdot u_{2,k}\\left(1\\right) = r^{T} C_{2} u_{k}\\left(1\\right) $  \n",
    "</center>  \n",
    "\n",
    "where $r^{T}$ is shorthand for $ \\mathcal{L}\\left(\\frac{1}{2}\\right) $ and $ C_{2} $ is a rectangular matrix that isolates the $ u_{2,k} $ portion of the $ u_{k} $ vector.\n",
    "\n",
    "subject to:  \n",
    "\n",
    "<center>  \n",
    "\n",
    "$\\begin{bmatrix}\n",
    "F_{k}(u_{k},y_{k},p) \\\\\n",
    "\\dot{u}_{k} - Ty_{k}\n",
    "\\end{bmatrix} = 0$,\n",
    "$ \\Psi = u_{k}\\left(0\\right) - u_{k}\\left(1\\right) = 0 $,\n",
    "$ \\mathcal{L}\\left(\\frac{1}{2}\\right) \\cdot u_{7,k}\\left(1\\right) = r^{T} C_{7} u_{k}\\left(1\\right) = 0 $\n",
    "</center>  \n",
    "\n",
    "where $ C_{7} $, is a rectangular matrix that isolates the $ u_{7,k} $ portion of the $ u_{k} $ vector.\n",
    "\n",
    "Introduce an additional set of continuation variables, $\\mu$, with the intent of using them to track the objective function and parameter values during continuation:\n",
    "<center>\n",
    "$  r^{T} C_{2} u_{k}\\left(1\\right) - \\mu_{1} $  \n",
    "$ p - \\mu_{2} $\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define a Lagrangian of this system:\n",
    "\n",
    "$ L = \n",
    "\\mu_{1} \n",
    "+ \\int_{0}^{1}\\lambda_{11}^{T} F_{k}\\, dt \n",
    "+ \\int_{0}^{1}\\lambda_{12}^{T}\\,\\left(\\dot{u}_{k} - Ty_{k}\\right)\\, dt \n",
    "+ \\lambda_{21}^{T} \\left(u_{k}\\left(0\\right) - u_{k}\\left(1\\right)\\right) \n",
    "+ \\lambda_{22}r^{T} C_{7} u_{k}\\left(1\\right) \n",
    "+ \\eta_{1} \\left(r^{T} C_{2} u_{k}\\left(1\\right) - \\mu_{1} \\right) \n",
    "+ \\eta_{2}^{T} \\left(p-\\mu_{2}\\right) $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First order optimality conditions:\n",
    "\n",
    "$ \\delta L = 0 $  \n",
    "\n",
    "$ \\delta L = \n",
    "\\delta \\mu_{1} \n",
    "+  \\int_{0}^{1}\\delta \\lambda_{11}^{T} F_{k}\\, dt  \n",
    "+ \\int_{0}^{1} \\lambda_{11}^{T} \\dfrac{\\partial F_{k}}{\\partial u_{k}} \\delta u_{k}\\, dt \n",
    "+ \\int_{0}^{1}\\lambda_{11}^{T} \\dfrac{\\partial F_{k}}{\\partial y_{k}} \\delta y_{k}\\, dt\n",
    "+ \\int_{0}^{1}\\lambda_{11}^{T} \\dfrac{\\partial F_{k}}{\\partial p} \\delta p\\, dt\n",
    "+ \\int_{0}^{1}\\delta \\lambda_{12}^{T}\\, \\left(\\dot{u}_{k} - Ty_{k}\\right)\\, dt\n",
    "+ \\int_{0}^{1}\\lambda_{12}^{T}\\, \\left(\\delta \\dot{u}_{k} - y_{k} \\delta T - T \\delta y_{k} \\right)\\, dt\n",
    "+ \\delta \\lambda_{21}^{T} \\left(u_{k}\\left(0\\right) - u_{k}\\left(1\\right)\\right)\n",
    "+ \\lambda_{21}^{T} \\left(\\delta u_{k}\\left(0\\right) - \\delta u_{k}\\left(1\\right)\\right)\n",
    "+ \\delta \\lambda_{22} r^{T} C_{7} \\delta u_{k}\\left(1\\right)\n",
    "+ \\lambda_{22} r^{T} C_{7} \\delta u_{k}\\left(1\\right)\n",
    "+ \\delta \\eta_{1} \\left(r^{T} C_{2} u_{k}\\left(1\\right) - \\mu_{1}\\right)\n",
    "+ \\eta_{1} \\left(r^{T} C_{2} \\delta u_{k}\\left(1\\right) - \\delta \\mu_{1}\\right) + \\delta \\eta_{2}^{T} \\left(p-\\mu_{2}\\right) + \\eta_{2}^{T} \\left(\\delta p - \\delta \\mu_{2} \\right) $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Integrate the $\\delta \\dot{u}$ by parts:  \n",
    "\n",
    "$\\int_{0}^{1} \\lambda_{12}^{T} \\delta \\dot{u}_{k} = \\lambda_{12}^{T} \\delta u_{k}|_{0}^{1}  - \\int_{0}^{1}  \\dot{\\lambda}_{12}^{T} \\delta u_{k} dt = \\lambda_{12}^{T}\\left(1\\right) \\delta u_{k} \\left(1\\right) - \\lambda_{12}^{T}\\left(0\\right) \\delta u_{k} \\left(0\\right) - \\int_{0}^{1}  \\dot{\\lambda}_{12}^{T} \\delta u_{k} dt $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plugging the above into the equation:  \n",
    "\n",
    "$ \\delta L = \n",
    "\\delta \\mu_{1} \n",
    "+  \\int_{0}^{1}\\delta \\lambda_{11}^{T} F_{k}\\, dt  \n",
    "+ \\int_{0}^{1} \\lambda_{11}^{T} \\dfrac{\\partial F_{k}}{\\partial u_{k}} \\delta u_{k}\\, dt \n",
    "+ \\int_{0}^{1}\\lambda_{11}^{T} \\dfrac{\\partial F_{k}}{\\partial y_{k}} \\delta y_{k}\\, dt\n",
    "+ \\int_{0}^{1}\\lambda_{11}^{T} \\dfrac{\\partial F_{k}}{\\partial p} \\delta p\\, dt\n",
    "+ \\int_{0}^{1}\\delta \\lambda_{12}^{T}\\, \\left(\\dot{u}_{k} - Ty_{k}\\right)\\, dt\n",
    "+ \\lambda_{12}^{T}\\left(1\\right) \\delta u_{k} \\left(1\\right) \n",
    "- \\lambda_{12}^{T}\\left(0\\right) \\delta u_{k} \\left(0\\right) \n",
    "- \\int_{0}^{1}  \\dot{\\lambda}_{12}^{T} \\delta u_{k} dt\n",
    "+ \\int_{0}^{1}\\lambda_{12}^{T}\\, \\left(- y_{k} \\delta T - T \\delta y_{k} \\right)\\, dt\n",
    "+ \\delta \\lambda_{21}^{T} \\left(u_{k}\\left(0\\right) - u_{k}\\left(1\\right)\\right)\n",
    "+ \\lambda_{21}^{T} \\left(\\delta u_{k}\\left(0\\right) - \\delta u_{k}\\left(1\\right)\\right)\n",
    "+ \\delta \\lambda_{22} r^{T} C_{7} u_{k}\\left(1\\right)\n",
    "+ \\lambda_{22} r^{T} C_{7} \\delta u_{k}\\left(1\\right)\n",
    "+ \\delta \\eta_{1} \\left(r^{T} C_{2} u_{k}\\left(1\\right) - \\mu_{1}\\right)\n",
    "+ \\eta_{1} \\left(r^{T} C_{2} \\delta u_{k}\\left(1\\right) - \\delta \\mu_{1}\\right) + \\delta \\eta_{2}^{T} \\left(p-\\mu_{2}\\right) + \\eta_{2}^{T} \\left(\\delta p - \\delta \\mu_{2} \\right) $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Collect Terms\n",
    "Original System:\n",
    "\n",
    "$ \\delta \\eta_{1}: r^{T}\\,C_{2}\\,u_{k}\\left(1\\right) - \\mu_{1} = 0 \\rightarrow 1$ equation\n",
    "\n",
    "$ \\delta \\lambda_{11}: F_{k} = 0 \\rightarrow 7M(p+1)+2$ equations\n",
    "\n",
    "$ \\delta \\lambda_{12}: \\dot{u}_{k} - Ty_{k} = 0 \\rightarrow 7M(p+1)+2$ equations\n",
    "\n",
    "$ \\delta \\lambda_{21}^{T}: u_{k}\\left(0\\right) - u_{k}\\left(1\\right) = 0 \\rightarrow 7M(p+1)+2$ equations  \n",
    "\n",
    "$ \\delta \\lambda_{22}: r^{T}\\,C_{7}\\,u_{k}\\left(1\\right) = 0 \\rightarrow  1$ equation\n",
    "\n",
    "$ \\delta \\eta_{2}: p-\\mu_{2} = 0 \\rightarrow  q$ equations\n",
    "\n",
    "Adjoint System\n",
    "\n",
    "$ \\delta \\mu_{1}: 1 - \\eta_{1} = 0 \\rightarrow 1$ equation\n",
    "\n",
    "$ \\delta \\mu_{2}: -\\eta_{2}^{T} = 0 \\rightarrow q$ equations\n",
    "\n",
    "$ \\delta u_{k}: \\lambda_{11}^{T} \\dfrac{\\partial F_{k}}{\\partial u_{k}} - \\dot{\\lambda}_{12}^{T} = 0 \\rightarrow 7M(p+1) + 2$ equations,\n",
    "\n",
    "$ \\delta y_{k}: \\lambda_{11}^{T} \\dfrac{\\partial F_{k}}{\\partial y_{k}} - \\lambda_{12}^{T} \\, T  = 0 \\rightarrow 7M(p+1) + 2 $ equations\n",
    "\n",
    "$ \\delta p:  \\lambda_{11}^{T} \\dfrac{\\partial F_{k}}{\\partial p} + \\eta_{2}^{T} = 0 \\rightarrow q $ equations\n",
    "\n",
    "$ \\delta u_{k} \\left(1\\right)+\\delta u_{k} \\left(0\\right): \\lambda_{12}^{T}\\left(1\\right) - \\lambda_{12}^{T}\\left(0\\right) + \\lambda_{22} r^{T}C_{7} + \\eta_{1} r^{T} C_{2} = 0 \\rightarrow 7M(p+1) $ equations  \n",
    "\n",
    "$ \\delta T: -\\lambda_{12}^{T} y_{k} = 0 $  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variable Sizes\n",
    "\n",
    "$ u_{k} \\in R^{\\left(7M(p+1)+2\\right)} $  \n",
    "\n",
    "$ y_{k} \\in R^{\\left(7M(p+1)+2\\right)} $  \n",
    "\n",
    "$ p \\in R^{q} $  \n",
    "\n",
    "$ \\lambda_{11} \\in R^{\\left(7M(p+1)+2\\right)} $  \n",
    "\n",
    "$ \\lambda_{12} \\in R^{\\left(7M(p+1)+2\\right)} $  \n",
    "\n",
    "$ \\lambda_{21} \\in R^{\\left(7M(p+1)+2\\right)} $  \n",
    "\n",
    "$ \\lambda_{22} \\in R $  \n",
    "\n",
    "$ \\eta_{1} \\in R $  \n",
    "\n",
    "$ \\eta_{2} \\in R^{q} $  \n",
    "\n",
    "Notes on equation counts and variable sizes:\n",
    "* 7 = number of states with spatial dependence,  \n",
    "* M = number of elements, p = order of polynomial,  \n",
    "* q = number of parameters,  \n",
    "* +2 is for forcing functions that are only time dependent"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### Next Steps\n",
    "1. Generate Code from Matrices \n",
    "   * May not even need to do this.  Required Matrices are same as those Mehdi already generated\n",
    "   * Make necessary substitutions to make $\\frac{\\partial{J}}{\\partial{p}}$ to come out like Mehdi's (a2 = a2*a10, z = s/l)\n",
    "   * matlab code, x[1,2] -> x(2,3,:), **->^ and various other substitutions\n",
    "2. Start writing matlab functions for adjoint equations\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [Root]",
   "language": "python",
   "name": "Python [Root]"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
