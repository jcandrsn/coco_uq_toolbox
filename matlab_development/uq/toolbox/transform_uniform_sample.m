function sample = transform_uniform_sample(sample, mu, CoV)
%  Linearly transform standard uniform random variable to non-standard 
%  uniform random variable
%     
%  Parameters
%  ----------
%  sample : array
%      Array of random variables uniformly distributed between -1 and 1
%  mu : float
%      Average value of transformed uniform distribution
%  CoV : float
%      Coefficient of Variance of transformed uniform distribution
%         
%  Returns
%  -------
%  out : array
%      Array of non-standard uniform random variates in the same array 
%      shape as sample
%
%  jca - 8/19/2017 - confirmed equivalence to original python function
%  Python test:
%  >>>transform_sample(np.array([[1,2,3,4],[5,6,7,8]]),1,0.5)
%  array([[ 1.8660254 ,  2.73205081,  3.59807621,  4.46410162],
%         [ 5.33012702,  6.19615242,  7.06217783,  7.92820323]])
%  Matlab test:
%  >>transform_uniform_sample([[1,2,3,4];[5,6,7,8]]),1,0.5)
% 
%  ans =
% 
%      1.8660    2.7321    3.5981    4.4641
%      5.3301    6.1962    7.0622    7.9282

sample = (1+sqrt(3)*sample*CoV)*mu;
end