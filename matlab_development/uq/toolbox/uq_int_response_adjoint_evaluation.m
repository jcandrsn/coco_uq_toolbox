function [data, J] = uq_int_response_adjoint_evaluation(prob, data, u)
  
  J = zeros(data.nsamples, data.resp.adjt_size);

  for i=1:data.nsamples
    T = u(data.resp.T_idx(i,1));
    x = u(data.resp.xbp_idx(i,1):data.resp.xbp_idx(i,end));
    p = u(data.resp.p_idx(i,1):data.resp.p_idx(i,end));
    
    fdata = coco_get_func_data(prob, ...
      coco_get_id(data.sids{i}, 'coll'), 'data');

    [J_T, J_xcn, J_p] = int_resp_adjoint(data, fdata, T, x, p);

    J(i, i) = J_T;
    J(i, data.resp.xcn_idx(i,1):data.resp.xcn_idx(i,2)) = J_xcn;
    J(i, data.resp.p_adjt_idx(i,1):data.resp.p_adjt_idx(i,2)) = J_p;
  end
  J = -1*data.wtd_psi_mat*J;
end

function [J_T, J_xcn, J_p] = int_resp_adjoint(uq_data, fdata, T, x, p)

maps = fdata.coll_seg.maps;
int  = fdata.coll_seg.int;

xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
pp  = repmat(p, maps.p_rep);

wts1 = repmat(int.wt',[maps.NTST,1]);

resp  = uq_data.rhan(xx, pp); 
drdx = uq_data.drdxhan(xx, pp);  
drdp = uq_data.drdphan(xx, pp);
 
J_T   = 1/(2*maps.NTST)*resp*wts1;
J_xcn = T/(2*maps.NTST)*drdx(:)';
J_p   = T/(2*maps.NTST)*drdp(:)'*kron(wts1,eye(maps.pdim));

end

function [J_T, J_xcn, J_p] = int_resp_adjoint_nu(uq_data, fdata, T, x, p)
% For non-uniform mesh
maps = fdata.coll_seg.maps;
mesh = fdata.coll_seg.mesh;

xx = reshape(maps.W*x, maps.x_shp);
pp  = repmat(p, maps.p_rep);

gcn = uq_data.rhan(xx, pp);
gcn = mesh.gka.*gcn;

gdxcn = uq_data.drdxhan(xx, pp);
gdxcn = mesh.gdxka.*gdxcn;

gdp = uq_data.drdphan(xx, pp);
gdp = mesh.gdpka.*gdp;
gdprows = kron(ones(maps.pdim,1), 1:maps.p_rep(2));
gpdcols = kron(ones(maps.p_rep),1:maps.pdim);
gdp = sparse(gdprows, gpdcols, gdp(:));

J_T   = (0.5/maps.NTST)*mesh.gwt*gcn';
J_xcn = (0.5*T/maps.NTST)*gdxcn(:);
J_p   = (0.5*T/maps.NTST)*mesh.gwt*gdp;

end