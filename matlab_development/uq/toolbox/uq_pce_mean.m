function [data, y] = uq_pce_mean(prob, data, u)
%UQ_PCE_MEAN Zero function to calculated mean value of PCE Expansion
%  Given an array of PCE coefficients return the mean value of the
%  expansion (first term in the expansion)

alpha0 = u(1);
mu = u(end);
y = mu - alpha0;

end