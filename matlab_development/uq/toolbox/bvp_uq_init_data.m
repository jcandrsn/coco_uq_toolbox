function data = bvp_uq_init_data(src_data, oid, varargin)
% Works for a single cid
% data = coco_func_data('protect');
data.oid = oid;

fields = varargin;
for i=1:numel(fields)
  field = fields{i};
  if isfield(src_data, field)
    data.(field) = src_data.(field);
  else
    data.(field) = struct();
  end
end

% UQ requires pnames as well.
data.pnames     = src_data.pnames;
data.xdim       = size(src_data.bvp_bc.x0_idx, 1);
data.pdim       = size(src_data.bvp_bc.p_idx, 1);

end