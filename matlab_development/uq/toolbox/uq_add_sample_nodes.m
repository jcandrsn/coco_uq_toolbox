function prob = uq_add_sample_nodes(prob, data, args)

% This function ties the values of parameters in the
% collocation zero problems to parameters that update when
% distribution parameters change.

if nargin < 3
  args = {};
end

% A kludge for checking if tangent diretions need to be
% added.  This is only true if a run and label are passed
% in varargin and then only if the type of point that the
% run and label represent is a Branch Point ('BP')
add_tl0 = 0;

if isfield(args, 'run') && isfield(args, 'lab')
  chart = coco_read_solution(args.run, args.lab, 'chart');
  if strcmpi(chart.pt_type, 'BP')
    add_tl0 = 1;
  end
end

if data.num_normals > 0
    muid = coco_get_id(data.oid, 'mus');
    sigid = coco_get_id(data.oid, 'sigs');
    mu = data.spdp(data.normal_par_idx,1);
    sig = data.spdp(data.normal_par_idx,2);    
    
    normal_names = data.spnames(data.normal_par_idx);
    munames = strcat('mu.', normal_names);
    signames = strcat('sig.', normal_names);
    prob = coco_add_pars(prob, muid, munames, mu);
    prob = coco_add_pars(prob, sigid, signames, sig);

    muidx = coco_get_func_data(prob, muid, 'uidx');
    sigidx = coco_get_func_data(prob, sigid, 'uidx');
    normal_sample_id = coco_get_id(data.oid, 'normal_sample');
    
    prob = coco_add_func(prob, normal_sample_id, @uq_normal_sample, ...
      @uq_normal_sample_dU, @uq_normal_sample_dUdU, data, 'zero', ...
      'uidx', [muidx;sigidx], 'u0', data.nds(data.normal_nds_idx));

    if data.addadjt
      % Add the adjoint for the above function
      if isfield(args, 'run')
        chart = coco_read_solution(args.run, args.lab, 'chart');
        cdata = coco_get_chart_data(chart, 'lsol');        
        [mu_chart, mu_lidx] = coco_read_adjoint(muid, args.run, args.lab, ...
          'chart', 'lidx');
        mu_l0 = mu_chart.x;
        
        [sig_chart, sig_lidx] = coco_read_adjoint(sigid, args.run, args.lab, ...
          'chart', 'lidx');
        sig_l0 = sig_chart.x;

        [norm_chart, normal_lidx] = coco_read_adjoint(normal_sample_id, args.run, args.lab, ...
          'chart', 'lidx');
        norm_l0 = norm_chart.x;
        if add_tl0
          mu_tl0 = cdata.v(mu_lidx);
          sig_tl0 = cdata.v(sig_lidx);
          norm_tl0 = cdata.v(normal_lidx);
        end
      else
        mu_l0 = zeros(size(mu));
        sig_l0 = zeros(size(sig));
        norm_l0 = zeros(size(data.normal_nds_idx'));
      end
      if add_tl0
        prob = coco_add_adjt(prob, muid, coco_get_id('d', munames), ...
          'l0', mu_l0, 'tl0', mu_tl0);
        prob = coco_add_adjt(prob, sigid, coco_get_id('d', signames), ...
          'l0', sig_l0, 'tl0', sig_tl0);
        amuidx = coco_get_adjt_data(prob, muid, 'axidx');
        asigidx = coco_get_adjt_data(prob, sigid, 'axidx');
        prob = coco_add_adjt(prob, normal_sample_id, 'aidx', ...
          [amuidx; asigidx], 'l0', norm_l0, 'tl0', norm_tl0);
      else
        prob = coco_add_adjt(prob, muid, coco_get_id('d', munames), ...
          'l0', mu_l0);
        prob = coco_add_adjt(prob, sigid, coco_get_id('d', signames), ...
          'l0', sig_l0);
        amuidx = coco_get_adjt_data(prob, muid, 'axidx');
        asigidx = coco_get_adjt_data(prob, sigid, 'axidx');
        prob = coco_add_adjt(prob, normal_sample_id, 'aidx', ...
          [amuidx; asigidx], 'l0', norm_l0);
      end
    end
    uidx = coco_get_func_data(prob, normal_sample_id, 'uidx');
    % Store the parameter index of the normal nodes for later use of
    % gluing them to collocation parameters
    data.sample_par_idx(data.normal_nds_idx) = ...
        uidx((2*data.num_normals+1):end);
    if data.addadjt
      aidx = coco_get_adjt_data(prob, normal_sample_id, 'axidx');
      data.adjt_sample_par_idx(data.normal_nds_idx) = ...
        aidx((2*data.num_normals+1):end);
    end      
end

if data.num_uniforms > 0
  % Function names for the distribution parameters
  upid = coco_get_id(data.oid, 'up');
  loid = coco_get_id(data.oid, 'lo');

  % Values for the distibution parmaeters
  lo = data.spdp(data.uniform_par_idx,1);
  up = data.spdp(data.uniform_par_idx,2);

  % Names for the parameters
  uniform_names = data.spnames(data.uniform_par_idx);
  lonames = strcat('lo.', uniform_names);
  upnames = strcat('up.', uniform_names);

  % Add the distribution parameters as inactive
  % continuation parameters
  prob = coco_add_pars(prob, loid, lonames, lo);
  prob = coco_add_pars(prob, upid, upnames, up);

  % Get the index of the distribution parameters
  loidx = coco_get_func_data(prob, loid, 'uidx');
  upidx = coco_get_func_data(prob, upid, 'uidx');
  uniform_sample_id = coco_get_id(data.oid, 'uniform_sample');

  % Function to tie the values of the parameters
  % individual samples to the values of the distribution
  % parameters so that if a distribution parameter
  % changes, the parameters values in the samples change
  % as well.
  prob = coco_add_func(prob, uniform_sample_id, @uq_uniform_sample, ...
    @uq_uniform_sample_dU, @uq_uniform_sample_dUdU, data, 'zero', ...
    'uidx', [loidx;upidx], 'u0', data.nds(data.uniform_nds_idx));

  if data.addadjt
    % Add the adjoint for the above functions
    if isfield(args, 'run')
      chart = coco_read_solution(args.run, args.lab, 'chart');
      cdata = coco_get_chart_data(chart, 'lsol');        
      [lo_chart, lo_lidx] = coco_read_adjoint(loid, args.run, ...
        args.lab, 'chart', 'lidx');
      lo_l0 = lo_chart.x;

      [up_chart, up_lidx] = coco_read_adjoint(upid, args.run, ...
        args.lab, 'chart', 'lidx');
      up_l0 = up_chart.x;

      [unif_chart, unif_lidx] = coco_read_adjoint(uniform_sample_id, ...
        args.run, args.lab, 'chart', 'lidx');
      unif_l0 = unif_chart.x;
      if add_tl0
        lo_tl0 = cdata.v(lo_lidx);      
        up_tl0 = cdata.v(up_lidx);
        unif_tl0 = cdata.v(unif_lidx);
      end
    else
      lo_l0 = zeros(size(lo));
      up_l0 = zeros(size(up));
      unif_l0 = zeros(size(data.uniform_nds_idx'));
    end
    if add_tl0
      prob = coco_add_adjt(prob, loid, coco_get_id('d', lonames), ...
        'l0', lo_l0, 'tl0', lo_tl0);
      prob = coco_add_adjt(prob, upid, coco_get_id('d', upnames), ...
        'l0', up_l0, 'tl0', up_tl0);
      aloidx = coco_get_adjt_data(prob, loid, 'axidx');
      aupidx = coco_get_adjt_data(prob, upid, 'axidx');
      prob = coco_add_adjt(prob, uniform_sample_id, 'aidx', ...
        [aloidx; aupidx], 'l0', unif_l0, 'tl0', unif_tl0);
    else
      prob = coco_add_adjt(prob, loid, coco_get_id('d', lonames), ...
        'l0', lo_l0);
      prob = coco_add_adjt(prob, upid, coco_get_id('d', upnames), ...
        'l0', up_l0);
      aloidx = coco_get_adjt_data(prob, loid, 'axidx');
      aupidx = coco_get_adjt_data(prob, upid, 'axidx');
      prob = coco_add_adjt(prob, uniform_sample_id, 'aidx', ...
        [aloidx; aupidx], 'l0', unif_l0);
    end
  end
  uidx = coco_get_func_data(prob, uniform_sample_id, 'uidx');
  % Store the parameter index of the uniform nodes for later use of
  % gluing them to collocation parameters 
  data.sample_par_idx(data.uniform_nds_idx) = ...
      uidx((2*data.num_uniforms+1):end);
  if data.addadjt
    aidx = coco_get_adjt_data(prob, uniform_sample_id, 'axidx');
    data.adjt_sample_par_idx(data.uniform_nds_idx) = ...
      aidx((2*data.num_uniforms+1):end);
  end
end

% This glues the values of the stochastic parameters in the collocation
% segments to parameters created with uq_add_sample_nodes which update when 
% the value of a Distribution Parameter is updated as part of continuation
seg2ndsid = coco_get_id(data.oid, 'uq.s_par_glue');
% Parameter indices in the collocation segments:
seg_par_idx = cell2mat(data.s_idx);
% Only the stochastic parameters
seg_par_idx = seg_par_idx(data.sp2p_idx, :);
seg_par_idx = seg_par_idx(:);

seg2ndsidx = [seg_par_idx; data.sample_par_idx];
% Zero function to match parameters boundary value problems
% to transformed random variables.
prob = coco_add_func(prob, seg2ndsid, @uq_seg_to_nds, @uq_seg_to_nds_du, ...
  @uq_seg_to_nds_dudu, data, 'zero', 'uidx', seg2ndsidx);
pnames = uq_get_sample_par_names(data.pnames(data.sp2p_idx), 1:data.nsamples);
nds2smplid = coco_get_id(data.oid, 'uq.nds2samples');

prob = coco_add_pars(prob, nds2smplid, seg_par_idx, pnames, 'active');

if data.addadjt
  adjt_seg_par_idx = cell2mat(data.adj_s_idx);
  adjt_seg_par_idx = adjt_seg_par_idx(data.sp2p_idx, :);
  adjt_seg_par_idx = adjt_seg_par_idx(:);

  if isfield(args, 'run')
    chart = coco_read_solution(args.run, args.lab, 'chart');
    cdata = coco_get_chart_data(chart, 'lsol');        
    [s2n_chart, s2n_lidx] = coco_read_adjoint(seg2ndsid, args.run, args.lab, ...
      'chart', 'lidx');
    s2n_l0 = s2n_chart.x;
    [n2s_chart, n2s_lidx] = coco_read_adjoint(nds2smplid, args.run, args.lab, ...
      'chart', 'lidx');
    n2s_l0 = n2s_chart.x;
    
    if add_tl0
      s2n_tl0 = cdata.v(s2n_lidx);
      prob = coco_add_adjt(prob, seg2ndsid, ...
        'aidx', [adjt_seg_par_idx; data.adjt_sample_par_idx], ...
        'l0', s2n_l0, ...
        'tl0', s2n_tl0);
      
      n2s_tl0 = cdata.v(n2s_lidx);
      prob = coco_add_adjt(prob, nds2smplid, coco_get_id('d',pnames), ...
        'aidx', adjt_seg_par_idx, ...
        'l0', n2s_l0, ...
        'tl0', n2s_tl0);
    else
      prob = coco_add_adjt(prob, seg2ndsid, ...
        'aidx', [adjt_seg_par_idx; data.adjt_sample_par_idx], ...
        'l0', s2n_l0);
      
      prob = coco_add_adjt(prob, nds2smplid, coco_get_id('d',pnames), ...
        'aidx', adjt_seg_par_idx, ...
        'l0', n2s_l0);
    end
  else
    prob = coco_add_adjt(prob, seg2ndsid, 'aidx', ...
      [adjt_seg_par_idx; data.adjt_sample_par_idx]);
    prob = coco_add_adjt(prob, nds2smplid, coco_get_id('d',pnames), ...
      'aidx', adjt_seg_par_idx);
  end
end

end