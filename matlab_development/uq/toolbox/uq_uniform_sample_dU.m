function [data, J] = uq_uniform_sample_dU(prob, data, u)

numnodes = numel(u) - 2*data.num_uniforms;
num_us = size(u, 1);
J = zeros(numnodes, num_us);
% This won't work if there are different numbers of nodes for each random
% variable.
J_spdp = ones(numnodes/data.num_uniforms,2);
J_spdp(:,1) = (1-data.st_nds(1:numnodes/data.num_uniforms))/2;
J_spdp(:,2) = (1+data.st_nds(1:numnodes/data.num_uniforms))/2;

start_nodes = 2*data.num_uniforms + 1;
J(:, 1:data.num_uniforms) = kron(eye(data.num_uniforms), J_spdp(:,1));
J(:, data.num_uniforms+1:2*data.num_uniforms) = kron(eye(data.num_uniforms), J_spdp(:,2));
J(:, start_nodes:end) = -eye(numnodes);

end