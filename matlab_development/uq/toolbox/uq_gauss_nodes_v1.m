function [nds, wts, idx] = uq_gauss_nodes_v1(m, n, type)
% Given a quadrature order, m, dimension, n, and quadrature type, type,
% return the tensor product of the quadrature node locations, nds, and 
% appropriately multiplied quadrature weight, wts. 
%
% m must either be a scalar (equal quadrature order in each dimension) or a
%   vector with numel(m) = n allow each direction to be set independently.
%
% n is an integer value for the number of dimension
%
% type is a string or a cell array of strings (with numel(type) == n) that
%      specify the quadrature rule for each dimension independently.
%
% https://www.mathworks.com/matlabcentral/answers/98191-how-can-i-obtain-all-possible-combinations-of-given-vectors-in-matlab
%
if ischar(type)
    type = cellstr(type);
end
assert(numel(m) == 1 || numel(m) == n, ['Number of entries for m must ' ...
       'either be 1 (equal quadrature order for all variables) ' ...
       'or equal to n (specified quadrature order for each variable)'])

assert(numel(type) == 1 || numel(type) == n, ['Number of entries for ' ... 
       'type must either be 1 (equal distribution for all variables) ' ...
       'or equal to n (specified quadrature order for each variable)'])

nds = cell(1, n);
wts = cell(1, n);
if n > 1
    if numel(m)==1
        m = repmat(m, 1, n);
    end

    if numel(type)==1
        type = repmat(type, 1, n);
    end
end

idx = cellfun(@(x) 1:x, num2cell(m), 'uniformoutput', false);
combinations = cell(1, numel(idx));
[combinations{:}] = ndgrid(idx{:});
combinations = cellfun(@(x) x(:), combinations, 'uniformoutput', false);
idx = [combinations{:}]';
os = cumsum(max(idx')');
idx2 = idx + repmat([0;os(1:end-1)],1,size(idx,2));

for i=1:n
    switch lower(type{i})
        case lower({'Legendre','Le','LeN', 'Uniform', 'U'})
            [nds{i}, wts{i}] = gauss_legendre_nodes(m(i));
        case lower({'Hermite','He','HeN', 'Normal', 'N'})
            [nds{i}, wts{i}] = gauss_hermite_nodes(m(i));
        otherwise
            warning(strcat('Specified Type not supported, Providing',...
                           ' weights and nodes for Gauss-Legendre',...
                           ' quadrature'))
            [nds{i}, wts{i}] = gauss_legendre_nodes(m(i));
    end
end
nds = [nds{:}];
wts = [wts{:}];
nds = nds(idx2);
wts = prod(wts(idx2), 1);

end

function [nds, wts] = gauss_legendre_nodes(m)
num = (1:m-1)';
g = num.*sqrt(1./(4*num.^2-1)); 
J = -diag(g,1)-diag(g,-1);
[w, x] = eig(J);
nds = diag(x)';
% Normalizing the weights so they add up to one.
% The weight function is 1/2 for a uniform distribution between -1 and 1
% instead of the typical uniform distribution between 0 and 1.
wts = (2*w(1,:).^2)/2;
end

function [nds, wts] = gauss_hermite_nodes(m)
num = (1:m-1)';
g = sqrt(num);
J = diag(g,1)+diag(g,-1);
[w, x] = eig(J);
[nds, idx] = sort(diag(x));
nds = nds';
wts = sqrt(2*pi)*w(1,:).^2;
% Normalizing the weights so they add up to one
% The weight function is the pdf of the standard normal distribution
% (1/sqrt(2*pi))*exp((-x.^2)/2).
wts = wts(idx)/(sqrt(2*pi));
end