function data = uq_coll_response_get_idx(prob, data)

  if strcmp(data.response_type,'int')
    data = uq_int_coll_response_get_idx(prob, data);
  elseif strcmp(data.response_type,'bv')
    data = uq_bv_coll_response_get_idx(prob, data);
  end
end

%% Response Evaluated on Boundary Values
function data = uq_bv_coll_response_get_idx(prob, data)

nsamples = data.nsamples;

fdata = coco_get_func_data(prob, coco_get_id(data.sids{1}, 'coll'), ...
  'data');

data.resp.uidx = zeros(nsamples, 2 + 2*fdata.xdim + fdata.pdim);

for i=1:nsamples
  [uidx, fdata] = coco_get_func_data(prob, ...
    coco_get_id(data.sids{i}, 'coll'), 'uidx', 'data');
  maps = fdata.coll_seg.maps;
  
  data.resp.uidx(i,1) = uidx(maps.T0_idx);
  data.resp.uidx(i,2) = uidx(maps.T_idx);
  data.resp.uidx(i,3:2+fdata.xdim) = uidx(maps.x0_idx);
  data.resp.uidx(i,2+fdata.xdim+1:2+2*fdata.xdim) = uidx(maps.x1_idx);
  data.resp.uidx(i,2+2*fdata.xdim+1:2+2*fdata.xdim+fdata.pdim) = uidx(maps.p_idx);
  
end
data.resp.uidx = data.resp.uidx';
data.resp.uidx = data.resp.uidx(:);

data.resp.nargs  = (nargin(data.rhan)==5);

end


%% Response Evaluated Over Entire Trajectory
function data = uq_int_coll_response_get_idx(prob, data)

nsamples = data.nsamples;

T_idx = cell(1, nsamples);
xbp_idx = cell(1, nsamples);
p_idx = cell(1, nsamples);

data.resp.T_idx = ones(nsamples,1);
% Collect collocation node sizes in case they're needed for an adjoint.
data.resp.xcn_idx = ones(nsamples,2); 
data.resp.xbp_idx = ones(nsamples,2);
data.resp.p_idx = ones(nsamples, 2);
adjt_size = 0;
for i=1:nsamples
  [uidx, fdata] = coco_get_func_data(prob, ...
    coco_get_id(data.sids{i}, 'coll'), 'uidx', 'data');
  maps = fdata.coll_seg.maps;
  data.resp.T_idx(i) = i;
  data.resp.xcn_idx(i,2) = size(maps.W,1);
  data.resp.xbp_idx(i,2) = size(maps.W,2);
  data.resp.p_idx(i,2) = size(maps.p_idx,1);
  adjt_size = adjt_size + 1 + size(maps.W,1) + size(maps.p_idx,1);
  
  T_idx{i} = uidx(maps.T_idx);
  xbp_idx{i} = uidx(maps.xbp_idx);
  p_idx{i} = uidx(maps.p_idx);
  
end

data.resp.xcn_idx(:,2) = cumsum(data.resp.xcn_idx(:,2));
data.resp.xcn_idx(2:end,1) = data.resp.xcn_idx(2:end,1) + data.resp.xcn_idx(1:end-1,2);
data.resp.xcn_idx = data.resp.xcn_idx + data.resp.T_idx(end);
data.resp.adjt_size = adjt_size;

data.resp.xbp_idx(:,2) = cumsum(data.resp.xbp_idx(:,2));
data.resp.xbp_idx(2:end,1) = data.resp.xbp_idx(2:end,1) + data.resp.xbp_idx(1:end-1,2);
data.resp.xbp_idx = data.resp.xbp_idx + data.resp.T_idx(end);

data.resp.p_idx(:,2) = cumsum(data.resp.p_idx(:,2));
data.resp.p_idx(2:end,1) = data.resp.p_idx(2:end,1) + data.resp.p_idx(1:end-1,2);
data.resp.p_adjt_idx = data.resp.p_idx + data.resp.xcn_idx(end);
data.resp.p_idx = data.resp.p_idx + data.resp.xbp_idx(end);

data.resp.uidx = vertcat(T_idx{:}, xbp_idx{:}, p_idx{:});

end