function data = uq_bvp_gen_samples(data, prob, str)
% Perform continuation to generate convergent initial guesses for all
% parameter space samples
tbid = coco_get_id(data.oid, 'uq');
num_pars = numel(data.spnames);
runs_per_parameter = cumprod([1,data.uq.M(1:end-1)]);
total_runs = sum(runs_per_parameter);
run_names = cell(1, total_runs);

% Kludge for getting 'important' coll and ode settings.
% Was previously starting from the passed instance of prob,
% which carried all of the problem settings with it.
% Realized that leads to an ever increasing problem size if
% you add more than one samples and the problems are
% potentially not closed.  Changing to a temporary problem
% instance which itself must be closed for continuation.
% This allows for a degree of embedability, but seems
% really shaky.  Would be nice if a utility function
% existed for copying settings from another problem
% instance, but I haven't found it.
prob_init = coco_prob();
% ODE settings
for setting={'autonomous', 'vectorized'}
  value = coco_get(prob, 'ode', setting{:});
  if ~isempty(value)
    prob_init = coco_set(prob_init, 'ode', setting{:}, value);
  end
end

% COLL settings
for setting={'NCOL', 'NTST', 'var'}
  value = coco_get(prob, 'coll', setting{:});
  if ~isempty(value)
    prob_init = coco_set(prob_init, 'coll', setting{:}, value);
  end
end

pnum = 1;
k = 1;
while pnum <= num_pars    
    temp_bds = cell(1, runs_per_parameter(pnum));
    idx = false(1, num_pars);
    idx(pnum) = true;
    idx = repelem(idx, data.uq.M);
    vals = data.nds(idx);
    low = min(vals);
    high = max(vals);
    msg = sprintf('\n%s: Generating sample points for parameter %s\n', ...
                  tbid, data.spnames{pnum});    
    fprintf(msg)
    if pnum == 1
        run_names{k} = [tbid, '_samples_p', int2str(pnum), '_run', int2str(1)];
        temp_prob2 = prob_init;
        temp_prob2 = ode_isol2bvp(temp_prob2, 'sample', str);       
        [~, opts] = uq_parse_str(str);
        pnames = {data.spnames{pnum}, data.pnames{data.dpar_idx}};
        
        if opts.addadjt
          temp_prob2 = adjt_isol2bvp(temp_prob2, 'sample');
          dnames = coco_get_id('d', data.pnames(~data.dpar_idx));
          pnames = {pnames{:}, dnames{:}};
        end
        temp_prob2 = coco_add_event(temp_prob2, 'UQ', ...
            data.spnames{pnum}, vals);
        temp_bds{1} = coco(temp_prob2, run_names{k}, [], 1, pnames, ...
            [low, high]);
        k = k+1;
    else
        run_count = 1;
        for run=1:runs_per_parameter(pnum-1)
            labs = coco_bd_labs(bds{run}, 'UQ');
            for lab = labs
                run_names{k} = [tbid, '_samples_p', int2str(pnum), ...
                    '_run', int2str(run_count)];
                previous_run_name = [tbid, '_samples_p', ...
                    int2str(pnum-1), '_run', int2str(run)];
                temp_prob2 = prob_init;
                temp_prob2 = ode_bvp2bvp(temp_prob2, 'sample', ...
                    previous_run_name, lab);
                pnames = {data.spnames{pnum}};
                pnames = {pnames{:}, data.pnames{data.dpar_idx}};
                if opts.addadjt
                  temp_prob2 = adjt_bvp2bvp(temp_prob2, 'sample', ...
                  previous_run_name, lab);
                  dnames = coco_get_id('d', data.pnames);
                  pnames = {pnames{:}, dnames{:}};
                end
                pnames = {pnames{:}, data.spnames{1:pnum-1}};
                temp_prob2 = coco_add_event(temp_prob2, 'UQ', ...
                    data.spnames{pnum}, vals);
                temp_bds{run_count} = coco(temp_prob2, run_names{k}, [], ...
                    1, pnames, ...
                    [low, ...
                     high]);
                run_count = run_count + 1;
                k = k+1;
            end

        end
    end
    bds = temp_bds;
    pnum = pnum+1;
end

% Delete the continuation result data folders for all but the last
% parameter.
for i=1:(numel(run_names) - runs_per_parameter(end))
    try
        rmdir(['data/', run_names{i}], 's')
    catch
        % In case there is an access issue, just leave the folders
    end
end

data.sample_run_names = run_names(end-runs_per_parameter(end)+1:end);

end