function [data, dJ] = uq_pce_variance_dUdU(prob, data, u)
%UQ_PCE_VARIANCE_DUDU Jacobian of the variance of a PCE w/r/t alphas

dJ = zeros(1, numel(u), numel(u));
dJ(1, 2:end-1, 2:end-1) = -2*eye(data.Nt - 1);

end

