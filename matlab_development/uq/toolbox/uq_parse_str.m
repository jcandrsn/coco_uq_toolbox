function [uqdata, opts] = uq_parse_str(str)

  grammar   = 'SPNAMES SPDIST SPDP [DPARNAMES] [OPTS]';
  args_spec = {
       'SPNAMES', 'cell', '{str}',    'spnames',       {}, 'read', {}
        'SPDIST', 'cell', '{str}',    'spdists',       {}, 'read', {}
          'SPDP',     '', '[num]',       'spdp',       {}, 'read', {}
     'DPARNAMES', 'cell', '{str}',   'dpdtpars',       {}, 'read', {}
    };

  opts_spec = {
    '-add-adjt', 'addadjt', false, 'toggle', {}
    };

  [uqdata, opts] = coco_parse(grammar, args_spec, opts_spec, str);

end

