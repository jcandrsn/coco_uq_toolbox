function psi = uq_orthogonal_poly_vals(x, types, max_order, norm)
% For given evaluation locations, x, return a matrix of orthogonal
% polynomial values of type, type, up to order, max_order.  The value of
% norm determines whether the coefficients are normalized such that the
% norm of a particular polynomial order equals one or not.  

% For now assuming that M and P is the same for all random variables
[M, s] = size(x);
psi = zeros(M, s, max_order+1);
psi(:,:,1) = 1;
psi(:,:,2) = x;

if nargin < 4
    norm = 1;
end

if ischar(types)
  types = repmat(cellstr(types), 1, s);
end

wts = ones(M, s, max_order+1);

for j=1:s
  % Building matrix in for loops to account for case where random variables
  % have different distributions.
  type = types{j};
  switch type
    case {'Legendre', 'Le', 'Uniform'}
      if norm
        wts(:,j,:) = repmat(sqrt(2*(0:max_order)+1), M, 1);
      else

      end
      if max_order==0
        % Special code for trivial case to get matrix dimensions to agree
        psi = psi(:,:,1);
      else    
        for i=3:(max_order+1)
          n = i-2;
          % Three term recurrence for Legendre Orthogonal Polynomials
          psi(:,j,i) = ((2*n+1)*x(:,j).*psi(:,j,i-1) - n*psi(:,j,i-2))/(n+1);
        end
      end

    case {'Hermite', 'He', 'Normal'}
      if norm
        wts(:,j,:) = repmat(sqrt(factorial(0:max_order)).^-1, M, 1);
      else

      end
      if max_order==0
        % Special code for trivial case to get matrix dimensions to agree
        psi = psi(:,:,1);
      else
        % Three term recurrence for Hermite Orthogonal polynomials
        for i=3:(max_order+1)
          psi(:,j,i) = x(:,j).*psi(:,j,i-1) - (i-2)*psi(:,j,i-2);
        end
      end
  end
end
psi = wts.*psi;
end