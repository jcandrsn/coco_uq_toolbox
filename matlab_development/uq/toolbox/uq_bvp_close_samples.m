function [prob, data] = uq_bvp_close_samples(prob, data, bc_data, args)

if nargin < 4
  args = {};
end

bc   = bc_data.bvp_bc;

nsamples  = data.nsamples;
s_idx  = cell(1, nsamples);

% Index of parameters that are deterministic and equal in all coll instances
fxd_p_idx = and(~data.sp2p_idx, ~data.dpar_idx);
% Index of parameters that are deterministic and potentially different in
% various coll instances
frd_p_idx = and(~data.sp2p_idx, data.dpar_idx);
has_unique_deterministic_parameters = sum(frd_p_idx) > 0;
if has_unique_deterministic_parameters
  unq_p_idx = cell(1, nsamples);
end

% A kludge for checking if tangent diretions need to be
% added.  This is only true if a run and label are passed
% in varargin and then only if the type of point that the
% run and label represent is a Branch Point ('BP')
add_tl0 = 0;

if isfield(args, 'run') && isfield(args, 'lab')
  chart = coco_read_solution(args.run, args.lab, 'chart');
  if strcmpi(chart.pt_type, 'BP')
    add_tl0 = 1;
  end
end

for i=1:nsamples
  fid  = coco_get_id(data.sids{i}, 'bc');

%   Not going to handle updates to b.c.'s for now
%   if ~isempty(data.bc_update) % Optional inclusion of update slot
%     prob = coco_add_slot(prob, fid, @update, data, 'update');
%   end
  
  coll_id = coco_get_id(data.sids{i}, 'coll');
  [fdata, uidx] = coco_get_func_data(prob, coll_id, 'data', 'uidx');
  maps = fdata.coll_seg.maps;
  T0_idx = uidx(maps.T0_idx);
  T_idx  = uidx(maps.T_idx);
  x0_idx = uidx(maps.x0_idx);
  x1_idx = uidx(maps.x1_idx);
  s_idx{i} = uidx(maps.p_idx);
  if has_unique_deterministic_parameters
    unq_p_idx{i} = uidx(maps.p_idx(frd_p_idx));
  end

  uidx = [T0_idx; T_idx; x0_idx; x1_idx; s_idx{i}];
  prob = coco_add_func(prob, fid, @F, @DF, @DDF, bc_data, 'zero', ...
    'uidx', uidx);
  prob = coco_add_slot(prob, fid, @coco_save_data, bc_data, 'save_full');

  if data.addadjt
    if isfield(args, 'run') && isfield(args, 'lab')
      chart = coco_read_solution(args.run, args.lab, 'chart');
      cdata = coco_get_chart_data(chart, 'lsol');        
      [chart, lidx] = coco_read_adjoint(fid, args.run, args.lab, ...
        'chart', 'lidx');
      l0 = chart.x;
      if add_tl0
        tl0 = cdata.v(lidx);
      end
    end
    
    [fdata, aidx] = coco_get_adjt_data(prob, coll_id, 'data', 'axidx');
    opt = fdata.coll_opt;
    T0_idx = aidx(opt.T0_idx);
    T_idx  = aidx(opt.T_idx);
    x0_idx = aidx(opt.x0_idx);
    x1_idx = aidx(opt.x1_idx);
    adj_s_idx{i} = aidx(opt.p_idx);
    if has_unique_deterministic_parameters
      adj_unq_p_idx{i} = aidx(opt.p_idx(frd_p_idx));
    end
    
    aidx = [T0_idx; T_idx; x0_idx; x1_idx; adj_s_idx{i}];
    if isfield(args, 'run') && isfield(args, 'lab')
      if add_tl0
        prob = coco_add_adjt(prob, fid, 'aidx', aidx, 'l0', l0, 'tl0', tl0);
      else
        prob = coco_add_adjt(prob, fid, 'aidx', aidx, 'l0', l0);
      end
    else
      prob = coco_add_adjt(prob, fid, 'aidx', aidx);
    end
  end
end
data.s_idx = s_idx;
s_idx1 = s_idx{1};
if data.addadjt
  data.adj_s_idx = adj_s_idx;
  adj_s_idx1 = adj_s_idx{1};
end

if sum(fxd_p_idx) > 0
  for i=2:nsamples % Glue redundant copies of problem parameters
    s_idxi = s_idx{i};

    sfid = coco_get_id(bc.fid, sprintf('shared%d', i-1));

    prob = coco_add_glue(prob, sfid, s_idx1(fxd_p_idx), s_idxi(fxd_p_idx));

    if data.addadjt
      adj_s_idxi = adj_s_idx{i};
      if isfield(args, 'run') && isfield(args, 'lab')
        chart = coco_read_solution(args.run, args.lab, 'chart');
        cdata = coco_get_chart_data(chart, 'lsol');        
        [chart, lidx] = coco_read_adjoint(sfid, args.run, args.lab, ...
          'chart', 'lidx');
        l0 = chart.x;
        if add_tl0
          tl0 = cdata.v(lidx);
        end    
      else
        l0 = zeros([fxd_p_idx,1]);
      end
      if add_tl0
        prob = coco_add_adjt(prob, sfid, ...
          'aidx', [adj_s_idx1(fxd_p_idx); adj_s_idxi(fxd_p_idx)], ...
          'l0', l0, ...
          'tl0', tl0);
      else
        prob = coco_add_adjt(prob, sfid, ...
          'aidx', [adj_s_idx1(fxd_p_idx); adj_s_idxi(fxd_p_idx)], ...
          'l0', l0);
      end
    end
  end
end

if ~isempty(data.pnames) && sum(fxd_p_idx) > 0
  pfid  = coco_get_id(bc.fid, 'shared_pars');
  prob = coco_add_pars(prob, pfid, s_idx1(fxd_p_idx), ...
    data.pnames(fxd_p_idx));
  if data.addadjt
    if isfield(args, 'run') && isfield(args, 'lab')
      chart = coco_read_solution(args.run, args.lab, 'chart');
      cdata = coco_get_chart_data(chart, 'lsol');        
      [chart, lidx] = coco_read_adjoint(pfid, args.run, args.lab, ...
        'chart', 'lidx');
      l0 = chart.x;
      if add_tl0
        tl0 = cdata.v(lidx);
        prob = coco_add_adjt(prob, pfid, ...
          coco_get_id('d', data.pnames(fxd_p_idx)), ...
          'aidx', adj_s_idx1(fxd_p_idx), ...
          'l0', l0, ...
          'tl0', tl0);
      else
        prob = coco_add_adjt(prob, pfid, ...
          coco_get_id('d', data.pnames(fxd_p_idx)), ...
          'aidx', adj_s_idx1(fxd_p_idx), ...
          'l0', l0); 
      end
    else
      prob = coco_add_adjt(prob, pfid, ...
        coco_get_id('d', data.pnames(fxd_p_idx)), 'aidx', ...
        adj_s_idx1(fxd_p_idx));      
    end
  end
end

if ~isempty(data.pnames) && sum(frd_p_idx) > 0
  unq_p_idx = cell2mat(unq_p_idx)';
  pnames = uq_get_sample_par_names(data.pnames(frd_p_idx), 1:nsamples);
  pfid  = coco_get_id(bc.fid, 'unique_pars');
  prob = coco_add_pars(prob, pfid, unq_p_idx, pnames, 'active');
  if data.addadjt
    adj_unq_p_idx = cell2mat(adj_unq_p_idx);
    if isfield(args, 'run') && isfield(args, 'lab')
      chart = coco_read_solution(args.run, args.lab, 'chart');
      cdata = coco_get_chart_data(chart, 'lsol');        
      [chart, lidx] = coco_read_adjoint(pfid, args.run, args.lab, ...
        'chart', 'lidx');
      l0 = chart.x;
      if add_tl0
        tl0 = cdata.v(lidx);
        prob = coco_add_adjt(prob, pfid, ...
          coco_get_id('d', pnames), ...
          'aidx', adj_unq_p_idx, ...
          'l0', l0, ...
          'tl0', tl0);
      else
        prob = coco_add_adjt(prob, pfid, ...
          coco_get_id('d', pnames), ...
          'aidx', adj_unq_p_idx, ...
          'l0', l0);
      end
    else
      prob = coco_add_adjt(prob, pfid, coco_get_id('d', pnames), ...
        'aidx', adj_unq_p_idx);
    end
  end
end

end

function [data, y] = F(prob, data, u)

bc = data.bvp_bc;

T0 = u(bc.T0_idx);
T  = u(bc.T_idx);
x0 = u(bc.x0_idx);
x1 = u(bc.x1_idx);
p  = u(bc.p_idx);

args = {T0, T, x0, x1, p};
y = data.fhan(data, args{bc.nargs+1:end});

end

function [data, J] = DF(prob, data, u)

bc = data.bvp_bc;

T0 = u(bc.T0_idx);
T  = u(bc.T_idx);
x0 = u(bc.x0_idx);
x1 = u(bc.x1_idx);
p  = u(bc.p_idx);

args = {T0, T, x0, x1, p};

% Similar to what is done in bvp_close_segs to account 
% for the autonomous vector field case when T0 is not 
% actually passed to the boundary condition.
J = data.dfdxhan(data, args{bc.nargs+1:end});
J = [zeros(size(J,1), bc.nargs*numel(T0)), J];

end

function [data, dJ] = DDF(prob, data, u)

bc = data.bvp_bc;

T0 = u(bc.T0_idx);
T  = u(bc.T_idx);
x0 = u(bc.x0_idx);
x1 = u(bc.x1_idx);
p  = u(bc.p_idx);

args = {T0, T, x0, x1, p};

dJ = data.dfdxdxhan(data, args{bc.nargs+1:end});

% Similar to what is done in bvp_close_segs to account 
% for the autonomous vector field case when T0 is not 
% actually passed to the boundary condition.
if bc.nargs
  dJ_tmp = zeros(size(dJ,1), numel(u), numel(u));
  dJ_tmp(:,numel(T0)+1:end, numel(T0)+1:end) = dJ;
  dJ = dJ_tmp;
end
  

end

% function data = update(prob, data, cseg, varargin)
% 
% bc = data.bvp_bc;
% 
% uidx = coco_get_func_data(prob, bc.fid, 'uidx');
% u    = cseg.src_chart.x(uidx);
% T0   = u(bc.T0_idx);
% T    = u(bc.T_idx);
% x0   = u(bc.x0_idx);
% x1   = u(bc.x1_idx);
% p    = u(bc.p_idx);
% args = {T0, T, x0, x1, p};
% 
% data = data.bc_update(data, args{bc.nargs+1:end});
% 
% end