function responses = uq_get_responses(varargin)
% Get response functions associated with given sample

switch numel(varargin)
  case 1
    sid = '';
    runid = varargin{1};
  case 2
    sid = varargin{1};
    runid = varargin{2};
  otherwise
   error('Expected 1 or 2 input arguments: Sample ID (optional), Run ID (required)')
end

data = coco_read_solution(runid, 1, 'data');
resp_idx = strfind(data(:,1), coco_get_id(sid, 'uq.responses'));

responses = {};

for i=1:numel(resp_idx)
  if ~isempty(resp_idx{i})
    responses = {responses{:}, data{i,1}};
  end
end

end