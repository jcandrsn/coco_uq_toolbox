function psi = uq_make_psi_mat_orig(sample, max_order, poly_type)
%  Given a random sample, sample, and a max polynomial order, max_order,
%  return the combination of polynomials whose product is lower or equal
%  order than max_order.  poly_type defines the basis functions to use.
%  ------------------------------------------------------------------------
%  max_order = P = Maximum Total Polynomial Order (as described above)
%  Number of Random Variables Describing the system, s = size(sample, 2)
%  Number of Quadrature points, M = size(sample, 1) = Quadrature_Order^s
%  psi is an M X Nt) matrix where Nt = (s + P)! / (s! P!).
%  Quadrature_Order is set independently of this function.

if nargin < 3
    poly_type = 'Legendre';
end

n = size(sample,2);
for k = 1:n
    ps{k} = 0:max_order;
end
grid_cells = cell(size(ps));


[grid_cells{:}] = ndgrid(ps{:});

grids = zeros(n, numel(grid_cells{1}));
for i=1:n
    grids(i,:) = reshape(grid_cells{i},1,[]);
end
idx = sum(grids,1) <= max_order;
grids = grids(:, idx)';
[~,idx] = sort(sum(grids,2));
grids = grids(idx,:);
idx = max_order*(0:(n-1)) + (1:n);
idx = grids + repmat(idx,size(grids,1),1);

[d1, d2] = size(sample);
[d3, d4] = size(idx);

psi = uq_orthogonal_poly_vals_orig(sample, poly_type, max_order, 1);
psi = permute(psi, [1,3,2]);
psi = reshape(psi, [], d2*(max_order+1));
psi = psi(:,idx)';
psi = reshape(psi,d3,d4,d1);
psi = prod(psi,2);
psi = permute(psi,[3,1,2]);
