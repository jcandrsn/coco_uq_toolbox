function [data, dJ] = uq_uniform_sample_dUdU(prob, data, u)

numnodes = numel(u) - 2*data.num_uniforms;
num_us = size(u, 1);
dJ = zeros(numnodes, num_us, num_us);


% Checked with
% [data, dJd] = coco_ezDFDX('f(o,d,x)', prob, data, @uq_uniform_sample_dU, u);
end