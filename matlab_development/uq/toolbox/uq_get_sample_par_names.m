function [names_out] = uq_get_sample_par_names(names, idx)

  names_out = {};

  if ischar(names)
    names = cellstr(names);
  end

  for i=1:numel(idx)
    for name=names
      n = sprintf('%s.%d', name{1}, idx(i));
      names_out = {names_out{:} n};
    end
  end
end

