function [prob, data] = uq_add_response_function(prob, data, args, opts)

if nargin < 3
  args = {};
end

if nargin < 4
  opts = {};
  if isfield(args, 'addadjt')
    opts.addadjt = args.addadjt;
  else
    opts.addadjt = false;
  end
end

% Set up indices for call to PCE construction
data.uq.xidx = ones(data.uq.nsamples,2);

for i=1:size(data.uq.xbp_idx,1)
    data.uq.xidx(i,2) = size(data.uq.xbp_idx{i},1);
end
data.uq.xidx(:,2) = cumsum(data.uq.xidx(:,2));
data.uq.xidx(2:end,1) = data.uq.xidx(2:end,1) + data.uq.xidx(1:end-1,2);

data.uq.pidx = ones(data.uq.nsamples, 2);
for i=1:size(data.uq.p_idx,1)
    data.uq.pidx(i,2) = size(data.uq.p_idx{i},1);
end
data.uq.pidx(:,2) = cumsum(data.uq.pidx(:,2));
data.uq.pidx(2:end,1) = data.uq.pidx(2:end,1) + data.uq.pidx(1:end-1,2);
data.uq.pidx = data.uq.pidx + data.uq.xidx(end);

% Generate an initial guess for the coefficients (just
% directly calculating them).
coll_id = coco_get_id(data.uq.sids{1}, 'coll');
u_ig = coco_get_func_data(prob, coll_id, 'u0');
for i = 2:size(data.uq.sids, 2)
  coll_id = coco_get_id(data.uq.sids{i}, 'coll');
  u_ig = [u_ig; coco_get_func_data(prob, coll_id, 'u0')];
end
r_idx = vertcat(data.uq.xbp_idx{:}, data.uq.p_idx{:});
r = uq_response_evaluation(data, u_ig(r_idx));
alpha_ig = data.uq.wtd_psi_mat*r;

% Need to handle cases where derivatives are not provided.
pce_id = coco_get_id(data.oid, 'uq.pce');
prob = coco_add_func(prob, pce_id, ...
    @uq_pce_coefficients, @uq_pce_coefficients_dU, ...
    @uq_pce_coefficients_dUdU, data, 'zero', 'uidx', ...
    vertcat(data.uq.xbp_idx{:}, data.uq.p_idx{:}), ...
    'u0', alpha_ig);
prob = coco_add_slot(prob, pce_id, @coco_save_data, data, 'save_full');

if opts.addadjt
  axidx = cell(data.uq.nsamples,1);
  apidx = cell(data.uq.nsamples,1);
  % Collect adjoint variable indices so they can be passed
  % to the function that calculates the adjoint of the PCE
  % expansion.
  for i=1:data.uq.nsamples
    coll_id = coco_get_id(data.uq.sids{i}, 'coll');
    [adata, aidx] = coco_get_adjt_data(prob, coll_id, 'data', 'axidx');
    axidx{i} = aidx(adata.coll_opt.fbp_idx');
    apidx{i} = aidx(adata.coll_opt.p_idx);
  end
  
  if isfield(args, 'run')
    chart = coco_read_solution(args.run, args.lab, 'chart');
    cdata = coco_get_chart_data(chart, 'lsol');        
    [chart, lidx] = coco_read_adjoint(pce_id, args.run, args.lab, ...
      'chart', 'lidx');
    l0 = chart.x;
    tl0 = cdata.v(lidx);
  else
    l0 = zeros(size(alpha_ig));
    tl0 = zeros(size(alpha_ig));
  end  

  axidx = cell2mat(axidx);
  apidx = cell2mat(apidx);
  prob = coco_add_adjt(prob, pce_id, 'aidx', [axidx;apidx], ...
    'l0', l0, 'tl0', tl0);
  dalpha_aidx = coco_get_adjt_data(prob, pce_id, 'axidx');
  dalpha_aidx = dalpha_aidx(end-data.uq.Nt+1:end);
end

[alpha_idx, alpha_data] = coco_get_func_data(prob, pce_id, 'uidx', 'data');

% Grab the last Nt parameters. This will only work for single output
% response function.  Leaving it for now, would like to generalize this to
% multiple output response functions.
alpha_idx = alpha_idx(end-alpha_data.uq.Nt+1:end);

% Calculate statistics from the PCE
% Add zero functions for the mean and variance along with
% inactive continuation parameters for tracking their
% values.

% Mean Zero Function
mean_id = coco_get_id(data.oid, 'uq.pce_mean');
prob = coco_add_func(prob, mean_id, ...
  @uq_pce_mean, @uq_pce_mean_dU, @uq_pce_mean_dUdU, ...
  data, 'zero', 'uidx', alpha_idx, 'u0', alpha_ig(1));

% Mean Continuation Parameter
mean_par_id = coco_get_id(data.oid, 'mean');
mean_idx = coco_get_func_data(prob, mean_id, 'uidx');
mean_name = coco_get_id(data.oid, 'mean');
prob = coco_add_pars(prob, mean_par_id, mean_idx(end), mean_name);

if opts.addadjt
  if isfield(args, 'run')
    chart = coco_read_solution(args.run, args.lab, 'chart');
    cdata = coco_get_chart_data(chart, 'lsol');

    [mean_chart, mean_lidx] = coco_read_adjoint(mean_id, args.run, ...
      args.lab, 'chart', 'lidx');
    mean_l0 = mean_chart.x;
    mean_tl0 = cdata.v(mean_lidx);

    [mean_par_chart, mean_par_lidx] = coco_read_adjoint(mean_par_id, ...
      args.run, args.lab, 'chart', 'lidx');
    mean_par_l0 = mean_par_chart.x;
    mean_par_tl0 = cdata.v(mean_par_lidx);    
  else
    % Will only work for a single response function.  If
    % adapting for higher dimension response functions,
    % need to replace with zeros(num_response_outputs).
    % num_response_outputs is not currently determined
    % anywhere in the code.
    mean_l0 = 0;
    mean_tl0 = 0;
    mean_par_l0 = 0;
    mean_par_tl0 = 0;    
  end
  prob = coco_add_adjt(prob, mean_id, 'aidx', dalpha_aidx, ...
    'l0', mean_l0, 'tl0', mean_tl0);
  mean_aidx = coco_get_adjt_data(prob, mean_id, 'axidx');
  dmean = coco_get_id('d', mean_name);
  prob = coco_add_adjt(prob, mean_par_id, dmean, 'aidx', mean_aidx(end),...
    'l0', mean_par_l0, 'tl0', mean_par_tl0);
end

% Variance Zero Function
var_id = coco_get_id(data.oid, 'uq.pce_variance');
prob = coco_add_func(prob, var_id, ...
  @uq_pce_variance, @uq_pce_variance_dU, @uq_pce_variance_dUdU,...
  data, 'zero', 'uidx', alpha_idx, 'u0', sum(alpha_ig(2:end).^2));

% Variance Parameter
var_par_id = coco_get_id(data.oid, 'variance');
var_idx = coco_get_func_data(prob, var_id, 'uidx');
var_name = coco_get_id(data.oid, 'var');
prob = coco_add_pars(prob, var_par_id, var_idx(end), var_name);

if opts.addadjt
  if isfield(args, 'run')
    chart = coco_read_solution(args.run, args.lab, 'chart');
    cdata = coco_get_chart_data(chart, 'lsol');

    [var_chart, var_lidx] = coco_read_adjoint(var_id, args.run, ...
      args.lab, 'chart', 'lidx');
    var_l0 = var_chart.x;
    var_tl0 = cdata.v(var_lidx);

    [var_par_chart, var_par_lidx] = coco_read_adjoint(var_par_id, ...
      args.run, args.lab, 'chart', 'lidx');
    var_par_l0 = var_par_chart.x;
    var_par_tl0 = cdata.v(var_par_lidx);    
  else
    % Will only work for a single response function.  If
    % adapting for higher dimension response functions,
    % need to replace with zeros(num_response_outputs).
    % num_response_outputs is not currently determined
    % anywhere in the code.
    var_l0 = 0;
    var_tl0 = 0;
    var_par_l0 = 0;
    var_par_tl0 = 0;    
  end  
  prob = coco_add_adjt(prob, var_id, 'aidx', dalpha_aidx, ...
    'l0', var_l0, 'tl0', var_tl0);
  var_aidx = coco_get_adjt_data(prob, var_id, 'axidx');
  dvar = coco_get_id('d', var_name);
  prob = coco_add_adjt(prob, var_par_id, dvar, 'aidx', var_aidx(end), ...
    'l0', var_par_l0, 'tl0', var_par_tl0);
end
                 
end