function [data, y] = uq_pce_coefficients_monitor(prob, data, u)
%UQ_PCE_COEFFICIENTS Summary of this function goes here
%   Evaluate Response function determine the polynomial
%   chaos expansion coefficients and calculate the mean and
%   variance of the response function
%   u contains all Basis point and parameter values for M
%   trajectories (where M is the numerical integration
%   order of the PCE) as well as continuation variables for
%   the mean and variance.  The continuation variables are
%   organized as follows:
%   u = [ xbp1; xbp2; ... xbpM; p1; p2; ... pM; mean; var];

r = uq_response_evaluation(data, u);

mean = u(data.uq.pidx(end)+1);
var = u(data.uq.pidx(end)+2);

alphas = data.uq.wtd_psi_mat*r;

y = [mean - alphas(1);
     var - sum(alphas(2:end).^2)];

end
