function data = uq_gen_nds(data)
% Function transforms nodes from a standard random variable
% distribution to a random variable space consistent with
% given distribution parameters.
% Currently handles normal and uniform distributions

if data.num_normals > 0
    mu = data.spdp(data.normal_par_idx,1);
    sig = data.spdp(data.normal_par_idx,2);

    mu_rep = repelem(mu, data.uq.M(data.normal_par_idx));
    sig_rep = repelem(sig, data.uq.M(data.normal_par_idx));

    if size(mu_rep,1) == 1
        mu_rep = mu_rep';
        sig_rep = sig_rep';
    end

    data.nds(data.normal_nds_idx) = ...
        mu_rep + sig_rep.*data.st_nds(data.normal_nds_idx);
end

if data.num_uniforms > 0
    % Ensure that 1st column is less than second column for uniform
    % distribution parameters.
    lo_gt_up = data.spdp(data.uniform_par_idx,1) > ...
               data.spdp(data.uniform_par_idx,2);

    if nnz(lo_gt_up) ~= 0
        uniform_rv_names = data.spnames(data.uniform_par_idx);
        bad_data_entries = uniform_rv_names(lo_gt_up);
        bad_data_string = strtrim(sprintf('%s, ', bad_data_entries{:}));
        assert(nnz(lo_gt_up) == 0, ...
           ['First distribution parameter for uniform random variables' ...
            ' must be smaller than second parameter.  Check values for' ...
            ' parameters: {%s}'], bad_data_string(1:end-1));
    end
    lo = data.spdp(data.uniform_par_idx,1);
    hi = data.spdp(data.uniform_par_idx,2);
    lo_rep = repelem(lo, data.uq.M(data.uniform_par_idx));
    up_rep = repelem(hi, data.uq.M(data.uniform_par_idx));

    if size(lo_rep,1) == 1
        lo_rep = lo_rep';
        up_rep = up_rep';
    end

    data.nds(data.uniform_nds_idx) = ...
        ((up_rep - lo_rep)/2).*data.st_nds(data.uniform_nds_idx) ...
        + (up_rep + lo_rep)/2; 
end

end