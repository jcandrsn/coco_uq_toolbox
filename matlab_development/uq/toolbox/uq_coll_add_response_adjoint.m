function prob = uq_coll_add_response_adjoint(prob, oid, rid, varargin)

if nargin < 4
  args = struct();
else
  args = varargin{1};
end

response_id = coco_get_id(oid, 'uq', 'responses');
pce_id = coco_get_id(response_id, rid, 'pce');
uq_data = coco_get_func_data(prob, pce_id, 'data');

% A kludge for checking if tangent diretions need to be
% added.  This is only true if a run and label are passed
% in varargin and then only if the type of point that the
% run and label represent is a Branch Point ('BP')
add_tl0 = 0;

if isfield(args, 'run')
  % Collect the values of the Adjoint Variables for the PCE
  % coefficients if a run name is passed to the function.
  chart = coco_read_solution(args.run, args.lab, 'chart');
  
    cdata = coco_get_chart_data(chart, 'lsol');        
    [chart, lidx] = coco_read_adjoint(pce_id, args.run, args.lab, ...
      'chart', 'lidx');
    l0 = chart.x;
  if strcmpi(chart.pt_type,'BP')
    % Check if the run and lab are from a branch point.  If
    % they are, then get the tangent direction for branch
    % switching and set a flag for later.
    tl0 = cdata.v(lidx);
    add_tl0 = 1;
  end
else
  % Otherwise initialize them to zero 
  l0 = zeros(uq_data.Nt,1);
end 

if strcmp(uq_data.response_type, 'int')
  
  aTidx = cell(uq_data.nsamples,1);
  axidx = cell(uq_data.nsamples,1);
  apidx = cell(uq_data.nsamples,1);
  % Collect adjoint variable indices so they can be passed
  % to the function that calculates the adjoint of the PCE
  % expansion.
  for i=1:uq_data.nsamples
    coll_id = coco_get_id(uq_data.sids{i}, 'coll');
    [adata, aidx] = coco_get_adjt_data(prob, coll_id, 'data', 'axidx');
    aTidx{i} = aidx(adata.coll_opt.T_idx);
    axidx{i} = aidx(adata.coll_opt.xcn_idx);
    apidx{i} = aidx(adata.coll_opt.p_idx);
  end
  aTidx = cell2mat(aTidx);
  axidx = cell2mat(axidx);
  apidx = cell2mat(apidx);
  
  aidx = [aTidx;axidx;apidx];
  if add_tl0
    % Tangent direction if from a Branch Point
    prob = coco_add_adjt(prob, pce_id, ...
      @uq_pce_coefficients_adjoint, ...
      @uq_pce_coefficients_adjoint_dU, uq_data, ...
      'aidx', aidx, 'l0', l0, 'tl0', tl0);
  else
    % No tangent direction if not
    prob = coco_add_adjt(prob, pce_id, ...
      @uq_pce_coefficients_adjoint, ...
      @uq_pce_coefficients_adjoint_dU, uq_data, ...
      'aidx', aidx, 'l0', l0);
  end

elseif strcmp(uq_data.response_type, 'bv')

  nsamples = uq_data.nsamples;
  xdim = uq_data.xdim;
  pdim = uq_data.pdim;
  aidx = zeros(nsamples, 2 + 2*xdim + pdim);
  x0end = 2+xdim;
  x1end = 2+2*xdim;
  pend  = 2+2*xdim+pdim;

  for i=1:nsamples
    [axidx, adata] = coco_get_adjt_data(prob, ...
      coco_get_id(uq_data.sids{i}, 'coll'), 'axidx', 'data');
    maps = adata.coll_opt;

    aidx(i,1) = axidx(maps.T0_idx);
    aidx(i,2) = axidx(maps.T_idx);
    aidx(i,3:x0end) = axidx(maps.x0_idx);
    aidx(i,x0end+1:x1end) = axidx(maps.x1_idx);
    aidx(i,x1end+1:pend) = axidx(maps.p_idx);
  end

  aidx = aidx';
  aidx = aidx(:);
  if add_tl0
    prob = coco_add_adjt(prob, pce_id, 'aidx', ...
      aidx, 'l0', l0, 'tl0', tl0);
  else
    prob = coco_add_adjt(prob, pce_id, 'aidx', ...
      aidx, 'l0', l0);
  end
end

dalpha_aidx = coco_get_adjt_data(prob, pce_id, 'axidx');
dalpha_aidx = dalpha_aidx(end-uq_data.Nt+1:end);

mean_id = coco_get_id(response_id, rid, 'pce_mean');
mean_par_id = coco_get_id(response_id, rid, 'mean');

if isfield(args, 'run')
  chart = coco_read_solution(args.run, args.lab, 'chart');
  cdata = coco_get_chart_data(chart, 'lsol');

  [mean_chart, mean_lidx] = coco_read_adjoint(mean_id, args.run, ...
    args.lab, 'chart', 'lidx');
  mean_l0 = mean_chart.x;

  [mean_par_chart, mean_par_lidx] = coco_read_adjoint(mean_par_id, ...
    args.run, args.lab, 'chart', 'lidx');
  mean_par_l0 = mean_par_chart.x;
  
  if add_tl0
    mean_tl0 = cdata.v(mean_lidx);  
    mean_par_tl0 = cdata.v(mean_par_lidx);
  end
else
  mean_l0 = 0;
  mean_par_l0 = 0;
end

if add_tl0
  prob = coco_add_adjt(prob, mean_id, 'aidx', dalpha_aidx, ...
    'l0', mean_l0, 'tl0', mean_tl0);
else
  prob = coco_add_adjt(prob, mean_id, 'aidx', dalpha_aidx, ...
    'l0', mean_l0);
end
mean_aidx = coco_get_adjt_data(prob, mean_id, 'axidx');
mean_name = coco_get_id(uq_data.oid, rid, 'mean');
dmean = coco_get_id('d', mean_name);

if add_tl0
  prob = coco_add_adjt(prob, mean_par_id, dmean, 'aidx', mean_aidx(end),...
    'l0', mean_par_l0, 'tl0', mean_par_tl0);
else
  prob = coco_add_adjt(prob, mean_par_id, dmean, 'aidx', mean_aidx(end),...
    'l0', mean_par_l0);
end

var_id = coco_get_id(response_id, rid, 'pce_variance');
var_par_id = coco_get_id(response_id, rid, 'variance');

if isfield(args, 'run')
  chart = coco_read_solution(args.run, args.lab, 'chart');
  cdata = coco_get_chart_data(chart, 'lsol');

  [var_chart, var_lidx] = coco_read_adjoint(var_id, args.run, ...
    args.lab, 'chart', 'lidx');
  var_l0 = var_chart.x;


  [var_par_chart, var_par_lidx] = coco_read_adjoint(var_par_id, ...
    args.run, args.lab, 'chart', 'lidx');
  var_par_l0 = var_par_chart.x;
  if add_tl0
    var_tl0 = cdata.v(var_lidx);
    var_par_tl0 = cdata.v(var_par_lidx);
  end
else
  var_l0 = 0;
  var_par_l0 = 0;
end

if add_tl0
  prob = coco_add_adjt(prob, var_id, 'aidx', dalpha_aidx, ...
    'l0', var_l0, 'tl0', var_tl0);
else
  prob = coco_add_adjt(prob, var_id, 'aidx', dalpha_aidx, ...
  'l0', var_l0);
end
var_aidx = coco_get_adjt_data(prob, var_id, 'axidx');
var_name = coco_get_id(uq_data.oid, rid, 'var');
dvar = coco_get_id('d', var_name);
if add_tl0
  prob = coco_add_adjt(prob, var_par_id, dvar, 'aidx', var_aidx(end), ...
    'l0', var_par_l0, 'tl0', var_par_tl0);
else
  prob = coco_add_adjt(prob, var_par_id, dvar, 'aidx', var_aidx(end), ...
    'l0', var_par_l0);
end

end