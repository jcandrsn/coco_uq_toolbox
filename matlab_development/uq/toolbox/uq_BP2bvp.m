function prob = uq_BP2bvp(prob, sid, varargin)
%UQ_BP2UQBVP Restart continuation from a 
%
% 
% PROB = UQ_BP2UQBVP(PROB, OID, VARARGIN)
% VARARGIN = { RUN [SOID] LAB RESP [OPTS] }
% OPTS = { '-add-adjt' }
%
% PROB : Continuation problem structure.
% SID  : Target sample instance identifier (string).
% RUN  : Run identifier (string or cell-array of strings).
% SOID : Source object instance identifier (string, optional).
% LAB  : Solution label (integer).

grammar   = 'RUN [SOID] LAB [OPTS]';
args_spec = {
     'RUN', 'cell', '{str}',  'run',  {}, 'read', {}
    'SOID',     '',   'str', 'soid', sid, 'read', {}
     'LAB',     '',   'num',  'lab',  [], 'read', {}
  };
opts_spec = {
  '-add-adjt', 'addadjt', false, 'toggle', {}
  '-add-resp', 'addresp', false, 'toggle', {}
  };

[args, opts] = coco_parse(grammar, args_spec, ...
  opts_spec, varargin{:});

responses = uq_get_responses(args.run);

data = coco_read_solution(responses{1}, args.run, ...
  args.lab, 'data');

% Clean up data fields
switch data.response_type
  case 'bv'
    fields = {'response_type', 'rhan', 'drduhan', ...
      'drduduhan', 'resp'};
  case 'int'
    fields = {'response_type', 'rhan', 'drdxhan', ...
      'drdphan', 'drdxdxhan', 'drdpdphan', ...
      'drdxdphan', 'resp'};
end
data = rmfield(data, fields);

bc_data = coco_read_solution(coco_get_id(sid, 'uq.sample1.bc'), ...
  args.run, args.lab, 'data');

% Need to include an update function in the uq problem
% instance that updates the value of stochastic parameter
% distribution parameters and node locations if they're 
% modified during continuation.  Then I could just read 
% it from data instead of this roundabout way of getting
% the distribution parameters.
if data.num_normals > 0
  muid = coco_get_id(sid, 'mus');
  mu_chart = coco_read_solution(muid, args.run, args.lab, 'chart');
  mus = mu_chart.x;
  sigid = coco_get_id(sid, 'sigs');
  sig_chart = coco_read_solution(sigid, args.run, args.lab, 'chart');
  sigs = sig_chart.x;
  data.spdp(data.normal_par_idx,:) = [mus, sigs];
end

if data.num_uniforms > 0
  lsid = coco_get_id(sid, 'lo');
  lo_chart = coco_read_solution(lsid, args.run, args.lab, 'chart');
  los = lo_chart.x;  
  upid = coco_get_id(sid, 'up');
  up_chart = coco_read_solution(upid, args.run, args.lab, 'chart');
  ups = up_chart.x;
  data.spdp(data.uniform_par_idx,:) = [los, ups];
end

% Generate new locations for data.nds based on updated
% distribution parameters.
data = uq_gen_nds(data);
[prob, data] = uq_BP2bvpsamples(prob, data, bc_data, args, opts); % handles -add-adjt
% Waiting until after this function because the grid nodes and integration 
% weights get re-ordered as part of uq_BP2bvpsamples
% Need to rethink psi_mat construction to handle different integration
% orders and polynomial degrees... another time.
psi_mat = uq_make_psi_mat(data.nds_grid, data.uq.Pt, data.spdists);
data.wtd_psi_mat = psi_mat*diag(data.wts);

prob = uq_add_sample_nodes(prob, data, args);

% Add back in all of the response functions for the given
% sample sid
if opts.addresp
  for i=1:numel(responses)
      data = coco_read_solution(responses{i}, ...
        args.run, args.lab, 'data');

      switch data.response_type
        case 'bv'
          r = data.rhan;
          dr = data.drduhan;
          ddr = data.drduduhan;
          resp_args = {data.response_type, r, dr, ddr};
        case 'int'
          r = data.rhan;
          drdx = data.drdxhan;
          drdp = data.drdphan;
          drdxdx = data.drdxdxhan;
          drdpdp = data.drdxdphan;
          drdxdp = data.drdpdphan;
          resp_args = {data.response_type, r,...
            drdx, drdp, drdxdx, drdpdp, drdxpdp};
      end
      r = strsplit(responses{i}, '.uq.responses.');
      rid = strsplit(r{2}, '.');
      rid = rid{1};
      prob = uq_coll_add_response(prob, sid, rid, resp_args{:});
    if opts.addadjt
      prob = uq_coll_add_response_adjoint(prob, sid, rid, args);
    end
  end
end

end