function [prob, data] = uq_BP2bvpsamples(prob, data, bc_data, args, opts)

[prob, data] = uq_BP2collsamples(prob, data, args, opts);
[prob, data] = uq_bvp_close_samples(prob, data, bc_data, args);

end