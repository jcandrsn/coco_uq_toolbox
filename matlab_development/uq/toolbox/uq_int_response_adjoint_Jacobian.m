function [data, dJ] = uq_int_response_adjoint_Jacobian(prob, data, u)
  dJ = zeros(prod(data.uq.M), data.resp.p_adjt_idx(end), data.resp.p_idx(end));

  for i=data.nsamples:-1:1 % going backwards to keep Matlab from squawking 
                           % about memory allocation
    % Cycling through like this to account for a potential future 
    % case where the trajectories are allowed to have different 
    % discretizations and thus will have varying numbers of basis points
    % Need to think through vectorization or using cellfun
    % to do this.
    %
    % Separate out components
    T = u(data.resp.T_idx(i,1));
    x = u(data.resp.xbp_idx(i,1):data.resp.xbp_idx(i,end));
    p = u(data.resp.p_idx(i,1):data.resp.p_idx(i,end));
    fdata = coco_get_adjt_data(prob, ...
      coco_get_id(data.sids{i}, 'coll'), 'data');

    [dJ_T, dJ_xcn, dJ_p] = adj_int_obj_du(data, fdata, T, x, p);

    %% Assign Values for dJ_T
    dJ(i, i, i) = dJ_T(1,1,1);
    % Could potentially be different for each trajectory if
    % the mesh is adaptive.
    J_T_xbp_idx = 1 + (1:size(fdata.coll_seg.maps.W, 2));
    dJ(i, i, data.resp.xbp_idx(i,1):data.resp.xbp_idx(i,2)) = dJ_T(1,1,J_T_xbp_idx);
    dJ(i, i, data.resp.p_idx(i,1):data.resp.p_idx(i,2)) = dJ_T(1,1,(J_T_xbp_idx(end) + 1):end);
    
    %% Assign Values for dJ_xcn
    dJ(i, data.resp.xcn_idx(i,1):data.resp.xcn_idx(i,2), i) = dJ_xcn(1, :, 1);
    dJ(i, data.resp.xcn_idx(i,1):data.resp.xcn_idx(i,2), ...
      data.resp.xbp_idx(i,1):data.resp.xbp_idx(i,2)) = ...
      dJ_xcn(1, :, J_T_xbp_idx);
    dJ(i, data.resp.xcn_idx(i,1):data.resp.xcn_idx(i,2), ...
      data.resp.p_idx(i,1):data.resp.p_idx(i,2)) = ...
      dJ_xcn(1, :, (J_T_xbp_idx(end) + 1):end);
    %% Assign values for dJ_p
    dJ(i, data.resp.p_adjt_idx(i,1):data.resp.p_adjt_idx(i,2), i) = dJ_p(1, :, 1);
    dJ(i, data.resp.p_adjt_idx(i,1):data.resp.p_adjt_idx(i,2), ...
      data.resp.xbp_idx(i,1):data.resp.xbp_idx(i,2)) = ...
      dJ_p(1, :, J_T_xbp_idx);
    dJ(i, data.resp.p_adjt_idx(i,1):data.resp.p_adjt_idx(i,2), ...
      data.resp.p_idx(i,1):data.resp.p_idx(i,2)) = ...
      dJ_p(1, :, (J_T_xbp_idx(end) + 1):end);    
  end
  sz = size(dJ);
  dJ = data.wtd_psi_mat*reshape(dJ, data.nsamples, []);
  dJ = -1*reshape(dJ, [size(data.wtd_psi_mat, 1), sz(2:end)]);
end

function [dJ_T, dJ_xcn, dJ_p] = adj_int_obj_du(uq_data, fdata, T, x, p)

% Jacobian of adjoint for an integral function

maps = fdata.coll_seg.maps;
int  = fdata.coll_seg.int;

NCOL = int.NCOL;
NTST = maps.NTST;
xdim = int.dim;
pdim = maps.pdim;

wts1 = repmat(int.wt',[maps.NTST,1]);

xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
pp  = repmat(p, maps.p_rep);

dcn  = size(maps.W,1);
dbp  = size(maps.W,2);

ddrdxdx = uq_data.drdxdxhan(xx, pp);  
ddrdxdp = uq_data.drdxdphan(xx, pp);
drdx    = uq_data.drdxhan(xx, pp);

J_xcn_T   = 1/(2*maps.NTST)*drdx(:);
J_xcn_xbp = sparse(maps.fdxrows, maps.fdxcols, ddrdxdx(:))*maps.W;
J_xcn_xbp = T/(2*maps.NTST)*J_xcn_xbp;
J_xcn_p = (T/(2*maps.NTST))*sparse(maps.fdprows, maps.fdpcols, ddrdxdp(:));

dJ_xcn = [J_xcn_T, J_xcn_xbp, J_xcn_p];
dJ_xcn = reshape(full(dJ_xcn), [1, size(dJ_xcn,1),size(dJ_xcn,2)]);

dim    = maps.x_shp(1);
xcnnum = maps.x_shp(2);
xcndim = maps.x_shp(1)*maps.x_shp(2);
drdx_rows = repmat(reshape(1:xcnnum, [1 xcnnum]), [dim 1]);
drdx_cols = repmat(1:xcndim, [1 1]);
drdx_2 = sparse(drdx_rows, drdx_cols, drdx(:))*maps.W;
drdp = uq_data.drdphan(xx, pp);

J_T_T   = 0;
J_T_xbp = 1/(2*maps.NTST)*drdx_2'*wts1;
J_T_xbp = J_T_xbp';
J_T_p   = 1/(2*maps.NTST)*drdp(:)'*kron(wts1,eye(maps.pdim));

dJ_T = [J_T_T, J_T_xbp, J_T_p];
dJ_T = reshape(dJ_T, [1, size(dJ_T,1),size(dJ_T,2)]);

J_p_xcn = permute(ddrdxdp, [1, 3 ,2, 4]);
J_p_xcn = reshape(J_p_xcn, [maps.pdim, prod(maps.x_shp)]);
J_p_xcn = (T/(2*maps.NTST))*J_p_xcn.*repelem(wts1', maps.pdim, maps.x_shp(1));
J_p_xbp = J_p_xcn*maps.W;
J_p_p = uq_data.drdpdphan(xx, pp);
w = kron(wts1, eye(maps.pdim^2));
J_p_p = reshape((T/(2*maps.NTST))*J_p_p(:)'*w, maps.pdim, maps.pdim);

dJ_p = [J_T_p' J_p_xbp J_p_p];
dJ_p = reshape(full(dJ_p), [1, size(dJ_p,1),size(dJ_p,2)]);

end

function [dJ_T, dJ_xcn, dJ_p] = adj_int_obj_du_nu(uq_data, fdata, T, x, p)
% Jacobian of adjoint for an integral function
% handlles non-uniform mesh.

% get indices for sparse matrix construction.
fdata = adj_obj_init_data(fdata);
maps = fdata.coll_seg.maps;
int  = fdata.coll_seg.int;

NCOL = int.NCOL;
NTST = maps.NTST;
xdim = int.dim;
pdim = maps.pdim;

wts1 = repmat(int.wt',[maps.NTST,1]);

xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
pp  = repmat(p, maps.p_rep);

dcn  = size(maps.W,1);
dbp  = size(maps.W,2);

ddrdxdx = uq_data.drdxdxhan(xx, pp);  
ddrdxdp = uq_data.drdxdphan(xx, pp);
drdx    = uq_data.drdxhan(xx, pp);

J_xcn_T   = 1/(2*maps.NTST)*drdx(:);
J_xcn_xbp = sparse(maps.fdxrows, maps.fdxcols, ddrdxdx(:))*maps.W;
J_xcn_xbp = T/(2*maps.NTST)*J_xcn_xbp;
J_xcn_p = (T/(2*maps.NTST))*sparse(maps.fdprows, maps.fdpcols, ddrdxdp(:));

dJ_xcn = [J_xcn_T, J_xcn_xbp, J_xcn_p];
dJ_xcn = reshape(full(dJ_xcn), [1, size(dJ_xcn,1),size(dJ_xcn,2)]);

dim    = maps.x_shp(1);
xcnnum = maps.x_shp(2);
xcndim = maps.x_shp(1)*maps.x_shp(2);
drdx_rows = repmat(reshape(1:xcnnum, [1 xcnnum]), [dim 1]);
drdx_cols = repmat(1:xcndim, [1 1]);
drdx_2 = sparse(drdx_rows, drdx_cols, drdx(:))*maps.W;
drdp = uq_data.drdphan(xx, pp);

J_T_T   = 0;
J_T_xbp = 1/(2*maps.NTST)*drdx_2'*wts1;
J_T_xbp = J_T_xbp';
J_T_p   = 1/(2*maps.NTST)*drdp(:)'*kron(wts1,eye(maps.pdim));

dJ_T = [J_T_T, J_T_xbp, J_T_p];
dJ_T = reshape(dJ_T, [1, size(dJ_T,1),size(dJ_T,2)]);

J_p_xcn = permute(ddrdxdp, [1, 3 ,2, 4]);
J_p_xcn = reshape(J_p_xcn, [maps.pdim, prod(maps.x_shp)]);
J_p_xcn = (T/(2*maps.NTST))*J_p_xcn.*repelem(wts1', maps.pdim, maps.x_shp(1));
J_p_xbp = J_p_xcn*maps.W;
J_p_p = uq_data.drdpdphan(xx, pp);
w = kron(wts1, eye(maps.pdim^2));
J_p_p = reshape((T/(2*maps.NTST))*J_p_p(:)'*w, maps.pdim, maps.pdim);

dJ_p = [J_T_p' J_p_xbp J_p_p];
dJ_p = reshape(full(dJ_p), [1, size(dJ_p,1),size(dJ_p,2)]);

end

function [data, J] = adj_objhan_du(prob, data, u) %#ok<INUSL>

pr   = data.pr;
maps = pr.coll_seg.maps;
mesh = pr.coll_seg.mesh;
opt  = pr.coll_opt;

T = u(maps.T_idx);
x = u(maps.xbp_idx);

xcn = reshape(maps.W*x, maps.x_shp);

gdxdxcn = pr.ghan_dxdx(xcn);
gdxdxcn = mesh.gdxdxka.*gdxdxcn;
gdxdxcn = sparse(opt.gdxdxrows1, opt.gdxdxcols1, gdxdxcn(:))*maps.W;
J       = (0.5*T/maps.NTST)*sparse(opt.gdxdxrows2, opt.gdxdxcols2, ...
  gdxdxcn(opt.gdxdxidx), opt.dJrows, opt.dJcols);

gdxcn   = pr.ghan_dx(xcn);
gdxcn   = mesh.gdxka.*gdxcn;
J       = J + (0.5/maps.NTST)*sparse(opt.gdxdTrows, opt.gdxdTcols, ...
  gdxcn(:), opt.dJrows, opt.dJcols);

gdxcn   = mesh.gwt*sparse(maps.gdxrows, maps.gdxcols, gdxcn(:))*maps.W;
J       = J + (0.5/maps.NTST)*sparse(opt.gdTdxrows, opt.gdTdxcols, ...
  gdxcn(:), opt.dJrows, opt.dJcols);

J       = J + sparse(opt.gdpdprows, opt.gdpdpcols, ones(1,3)/5, ...
  opt.dJrows, opt.dJcols);

end

function data = adj_obj_init_data(fdata)

data.coll_seg  = fdata.coll_seg;
data.ghan      = @ghan;
data.ghan_dx   = @ghan_dx;
data.ghan_dxdx = @ghan_dxdx;

seg  = fdata.coll_seg;
maps = seg.maps;
int  = seg.int;

NCOL = int.NCOL;
NTST = maps.NTST;
xdim = int.dim;
pdim = maps.pdim;

rows = NCOL*NTST*kron(0:(xdim-1), ones(1,xdim));
opt.gdxdxrows1 = repmat(rows, [1 NCOL*NTST]) + ...
  kron(1:NCOL*NTST, ones(1,xdim^2));
cols = reshape(1:xdim*NCOL*NTST, [xdim NCOL*NTST]);
opt.gdxdxcols1 = repmat(cols, [xdim 1]);

% Derivative of (T/2N)*gxcn with respect to xbp:
step = 1+xdim*(0:NCOL-1); % NCOL
step = repmat(step(:), [1 xdim]) + repmat(0:xdim-1, [NCOL 1]); % xdim*NCOL
step = repmat(step(:), [1 xdim*(NCOL+1)]) + ...
  (xdim*NCOL*NTST+2+pdim)*repmat(0:xdim*(NCOL+1)-1, [xdim*NCOL 1]); % xdim^2*NCOL*(NCOL+1)
step = repmat(step(:), [1 NTST]) + ...
  (xdim*NCOL+xdim*(NCOL+1)*(xdim*NCOL*NTST+2+pdim))*...
  repmat(0:NTST-1, [xdim^2*NCOL*(NCOL+1) 1]); % xdim^2*NCOL*(NCOL+1)*NTST
opt.gdxdxcols2 = step(:);
opt.gdxdxrows2 = ones(xdim^2*NCOL*(NCOL+1)*NTST, 1);

step = 1:NCOL; % NCOL
step = repmat(step(:), [1 xdim]) + NTST*NCOL*repmat(0:xdim-1, [NCOL 1]); % xdim*NCOL
step = repmat(step(:), [1 xdim*(NCOL+1)]) + ...
  xdim*NTST*NCOL*repmat(0:xdim*(NCOL+1)-1, [xdim*NCOL 1]); % xdim^2*NCOL*(NCOL+1)
step = repmat(step(:), [1 NTST]) + (NCOL+xdim^2*NTST*NCOL*(NCOL+1))*...
  repmat(0:NTST-1, [xdim^2*NCOL*(NCOL+1) 1]); % xdim^2*NCOL*(NCOL+1)*NTST
opt.gdxdxidx = step(:);

% Derivative of (T/2N)*gxcn with respect to T:
opt.gdxdTrows = ones(xdim*NTST*NCOL, 1);
opt.gdxdTcols = (xdim*NCOL*NTST+2+pdim)*xdim*(NCOL+1)*NTST + ...
  (1:xdim*NTST*NCOL)';

% Derivative of (1/2N)*w*g' with respect to xbp:
opt.gdTdxcols = NTST*NCOL*xdim+2 + ...
  (xdim*NTST*NCOL+2+pdim)*(0:xdim*(NCOL+1)*NTST-1)';
opt.gdTdxrows = ones(xdim*(NCOL+1)*NTST, 1);

% Derivative of [p1/5, p2/5, p3/5] with respect to p:

opt.gdpdprows = ones(3,1);
opt.gdpdpcols = (NTST*xdim*NCOL+2+pdim)*(xdim*(NCOL+1)*NTST+2) + ...
  xdim*NTST*NCOL+2+[1 NTST*xdim*NCOL+2+pdim+2 2*(NTST*xdim*NCOL+2+pdim)+3]';

opt.dJrows = 1;
opt.dJcols = (xdim*NTST*NCOL+2+pdim)*(xdim*NTST*(NCOL+1)+2+pdim);

data.coll_opt = opt;

data = coco_func_data(data);

end