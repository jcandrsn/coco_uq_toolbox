function [data, J] = uq_seg_to_nds_du(prob, data, u)

negs = data.idx+numel(data.idx);
negs = negs(:);
i = repmat(1:numel(data.idx),[1,2]);
j = [1:numel(data.idx), negs'];
v = [ones(1,numel(data.idx)), -ones(1,numel(data.idx))];
J = sparse(i, j, v);

end