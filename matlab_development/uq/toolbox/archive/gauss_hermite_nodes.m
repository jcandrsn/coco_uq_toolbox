function [nds, wts] = gauss_hermite_nodes(m)
n = (1:m-1)';
g = sqrt(n); 
J = diag(g,1)+diag(g,-1);
[w, x] = eig(J);
[nds, idx] = sort(diag(x));
nds = nds';
wts = sqrt(2*pi)*w(1,:).^2;
% Normalizing the weights so they add up to one
% The weight function is the pdf of the standard normal distribution
% (1/sqrt(2*pi))*exp((-x.^2)/2).
wts = wts(idx)/(sqrt(2*pi));
end