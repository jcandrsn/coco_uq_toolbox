function [nds, wts] = gauss_legendre_nodes(m)
n = (1:m-1)';
g = n.*sqrt(1./(4*n.^2-1)); 
J = -diag(g,1)-diag(g,-1);
[w, x] = eig(J);
nds = diag(x)';
% Normalizing the weights so they add up to one.
% The weight function is 1/2 for a uniform distribution between -1 and 1
% instead of the typical uniform distribution between 0 and 1.
wts = (2*w(1,:).^2)/2;
end