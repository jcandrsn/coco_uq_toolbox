function [nds, wts] = uq_gauss_nodes_orig(m, n, type)
% Given a quadrature order, m (integer, is the same in each dimension), 
% a number of , n (integer), and a quadrature rule type, return the tensor 
% product of the quadrature node locations, nds, and appropriately
% multiplied quadrature weight, wts.

switch type
    case {'Legendre','Le','LeN'}
        [nds, wts] = gauss_legendre_nodes(m);
    case {'Hermite','He','HeN'}
        [nds, wts] = gauss_hermite_nodes(m);
    otherwise
        warning(strcat('Specified Type not supported, Providing',...
                       ' weights and nodes for Gauss-Legendre',...
                       ' quadrature'))
        [nds, wts] = gauss_legendre_nodes(m);
end

nd_cells = repmat({nds}, 1,n);
wt_cells = repmat({wts}, 1,n);
[nd_cells{:}] = ndgrid(nd_cells{:});
[wt_cells{:}] = ndgrid(wt_cells{:});

nds = zeros(n, numel(nd_cells{1}));
wts = zeros(n, numel(wt_cells{1}));
for i=1:n
    nds(i,:) = reshape(nd_cells{i},1,[]);
    wts(i,:) = reshape(wt_cells{i},1,[]);
end
nds = nds';
wts = prod(wts,1)';

end

function [nds, wts] = gauss_legendre_nodes(m)
n = (1:m-1)';
g = n.*sqrt(1./(4*n.^2-1)); 
J = -diag(g,1)-diag(g,-1);
[w, x] = eig(J);
nds = diag(x)';
% Normalizing the weights so they add up to one.
% The weight function is 1/2 for a uniform distribution between -1 and 1
% instead of the typical uniform distribution between 0 and 1.
wts = (2*w(1,:).^2)/2;
end

function [nds, wts] = gauss_hermite_nodes(m)
n = (1:m-1)';
g = sqrt(n); 
J = diag(g,1)+diag(g,-1);
[w, x] = eig(J);
[nds, idx] = sort(diag(x));
nds = nds';
wts = sqrt(2*pi)*w(1,:).^2;
% Normalizing the weights so they add up to one
% The weight function is the pdf of the standard normal distribution
% (1/sqrt(2*pi))*exp((-x.^2)/2).
wts = wts(idx)/(sqrt(2*pi));
end