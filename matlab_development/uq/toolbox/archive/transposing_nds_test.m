quadrature_order=4;
stochastic_dimension = 3;
pce_order = 10;
orthogonal_polynomial_type = 'Legendre';

[nds, wts] = uq_gauss_nodes_orig(quadrature_order, ...
                                stochastic_dimension, ...
                                orthogonal_polynomial_type);
psi_mat = uq_make_psi_mat_orig(nds, pce_order, orthogonal_polynomial_type);
orig = diag(wts)*psi_mat;

[nds, wts] = uq_gauss_nodes_v1(quadrature_order, ...
                                stochastic_dimension, ...
                                orthogonal_polynomial_type);
psi_mat = uq_make_psi_mat(nds, pce_order, ...
                                  orthogonal_polynomial_type);
nds
a = psi_mat*diag(wts) == orig';
min(min(a))
max(max(a))