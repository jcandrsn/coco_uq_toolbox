function alphas = uq_pce_coeff(r, wtd_Psi)
% Given the function values of an observable function, r = f(u), and a
% weighted matrix of basis polynomials evaluated at quadrauture node
% locations, wtd_Psi, return the Polynomial Chaos Coefficients, alpha

alphas = wtd_Psi'*r;

end

