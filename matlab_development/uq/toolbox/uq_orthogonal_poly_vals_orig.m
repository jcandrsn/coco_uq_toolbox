function psi = uq_orthogonal_poly_vals(x, type, max_order, norm)
% For given evaluation locations, x, return a matrix of orthogonal
% polynomial values of type, type, up to order, max_order.  The value of
% norm determines whether the coefficients are normalized such that the
% norm of a particular polynomial order equals one or not.  
[d1, d2] = size(x);
psi = zeros(d1, d2, max_order+1);
psi(:,:,1) = 1;
psi(:,:,2) = x;

if nargin < 4
    norm = 1;
end

switch type
  case {'Legendre', 'Le'}
    if norm
      wts = permute(repmat(sqrt(2*(0:max_order)+1), d1, 1, d2), [1,3,2]);
    else
      wts = ones(d1, d2, max_order+1);
    end
      if max_order==0
          % Special code for trivial case to get matrix dimensions to agree
          psi = psi(:,:,1);
      else    
          for i=3:(max_order+1)
              n = i-2;
              % Three term recurrence for Legendre Orthogonal Polynomials
              psi(:,:,i) = ((2*n+1)*x.*psi(:,:,i-1) - n*psi(:,:,i-2))/(n+1);
          end
      end

  case {'Hermite', 'He'}
    if norm
      wts = permute(repmat(sqrt(factorial(0:max_order)).^-1, d1, 1, d2),...
                    [1,3,2]);
    else
      wts = ones(d1, d2, max_order+1);
    end
      
      if max_order==0
          % Special code for trivial case to get matrix dimensions to agree
          psi = psi(:,:,1);
      else
          % Three term recurrence for Hermite Orthogonal polynomials
          for i=3:(max_order+1)
              psi(:,:,i) = x.*psi(:,:,i-1) - (i-2)*psi(:,:,i-2);
          end
      end
end

psi = wts.*psi;

end