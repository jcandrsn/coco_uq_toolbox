function [data, J] = uq_pce_coefficients_dU(prob, data, u)
% UQ_PCE_COEFFICIENTS_DU Jacobian of the Polynomial Chaos
% Expansion (PCE) coefficient Evaluation function


[~, dr] = uq_response_Jacobian(prob, data, u);

J = [dr, ...           % dr/du
     speye(data.Nt)];  % alphas are linear
   
%% Test Jacobian with following sequence:
% [data, Jd] = coco_ezDFDX('f(o,d,x)', prob, data, @uq_pce_coefficients, u);
% diff_J = abs(J-Jd);
% max(max(diff_J))
% fprintf('test')
end