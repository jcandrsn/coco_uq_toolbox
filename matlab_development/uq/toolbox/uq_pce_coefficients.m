function [data, y] = uq_pce_coefficients(prob, data, u)
%UQ_PCE_COEFFICIENTS Summary of this function goes here
%   Evaluate Response function and calculate coefficients of polynomial
%   chaos expansion.

[~, r] = uq_response_evaluation(prob, data, u);

alphas = u(end-data.Nt+1:end);
R = data.wtd_psi_mat*r;

y = alphas - R;

end
