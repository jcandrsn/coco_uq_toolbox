function [data, r] = uq_response_evaluation(prob, data, u)


if strcmp(data.response_type, 'int')
  for i=1:data.nsamples
    % Separate out components
    T = u(data.resp.T_idx(i,1));
    x = u(data.resp.xbp_idx(i,1):data.resp.xbp_idx(i,end));
    p = u(data.resp.p_idx(i,1):data.resp.p_idx(i,end));
    fdata = coco_get_func_data(prob, ...
      coco_get_id(data.sids{i}, 'coll'), 'data');

    if i == 1
      % Set the size of the response array on the first trip through
      % the loop and assign the first value
      r1 = int_resp(data, fdata, T, x, p);
      % This assumes equal integration order for all parameters.  Need
      % to think through what the shape should be if there are varying
      % integration orders.  Will also affect construction of psi matrix
      r = zeros(size(r1,2), data.nsamples);
      r(:,i) = r1;
    else
      % Only Calculate the function value and assign it to correct spot
      r(:,i) = int_resp(data, fdata, T, x, p);
    end
  end
elseif strcmp(data.response_type, 'bv')

  xdim = data.xdim;
  pdim = data.pdim;
  ns = data.nsamples;
  nu = 2 + 2*xdim + pdim;

  idx = reshape(1:nu*ns, [], ns);
  T0  = u(idx(1,:)); 
  T   = u(idx(2,:));
  x0  = u(idx(3:2+xdim,:));
  x1  = u(idx(3+xdim:2*(1+xdim),:));
  p   = u(idx(3+2*xdim:end,:)); 
  
  args = {T0, T, x0, x1, p};

  r = data.rhan(data, args{data.resp.nargs + 1:end});

end

r = r';
end

function r = int_resp(uq_data, fdata, T, x, p)

% Implementation of Formulat 7.16 of Recipes for
% Continuation with the addition of parameter arguments in
% the integrand.

maps = fdata.coll_seg.maps;
int  = fdata.coll_seg.int;
xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
pp  = repmat(p, maps.p_rep);

resp = uq_data.rhan(xx, pp);
wts1 = repmat(int.wt',[maps.NTST,1]);

r = T/(2*maps.NTST)*resp*wts1;

end

function r = int_resp_nu(uq_data, fdata, T, x, p) 
% Incomplete!
% For non-uniform mesh
maps = fdata.coll_seg.maps;
mesh = fdata.coll_seg.mesh;

xcn = reshape(maps.W*x, maps.x_shp);
pp  = repmat(p, maps.p_rep);

rcn = uq_data.rhan(xcn, pp);
rcn = mesh.gka.*rcn; % mesh here is related adaptive meshing


r = (0.5*T/maps.NTST)*mesh.gwt*rcn';

end