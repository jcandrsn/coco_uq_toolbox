function [data, y] = uq_seg_to_nds(prob, data, u)

seg_pars  = u(1:numel(data.idx));
nds_start = numel(data.idx) + 1;
node_pars = u(nds_start:end);
node_pars = node_pars(data.idx);
node_pars = node_pars(:);

y = seg_pars - node_pars;

end