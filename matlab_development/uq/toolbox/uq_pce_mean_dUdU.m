function [data, dJ] = uq_pce_mean_dUdU(prob, data, u)
%UQ_PCE_MEAN Summary of this function goes here
%  Given an array of PCE coefficients return the Jacobian of the mean value
%  with respect to the expansion coefficients

dJ = zeros(1, numel(u), numel(u));

end
