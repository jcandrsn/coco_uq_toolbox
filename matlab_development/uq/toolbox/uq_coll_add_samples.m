function [prob, data] = uq_coll_add_samples(prob, data)

% Add coll instances for each point in the sample parameter space.
% Starting information for the points in parameter space are taken from
% continuation runs performed in an earlier continuation step whose run
% names are stored in data.sample_run_names.

tbid = coco_get_id(data.oid, 'uq');
reorder_idx = zeros(size(data.wts));
run_count = 1;
run_names = data.sample_run_names;
data.sids = cell(1, data.nsamples);
ndvals = data.nds(data.idx);
if size(ndvals,2) == 1
    ndvals = ndvals';
end
for i=1:numel(run_names)
  bd = coco_bd_read(run_names{i});
  labs = coco_bd_labs(bd, 'UQ');
  for lab=labs
    sol = coll_read_solution('sample.bvp.seg1', run_names{i}, lab);
    % Order that parameters were reached in the continuation runs is
    % unlikely to be the same as the order they were initially
    % calculated in, so this code determines the order to rearrange the
    % nds_grid and wts fields of the data structure
    % -----------------------------------------------------------------
    p_rep = repmat(sol.p(data.sp2p_idx), 1, prod(data.uq.M, 2));
    nd_diff = sum((ndvals - p_rep).^2,1);
    reorder_idx(run_count) = find(nd_diff==min(nd_diff));
    % -----------------------------------------------------------------
    sample_name = coco_get_id(tbid, sprintf('sample%s',int2str(run_count)));
    prob = ode_coll2coll(prob, sample_name, run_names{i}, 'sample.bvp.seg1', lab);
    if data.addadjt
      prob = adjt_coll2coll(prob, sample_name, run_names{i}, 'sample.bvp.seg1', lab);
    end

    data.sids{run_count} = sample_name;
    run_count = run_count+1;
  end
end

data.xdim = size(sol.xbp, 2);
data.pdim = size(sol.p, 1);
data.idx = data.idx(:, reorder_idx);
data.wts = data.wts(reorder_idx);
data.nds_grid = data.nds_grid(:, reorder_idx);
data.reorder_idx = reorder_idx;

% Delete the now unncessary sample folders
for i=1:numel(run_names)
  try
    rmdir(['data/', run_names{i}], 's')
  catch
    % In case there is an access issue, just leave the folders
  end
end

end