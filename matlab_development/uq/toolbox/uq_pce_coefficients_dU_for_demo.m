function [data, J] = uq_pce_coefficients_dU(prob, data, u)
%UQ_TEMP Summary of this function goes here
%   u contains all Basis point and parameter values for M trajectories as
%   well as continuation variables for the alphas where M is the numerical
%   integration order of the PCE.
%   u = [ xbp1, xbp2, ... xbpM, p1, p2, ... pM ,alpha];
%
%   y = alpha - w*Psi*r(x,p);
%   J_x = dy/dx = (dy/dr)*(dr/dx) = -w*Psi*dr/dx(x,p);
%   J_p = dy/dp = (dy/dr)*(dr/dp) = -w*Psi*dr/dp(x,p);
%   J_alpha = eye(size(alpha);
%   J = [J_x J_p J_alpha];
%

pidx_shift = data.pidx - data.xidx(end);
dr.dx = sparse(1,data.xidx(end));
dr.dp = sparse(1,pidx_shift(end));

for i=data.M^data.s:-1:1   % M^s is the number of quadrature points, going backwards
                           % to keep Matlab from squawking about memory allocation

     % Cycling through like this to account for a potential case where the
     % trajectories are allowed to have discretizations and thus will have
     % varying numbers of basis point values.

     % Separate out components
     x = u(data.xidx(i,1):data.xidx(i,2));
     x = reshape(x, data.xbp_shp{i});
     p = u(data.pidx(i,1):data.pidx(i,end));
     
     dr(i).dx = sparse(1, data.xidx(i,1):data.xidx(i,2), ...
                      reshape(data.drdxhan(x,p),1,[]), 1, data.xidx(end));
     
     dr(i).dp = sparse(1, pidx_shift(i,1):pidx_shift(i,2), ...
                      reshape(data.drdphan(x,p),1,[]), 1, pidx_shift(end));
end

J = [sparse(-1*data.wtd_psi_mat*vertcat(dr.dx)) ...  % w*Psi * dr/dx
     sparse(-1*data.wtd_psi_mat*vertcat(dr.dp)) ...  % w*Psi * dr/dp
     speye(data.Nt)];                                % alphas are linear

end

