function [uq, spec] = uq_get_settings(prob, tbid, uq)

spec = {  'M',   '[int]',   4, 'read',   {},  'Numerical Integration Order'
          'Pt',  'int',     3, 'read',   {},  'Max Polynomial Order for Polynomial Chaos Expansion'
 };

uq = coco_parse_settings(prob, spec, uq, tbid);

end