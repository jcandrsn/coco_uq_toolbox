function [data, J] = uq_pce_coefficients_adjoint_dU(prob, data, u)
% UQ_PCE_COEFFICIENTS_DU Jacobian of the Polynomial Chaos
% Expansion (PCE) coefficient Evaluation function

J = zeros(data.Nt, data.resp.p_adjt_idx(end) + data.Nt, ...
  data.resp.p_idx(end) + data.Nt);
[~, ddr] = uq_int_response_adjoint_Jacobian(prob, data, u);

J(:, 1:data.resp.p_adjt_idx(end), 1:data.resp.p_idx(end)) = ddr;

% %% Test Jacobian with following sequence:
% [data, Jd] = coco_ezDFDX('f(o,d,x)', prob, data, @uq_pce_coefficients_adjoint, u);
% diff_J = abs(J-Jd);
% max(max(max(diff_J)))
% fprintf('test')
end