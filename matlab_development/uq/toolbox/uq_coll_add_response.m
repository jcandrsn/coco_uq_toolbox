function prob = uq_coll_add_response(prob, oid, rid, varargin)
% UQ_COLL_ADD_RESPONSE Add a response function to an 
% existing sample of coll instances

% PROB = UQ_COLL_ADD_RESPONSE(PROB, OID, RID, VARARGIN)
% VARARGIN = {TYPE, R DR [DDR]}
% TYPE     = { ('bv' | 'int') }
% 
% On input:
% PROB       : Continuation problem structure.  Must
%              contain an existing sample of coll problem
%              instances created by the uq_isol2coll or
%              uq_isol2bvp constructor.
% OID        : Objects instance identifier for the coll
%              sample to be used in the response function
% RID        : Response Function object identifier.
%              The problem instances 
%              coco_get_id(response_id, RID, 'uq.pce'),
%              coco_get_id(response_id, RID, 'uq.pce_mean')
%              coco_get_id(response_id, RID, 'uq.pce_variance') as well
%              as inactive continuation parameters for the
%              mean and variance of the response function
%              whose names are given by
%              coco_get_id(OID, RID, 'mean') and
%              coco_get_id(OID, RID, 'variance') will be added
%              to the continuation problem structure as a
%              result of a call to this function.
%              The value of response_id is given by
%              response_id = coco_get_id(oid, 'uq', 'responses');
% R          : Function handle to the response function.
%              Can either be a function on the boundary
%              values of the coll instance 
%              r(T0,T,x0,x1,p) or a function over the
%              entire trajectory r(x,p).  Response
%              functions over an entire trajectory will be
%              integrated over the segment period.
% DR         : Jacobian of the response function. Either a
%              single function DRDU for the boundary
%              value case or a pair of functions DRDX,
%              DRDP for the integral case
% DDR        : Second derivative of the response function
%              with respect to its input variables.  Either
%              DRDUDU for a boundary value response or
%              DRDXDX [DRDXDP [DRDPDP]] for an integral
%              response function
% TYPE       : Identifies the form of the response
%              function. 'bv' indicates that the response
%              function is evaluated at the boundary values
%              and 'int' indicates that the response
%              function uses interior values for its
%              evaluation.

err_string = ['Expected either ''bv''', ...
              ' or ''int'' for fourth argument entry'];

if ischar(varargin{1})
  if strcmp(varargin{1}, 'bv') || strcmp(varargin{1}, 'int')
    response_type = varargin{1};
  else
    assert(strcmp(varargin{1}, 'bv') || strcmp(varargin{1}, 'int'), err_string);
  end
else  
  assert(ischar(varargin{1}), err_string);
end

if strcmp(response_type,'int')
  grammar   = 'R [DRDX [DRDP [DRDXDX [DRDXDP [DRDPDP]]]]] [OPTS]';
  args_spec = {
             'R',     '',     '@',       'rhan',       [], 'read', {}
          'DRDX',     '',  '@|[]',    'drdxhan',       [], 'read', {}         
          'DRDP',     '',  '@|[]',    'drdphan',       [], 'read', {}
        'DRDXDX',     '',  '@|[]',  'drdxdxhan',       [], 'read', {}         
        'DRDXDP',     '',  '@|[]',  'drdxdphan',       [], 'read', {}
        'DRDPDP',     '',  '@|[]',  'drdpdphan',       [], 'read', {}
    };
elseif strcmp(response_type,'bv')
  grammar   = 'R [DRDU [DRDUDU]] [OPTS]';
  args_spec = {
         'R',     '',     '@',       'rhan',       [], 'read', {}
      'DRDU',     '',  '@|[]',    'drduhan',       [], 'read', {}         
    'DRDUDU',     '',  '@|[]',  'drduduhan',       [], 'read', {}
    };
end

[args, ~] = coco_parse(grammar, args_spec, {}, varargin{2:end});

seg2ndsid = coco_get_id(oid, 'uq.s_par_glue');
uq_data = coco_get_func_data(prob, seg2ndsid, 'data');
uq_data.response_type = response_type;

names = fieldnames(args);
for i=1:numel(names)
  uq_data.(names{i}) = args.(names{i});
end

% Collect indices for the type of response function (bv
% or int)
uq_data = uq_coll_response_get_idx(prob, uq_data);

% Calculate an initial guess for the alphas
u = prob.efunc.x0(uq_data.resp.uidx);
[~, r] = uq_response_evaluation(prob, uq_data, u);
alpha_ig = uq_data.wtd_psi_mat*r;

response_id = coco_get_id(oid, 'uq', 'responses');
pce_id = coco_get_id(response_id, rid, 'pce');

% Need to handle cases where derivatives are not provided
if strcmp(uq_data.response_type, 'bv')
  % Provide the second derivative when the response is
  % evaluated on the boundary values because it uses
  % coco_add_adjt to construct the adjoint instead of
  % providing an explicit Jacobian for the adjoint.
  prob = coco_add_func(prob, pce_id, ...
      @uq_pce_coefficients, @uq_pce_coefficients_dU, ...
      @uq_pce_coefficients_dUdU, ...
      uq_data, 'zero', 'uidx', ...
      uq_data.resp.uidx, 'u0', alpha_ig);
elseif strcmp(uq_data.response_type, 'int')
  % The second derivative can be omitted for response
  % functions that evaluate integrals over a trajectory
  % because the adjoint Jacobian is explicitly provided to
  % the coco_add_adjt constructor.
  prob = coco_add_func(prob, pce_id, ...
      @uq_pce_coefficients, @uq_pce_coefficients_dU, ...
      uq_data, 'zero', 'uidx', ...
      uq_data.resp.uidx, 'u0', alpha_ig);  
end
prob = coco_add_slot(prob, pce_id, @coco_save_data, uq_data, 'save_full');

alpha_idx = coco_get_func_data(prob, pce_id, 'uidx');

% Grab the last Nt parameters.
alpha_idx = alpha_idx(end-uq_data.Nt+1:end);

% Calculate statistics from the PCE
% Add zero functions for the mean and variance along with
% inactive continuation parameters for tracking their
% values.

% Mean Zero Function
mean_id = coco_get_id(response_id, rid, 'pce_mean');
prob = coco_add_func(prob, mean_id, ...
  @uq_pce_mean, @uq_pce_mean_dU, @uq_pce_mean_dUdU, ...
  uq_data, 'zero', 'uidx', alpha_idx, 'u0', alpha_ig(1));

% Mean Continuation Parameter
mean_par_id = coco_get_id(response_id, rid, 'mean');
mean_idx = coco_get_func_data(prob, mean_id, 'uidx');
mean_name = coco_get_id(uq_data.oid, rid, 'mean');
prob = coco_add_pars(prob, mean_par_id, mean_idx(end), mean_name);

% Variance Zero Function
var_id = coco_get_id(response_id, rid, 'pce_variance');
prob = coco_add_func(prob, var_id, ...
  @uq_pce_variance, @uq_pce_variance_dU, @uq_pce_variance_dUdU,...
  uq_data, 'zero', 'uidx', alpha_idx, 'u0', sum(alpha_ig(2:end).^2));

% Variance Parameter
var_par_id = coco_get_id(response_id, rid, 'variance');
var_idx = coco_get_func_data(prob, var_id, 'uidx');
var_name = coco_get_id(uq_data.oid, rid, 'var');
prob = coco_add_pars(prob, var_par_id, var_idx(end), var_name);

end