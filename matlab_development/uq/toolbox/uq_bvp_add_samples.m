function [prob, data] = uq_bvp_add_samples(prob, data, bc_data, args)

[prob, data] = uq_coll_add_samples(prob, data);
[prob, data] = uq_bvp_close_samples(prob, data, bc_data, args);

end