function [data, J] = uq_pce_mean_dU(prob, data, u)
%UQ_PCE_MEAN Summary of this function goes here
%  Given an array of PCE coefficients return the Jacobian of the mean value
%  with respect to the expansion coefficients

J = zeros(size(u'));
J(1) = -1;
J(end) = 1;

end
