function [data, y] = uq_pce_variance(prob, data, u)
%UQ_PCE_VARIANCE 
%  Given the coefficient of a polynomial chaos expansion, return the
%  variance of the expansion.

alphas = u(1:end-1);
alphas = reshape(alphas, [], data.Nt);
variance = u(end);

y = variance - sum(alphas(:,2:end).^2,2);

end

