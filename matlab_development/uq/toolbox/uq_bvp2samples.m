function [prob, data] = uq_bvp2samples(prob, data, bc_data, args, opts)

[prob, data] = uq_coll2collsamples(prob, data, args, opts);
[prob, data] = uq_bvp_close_samples(prob, data, bc_data, args, opts);

end