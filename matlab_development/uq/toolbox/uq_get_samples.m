function samples = uq_get_samples(runid)
%UQ_GET_RESPONSES Get names of samples in given runid

data = coco_read_solution(runid, 1, 'data');
resp_idx = strfind(data(:,1), 'uq.responses');

samples = {};

for i=1:numel(resp_idx)
  if ~isempty(resp_idx{i})
    sid = strsplit(data{i,1}, '.uq.responses.');
    samples = {samples{:}, sid{1}};
  end
end

end