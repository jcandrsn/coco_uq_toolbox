function [nds, wts, idx] = uq_gauss_nodes(m, n, type)
% Given a quadrature order, m, dimension, n, and quadrature type,
% return the tensor product of the quadrature node locations, nds, and 
% appropriately multiplied quadrature weight, wts. Weights
% are normalized so they add up to 1.
%
% m must either be a scalar (equal quadrature order in each dimension) or a
%   vector with numel(m) = n to allow each direction to be set independently.
%
% n is an integer value for the number of dimension
%
% type is a string or a cell array of strings (with numel(type) == n) that
%      specify the quadrature rule for each dimension independently.
%
% https://www.mathworks.com/matlabcentral/answers/98191-how-can-i-obtain-all-possible-combinations-of-given-vectors-in-matlab

if ischar(type)
    type = cellstr(type);
end
assert(numel(m) == 1 || numel(m) == n, ['Number of entries for m must ' ...
       'either be 1 (equal quadrature order for all variables) ' ...
       'or equal to n (specified quadrature order for each variable)'])

assert(numel(type) == 1 || numel(type) == n, ['Number of entries for ' ... 
       'type must either be 1 (equal distribution for all variables) ' ...
       'or equal to n (specified quadrature order for each variable)'])
single_integration_node = false;
if n > 1
    if numel(m)==1
      if m == 1
        single_integration_node = true;
      end
        m = repmat(m, 1, n);
    end

    if numel(type)==1
        type = repmat(type, 1, n);
    end
end

idx = cellfun(@(x) 1:x, num2cell(m), 'uniformoutput', false);
combinations = cell(1, numel(idx));
[combinations{:}] = ndgrid(idx{:});
combinations = cellfun(@(x) x(:), combinations, 'uniformoutput', false);
idx = [combinations{:}]';
os = cumsum(max(idx')');
idx = idx + repmat([0;os(1:end-1)],1,size(idx,2));
nds = cell(1, n);
wts = cell(1, n);
if single_integration_node
  % handle the trivial case
  nds = 0;
  wts = 1;
else
  for i=1:n
      switch lower(type{i})
          case lower({'Legendre','Le','LeN', 'Uniform', 'U'})
              [nds{i}, wts{i}] = gauss_legendre_nodes(m(i));
          case lower({'Hermite','He','HeN', 'Normal', 'N'})
              [nds{i}, wts{i}] = gauss_hermite_nodes(m(i));
          otherwise
              warning(strcat('Specified Type not supported, Providing',...
                             ' weights and nodes for Gauss-Legendre',...
                             ' quadrature'))
              [nds{i}, wts{i}] = gauss_legendre_nodes(m(i));
      end
  end
  nds = [nds{:}];
  wts = [wts{:}];
  wts = prod(wts(idx), 1);
end

end

function [nds, wts] = gauss_legendre_nodes(m)
num = (1:m-1)';
g = num.*sqrt(1./(4*num.^2-1)); 
J = -diag(g,1)-diag(g,-1);
[w, x] = eig(J);
nds = diag(x)';
wts = (2*w(1,:).^2)/2;
end

function [nds, wts] = gauss_hermite_nodes(m)
  num = (1:m-1)';
  g = sqrt(num);
  J = diag(g,1)+diag(g,-1);
  [w, x] = eig(J);
  [nds, idx] = sort(diag(x));
  nds = nds';
  wts = sqrt(2*pi)*w(1,:).^2;
  wts = wts(idx)/(sqrt(2*pi));
end