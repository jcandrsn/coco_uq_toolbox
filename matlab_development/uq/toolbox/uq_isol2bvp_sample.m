function prob = uq_isol2bvp_sample(prob, oid, varargin)
%UQ_ISOL2BVP_SAMPLE Generate a sample of trajectories from
% a given initial solution guess. 
% 
% PROB = UQ_ISOL2BVP_SAMPLE(PROB, OID, VARARGIN)
% VARARGIN = {COLL PNAMES BCND SPNAMES SPDIST SPDISTPARAMS [FPARS] [OPTS] }
% COLL    = Argument string as would be passed to ode_isol2coll (without
%           parameter names
% PNAMES  = Parameter Names (Deterministic and Stochastic)
% BCND    = Argument string as would be pass to ode_isol2bvp
% SPNAMES = Stochastic Parameter Names
% SPDIST  = Distributions of Stochastic Parameters
% SPDP    = Distribution Parameters for Given Distributions
% FPARS   = Parameters that while not stochastic may not be equal in all
%           instances of the boundary value problem.  Example is a phase
%           variable used to ensure a linear harmonic oscillator
%           is at its peak at t=0. As in the vector field
%           x'' + b*x' + k*x = A cos(w*t + phi)
%           With BC's
%           x(T) - x(0) = 0
%           x'(T) - x'(0) = 0
%           T - 2*pi / w = 0
%           x'(0) = 0 <-- Additional Boundary Condition requires an extra
%                         d.o.f. that the phase variable, phi, provides
% 
% OPTS = { '-add-adjt' }

  tbid = coco_get_id(oid, 'uq');
  str  = coco_stream(varargin{:});
  % Required for continuation to sample points
  temp_str = coco_stream(varargin{:});
  % This temporary instance is used to initialize the bvp data structure
  temp_prob2 = ode_isol2bvp(prob, tbid, str);
  [args, opts] = uq_parse_str(str);
  bvp_id = coco_get_id(tbid, 'bvp');
  bc_data = coco_get_func_data(temp_prob2, bvp_id, 'data');

  data = bvp_uq_init_data(bc_data, oid);
  data = uq_init_data(prob, data, args, opts);
  data = uq_bvp_gen_samples(data, prob, temp_str);

  [prob, data] = uq_bvp_add_samples(prob, data, bc_data, args);
  % Waiting until after this function because the grid nodes
  % and integration weights get re-orded as part of
  % uq_bvp_add_samples.
  % Psi_mat construction currently only handles equal
  % integration order and max polynomial order for each
  % random variable
  psi_mat = uq_make_psi_mat(data.nds_grid, data.uq.Pt, data.spdists);
  data.wtd_psi_mat = psi_mat*diag(data.wts);

  prob  = uq_add_sample_nodes(prob, data, args);

end