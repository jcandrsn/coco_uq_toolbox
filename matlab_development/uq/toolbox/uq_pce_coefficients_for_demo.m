function [data, y] = uq_pce_coefficients(prob, data, u)
%UQ_PCE_COEFFICIENTS Summary of this function goes here
%   Evaluate Response function and calculate coefficients of polynomial
%   chaos expansion.
%   u contains all Basis point and parameter values for M trajectories
%   (where M is the numericalnintegration order of the PCE) as well as
%   continuation variables for the alphas.
%   u = [ xbp1, xbp2, ... xbpM, p1, p2, ... pM ,alpha];
%
%   y = alpha - Psi'*r(x,p);

for i=1:data.M^data.s   % M^s is the number of quadrature points
     % Separate out components
     x = u(data.xidx(i,1):data.xidx(i,end));
     x = reshape(x, data.xbp_shp{i});
     p = u(data.pidx(i,1):data.pidx(i,end));
     
     if i == 1
         % Set the size of the response array on the first trip through
         % the loop and assign the first value
         r1 = data.rhan(x, p);
         r = zeros(data.M, size(r1,2));
         r(i,:) = r1;
     else
         % Only Calculate the function value and assign it to correct spot
         r(i,:) = data.rhan(x, p);
     end
end

alphas = u(data.pidx(end)+1:end);

R = data.wtd_psi_mat*r;
y = alphas - R;

end
