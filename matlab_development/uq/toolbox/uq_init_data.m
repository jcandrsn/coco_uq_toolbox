function data = uq_init_data(prob, data, args, opts)
% UQ_INIT_DATA
  tbid = coco_get_id(data.oid, 'uq');
  data.uq = uq_get_settings(prob, tbid, struct());
  fields = fieldnames(args);
  for i = 1:numel(fields)
      data.(fields{i}) = args.(fields{i});
  end
  
  if ischar(data.spnames)
      % Convert a single passed parameter to a cell array so the below
      % code works correctly.
      data.spnames = cellstr(data.spnames);
      data.spdists = cellstr(data.spdists);
  end
  data.s = length(data.spnames);
  data.sp2p_idx = ismember(data.pnames, data.spnames);
  data.dpar_idx = ismember(data.pnames, data.dpdtpars);
  p_in_sp = ismember(data.spnames, data.pnames);    

  if numel(data.uq.M) == 1
      % If integration order is equal for all variables, create an array with
      % an equal number of values as the stochastic dimension.
      data.uq.M = repelem(data.uq.M, data.s);
  end

  % Nt = Total Number of columns of Psi matrix and number of terms in the PCE
  % expansion.
  data.Nt = factorial(data.s + data.uq.Pt)/...
       ((factorial(data.s))*(factorial(data.uq.Pt)));

  [nds, wts, idx] = uq_gauss_nodes(data.uq.M, data.s, data.spdists);
  data.wts = wts;
  data.idx = idx;
  data.nsamples = numel(data.wts);

  if sum(p_in_sp) < data.s
      not_in_p = data.spnames(~p_in_sp);
      not_in_p_string = strtrim(sprintf('%s, ', not_in_p{:}));
      err_string = sprintf(['Parameter(s) {%s} not found in bvp ',...
                            'problem instance'], not_in_p_string(1:end-1));
      assert(sum(p_in_sp) == data.s, err_string)
  end

  % Node locations for standard distribution
  data.st_nds = nds';
  % Transformed Node locations for non-standard distribution
  % Transformation happens in a later function.
  data.nds = nds';
  data.nds_grid = data.nds(data.idx);
  if data.s == 1
    data.nds_grid = data.nds_grid';
  end

  data.sample_par_idx = zeros(size(data.nds));
  data.normal_par_idx = strcmpi('normal', data.spdists);
  data.normal_nds_idx = repelem(data.normal_par_idx, data.uq.M);
  data.num_normals = nnz(data.normal_par_idx);
  data.uniform_par_idx = strcmpi('uniform', data.spdists);
  data.uniform_nds_idx = repelem(data.uniform_par_idx, data.uq.M);
  data.num_uniforms = nnz(data.uniform_par_idx);
  if opts.addadjt
    data.addadjt = 1;
    data.adjt_sample_par_idx = zeros(size(data.nds));
  else
    data.addadjt = 0;
  end

  data = uq_gen_nds(data);
end