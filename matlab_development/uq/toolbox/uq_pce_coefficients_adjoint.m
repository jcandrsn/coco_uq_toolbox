function [data, J] = uq_pce_coefficients_adjoint(prob, data, u)
%UQ_PCE_COEFFICIENTS Summary of this function goes here
%   Calculate the adjoint for an integral response function

[~, dr] = uq_int_response_adjoint_evaluation(prob, data, u);

J = [dr, ...           % dr/du
     speye(data.Nt)];  % alphas are linear

end
