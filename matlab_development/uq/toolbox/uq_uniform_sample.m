function [data, y] = uq_uniform_sample(prob, data, u)

% Get the value of the limits of the uniform random
% variables
lo = u(1:data.num_uniforms);
hi = u(data.num_uniforms+1:2*data.num_uniforms);

% Get the value of the nodes that will be tied to the
% uniform random variables
transformed_node_locations = u(2*data.num_uniforms+1:end);

% Repeat the bounds the necessary number of times
lo_rep = repelem(lo, data.uq.M(data.uniform_par_idx));
up_rep = repelem(hi, data.uq.M(data.uniform_par_idx));

% Fix the dimension of the repeat when there is only
% uniform random variable
if size(lo_rep,1) == 1
    lo_rep = lo_rep';
    up_rep = up_rep';
end

% Get the value of standardized uniform random variables
% (precalculated for U(-1,1))
st_nds = data.st_nds(data.uniform_nds_idx);

% Transform standard random variables to the non-standard
% upper and lower limit
vals = (up_rep.*(st_nds+1)+lo_rep.*(1-st_nds))/2;

y = vals - transformed_node_locations;

end