function [data, dJ] = uq_normal_sample_dUdU(prob, data, u)

numnodes = numel(u) - 2*data.num_normals;
num_us = size(u, 1);
dJ = zeros(numnodes, num_us, num_us);

end