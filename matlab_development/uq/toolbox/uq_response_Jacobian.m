function [data, J] = uq_response_Jacobian(prob, data, u)
  
  if strcmp(data.response_type, 'int')
    
    J = zeros(prod(data.uq.M), data.resp.p_idx(end));

    for i=data.nsamples:-1:1 % going backwards to keep Matlab from squawking 
                             % about memory allocation
      % Cycling through like this to account for a potential future 
      % case where the trajectories are allowed to have different 
      % discretizations and thus will have varying numbers of basis points
      % Need to think through vectorization or using cellfun
      % to do this.
      %
      % Separate out components
      T = u(data.resp.T_idx(i,1));
      x = u(data.resp.xbp_idx(i,1):data.resp.xbp_idx(i,end));
      p = u(data.resp.p_idx(i,1):data.resp.p_idx(i,end));
      fdata = coco_get_func_data(prob, ...
        coco_get_id(data.sids{i}, 'coll'), 'data');
      [J_T, J_xbp, J_p] = int_resp_du(data, fdata, T, x, p);
      
      J(i, i) = J_T;
      J(i, data.resp.xbp_idx(i,1):data.resp.xbp_idx(i,2)) = J_xbp;
      J(i, data.resp.p_idx(i,1):data.resp.p_idx(i,2)) = J_p;
    end

    J = -1*data.wtd_psi_mat*J;

  elseif strcmp(data.response_type, 'bv')
    
    xdim = data.xdim;
    pdim = data.pdim;
    ns = data.nsamples;
    nu = 2 + 2*xdim + pdim;
    
    idx = reshape(1:nu*ns, [], ns);
    T0  = u(idx(1,:)); 
    T   = u(idx(2,:));
    x0  = u(idx(3:2+xdim,:));
    x1  = u(idx(3+xdim:2*(1+xdim),:));
    p   = u(idx(3+2*xdim:end,:)); 

    args = {T0, T, x0, x1, p};
    J = data.drduhan(data, args{data.resp.nargs + 1:end});
    rows = repelem(1:ns, 1, nu);
    cols = 1:ns*nu;
    J = sparse(rows, cols, J); 
    J = -1*data.wtd_psi_mat*J;
    
  end
end

function [J_T, J_xbp, J_p] = int_resp_du(uq_data, fdata, T, x, p)

maps = fdata.coll_seg.maps;
int  = fdata.coll_seg.int;

xx  = reshape(maps.W*x, maps.x_shp); % Trajectory values at collocation nodes
pp  = repmat(p, maps.p_rep);

wts1    = repmat(int.wt',[maps.NTST,1]);
wts2    = diag(kron(ones(1,maps.NTST), kron(int.wt, ones(1,int.dim))));

resp  = uq_data.rhan(xx, pp); 
drdx = uq_data.drdxhan(xx, pp);  
drdp = uq_data.drdphan(xx, pp);

J_T   = 1/(2*maps.NTST)*resp*wts1;
J_xbp = T/(2*maps.NTST)*drdx(:)'*wts2*maps.W; % Post Multiply by W to get back to basis points.
J_p   = T/(2*maps.NTST)*drdp(:)'*kron(wts1,eye(maps.pdim));

end

function [J_T, J_xbp, J_p] = int_resp_du_num(uq_data, fdata, T, x, p) %#ok<INUSL>
% For non-uniform mesh
maps = fdata.coll_seg.maps;
mesh = fdata.coll_seg.mesh;

xx = reshape(maps.W*x, maps.x_shp);
pp  = repmat(p, maps.p_rep);

gcn = uq_data.rhan(xx, pp);
gcn = mesh.gka.*gcn;

gdxcn = uq_data.drdxhan(xx, pp);
gdxcn = mesh.gdxka.*gdxcn;

gdxcn = sparse(maps.gdxrows, maps.gdxcols, gdxcn(:));

gdp = uq_data.drdphan(xx, pp);
gdp = mesh.gdpka.*gdp;
gdprows = kron(ones(maps.pdim,1), 1:maps.p_rep(2));
gpdcols = kron(ones(maps.p_rep),1:maps.pdim);
gdp = sparse(gdprows, gpdcols, gdp(:));

J_T   = (0.5/maps.NTST)*mesh.gwt*gcn';
J_xbp = (0.5*T/maps.NTST)*mesh.gwt*gdxcn*maps.W;
J_p   = (0.5*T/maps.NTST)*mesh.gwt*gdp;

end