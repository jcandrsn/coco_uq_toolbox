function [data, dJ] = uq_pce_coefficients_dUdU(prob, data, u)
  % This code is only used for boundary value response
  % functions, so there is no logic to check the response
  % type like in uq_pce_coefficients and it's Jacobian.
  
  xdim = data.xdim;
  pdim = data.pdim;
  ns = data.nsamples;
  nu = 2 + 2*xdim + pdim;
  
  dJ = zeros(ns, ns*nu + data.Nt, ...
  data.nsamples*nu + data.Nt);
  
  idx = reshape(1:nu*ns, [], ns);
  T0  = u(idx(1,:)); 
  T   = u(idx(2,:));
  x0  = u(idx(3:2+xdim,:));
  x1  = u(idx(3+xdim:2*(1+xdim),:));
  p   = u(idx(3+2*xdim:end,:)); 

  args = {T0, T, x0, x1, p};

  drdudu = data.drduduhan(data, ...
    args{data.resp.nargs + 1:end});
  for i=1:data.nsamples
    idx1 = (i-1)*nu+1;
    idx2 = i*nu;
    dJ(i,idx1:idx2,idx1:idx2) = reshape(drdudu(:,:,i), ...
      [1, nu, nu]);
  end

  dJ = -1*data.wtd_psi_mat*reshape(dJ, ns, []);
  dJ = reshape(dJ, data.Nt, ns*nu + data.Nt, ns*nu + data.Nt);

%% Test Jacobian with following sequence:
% [data, dJd] = coco_ezDFDX('f(o,d,x)', prob, data, @uq_pce_coefficients_dU, u);
% diff_J = abs(dJ-dJd);
% max(max(max(diff_J)))
% fprintf('test')
end

