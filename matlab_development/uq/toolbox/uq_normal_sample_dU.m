function [data, J] = uq_normal_sample_dU(prob, data, u)

numnodes = numel(u) - 2*data.num_normals;
num_us = size(u, 1);
J = zeros(numnodes, num_us);
% This won't work if there are different numbers of nodes for each random
% variable.
J_spdp = ones(numnodes/data.num_normals,2);
J_spdp(:,2) = data.st_nds(1:numnodes/data.num_normals);
J_spdp(:,1) = 1;
start_nodes = 2*data.num_normals + 1;
J(:, 1:data.num_normals) = kron(eye(data.num_normals), J_spdp(:,1));
J(:, data.num_normals+1:2*data.num_normals) = kron(eye(data.num_normals), J_spdp(:,2));
J(:, start_nodes:end) = -eye(numnodes);

end