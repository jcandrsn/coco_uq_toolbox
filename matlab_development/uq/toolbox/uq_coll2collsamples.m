function [prob, data] = uq_coll2collsamples(prob, data, args, opts)

% Add coll instances for each point in the sample parameter space.
% Starting information for the points in parameter space are taken from
% continuation runs performed in an earlier continuation step whose run
% names are stored in data.sample_run_names.

if nargin < 4
  opts ={};
  opts.addadjt=0;
end

reorder_idx = zeros(size(data.wts));

for sample_count = 1:numel(data.sids)
  prob = ode_coll2coll(prob, data.sids{sample_count}, args.run, args.lab); 
  if opts.addadjt
    prob = adjt_coll2coll(prob, data.sids{sample_count}, args.run, args.lab); 
  end

  sol = coll_read_solution(data.sids{sample_count}, args.run, args.lab);

  % Order that parameters were reached in the continuation runs is
  % unlikely to be the same as the order they were initially
  % calculated in, so this code determine the order to rearrange the
  % nds_grid and wts fields of the data structure
  % -----------------------------------------------------------------
  ndvals = data.nds(data.idx);
  if size(ndvals,2) == 1
      ndvals = ndvals';
  end
  p_rep = repmat(sol.p(data.sp2p_idx), 1, prod(data.uq.M, 2));
  nd_diff = sum((ndvals - p_rep).^2,1);
  reorder_idx(sample_count) = find(nd_diff==min(nd_diff));
  % ----------------------------------------------------------------
end

data.idx = data.idx(:, reorder_idx);
data.wts = data.wts(reorder_idx);
data.nds_grid = data.nds_grid(:, reorder_idx);
data.reorder_idx = reorder_idx;

end