function [data, y] = uq_normal_sample(prob, data, u)

mu = u(1:data.num_normals);
sig = u(data.num_normals+1:2*data.num_normals);
transformed_node_locations = u(2*data.num_normals+1:end);

mu_rep = repelem(mu, data.uq.M(data.normal_par_idx));
sig_rep = repelem(sig, data.uq.M(data.normal_par_idx));

if size(mu_rep,1) == 1
    mu_rep = mu_rep';
    sig_rep = sig_rep';
end

st_nds = data.st_nds(data.normal_nds_idx);
vals = mu_rep + sig_rep.*st_nds;

y = vals - transformed_node_locations;

end