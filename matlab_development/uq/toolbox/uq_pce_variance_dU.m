function [data, J] = uq_pce_variance_dU(prob, data, u)
%UQ_PCE_VARIANCE_DU Jacobian of the variance of a PCE w/r/t alphas
%  Calculate the Jacobian of the variance of a Polynomial Chaos Expansion
%  (PCE) with respect to the PCE coefficients.

J = zeros(1, numel(u));
J(2:end-1) = -2*u(2:end-1);
J(end) = 1;

end

