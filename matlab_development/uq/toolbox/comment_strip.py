import os

def matlab_comment_strip(filepath):
    comment_free_code = ""
    with open(filepath, 'r') as f:
        for line in f:
            keep = ''
            comment_split = line.split('%')
            if len(comment_split) > 1 and comment_split[0].strip() != "":
                # Handles in line comments
                keep = comment_split[0] +'\n'
            elif len(comment_split) == 1:
                keep = comment_split[0]
            comment_free_code += keep

    path, name = os.path.split(filepath)
    name, ext = os.path.splitext(name)
    new_path = os.path.join(path, 'no_comment_for_thesis', name + '_no_comment' + ext)
    
    with open(new_path, 'w') as f:
        f.write(comment_free_code)


if __name__ == "__main__":
    for filename in os.listdir('.'):
        name, ext = os.path.splitext(filename)
        if ext == '.m':
            matlab_comment_strip(filename)