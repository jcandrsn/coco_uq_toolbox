function uq_coll_plot_samples(run_id, lab)
%UQ_PLOT_SAMPLES Summary of this function goes here
%   Detailed explanation goes here

sids = uq_get_samples(run_id);

for i=1:numel(sids)
  figure()
  hold on
  sid = sids{1};
  resps = uq_get_responses(sid, run_id);
  data = coco_read_solution(resps{1}, run_id, 1, 'data');
  nsamples = data.nsamples;
  chk = zeros(nsamples,1);
  for j=1:nsamples
    coll_id = coco_get_id(data.sids{j}, 'coll');
%     [coll_data, chart] = coco_read_solution(coll_id, run_id, lab, 'data', 'chart');
%     maps = coll_data.coll_seg.maps;
%     save chart
%     save coll_data
    sol = coll_read_solution(data.sids{j}, run_id, lab);
    plot(sol.tbp, sol.xbp(:,1))
    chk(j) = sol.xbp(1,1);
  end
  mean(chk)
end



end

