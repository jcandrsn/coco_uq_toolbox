function [nds, wts] = uq_nds_n_wts(nds, wts)
% nds is a cell array of all of the 1-D node positions for the individual
% stochastic parameters.  wts is a cell array of the node weights for the
% 1-D quadrature rules.
%
% This function is able to accept quadrature nodes and weights from 
% differing rules and orders and combine them appropriately.
% Found at:

combinations = cell(1, numel(nds));
[combinations{:}] = ndgrid(nds{:});
combinations = cellfun(@(x) x(:), combinations, 'uniformoutput', false);
nds = [combinations{:}]';

combinations = cell(1, numel(wts));
[combinations{:}] = ndgrid(wts{:});
combinations = cellfun(@(x) x(:), combinations, 'uniformoutput', false);
wts = [combinations{:}];
wts = prod(wts,2)';
end