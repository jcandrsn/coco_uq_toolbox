function J = dhdp(x, p)
%DGDP Summary of this function goes here
%   Detailed explanation goes here

p1 = p(1,:);
p2 = p(2,:);

J(1,1) = -4*p1*cos(4*p1) - sin(4*p1);
J(1,2) = -2*1.1*p2.*cos(2*p2) - 1.1*sin(2*p2);

end

