% Proposed Steps for using coco to complete a uncertainty quantification
% analysis through the use of a Polynomial Chaos Expansion (PCE)
%% Step 1
%  Start with a convergent solution to the nominal problem (which is
%  required by coco anyway) and an response (monitor) function whose 
%  statistics are of interest.
clear variables;
%%

% Response Function:
% Two Uniformly Distributed random variables (mean = 2, 
% Coefficient of Variance = 0.2)
f = @(x, p) log(1 + p(1,:).^2).*sin(5*p(2,:));

%% Step 2
%  Generate a sample points for the stochastic parameters.  In this code
%  this will be done using the tensor product quadrature method of 
%  calculating PCE coefficients, so the sample points will be a tensor
%  product of nodes and appropriately multiplied weights.

% Examples for f uses 2 uniform random variables with mean and coefficient 
% of variances as follows
mu = 2;
CoV = 0.2;

for quadrature_order=10

stochastic_dimension = 2;
orthogonal_polynomial_type = 'Legendre';

[nds, wts, ~] = uq_gauss_nodes_v1(quadrature_order, ...
                               stochastic_dimension, ...
                               orthogonal_polynomial_type);

p_vals = transform_uniform_sample(nds, mu, CoV);

%% Step 3 (Optional)
%  Sort the sample points and place them in an order that minimizes
%  their distance relative to one another

% Skip, current method of seeking numerical integration points couldn't
% take advantage of this anyway.  Must do successive 1-d continuations to
% fill out the grid.

%% Step 4
%  Perform continuation to generate a convergent initial guess at each 
%  sample point.  Introduce zero functions that tie problem parameters
%  together between problem instances.


%% Step 5
%  Collect trajectories from previous step and include them in a single
%  continuation problem tying problem parameters together as appropriate.
for poly_order=7
uq_run_name = strcat(['UQ_M=', int2str(quadrature_order), ...
                   '_P=', int2str(poly_order)]);
xbp_idx = cell(size(nds, 2), 1);
xbp_shp = cell(size(nds, 2), 1);

prob = coco_prob();

for i=1:size(nds,2)
    sample_name = strcat('sample', int2str(i));
    p_names = {strcat('p1(',int2str(i),')'),strcat('p2(',int2str(i),')')};
    prob = coco_add_pars(prob, sample_name, p_names, p_vals(:,i));
end


%% Step 6
%  Introduce a zero function to construct the PCE from the desired
%  response function.


% Set up a data structure to pass to the uncertainty quantification
% problem.  Since the psi matrix is based on the standard random variables,
% its values don't change and it can be pre-calculated.

data = struct();
data.M = quadrature_order;
data.s = stochastic_dimension;
data.xbp_idx = xbp_idx;
data.xbp_shp = xbp_shp;
data.p_idx = {[1:numel(nds)]'};
data.pidx = reshape(1:numel(p_vals),data.s,[])';

max_poly_order = poly_order;
psi_mat = uq_make_psi_mat_for_demo(nds, max_poly_order, orthogonal_polynomial_type);
data.P = max_poly_order;

% The quadrature weights are similarly identical for each continuation
% step and so a weighted psi matrix can be constructed and passed to the
% function for repeated use.
data.wtd_psi_mat = psi_mat*diag(wts);

data.rhan = f;

% Nt = Total Number of columns of Psi matrix and number of terms in the PCE
% expansion.  Casting to int64 because the factorial function was returning
% a double for some reason.
Nt = factorial(stochastic_dimension + max_poly_order)/...
     ((factorial(stochastic_dimension))*(factorial(max_poly_order)));
data.Nt = Nt;

alphas = cell(1,Nt);
for i=1:Nt
    alphas{i} = strcat('alpha', int2str(i-1));
end

% Initial Guess for the alphas.  This can be bad as the zero problem is
% linear in the alphas and should converge after one step of the Newton
% Iteration regardless.
alpha_ig = zeros(size(alphas));

prob = coco_add_func(prob, 'pce', @uq_pce_coefficients_hosder_test, ...
                     data, 'zero', ...
                     'uidx', vertcat(data.xbp_idx{:}, data.p_idx{:}), ...
                     'u0', alpha_ig);

[alpha_idx, alpha_data] = coco_get_func_data(prob, 'pce', 'uidx', 'data');
% Grab the last Nt parameters
alpha_idx = alpha_idx(end-alpha_data.Nt+1:end);

%% Step 7
%  Calculate statistics from the PCE
%  Monitor functions for mean and variance given the alphas.
prob = coco_add_func(prob, 'PCE_mean', @uq_pce_mean, ...
                     @uq_pce_mean_dU, data, 'inactive', 'mean', 'uidx', ...
                     alpha_idx);

prob = coco_add_func(prob, 'PCE_variance', @uq_pce_variance, ...
                     @uq_pce_variance_dU, data, 'inactive', 'variance', ...
                     'uidx', alpha_idx);

fprintf('\nTest for function f = log(1+p(1,:).^2).*sin(5*p(2,:))\n')
fprintf(strcat(['Polynomial Chaos Expansion contains ', ...
              int2str(data.Nt),' terms\n']));
uq_bd = coco(prob, uq_run_name, [], 0, {'mean', 'variance'});
end
end

%% Step 1
%  Start with a convergent solution to the nominal problem (which is
%  required by coco anyway) and an response (monitor) function whose 
%  statistics are of interest.
%%

% Response Function:
% Four Uniformly Distributed random variables (mean = 0.4, 
% Coefficient of Variance = 0.4);

%% Step 2
%  Generate a sample points for the stochastic parameters.  In this code
%  this will be done using the tensor product quadrature method of 
%  calculating PCE coefficients, so the sample points will be a tensor
%  product of nodes and appropriately multiplied weights.

% Examples for f uses 2 uniform random variables with mean and coefficient 
% of variances as follows
mu = 0.4;
CoV = 0.4;

for quadrature_order=6

stochastic_dimension = 4;
orthogonal_polynomial_type = 'Legendre';

[nds, wts, ~] = uq_gauss_nodes_v1(quadrature_order, ...
                               stochastic_dimension, ...
                               orthogonal_polynomial_type);

p_vals = transform_uniform_sample(nds, mu, CoV);

%% Step 3 (Optional)
%  Sort the sample points and place them in an order that minimizes
%  their distance relative to one another

% Skip, current method of seeking numerical integration points couldn't
% take advantage of this anyway.  Must do successive 1-d continuations to
% fill out the grid.

%% Step 4
%  Perform continuation to generate a convergent initial guess at each 
%  sample point.  Introduce zero functions that tie problem parameters
%  together between problem instances.


%% Step 5
%  Collect trajectories from previous step and include them in a single
%  continuation problem tying problem parameters together as appropriate.
for poly_order=7
uq_run_name = strcat(['UQ_M=', int2str(quadrature_order), ...
                   '_P=', int2str(poly_order)]);
xbp_idx = cell(size(nds, 2), 1);
xbp_shp = cell(size(nds, 2), 1);

prob = coco_prob();

for i=1:size(nds,2)
    sample_name = strcat('sample', int2str(i));
    p_names = {strcat('p1(',int2str(i),')'), ...
               strcat('p2(',int2str(i),')'), ...
               strcat('p3(',int2str(i),')'), ...
               strcat('p4(',int2str(i),')')};
    prob = coco_add_pars(prob, sample_name, p_names, p_vals(:,i));
end


%% Step 6
%  Introduce a zero function to construct the PCE from the desired
%  response function.


% Set up a data structure to pass to the uncertainty quantification
% problem.  Since the psi matrix is based on the standard random variables,
% its values don't change and it can be pre-calculated.

data = struct();
data.M = quadrature_order;
data.s = stochastic_dimension;
data.xbp_idx = xbp_idx;
data.xbp_shp = xbp_shp;
data.p_idx = {[1:numel(nds)]'};
data.pidx = reshape(1:numel(p_vals),data.s,[])';

max_poly_order = poly_order;
psi_mat = uq_make_psi_mat_for_demo(nds, max_poly_order, orthogonal_polynomial_type);
data.P = max_poly_order;

% The quadrature weights are similarly identical for each continuation
% step and so a weighted psi matrix can be constructed and passed to the
% function for repeated use.
data.wtd_psi_mat = psi_mat*diag(wts);

data.rhan = @g;
data.drdphan = @dgdp;

% Nt = Total Number of columns of Psi matrix and number of terms in the PCE
% expansion.  Casting to int64 because the factorial function was returning
% a double for some reason.
Nt = factorial(stochastic_dimension + max_poly_order)/...
     ((factorial(stochastic_dimension))*(factorial(max_poly_order)));
data.Nt = Nt;

alphas = cell(1,Nt);
for i=1:Nt
    alphas{i} = strcat('alpha', int2str(i-1));
end

% Initial Guess for the alphas.  This can be bad as the zero problem is
% linear in the alphas and should converge after one step of the Newton
% Iteration regardless.
alpha_ig = zeros(size(alphas));

prob = coco_add_func(prob, 'pce', @uq_pce_coefficients_hosder_test, ...
                     @uq_pce_coefficients_hosder_test_dU, data, 'zero', ...
                     'uidx', vertcat(data.xbp_idx{:}, data.p_idx{:}), ...
                     'u0', alpha_ig);

[alpha_idx, alpha_data] = coco_get_func_data(prob, 'pce', 'uidx', 'data');
% Grab the last Nt parameters
alpha_idx = alpha_idx(end-alpha_data.Nt+1:end);

%% Step 7
%  Calculate statistics from the PCE
%  Monitor functions for mean and variance given the alphas.
% prob = coco_add_func(prob, 'PCE_mean', @uq_pce_mean, ...
%                      @uq_pce_mean_dU, data, 'active', 'mean', 'uidx', ...
%                      alpha_idx);
% 
% prob = coco_add_func(prob, 'PCE_variance', @uq_pce_variance, ...
%                      @uq_pce_variance_dU, data, 'active', 'variance', ...
%                      'uidx', alpha_idx);

fprintf('\nTest for function g = exp(1.5*(p1 + p2 + p3 + p4))\n')
fprintf(strcat(['Polynomial Chaos Expansion contains ', ...
              int2str(data.Nt),' terms\n']));

uq_bd = coco(prob, uq_run_name, [], 0);%, {'mean', 'variance'});


end
end

%% Step 1
%  Start with a convergent solution to the nominal problem (which is
%  required by coco anyway) and an response (monitor) function whose 
%  statistics are of interest.
%%

% Response Function:
% Two Normally distributed random variables (mean = 0,
% Standard Deviation = 0.1)

%% Step 2
%  Generate a sample points for the stochastic parameters.  In this code
%  this will be done using the tensor product quadrature method of 
%  calculating PCE coefficients, so the sample points will be a tensor
%  product of nodes and appropriately multiplied weights.

% Examples for f uses 2 uniform random variables with mean and coefficient 
% of variances as follows
mu = 0;
sig = sqrt(0.1);

for quadrature_order=10

stochastic_dimension = 2;
orthogonal_polynomial_type = 'Hermite';

[nds, wts, ~] = uq_gauss_nodes_v1(quadrature_order, ...
                               stochastic_dimension, ...
                               orthogonal_polynomial_type);

p_vals = mu + sig*nds;

%% Step 3 (Optional)
%  Sort the sample points and place them in an order that minimizes
%  their distance relative to one another

% Skip, current method of seeking numerical integration points couldn't
% take advantage of this anyway.  Must do successive 1-d continuations to
% fill out the grid.

%% Step 4
%  Perform continuation to generate a convergent initial guess at each 
%  sample point.  Introduce zero functions that tie problem parameters
%  together between problem instances.


%% Step 5
%  Collect trajectories from previous step and include them in a single
%  continuation problem tying problem parameters together as appropriate.
for poly_order=7
uq_run_name = strcat(['UQ_M=', int2str(quadrature_order), ...
                   '_P=', int2str(poly_order)]);
xbp_idx = cell(size(nds, 2), 1);
xbp_shp = cell(size(nds, 2), 1);

prob = coco_prob();

for i=1:size(nds,2)
    sample_name = strcat('sample', int2str(i));
    p_names = {strcat('p1(',int2str(i),')'),strcat('p2(',int2str(i),')')};
    prob = coco_add_pars(prob, sample_name, p_names, p_vals(:,i));
end


%% Step 6
%  Introduce a zero function to construct the PCE from the desired
%  response function.


% Set up a data structure to pass to the uncertainty quantification
% problem.  Since the psi matrix is based on the standard random variables,
% its values don't change and it can be pre-calculated.

data = struct();
data.M = quadrature_order;
data.s = stochastic_dimension;
data.xbp_idx = xbp_idx;
data.xbp_shp = xbp_shp;
data.p_idx = {[1:numel(nds)]'};
data.pidx = reshape(1:numel(p_vals),data.s,[])';

max_poly_order = poly_order;
psi_mat = uq_make_psi_mat_for_demo(nds, max_poly_order, orthogonal_polynomial_type);
data.P = max_poly_order;

% The quadrature weights are similarly identical for each continuation
% step and so a weighted psi matrix can be constructed and passed to the
% function for repeated use.
data.wtd_psi_mat = psi_mat*diag(wts);

data.rhan = @h;
data.drdphan = @dhdp;

% Nt = Total Number of columns of Psi matrix and number of terms in the PCE
% expansion.  Casting to int64 because the factorial function was returning
% a double for some reason.
Nt = factorial(stochastic_dimension + max_poly_order)/...
     ((factorial(stochastic_dimension))*(factorial(max_poly_order)));
data.Nt = Nt;

alphas = cell(1,Nt);
for i=1:Nt
    alphas{i} = strcat('alpha', int2str(i-1));
end

% Initial Guess for the alphas.  This can be bad as the zero problem is
% linear in the alphas and should converge after one step of the Newton
% Iteration regardless.
alpha_ig = zeros(1, data.Nt);

prob = coco_add_func(prob, 'pce', @uq_pce_coefficients_hosder_test, ...
                     @uq_pce_coefficients_hosder_test_dU, data, 'zero', ...
                     'uidx', vertcat(data.xbp_idx{:}, data.p_idx{:}), ...
                     'u0', alpha_ig);

[alpha_idx, alpha_data] = coco_get_func_data(prob, 'pce', 'uidx', 'data');
% Grab the last Nt parameters
alpha_idx = alpha_idx(end-alpha_data.Nt+1:end);

%% Step 7
%  Calculate statistics from the PCE
%  Monitor functions for mean and variance given the alphas.
prob = coco_add_func(prob, 'PCE_mean', @uq_pce_mean, ...
                     @uq_pce_mean_dU, data, 'inactive', 'mean', 'uidx', ...
                     alpha_idx);

prob = coco_add_func(prob, 'PCE_variance', @uq_pce_variance, ...
                     @uq_pce_variance_dU, data, 'inactive', 'variance', ...
                     'uidx', alpha_idx);

fprintf('\nTest for function h = -p1.*sin(4*p1)-1.1*p2.*sin(2*p2)\n')
fprintf(strcat(['Polynomial Chaos Expansion contains ', ...
              int2str(data.Nt),' terms\n']));
uq_bd = coco(prob, uq_run_name, [], 0, {'mean', 'variance'});
end
end