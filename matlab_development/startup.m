function startup
root_path = fileparts(mfilename('fullpath'));
% Copy below to coco's startup.m if this becomes a full fledged toolbox
addpath(fullfile(root_path, 'uq', 'toolbox'));
end